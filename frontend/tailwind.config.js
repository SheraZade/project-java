/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    fontFamily: {
      'Blackadder': ['Blackadder ITC', 'sans-serif'],
    },
    extend: {
      animation: {
        'spin': 'spin 0.5s ease-in-out',
      },
      colors: {
        'bgColor': '#F8F8F8',
        'roze': '#EEAA9C'
      },
    },
  },
  plugins: [],
}