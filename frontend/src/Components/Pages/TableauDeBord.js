// eslint-disable-next-line import/no-extraneous-dependencies
import {Chart, initTE} from "tw-elements";
import Navigate from "../Router/Navigate";


const createBodyWithState = (main) => {

  let body = document.querySelector("#searchBody");
  if (!body) {
    body = document.createElement('section');
    body.setAttribute("id", "searchBody");
  }
  body.innerHTML = `<div class="users-container mt-16"> 
                      <div class="flex flex-col justify-center w-4/5 mx-auto">
                        <table id="searchTable" class="text-left"> 
                          <thead>
                            <tr id="tableHeader" class="border-b">
                              <th class="pr-10">Image</th> 
                              <th>Description</th>
                              <th>Prix</th>
                              <th>Type</th>
                              <th>État actuel de l'objet</th> 
                              <th>État de l'objet pendant la période sélectionnée</th> 
                            </tr> 
                          </thead> 
                          <tbody id="object-list">
                          </tbody> 
                        </table>
                        <div id="searchError">
                    
                        </div>
                      </div>
                    </div>
                    `;
  main.appendChild(body);

}

const createRowWithState = (objet, state) => {
  const body = document.querySelector('#object-list');
  const row = document.createElement('tr');
  row.setAttribute("class",
      "cursor-pointer w-4/5 border-roze border-t  transition-colors duration-500 hover:bg-red-50");
  row.innerHTML = `
           <td class="py-4 pr-10"><img class="max-h-40 min-h-40 p-1" src="${"/api/picture/getPicture/".concat(
      objet.picture.url)}" alt="image de l'objet"</td>
           <td class="text-left py-4 pr-52">${objet.description}</td> 
           <td class="test-left py-4 pr-52">${objet.price}€</td>
           <td class="text-left py-4 pr-52">${objet.type.typeName}</td>
           <td class="text-left py-4 pr-52">${objet.state}</td> 
           <td class="text-left py-4 pr-52">${state}</td> 
  `
  row.addEventListener('click', () => {
    Navigate(`/object?id=${objet.id}`);
  });
  body.appendChild(row);
}

const displayStateStats = (stats, state) => {
  createBodyWithState(document.querySelector("#listObjects"));
  const graph = document.querySelector("#graph");
  graph.innerHTML = `<canvas id="bar-chart"></canvas>`;
  const labelNames = [];
  const values = [];
  stats.forEach((object) => {
    const optionsGetObject = {
      method: "GET",
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      }
    }
    fetch(`api/object/${object.objectID}/getObject`, optionsGetObject)
    .then(r => r.json())
    .then(r => createRowWithState(r, state));
    const date = "".concat(object.date[2]).concat("/").concat(
        object.date[1]).concat("/").concat(object.date[0]);
    if (labelNames.length === 0) {
      labelNames.push(date);
      values.push(1);
    } else {
      let index = 0;
      let finito = false;
      labelNames.forEach((label) => {
        if (label === date) {
          values[index] += 1;
          finito = true;
        }
        index += 1;
      });
      if (!finito) {
        labelNames.push(date);
        values.push(1);
      }

    }

  });

  initTE({Chart});

  const dataBar = {
    type: 'bar',
    data: {
      labels: labelNames,
      datasets: [
        {
          label: 'Objects By date',
          data: values,
        },
      ],
    },
  };

  // eslint-disable-next-line no-new
  new Chart(document.getElementById('bar-chart'), dataBar);

}
const makeStatePage = async (state, main) => {
  // eslint-disable-next-line no-param-reassign
  main.innerHTML = `
    <div class="flex flex-col justify-center w-1/3 border-r-2 border-gray-200 h-min mt-4 mb-auto">
      <form class="m-4 flex flex-col justify-center">
        <label for="dateMin" class="mt-1 p-2">Date de début</label>
        <input type="date" id="dateMin" class="cursor-pointer p-2 rounded-full bg-white border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full">
        <label for="dateMax" class="mt-1 p-2">Date de fin</label>
        <input type="date" id="dateMax" class="cursor-pointer p-2 rounded-full bg-white border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full"> 
      </form>
      <div id="graph" class="m-2">
        <canvas id="bar-chart"></canvas>
      </div>
    </div>
    <div class="flex flex-col justify-center">
      <h1 class="text-4xl text-bold text-center"> objets dans l'état ${state}</h1>
      <div class="m-4 flex flex-col justify-center w-2/3" id="listObjects">
      </div>
    </div>
  `;

  let dateMin = "";
  let dateMax = "";

  const minDate = document.querySelector("#dateMin");
  minDate.addEventListener('change', async () => {
    dateMin = minDate.value;
    const searchOptions = {
      method: 'POST',
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        "state": state,
        dateMin,
        dateMax,
      }),
    };
    const statsSearch = await fetch('api/object/getStateStats',
        searchOptions).then(r => r.json());
    displayStateStats(statsSearch, state);
  });

  const maxDate = document.querySelector("#dateMax");
  maxDate.addEventListener('change', async () => {
    dateMax = maxDate.value;
    const searchOptions = {
      method: 'POST',
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        "state": state,
        dateMin,
        dateMax,
      }),
    };
    const statsSearch = await fetch('api/object/getStateStats',
        searchOptions).then(r => r.json());
    displayStateStats(statsSearch, state);
  });

  const options = {
    method: 'POST',
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      'Content-type': 'application/json',
    },
    body: JSON.stringify({
      "state": state,
    }),
  };
  const results = await fetch('api/object/getStateStats', options).then(
      r => r.json());
  displayStateStats(results, state);
}

const displayStats = (stats) => {
  initTE({Chart});
  const pieChart = document.querySelector("#chart");
  pieChart.innerHTML = `<canvas id="pie-chart"></canvas>`;

  const labelNames = [];
  const values = [];
  const colors = [];
  stats.forEach((state) => {
    if (state.stateName !== "total") {
      labelNames.push(state.stateName);
      values.push(state.count);
      const red = 256;
      const green = Math.round(Math.random() * 256);
      const blue = Math.round(Math.random() * 256);
      colors.push(`rgba(${red},${green},${blue}, 0.5)`);
    }

  });
  const statsDiv = document.querySelector("#stats");
  statsDiv.innerHTML = ``;
  let index = -1;
  let currentDiv;
  stats.forEach((state) => {
    if (index === -1 || index % 2 === 1) {
      currentDiv = document.createElement("div");
      currentDiv.setAttribute("class", "flex flex-rox justify-around m-0 p-0")
      const display = document.createElement("div");
      display.setAttribute("class",
          "flex flex-row justify-between m-1 p-1 cursor-pointer bg-white transition-colors duration-500 "
          + "hover:bg-red-50 rounded-3xl border-2 border-roze w-1/3");
      display.innerHTML = `
      <div class="h-12 w-12 rounded-full border-2 border-gray-500 my-auto" style="background-color: ${index
      >= 0 ? colors[index] : 'rgba(0,0,0,0)'};"></div>
      <div class="flex flex-col justify-center m-1 p-1 "> 
        <p class="text-gray-700 text-center">${state.stateName}</p>
        <p class="text-xl text-gray-900 text-center">${state.count}</p>
      </div>
    `;
      if (state.stateName !== "total") {
        display.addEventListener('click', () => {
          makeStatePage(state.stateName, document.querySelector("#pageBody"));
        });
      }
      currentDiv.appendChild(display);
      statsDiv.appendChild(currentDiv);
    } else {
      const display = document.createElement("div");
      display.setAttribute("class",
          "flex flex-row justify-between m-1 p-1 cursor-pointer bg-white transition-colors duration-500 "
          + "hover:bg-red-50 rounded-3xl border-2 border-roze w-1/3");
      display.innerHTML = `
      <div class="h-12 w-12 rounded-full border-2 border-gray-500 my-auto" style="background-color: ${index
      >= 0 ? colors[index] : 'rgba(0,0,0,0)'};"></div>
      <div class="flex flex-col justify-center m-1 p-1 "> 
        <p class="text-gray-700 text-center">${state.stateName}</p>
        <p class="text-xl text-gray-900 text-center">${state.count}</p>
      </div>
    `;
      if (state.stateName !== "total") {
        display.addEventListener('click', () => {
          makeStatePage(state.stateName, document.querySelector("#pageBody"));
        });
      }
      currentDiv.appendChild(display);

    }
    index += 1;
  });

  const dataPie = {
    type: 'pie',
    data: {
      labels: labelNames,
      datasets: [
        {
          label: 'Objects by type',
          data: values,
          backgroundColor: colors,
        },
      ],
    },
  };

  // eslint-disable-next-line no-new
  new Chart(document.getElementById('pie-chart'), dataPie);
}

const makeAllPage = async (main) => {
  // eslint-disable-next-line no-param-reassign
  main.innerHTML = `
        <div class="flex flex-col justify-center w-1/3 border-r-2 border-gray-200">
          <form class="flex flex-col justify-center mr-4">
            <label for="dateMin" class="mt-1 p-2">Date de début</label>
            <input type="date" id="dateMin" class="cursor-pointer p-2 rounded-full bg-white border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full">
            <label for="dateMax" class="mt-1 p-2">Date de fin</label>
            <input type="date" id="dateMax" class="cursor-pointer p-2 rounded-full bg-white border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full">
          
          </form>
          <div id="stats"></div>
        </div>
        <div class="m-4 flex flex-col justify-center w-2/3" id="graph">
          <h1 class="text-4xl font-bold text-center"> Objets selon leur état </h1>
          <div class="h-3/5 mx-auto mt-4 mb-auto overflow-hidden" id="chart">
            <canvas id="pie-chart"></canvas>
          </div>
      
        </div>
  `;

  let dateMin = "";
  let dateMax = "";

  const options = {
    method: 'POST',
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      'Content-type': 'application/json',
    },
    body: JSON.stringify({}),
  };

  const stats = await fetch('api/object/getStats', options).then(r => r.json());
  displayStats(stats);

  const minDate = document.querySelector("#dateMin");
  minDate.addEventListener('change', async () => {
    dateMin = minDate.value;
    const searchOptions = {
      method: 'POST',
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        dateMin,
        dateMax,
      }),
    };
    const statsSearch = await fetch('api/object/getStats', searchOptions).then(
        r => r.json());
    displayStats(statsSearch);
  });

  const maxDate = document.querySelector("#dateMax");
  maxDate.addEventListener('change', async () => {
    dateMax = maxDate.value;
    const searchOptions = {
      method: 'POST',
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        dateMin,
        dateMax,
      }),
    };
    const statsSearch = await fetch('api/object/getStats', searchOptions).then(
        r => r.json());
    displayStats(statsSearch);
  });

}

const tableauDeBord = async () => {
  const main = document.querySelector("main");
  main.setAttribute("class", "pt-28");
  main.innerHTML = `
    <div class="flex flex-col w-full">
      <div id="tabs" class="flex flex-row justify-start border-b border-gray-200">
        <div class="w-12 border-x border-t border-gray-200 "></div> 
        <div id="all" class="text-lg h-full font-bold bg-bgColor transition-colors duration-500 hover:bg-red-100 cursor-pointer border-t border-x border-gray-200 p-2">
          <p>Général</p>
        </div>
        <div id="list" class="text-lg h-full font-bold bg-bgColor transition-colors duration-500 hover:bg-red-100 cursor-pointer border-t border-x border-gray-200 p-2">
          <p>Liste des objets</p>
        </div>
      
      </div>
      <div class="flex flex-row w-10/12 mx-auto justify-around" id="pageBody">
      </div>
    
    </div>
  `;

  const lolélolé = document.querySelector("#pageBody");
  makeAllPage(lolélolé);

  const options = {
    method: 'POST',
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      'Content-type': 'application/json',
    },
    body: JSON.stringify({}),
  };

  const stats = await fetch('api/object/getStats', options).then(r => r.json());
  const tabs = document.querySelector("#tabs");
  stats.forEach((state) => {
    if (state.stateName !== "total") {
      const button = document.createElement("div");
      button.setAttribute("class",
          "text-lg font-bold bg-bgColor transition-colors duration-500 hover:bg-red-100 cursor-pointer border-t border-x border-gray-200 p-2");
      button.innerHTML = state.stateName;
      button.addEventListener('click', () => {
        makeStatePage(state.stateName, lolélolé);
      });
      tabs.appendChild(button);

    }
  });

  const all = document.querySelector("#all");
  all.addEventListener('click', () => {
    makeAllPage(lolélolé);
  });

  const listTab = document.querySelector("#list");
  listTab.addEventListener('click', () => {
    makeStatePage("", lolélolé);
  });

}

export default tableauDeBord;