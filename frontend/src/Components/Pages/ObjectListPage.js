import Navigate from "../Router/Navigate";
import date1 from "../../img/C2.png";
import date0 from "../../img/C1.png";
import euro0 from "../../img/€0.png";
import euro1 from "../../img/€1.png";
import euro2 from "../../img/€2.png";
import {clearPage} from "../../utils/render";

const main = document.querySelector('main');

const genError = (message) => {
  const body = document.createElement("div");
  body.setAttribute("class",
      "p-2 m-2 w-full bg-red-100 justify-center align-center rounded-3xl");
  body.innerHTML = `<p>${message}</p>`;
  return body;
}
const createBody = () => {

  let body = document.querySelector("#searchBody");
  if (!body) {
    body = document.createElement('section');
    body.setAttribute("id", "searchBody");
  }
  body.innerHTML = `<div class="users-container mt-16"> 
                      <div class="flex flex-col justify-center w-4/5 mx-auto">
                        <table id="searchTable" class="text-left"> 
                          <thead>
                            <tr id="tableHeader" class="border-b">
                              <th class="pr-10">Image</th> 
                              <th>Description</th>
                              <th>Prix</th>
                              <th>Type</th>
                              <th>État de l'objet</th> 
                            </tr> 
                          </thead> 
                          <tbody id="object-list">
                          </tbody> 
                        </table>
                        <div id="searchError">
                    
                        </div>
                      </div>
                    </div>
                    `;
  main.appendChild(body);

}

const createRow = (objet) => {
  const body = document.querySelector('#object-list');
  const row = document.createElement('tr');
  row.setAttribute("class",
      "w-4/5 border-roze border-t  transition-colors duration-500 hover:bg-red-50");
  row.innerHTML = `
  <td class="py-4 pr-10"><img class="max-h-40 min-h-40 p-1" src="${"/api/picture/getPicture/".concat(objet.picture.url)}" alt="image de l'objet"></td>
  <td class="text-left py-4 px-2 md:px-8 pr-2 md:pr-16 lg:pr-52 text-lg">${objet.description}</td> 
  <td class="test-left py-4 pr-2 md:pr-8 text-lg">${objet.price !== 0 ? objet.price.toString().concat(" €") : "N/A"}</td>
  <td class="text-left py-4 pr-2 md:pr-8 text-lg">${objet.type.typeName}</td>
  <td class="text-left py-5 px-2 md:px-12 pr-2 md:pr-16 lg:pr-62 text-lg">${objet.state}</td> 
`;

  row.addEventListener('click', () => {
    Navigate(`/object?id=${objet.id}`);
  });
  body.appendChild(row);
}
const ListObjects = async () => {
  clearPage();
  let selectedTypes = [];

  const user = JSON.parse(sessionStorage.getItem("user"));

  if (user.role === "R" || user.role === "A") {
    const options = {
      method: 'GET',
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        'Content-type': 'application/json',
      },
    };

    fetch('/api/object/getAllObjects', options)
    .then(response => response.json())
    .then(objects => {
      objects.forEach(objet => {
        if (!objet) {
          const searchErrorDiv = document.querySelector("#searchError");
          searchErrorDiv.innerHTML = `<div class="flex justify-center w-full m-2 p-2"><p>Aucun objet trouvé.</p></div>`;
        } else {
          createRow(objet);
        }
      });
    })
  } else {
    Navigate('/')
  }

  let minPrice = 0;
  let maxPrice = Number.MAX_VALUE;

  const connectedSearch = document.createElement('section');
  connectedSearch.innerHTML = `
      <div class="flex flex-col justify-center w-1/2 mx-auto">
        <div id="searchParamError">
        </div>
        <form class="flex flex-col justify-center">
          <div class="flex flex-row justify-between" id="PriceContainer">
            <input type="number" default="${minPrice}" max="${maxPrice}" min="0" id="minPrice" name="minPrice" placeholder="prix minimum" 
            class="m-2 p-2 rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100">
            <input type="number" default="${maxPrice}" min="${minPrice}" id="maxPrice" name="maxPrice" placeholder="prix maximum"
            class="m-2 p-2 rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100 ">
          </div>
          <div class="flex flex-col justify-center">
            <div class="flex flex-rox items-center p-0 h-12">
              <input class="m-0 p-2 w-full m-2 h-full bg-white border-2 border-roze rounded-full transition-colors duration-500 hover:bg-red-100"
              type="text" name="type" value="" placeholder="types des objets" id="typeInput">
              <div class="flex flex-rox items-center m-2 items-center bg-red-300 h-12 w-12 
              rounded-full transition-colors duration-500 hover:bg-red-100" id="typeAdd">
                <p class="text-4xl align-text-middle text-center h-full w-full">+</p>
               </div>
            </div>
            <div class="flex m-2 z-10 w-4/5 justify-center flex-col mx-auto" id="suggestions">
              
            </div>
            <div class="flex flex-row min-w-12 border-t-1 border-b-1 border-grey-200 m-2" id="activeTypes">
              
            </div>
          </div>
          
          
          
          <div class="flex flex-col justify-center">
            <div class="flex flex-rox items-center m-2 p-0 h-12">
              <select class="bg-white rounded-full border-2 border-roze p-2 w-full" id="state" name="typeSearch">
                <option id="allStates" value="">Filtrer par etat</option>
               </select>
            </div>
          </div>
          
          
          
          
          <div class="flex flex-row justify-between" id="dateContainer">
            <input type="date" id="dateObject" class="m-2 p-2 w-full rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100">
          </div>
          <div class="flex flex-row justify-between mt-10">
            <input type="button" id="connectedSearch" class="bg-red-300 transition-colors duration-500 hover:bg-red-200 w-full p-2 m-2 rounded-full" value="chercher">
            <input type="button" id="clearSearch" class="bg-red-200  transition-colors duration-500 hover:bg-red-100 p-2 m-2 rounded-full h-12 w-12" value="clear">
          </div>
        </form>
      </div>
    `;
  main.appendChild(connectedSearch);

  const optionstate = {
    method: "GET",
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      "Content-type": "application/json",
    },
  };
  const listState = await fetch('api/object/allStates', optionstate).then(
      r => r.json());
  const selectState = document.querySelector("#state");
  listState.forEach(objectType => {
    const typeSelect = document.createElement("option");
    const {id, State} = objectType;
    typeSelect.innerHTML = State;
    typeSelect.setAttribute("value", State);
    typeSelect.setAttribute("id", id);
    selectState.appendChild(typeSelect);
  });

  const optionTypes = {
    method: "GET",
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      "Content-type": "application/json",
    },
  };

  const speDiv = document.querySelector("#searchParamError");

  const listTypes = await fetch('api/object/allTypes', optionTypes).then(
      r => r.json());

  const typeInput = document.querySelector("#typeInput");
  typeInput.addEventListener('input', () => {
    const search = "^".concat(
        typeInput.value.toLowerCase().concat("[A-Za-z0-9]*"));
    const suggestions = document.querySelector("#suggestions");
    suggestions.innerHTML = ``;
    listTypes.forEach((type) => {
      if (type.typeName.toLowerCase().match(search)) {
        const typeSuggestion = document.createElement("div");
        typeSuggestion.setAttribute("class",
            "m-2 p-2 rounded-full border-2 border-roze w-full bg-white  transition-colors duration-500 hover:bg-red-100");
        typeSuggestion.addEventListener('click', () => {
          suggestions.innerHTML = ``;
          let addType = true;
          let typeFound = false;
          selectedTypes.every((type2) => {
            if (type2.toLowerCase() === type.typeName.toLowerCase()) {
              addType = false;
              return true;
            }
            return false;
          });
          if (addType) {
            selectedTypes.push(type.typeName);
            const displayTypeNode = document.createElement("div");
            displayTypeNode.setAttribute("id", "dt_".concat(type.typeName));
            displayTypeNode.setAttribute("class",
                "flex flex-row h-10 m-2 p-2 rounded-full bg-white border-2 border-roze "
                + "justify-center align-middle  transition-colors duration-500 hover:bg-red-100");
            displayTypeNode.innerHTML = `<p class="align-text-middle text-center">${type.typeName}</p>`
            const glubglub = document.createElement("div");
            glubglub.innerHTML = `<p class="align-text-middle text-center">x</pclass>`;
            glubglub.setAttribute("class",
                "mx-2 px-2 border-l-2 border-grey-300 justify-center align-middle");
            displayTypeNode.addEventListener('click', () => {
              selectedTypes = selectedTypes.filter(
                  (type2) => type2 !== type.typeName);
              displayTypeNode.remove("#".concat("dt_").concat(type.typeName));
            });
            displayTypeNode.appendChild(glubglub);
            document.querySelector("#activeTypes").appendChild(displayTypeNode);
            typeFound = true;
          }
          if (!typeFound) {
            speDiv.appendChild(genError("Le type recherché n'existe pas."));
          } else {
            typeInput.value = ``;
          }
        });
        typeSuggestion.innerHTML = `<p>${type.typeName}</p>`;
        suggestions.appendChild(typeSuggestion);
      }
    });
  });
  typeInput.addEventListener('blur', () => {
    window.setTimeout(() => {
      const suggestions = document.querySelector("#suggestions");
      suggestions.innerHTML = ``;
    }, 500);
  });
  typeInput.addEventListener('click', () => {
    const suggestions = document.querySelector("#suggestions");
    suggestions.innerHTML = ``;
    listTypes.forEach((type) => {
      const typeSuggestion = document.createElement("div");
      typeSuggestion.setAttribute("class",
          "m-2 p-2 rounded-full border-2 border-roze w-full bg-white  transition-colors duration-500 hover:bg-red-100");
      typeSuggestion.addEventListener('click', () => {
        suggestions.innerHTML = ``;
        let addType = true;
        let typeFound = false;
        selectedTypes.every((type2) => {
          if (type2.toLowerCase() === type.typeName.toLowerCase()) {
            addType = false;
            return true;
          }
          return false;
        });
        if (addType) {
          selectedTypes.push(type.typeName);
          const displayTypeNode = document.createElement("div");
          displayTypeNode.setAttribute("id", "dt_".concat(type.typeName));
          displayTypeNode.setAttribute("class",
              "flex flex-row h-10 m-2 p-2 rounded-full bg-white border-2 border-roze "
              + "justify-center align-middle  transition-colors duration-500 hover:bg-red-100");
          displayTypeNode.innerHTML = `<p class="align-text-middle text-center">${type.typeName}</p>`
          const glubglub = document.createElement("div");
          glubglub.innerHTML = `<p class="align-text-middle text-center">x</pclass>`;
          glubglub.setAttribute("class",
              "mx-2 px-2 border-l-2 border-grey-300 justify-center align-middle");
          displayTypeNode.addEventListener('click', () => {
            selectedTypes = selectedTypes.filter(
                (type2) => type2 !== type.typeName);
            displayTypeNode.remove("#".concat("dt_").concat(type.typeName));
          });
          displayTypeNode.appendChild(glubglub);
          document.querySelector("#activeTypes").appendChild(displayTypeNode);
          typeFound = true;
        }
        if (!typeFound) {
          speDiv.appendChild(genError("Le type recherché n'existe pas."));
        } else {
          typeInput.value = ``;
        }
      });
      typeSuggestion.innerHTML = `<p>${type.typeName}</p>`;
      suggestions.appendChild(typeSuggestion);
    });

  });

  const typeDisplay = document.querySelector("#activeTypes");
  const typeAddButton = document.querySelector("#typeAdd");

  typeAddButton.addEventListener('click', () => {

    const typename = typeInput.value;
    let addType = true;
    let typeFound = false;
    selectedTypes.every((type) => {
      if (type.toLowerCase() === typename.toLowerCase()) {
        addType = false;
        return true;
      }
      return false;
    });
    if (addType) {
      listTypes.every((type) => {
        if (type.typeName.toLowerCase() === typename.toLowerCase()) {
          selectedTypes.push(type.typeName);
          const displayTypeNode = document.createElement("div");
          displayTypeNode.setAttribute("id", "dt_".concat(type.typeName));
          displayTypeNode.setAttribute("class",
              "flex flex-row h-10 m-2 p-2 rounded-full bg-white border-2 border-roze "
              + "justify-center align-middle  transition-colors duration-500 hover:bg-red-100");
          displayTypeNode.innerHTML = `<p class="align-text-middle text-center">${type.typeName}</p>`
          const glubglub = document.createElement("div");
          glubglub.innerHTML = `<p class="align-text-middle text-center">x</pclass>`;
          glubglub.setAttribute("class",
              "mx-2 px-2 border-l-2 border-grey-300 justify-center align-middle");
          glubglub.addEventListener('click', () => {
            selectedTypes = selectedTypes.filter(
                (type2) => type2 !== type.typeName);
            displayTypeNode.remove("#".concat("dt_").concat(type.typeName));
          });
          displayTypeNode.appendChild(glubglub);
          typeDisplay.appendChild(displayTypeNode);
          typeFound = true;
          return false;
        }
        return true;
      });
      if (!typeFound) {
        speDiv.appendChild(genError("Le type recherché n'existe pas."));
      } else {
        typeInput.value = ``;
      }
    }
  });

  let dateSortState = "N";
  const dateSort = document.createElement("img");
  dateSort.setAttribute("src", date1);
  dateSort.setAttribute("class",
      "bg-white  transition-colors duration-500 hover:bg-red-100"
      + " border-2 border-roze rounded-full items-center p-2 m-2 h-14");
  dateSort.addEventListener('click', () => {
    dateSort.classList.add("animate-spin");
    window.setTimeout(() => {
      if (dateSortState === "N") {
        dateSortState = "Y";
        dateSort.setAttribute("src", date0);
      } else {
        dateSortState = "N";
        dateSort.setAttribute("src", date1);
      }
    }, 250);
    window.setTimeout(() => {
      dateSort.classList.remove("animate-spin");
    }, 500)
  });

  document.querySelector("#dateContainer").appendChild(dateSort);

  let priceSortState = "0";
  const priceSort = document.createElement("img");
  priceSort.setAttribute("src", euro0);
  priceSort.setAttribute("class",
      "bg-white border-2 transition-colors duration-500 "
      + " hover:bg-red-100 border-roze rounded-full items-center m-2 h-14");
  priceSort.addEventListener('click', () => {
    priceSort.classList.add("animate-spin");
    window.setTimeout(() => {
      if (priceSortState === "0") {
        priceSortState = "A";
        priceSort.setAttribute("src", euro1);
      } else if (priceSortState === "A") {
        priceSortState = "D";
        priceSort.setAttribute("src", euro2);
      } else if (priceSortState === "D") {
        priceSortState = "0";
        priceSort.setAttribute("src", euro0);
      }
    }, 250);
    window.setTimeout(() => {
      priceSort.classList.remove("animate-spin");
    }, 500);
  });

  const priceContainer = document.querySelector("#PriceContainer");
  priceContainer.appendChild(priceSort);

  const dateSelect = document.querySelector("#dateObject");
  dateSelect.valueAsDate = new Date();

  const connectedSearchButton = document.querySelector("#connectedSearch");
  connectedSearchButton.addEventListener('click', async () => {
    speDiv.innerHTML = ``;
    const objectState = document.querySelector("#state").value;
    minPrice = document.querySelector("#minPrice").value;
    maxPrice = document.querySelector("#maxPrice").value;
    const objectType = selectedTypes.length === 0 ? ["all"] : selectedTypes;
    if (minPrice === "") {
      minPrice = 0;
    }
    if (maxPrice === "") {
      maxPrice = Number.MAX_VALUE;
    }
    if (minPrice > maxPrice) {
      speDiv.appendChild(genError(
          "Le prix minimum ne peut pas être plus élevé que le prix maximum"));
    }
    const objectDate = document.querySelector("#dateObject").value;
    const connectedSearchOptions = {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        "state": objectState,
        "SBD": dateSortState,
        "SBP": priceSortState,
        "type": objectType,
        "minPrice": minPrice,
        "maxPrice": maxPrice,
        "date": objectDate,
      }),
    };
    const results = await fetch("api/object/search",
        connectedSearchOptions).then(r => r.json());
    createBody();
    if (!results || results.length === 0) {
      const searchErrorDiv = document.querySelector("#searchError");
      searchErrorDiv.innerHTML = `<div class="flex justify-center w-full m-2 p-2"><p>Aucun objet trouvé.</p></div>`;
    } else {
      results.forEach(r => {
        createRow(r);
      });

    }
  });

  const clearButton = document.querySelector("#clearSearch");
  clearButton.addEventListener('click', () => {
    speDiv.innerHTML = ``;
    priceSort.classList.add("animate-spin");
    dateSort.classList.add("animate-spin")
    document.querySelector("#minPrice").value = ``;
    document.querySelector("#maxPrice").value = ``;
    document.querySelector("#typeInput").value = ``;
    document.querySelector("#dateObject").valueAsDate = new Date();
    window.setTimeout(() => {
      dateSortState = "N";
      dateSort.setAttribute("src", date1);
      priceSortState = "0";
      priceSort.setAttribute("src", euro0);
      while (selectedTypes.length > 0) {
        selectedTypes.pop();
      }
      typeDisplay.innerHTML = ``;
      selectState.selectedIndex = 0;

    }, 250);
    window.setTimeout(() => {
      priceSort.classList.remove("animate-spin");
      dateSort.classList.remove("animate-spin");
    }, 500);
  });

  createBody();
}

window.navToObjEdit = async function navToEdit(user) {

  Navigate('/updateObject?id='.concat(user));
};

window.navToObj = async function navToObjPage(user) {

  Navigate('/object?id='.concat(user));
};

export default ListObjects;