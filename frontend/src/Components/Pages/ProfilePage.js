import Swal from 'sweetalert2'
import Navigate from '../Router/Navigate';

const ProfilePage = async () => {
  const body = document.querySelector("main");
  const user = JSON.parse(sessionStorage.getItem("user"));

// Appel de l'API pour récupérer les données de l'utilisateur
  const options = {
    method: "GET",
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      "content-type": "application/json",

    },
  };
  const request = await fetch(`/api/users/getOne/${user.id}`,
      options);

  if (request.ok) {
    const theUser = await request.json();

    // Affichage des données de l'utilisateur dans le DOM
    body.innerHTML = `
  <div class="grid place-items-center pt-10">
    <h1 class="text-4xl font-bold pb-8">Éditer mon profil</h1>
    <div class="fs-5 rounded text-center" id="alert">
      </div>

    <div class="max-w-sm w-full space-y-6">
 <div>
  <label for="firstname" class="block text-lg font-medium text-gray-700">
    Prénom :
  </label>
  
  <div class="mt-1 relative rounded-md shadow-sm">
    <input
      type="text"
      name="firstname"
      id="firstname"
      value="${theUser.firstname}"
      class="m-2 p-2 rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full pl-10"
    />
    <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
      <i class="fas fa-pencil-alt text-gray-400"></i>
    </div>
  </div>
</div>

         <div>
  <label for="lastname" class="block text-lg font-medium text-gray-700">
    Nom :
  </label>
  <div class="mt-1 relative rounded-md shadow-sm">
    <input
      type="text"
      name="lastname"
      id="lastname"
      value="${theUser.lastname}"
      class="m-2 p-2 rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full pl-10"
    />
    <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
      <i class="fas fa-pencil-alt text-gray-400"></i>
    </div>
  </div>
</div>


<div>
  <label for="email" class="block text-lg font-medium text-gray-700">
    Adresse email
  </label>
  <div class="mt-1 relative rounded-md shadow-sm">
    <input
      type="text"
      name="email"
      id="email"
      value="${theUser.email}"
      class="m-2 p-2 rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full pl-10"
    />
   <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
      <i class="fas fa-pencil-alt text-gray-400"></i>
    </div>
  </div>
</div>

      
      
<div>
  <label for="phone" class="block text-lg font-medium text-gray-700">
    Numéro de téléphone :
  </label>
  <div class="mt-1 relative rounded-md shadow-sm">
    <input 
      type="tel" 
      name="phone" 
      id="phone" 
      value="${theUser.phoneNumber}" 
      class="m-2 p-2 rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full pl-10" 
      placeholder="Entrez votre numéro de téléphone" 
      pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" 
      required
    />
    <div class="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
      <i class="fas fa-pencil-alt text-gray-400"></i>
    </div>
  </div>
</div>

<div class="mt-1">
      <label for="password" class="block text-lg font-medium text-gray-700">
    Nouveau mot de passe :
</label>
    <input
        type="password"
        name="newPassword"
        id="newPassword"
        value=""
          class="m-2 p-2 rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full pl-10"
    />
</div>
<div class="mt-1">
<label for="confirmPassword" class="block text-lg font-medium text-gray-700">
    Confirmer le nouveau mot de passe :
</label>
    <input
        type="password"
        name="confirmPassword"
        id="confirmPassword"
        value=""
          class="m-2 p-2 rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full pl-10"
    />
    
</div>
    
    <label for="confirmPassword" class="block text-lg font-medium text-gray-700">
    Photo de profil:
</label>

<div>
    <img alt="image user" class="w-max h-32 object-contain" src="${"/api/picture/getPicture/".concat(
        theUser.picture.url)}" id="picture">
    <input class=" my-2 text-sm border border-gray-800 border-1 rounded-lg cursor-pointer " id="file_input" type="file" accept="image/png, image/jpg" >
</div>
    
<div class="flex justify-center mt-4 flex-row">
  <button class="block px-4 py-2 bg-roze border-roze border-2 rounded-3xl transition-colors duration-500 hover:bg-red-100 text-lg  mr-4" id="btn-valider">Éditer</button>
  <button class="block px-4 py-2 text-white bg-red-500 border-red-500 border-2 rounded-3xl transition-colors duration-500 hover:bg-red-600 text-lg " id="btn-annuler">Annuler</button>
</div>
      </div>
      <div>
    </div>
  </div>
`;
    const idUser = theUser.id;
    const firstname = document.getElementById("firstname");
    const lastname = document.getElementById("lastname");
    const email = document.getElementById("email");
    const phoneNumber = document.getElementById("phone");
    const newPasswordField = document.getElementById('newPassword');
    const confirmPasswordField = document.getElementById('confirmPassword');
    const validerBtn = document.getElementById("btn-valider");
    const annulerBtn = document.getElementById("btn-annuler");

    validerBtn.addEventListener("click", async () => {
      let passwordChanged = false; // initialiser la variable à false
      let fileName = theUser.picture.url;

      // Vérifier si une nouvelle image a été mise et l'upload sur le serveur si oui
      if (document.getElementById("file_input").files[0] != null) {
        const sendPictureOptions = {
          method: "POST",
          body: document.getElementById("file_input").files[0],
        }

        fileName = await fetch('api/picture/upload', sendPictureOptions).then(
            r => r.json());
        fileName = fileName.fileName;
      }

      // Vérifier si les champs de mot de passe ont été modifiés
      if (newPasswordField.value.trim() !== ''
          || confirmPasswordField.value.trim() !== '') {
        passwordChanged = true; // marquer la variable comme true si les champs ont été modifiés
      }

      // Si les champs de mot de passe ont été modifiés, vérifié leur correspondance
      if (passwordChanged && newPasswordField.value.trim()
          !== confirmPasswordField.value.trim()) {
        //    alert('Les deux champs de mot de passe ne correspondent pas.');
        await Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Les deux champs de mot de passe ne correspondent pas.',
        });
        return;
      }
      const updatedUser = {
        id: idUser,
        firstname: firstname.value,
        lastname: lastname.value,
        email: email.value,
        phoneNumber: phoneNumber.value,
        encryptedPassword: newPasswordField.value.trim(),
        picture: {
          "url": fileName,
        }
      };

      const theOptions = {
        method: "PUT",
        headers: {
          "Authorization": sessionStorage.getItem("token")
              ? sessionStorage.getItem("token") : localStorage.getItem("token"),
          "content-type": "application/json",
        },
        body: JSON.stringify(updatedUser),
      };

      const response = await fetch(`/api/users/editMyProfile`, theOptions);

      if (response.ok) {
        await Swal.fire({
          icon: 'success',
          title: 'Profil modifié !',
          text: 'Votre profil a été mis à jour avec succès.',
          confirmButtonText: 'OK'
        });

        document.getElementById(
            "picture").src = "/api/picture/getPicture/".concat(
            fileName)
        document.getElementById("pp").src = "/api/picture/getPicture/".concat(
            fileName)

        const newUser = await response.json();
        sessionStorage.setItem("user", JSON.stringify(newUser));
        Navigate("/");

      } else {
        await Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Erreur lors de la mise à jour de l\'utilisateur.',
        });
      }
    });

    annulerBtn.addEventListener("click", () => {
      Navigate("/profile");
    });

  } else {
    body.innerHTML = "<h1>Erreur lors de la récupération des données de l'utilisateur</h1>";
  }
};

export default ProfilePage;