import Swal from "sweetalert2";

const roleMap = {
  'R': 'Responsable',
  'M': 'Membre',
  'A': 'Aidant'
};
const fillTable = (users) => {
  const alertDiv = document.querySelector(".alert");
  alertDiv.classList.toggle('hidden', true);
  alertDiv.innerHTML = ``;

  const userList = document.getElementById('users-list');
  userList.innerHTML = ``;

  if (users.length === 0) {
    alertDiv.classList.toggle('hidden', false);
    alertDiv.innerHTML = `<p>No users found.</p>`;
  }
  users.forEach(user => {
    const roleName = roleMap[`${user.role}`];
    const row = document.createElement('tr');
    row.setAttribute("class",
        "border-roze border-t transition-colors duration-500 hover:bg-red-50")
    row.innerHTML = `
        <td class="text-center py-4 pr-52">
        <img src="${"/api/picture/getPicture/".concat(user.picture.url)}" alt="img user" class="h-20">
                <a href="/pr?id=${user.id}">
        </a>
  </td>
    <td class="text-center py-4 pr-52">
      <a href="/pr?id=${user.id}">
        ${user.firstname}
      </a>
    </td>
    <td class="text-center py-4 pr-52">
      <a href="/pr?id=${user.id}">
        ${user.lastname}
      </a>
    </td>
    <td class="text-center py-4 pr-52">
      <a href="/pr?id=${user.id}">
        ${user.email}
      </a>
    </td>
    <td class="text-center py-4 ">
      <a href="/pr?id=${user.id}">
        ${user.phoneNumber}
      </a>
    </td>
    <td class="text-center">
      <button id="btnUserRole${user.id}" class="nav-link block px-3 py-2 bg-roze border-roze border-2 rounded-3xl transition-colors duration-500 hover:bg-red-100" onclick="updateRole('${user.id}')">
        ${roleName}
      </button>
    </td>
        `;

    userList.appendChild(row);

  });

}
const UsersPage = async () => {
  const color0 = "bg-white";
  const colorY = "bg-green-50";
  const colorN = "bg-red-50"

  const body = document.querySelector('main');

  body.innerHTML = ` 
  <div class="users-container mt-16 justify-center flex flex-col"> 
    <div id="search" class="flex flex-row w-3/4 justify-center mx-auto">
      <input type="text" id="name" class="m-2 p-2 bg-white border-2 border-roze rounded-full w-full
      transition-colors duration-500 hover:bg-red-100" placeholder="nom/prénom">
      <input type="button" id="selectAidant" class="bg-white border-2 border-roze m-2 p-2 rounded-full 
      transition-colors duration-500 hover:bg-red-100 cursor-pointer" value="aidant">
      <input type="button" id="rechercher" class="bg-white border-2 border-roze m-2 p-2 rounded-full 
      transition-colors duration-500 hover:bg-red-100 cursor-pointer" value="rechercher">
      <input type="button" id="clear" class="bg-white border-2 border-roze m-2 p-2 rounded-full 
      transition-colors duration-500 hover:bg-red-100 cursor-pointer" value="clear">
    </div>
    <div id="suggestions" class="hidden w-1/3 mx-auto"></div>
    <div class="hidden alert w-1/3 bg-red-100 rounded-2xl mx-auto m-2 p-2"></div>
         <div class="alert"></div>
    <table class="text-left mx-auto"> 
      <thead>
        <tr class="border-b">
          <th>Photo de profil</th>
          <th>Prénom</th> 
          <th>Nom de famille</th>
          <th>Email</th> 
          <th >Numéro de téléphone</th>
          <th class="text-center">Rôle</th>
        </tr> 
      </thead> 
      <tbody id="users-list">
      
      </tbody> 
    </table>
  </div> 
`;

  window.updateRole = async function updateRole(idUser) {
    const {value: role} = await Swal.fire({
      title: 'Changer le rôle de l\'utilisateur',
      input: 'select',
      inputOptions: {
        'A': 'Aidant',
        'R': 'Responsable',
      },
      inputPlaceholder: 'Choisir le rôle',
      showCancelButton: true,
      cancelButtonText: 'Annuler',
      confirmButtonText: 'Enregistrer',
    });
    if (role) {
      const theOptions = {
        method: "POST",
        headers: {
          "Authorization": sessionStorage.getItem("token")
              ? sessionStorage.getItem("token") : localStorage.getItem("token"),
          "Content-type": "application/json",
        },
      };
      const apiUrl = `api/users/confirm${role === 'A' ? 'Assistant'
          : 'Responsable'}/${idUser}`;
      const request = await fetch(apiUrl, theOptions);
      if (request.ok) {
        const btnUserAidant = document.getElementById(
            'btnUserAidant'.concat(idUser));
        const btnUserResponsable = document.getElementById(
            'btnUserResponsable'.concat(idUser));
        if (btnUserAidant && btnUserResponsable) {
          btnUserAidant.classList.toggle("bg-roze", role === 'A');
          btnUserResponsable.classList.toggle("bg-roze", role === 'R');
        }
        await Swal.fire({
          title: 'Modification enregistrée',
          icon: 'success',
        });
        const roleElement = document.getElementById(`btnUserRole${idUser}`);
        roleElement.textContent = roleMap[`${role}`];
      } else {
        const alertDiv = document.getElementById('alert');
        alertDiv.innerHTML = `<div class="text-red-600" role="alert">Erreur lors de la modification</div>`;
      }
    }
  };

  const options = {
    method: 'GET',
    headers: {
      'Authorization': sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      'content-type': 'application/json',
    }
  }

  const users = await fetch('/api/users', options)
  .then(response => response.json());

  fillTable(users);

  let aidantState = "0";
  const selectAidantButton = document.querySelector("#selectAidant");
  selectAidantButton.addEventListener('click', () => {
    if (aidantState === "0") {
      aidantState = "Y";
      selectAidantButton.classList.replace(color0, colorY);
    } else if (aidantState === "Y") {
      aidantState = "N";
      selectAidantButton.classList.replace(colorY, colorN);
    } else if (aidantState === "N") {
      aidantState = "0";
      selectAidantButton.classList.replace(colorN, color0);
    }
  });

  const selectName = document.querySelector("#name");
  selectName.addEventListener('input', () => {
    const search = "^".concat(selectName.value.toLowerCase()).concat(
        "[a-zA-Z0-9]*");
    const suggestions = document.querySelector("#suggestions");
    suggestions.innerHTML = ``;
    suggestions.classList.toggle('hidden', false);
    users.forEach((user) => {
      if (user.firstname.toLowerCase().match(search)
          || user.lastname.toLowerCase().match(search)) {
        const nameSuggestion = document.createElement("div");
        nameSuggestion.setAttribute("class",
            "cursor-pointer bg-white border-2 border-roze rounded-full m-2 p-2 "
            + "transition-colors duration-500 hover:bg-red-100");
        nameSuggestion.innerHTML = `<p>${user.firstname} ${user.lastname}</p>`;
        nameSuggestion.addEventListener('click', () => {
          selectName.value = user.lastname;
          suggestions.innerHTML = ``;
          suggestions.classList.toggle('hidden', 'true');
        });
        suggestions.appendChild(nameSuggestion);
      }
    });

  });
  selectName.addEventListener('click', () => {
    const suggestions = document.querySelector("#suggestions");
    suggestions.innerHTML = ``;
    suggestions.classList.toggle('hidden', false);
    users.forEach((user) => {
      const nameSuggestion = document.createElement("div");
      nameSuggestion.setAttribute("class",
          "cursor-pointer bg-white border-2 border-roze rounded-full m-2 p-2 "
          + "transition-colors duration-500 hover:bg-red-100");
      nameSuggestion.innerHTML = `<p>${user.firstname} ${user.lastname}</p>`;
      nameSuggestion.addEventListener('click', () => {
        selectName.value = user.lastname;
        suggestions.innerHTML = ``;
        suggestions.classList.toggle('hidden', 'true');
      });
      suggestions.appendChild(nameSuggestion);
    });

  });
  selectName.addEventListener('blur', () => {
    window.setTimeout(() => {
      const suggestions = document.querySelector("#suggestions");
      suggestions.innerHTML = ``;
      suggestions.classList.toggle('hidden', true);
    }, 200);
  });

  const searchButton = document.querySelector("#rechercher");
  searchButton.addEventListener('click', async () => {
    const optionsSearch = {
      method: "POST",
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        "content-type": "application/json",
      },
      body: JSON.stringify({
        "name": selectName.value,
        "role": aidantState
      })
    }

    const results = await fetch("api/users/search", optionsSearch).then(
        r => r.json());
    fillTable(results);
  });

  const clearButton = document.querySelector("#clear");
  clearButton.addEventListener('click', () => {
    selectName.value = ``;
    selectAidantButton.classList.replace(colorY, color0);
    selectAidantButton.classList.replace(colorN, color0);
    aidantState = "0";
    fillTable(users);
  });

}

window.switchCheck = async function checkAndAddStaff(user) {

  const options = {
    method: "POST",
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      "Content-type": "application/json",
    },
  };
  const apiUrl = `api/users/confirmAssistant/`.concat(user)
  const request = await fetch(apiUrl, options);
  if (request.ok) {
    document.getElementById('btnUser'.concat(user)).classList.add("bg-roze");
  } else {
    const alertDiv = document.getElementById('alert');
    alertDiv.innerHTML = `<div class="text-red-600" role="alert">Error Response </div>`;
  }
};

// For some reason, eslint will not accept a default export here,
// and will not accept a named export for just one function either.
// So to make it shut up, I have this beautiful useless function to export
// that is used litterally nowhere. This code is never run!
const uselessFunction = () => "I'm useless, lol!";

export {UsersPage, uselessFunction};