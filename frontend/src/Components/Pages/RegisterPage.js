import Navigate from '../Router/Navigate';

const RegisterPage = () => {

  const body = document.querySelector('main');

  body.innerHTML = `
    <div class="grid place-items-center pt-15">
    <div class=" max-w-sm ">
      <div class="flex justify-center">
        <h1 class="pb-10 text-4xl font-bold">Inscription</h1>
      </div>

      <form>
      
        <label for="confirmPassword" class="pb-2 text-lg font-semibold"> Changer de photo de profile :</label>
        <img alt="image user" class="w-max h-32 object-contain" src="${"/api/picture/getPicture/".concat("defaultPP.png")}" id="picture">
        <input class="my-2 text-sm border border-gray-800 border-1 rounded-lg cursor-pointer " id="file_input" type="file" accept="image/png, image/jpg" >
        
        <br>
        <label class="pb-2 text-lg font-semibold" for="firstname">Prénom :</label>
        <input class="appearance-none border rounded-xl border-roze w-full py-2 px-4 mb-3 focus:outline-none" type="text" id="firstname" name="firstname" required><br><br>

        <label class="pb-2 text-lg font-semibold" for="lastname">Nom de famille :</label>
        <input class="appearance-none border rounded-xl border-roze w-full py-2 px-4 mb-3 focus:outline-none" type="text" id="lastname" name="lastname" required><br><br>

        <label class="pb-2 text-lg font-semibold" for="email">Email :</label>
        <input class="appearance-none border rounded-xl border-roze w-full py-2 px-4 mb-3 focus:outline-none" type="email" id="email" name="email" required><br><br>

        <label class="pb-2 text-lg font-semibold" for="phone">Numéro de télephone :</label>
        <input class="appearance-none border rounded-xl border-roze w-full py-2 px-4 mb-3 focus:outline-none" type="tel" id="phone" name="phone" required><br><br>

        <label class="pb-2 text-lg font-semibold" for="password">Mot de passe :</label>
        <input class="appearance-none border rounded-xl border-roze w-full py-2 px-4 mb-3 focus:outline-none" type="password" id="password" name="password" required><br><br>

        <label class="pb-2 text-lg font-semibold" for="confirm-password">Confirmer le mot de passe :</label>
        <input class="appearance-none border rounded-xl border-roze w-full py-2 px-4 mb-3 focus:outline-none" type="password" id="confirm-password" name="confirm-password" required><br><br>
        
        <div id="alert"></div>
        
        <input class="mt-6 font-semibold bg-roze h-10 w-full text-lg rounded-xl" type="submit" value="Valider">
      </form>
    </div>
    </div>
  `;

  const form = document.querySelector('form');
  const alertDiv = document.getElementById('alert');

  form.addEventListener('submit', onSubmit);

  async function onSubmit(e) {
    e.preventDefault();

    const firstname = document.getElementById('firstname').value;
    const lastname = document.getElementById('lastname').value;
    const email = document.getElementById('email').value;
    const phoneNumber = document.getElementById('phone').value;
    const encryptedPassword = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirm-password').value;
    let fileName;


    // eslint-disable-next-line no-useless-escape
    const regex = /^(\+[0-9]2[ \.\/\-]{0,1}[0-9]{3}(([\.\/\- ]{0,1}[0-9]{2}){3}|([\.\/\- ]{0,1}[0-9]{3}){2}))|(0[0-9][\.\/\- ]{0,1}[0-9]{3}([\.\/\- ]{0,1}[0-9]{2,3}){2})|(0[0-9]{3}(([\.\/\- ]{0,1}[0-9]{2}){3}|([\.\/\- ]{0,1}[0-9]{3}){2}))$/;




    if (document.getElementById("file_input").files[0] != null) {

      const sendPictureOptions = {
        method: "POST",
        body: document.getElementById("file_input").files[0],
      }
      fileName = await fetch('api/picture/upload', sendPictureOptions).then(r => r.json());
      fileName = fileName.fileName;
    } else {
      fileName = "defaultPP.png"
    }

    if (!firstname || !lastname || !email || !phoneNumber || !encryptedPassword
        || !confirmPassword) {
      alertDiv.innerHTML = `<div class="alert alert-danger fs-5" role="alert">
    Veuillez remplir tous les champs
    </div>`;

    } else if (encryptedPassword !== confirmPassword) {
      alertDiv.innerHTML = `<div class="text-red-600" role="alert">
        Les mots de passe ne correspondent pas.
      </div>`;
    } else if (!phoneNumber.match(regex)) {
      alertDiv.innerHTML = `<div class="text-red-600" role="alert">
        Ce numéro de téléphone n'est pas valide.
      </div>`;
    } else {
      const options = {
        method: 'PUT',
        body: JSON.stringify({
          firstname,
          lastname,
          email,
          phoneNumber,
          encryptedPassword,
          picture: {
            "url": fileName,
          }
        }),
        headers: {
          'Content-type': 'application/json',
        },
      };
      const request = await fetch('api/auths/register', options);
      if (request.ok) {
        const response = await request.json();
        if (response) {
          /*
          if (rememberMe.checked) {
            localStorage.setItem('token', response.token);
          } else {
            sessionStorage.setItem('token', response.token);
          }
          sessionStorage.setItem('user', JSON.stringify(response.user));
           */

          Navigate('/login');
          window.location.reload();
        } else {
          alertDiv.innerHTML = `<div class="text-red-600" role="alert">Error Response </div>`;
        }
      } else {
        alertDiv.innerHTML = `<div class="text-red-600" role="alert">Error Request </div>`;
      }
      form.reset();
    }
  }
}
export default RegisterPage;

