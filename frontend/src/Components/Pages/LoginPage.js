import Navigate from '../Router/Navigate';

const LoginPage = () => {

  const body = document.querySelector("main");

  body.innerHTML = `
    <div class="grid place-items-center pt-40">
    <div class=" max-w-sm ">

      <div class="flex justify-center">
        <h1 class="pb-10 text-4xl font-bold">Connexion</h1>
      </div>

      <form>
        <label class="pb-2 text-lg font-semibold" for="email">Adresse email</label>
        <input class="appearance-none border rounded-xl border-roze w-full py-2 px-4 mb-3 focus:outline-none" type="email" id="email" name="email" required>
        
        <label class="pb-2 pt-5 text-lg font-semibold" for="password">Mot de passe</label>
        <input class="appearance-none border rounded-xl border-roze w-full py-2 px-4 mb-3 focus:outline-none" type="password" id="password" name="password" required>
        
        <label class="text-lg" for="rememberMe">Se souvenir de moi ?</label>
        <input class="accent-roze w-4 h-4 hover:accent-roze" type="checkbox" id="rememberMe" name="rememberMe">
        
        <div id="alert"></div>
        
        <input class="mt-8 font-semibold bg-roze h-10 w-full text-lg rounded-xl" type="submit" value="Se connecter">
      </form>
	  </div>
	  </div>
	`;

  const form = document.querySelector("form")
  const alertDiv = document.getElementById("alert");
  const souvenir = document.getElementById("rememberMe");

  form.addEventListener("submit", onSubmit);

  async function onSubmit(e) {
    e.preventDefault();
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;

    if (!email || !password) {
      erreursChampsVide(email, password);
    } else {
      const options = {
        method: "POST",
        body: JSON.stringify({
          email,
          password,
        }),
        headers: {
          "Content-type": "application/json",
        },
      };
      const request = await fetch(`/api/auths/login`, options);

      if (request.ok) {
        const response = await request.json();
        if (response) {
          if (souvenir.checked) {
            localStorage.setItem("token", response.token);
          } else {
            sessionStorage.setItem("token", response.token);
          }
          sessionStorage.setItem("user", JSON.stringify(response.user));
          Navigate('/');
          window.location.reload();
        } else {
          alertDiv.innerHTML = `<div class="text-red-600" role="alert">Error Respond </div>`;
        }
      } else {
        alertDiv.innerHTML = `<div class="font-bold">Utilisateur non inscrit ou mauvais mot-de-passe</div>`;
      }
      form.reset();
    }
  }

  /**
   * Le message d'alerte change en fonction de l'erreur
   * @param email - email de l'utilisateur
   * @param motDePasse - mot de passe de l'utilisateur
   *
   */
  function erreursChampsVide(email, motDePasse) {
    if (email === "" && motDePasse === "") {
      alertDiv.innerHTML = `<div class="alert alert-danger fs-5" role="alert">
    Veuillez introduire votre email et votre mot de passe
    </div>`;
    } else if (email === "") {
      alertDiv.innerHTML = `<div class="font-bold" role="alert">
    Veuillez introduire votre email
    </div>`;
    } else {
      alertDiv.innerHTML = `<br><div class="font-bold" role="alert">
    Veuillez introduire votre mot de passe
    </div>`;
    }
  }
}

export default LoginPage;
