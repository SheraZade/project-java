import Navigate from "../Router/Navigate";
/*
Mother of all regex to check if phone number is valid
^(\+[0-9]2[ \.\/\-]{0,1}[0-9]{3}(([\.\/\- ]{0,1}[0-9]{2}){3}|([\.\/\- ]{0,1}[0-9]{3}){2}))|(0[0-9][\.\/\- ]{0,1}[0-9]{3}([\.\/\- ]{0,1}[0-9]{2,3}){2})|(0[0-9]{3}(([\.\/\- ]{0,1}[0-9]{2}){3}|([\.\/\- ]{0,1}[0-9]{3}){2}))$
 */

const option = {
  method: "GET",
  headers: {},
};

const quidam = async () => {
  const body = document.querySelector("main");
  body.innerHTML = `
    <section>
      <div class="flex flex-col justify-center">
        <h1 class="flex justify-center text-4xl text-neutral-500 p-10">Connectez-vous ou entrez votre numéro de téléphone pour continuer</h1>
        <div id="errorDiv"></div>
        <div class="flex flex-row justify-center my-auto h-1/2 w-3/4 mx-auto">
          <form class="flex flex-col items-center border-r-2 border-gray-300 p-10 w-3/4">
            <input class="bg-white border-2 border-roze rounded-full w-full p-2 m-2 my-auto" type="text" name="phoneNumber" id="phoneNumber" placeholder="Entrez votre numéro de téléphone S.V.P.">
            <input type="button" value="continuer" id="phoneConnect" class="bg-red-300 p-2 rounded-full w-1/2">
          </form>
          <form class="flex flex-col items-center p-10 w-3/4">
            <input class="bg-white border-2 border-roze rounded-full w-full p-2 m-2" type="text" name="email" id="email" placeholder="Entrez votre e-mail S.V.P.">
            <input class="bg-white border-2 border-roze rounded-full w-full p-2 m-2" type="password" name="password" id="password" placeholder="Entrez votre mot de passe S.V.P.">
            <div class="flex flex-row justify-center">
              <label class="text-lg" for="rememberMe">Se souvenir de moi ?</label>
              <input class="accent-roze w-4 h-4 hover:accent-roze my-auto mx-2" type="checkbox" id="rememberMe" name="rememberMe">
            </div>
            <input type="button" value="se connecter" id="accountConnect" class="bg-red-300 p-2 rounded-full w-1/2">
          </form>
        </div>
      </div>
    </section>
  `;

  const phoneBTN = document.querySelector("#phoneConnect");
  const accountBTN = document.querySelector("#accountConnect");

  phoneBTN.addEventListener('click', async () => {
    const phoneNumber = document.querySelector("#phoneNumber").value;
    // eslint-disable-next-line no-useless-escape
    const regex = /^(\+[0-9]2[ \.\/\-]{0,1}[0-9]{3}(([\.\/\- ]{0,1}[0-9]{2}){3}|([\.\/\- ]{0,1}[0-9]{3}){2}))|(0[0-9][\.\/\- ]{0,1}[0-9]{3}([\.\/\- ]{0,1}[0-9]{2,3}){2})|(0[0-9]{3}(([\.\/\- ]{0,1}[0-9]{2}){3}|([\.\/\- ]{0,1}[0-9]{3}){2}))$/;
    if(!phoneNumber.match(regex)) {
      document.querySelector("#errorDiv").innerHTML = `<div class="rounded-3xl bg-red-100 w-1/3 mx-auto p-2 m-2"><p class="text-center align-text-center">Ce numéro de téléphone n'est pas valide.</p></div>`;
    } else {

      const optionsPhoneNumber = {
        method: "POST",
        body: JSON.stringify({
          "phoneNumber": phoneNumber,
        }),
        headers: {
          "Content-type": "application/json"
        }
      };

      const res = await fetch("api/auths/phoneNumber", optionsPhoneNumber)
      .then(r => r.json());
      if (res) {
        sessionStorage.setItem("token", res.token);
        sessionStorage.setItem("phone", phoneNumber);
        window.location.reload();
      }
    }
  });

  accountBTN.addEventListener('click', async () => {
    const email = document.querySelector("#email").value;
    const password = document.querySelector("#password").value;
    const souvenir = document.querySelector("#rememberMe").value;

    const options = {
      method: "POST",
      body: JSON.stringify({
        email,
        password,
      }),
      headers: {
        "Content-type": "application/json",
      },
    };
    const request = await fetch(`/api/auths/login`, options);
    if (request.ok) {
      const response = await request.json();
      if (response) {
        if (souvenir.checked) {
          localStorage.setItem("token", response.token);
        } else {
          sessionStorage.setItem("token", response.token);
        }
        sessionStorage.setItem("user", JSON.stringify(response.user));
        window.location.reload();
      }
    }

  });
}

const connectedUserPage = async () => {
  const body = document.querySelector("main");
  body.innerHTML = `
    <div class="object-container flex justify-center">
  <div class="flex rounded-3xl w-1/2 p-10 justify-around">
    <form class="flex flex-col w-1/2 justify-around align-middle">
      <label class="m-1 pl-1" for="type">Type</label>
      <select class="m-1 p-3 pr-5 rounded-full border-roze border-2 bg-white" id="type" required name="type"></select>
     
      <label class="m-1 pl-1" for="description">Description</label>
      <input class="m-1 p-3 rounded-full border-roze border-2" type="text" id="description" required name="description" value="">
      
      <label class="m-1 pl-1" for="date">Liste des samedis</label>
      <select class="m-1 p-3 pr-5 rounded-full border-roze border-2 bg-white" id="date" required name="date"></select>
          
      <label for="file_input" class="m-1 pr-3 pl-1">Photo de l'objet</label>
      <input type="file" class="m-2 p-2 rounded-full bg-white border-2 border-roze" id="file_input">
      
      <input class="bg-red-300 transition-colors duration-500 hover:bg-red-200 p-2 m-1 mt-10 rounded-full" id="submit" type="button" value="Proposer l'objet">
      
      <div id="alert"></div>
    </form>
  </div>
</div>

  `;

  const requestTypes = await fetch("/api/object/allTypes", option)
  .then(rt => rt.json());

  const selectType = document.querySelector("#type");

  requestTypes.forEach(objectType => {
    const typeToSelect = document.createElement("option");
    const {id, typeName} = objectType;
    typeToSelect.innerHTML = typeName;
    typeToSelect.setAttribute("value", id);
    typeToSelect.setAttribute("id", id);
    selectType.appendChild(typeToSelect);
  });

  /// _----------------------------//

  const requestDates = await fetch("/api/availabilities/getAll")
  .then(rt => rt.json());

  const selectDate = document.querySelector("#date");

  const formatter = new Intl.DateTimeFormat("fr-FR", {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  });

  requestDates.forEach((dateObj) => {
    const dateToSelect = document.createElement("option");
    const { id, date, timeSlot } = dateObj;

    const formattedDate = new Date(date[0], date[1] - 1, date[2]);

    if (formattedDate > Date.now()) {
      const formattedDateString = formatter.format(formattedDate);

      let timeSlotString = "";
      if (timeSlot === "AM") {
        timeSlotString = "11h - 13h";
      } else if (timeSlot === "PM") {
        timeSlotString = "14h - 16h";
      }

      dateToSelect.innerHTML = `${formattedDateString} (${timeSlotString})`; // Affichage de la date formatée et du timeSlot
      dateToSelect.setAttribute(
          "value",
          "".concat(formattedDate).concat("_").concat(timeSlot)
      );
      dateToSelect.setAttribute("id", id);
      dateToSelect.setAttribute("data-time-slot", timeSlot);
      selectDate.appendChild(dateToSelect);
    }
  });


  // _-------------------------------------//

  const submit = document.querySelector("#submit");
  submit.addEventListener("click", async () => {
    let fileName = "";

    if (document.getElementById("file_input").files[0] != null) {
      const sendPictureOptions = {
        method: "POST",
        body: document.getElementById("file_input").files[0],
      }

      fileName = await fetch('api/picture/upload', sendPictureOptions).then(
          r => r.json());
    }
    const description = document.querySelector("#description").value;

    const dateObjectString = document.querySelector("#date").value;
    const [dateStr, timeSlot] = dateObjectString.split("_");
    const dateObject = new Date(dateStr);
    const year = dateObject.getFullYear();
    const month = dateObject.getMonth() + 1;
    const day = dateObject.getDate();

    const typeObject = {
      id: parseInt(document.querySelector("#type").value, 10),
    };

    const user = sessionStorage.getItem("user");
    const parseUser = user ? JSON.parse(user) : {};

    const submitOptions = {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify({
        type: typeObject,
        description,
        dateOfReceipt: {
          date: [year, month, day],
          timeSlot,
        },
        phoneNumber: user ? parseUser.phoneNumber : sessionStorage.getItem("phone"),
        offeringMember: {
          id: user ? parseUser.id : -1,
          phoneNumber: user ? parseUser.phoneNumber : 0,
        },
      }),
    };


    const response = await fetch('api/object/offer', submitOptions);
    const id = await response.json();

    const pictureUpdateOptions = {
      method: "POST",
      headers: {
        "content-type": "application/json"
      },
      body: JSON.stringify({
        "fileName": fileName.fileName,
        "id": id,
      }),
    };
    fetch('api/object/setImage', pictureUpdateOptions)
    .then(rep => {
      if (!rep.ok) {
        throw new Error('Network response was not ok');
      }
      Navigate("/");
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.error('Il y a eu un problème avec la requête fetch:', error);
    });

  });

}

const makeOffer = async () => {
  const token = sessionStorage.getItem("token") ? sessionStorage.getItem(
      "token") : localStorage.getItem("token");
  if (token) {
    await connectedUserPage();
  } else {
    await quidam();
  }
}

export default makeOffer;