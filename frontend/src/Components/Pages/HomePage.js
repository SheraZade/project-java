import Carousel from "./carousel";
import Navigate from "../Router/Navigate";

const main = document.querySelector('main');
const user = JSON.parse(sessionStorage.getItem("user"));

const createBody = () => {

  let body = document.querySelector("#searchBody");
  if (!body) {
    body = document.createElement('section');
    body.setAttribute("id", "searchBody");
  }
  body.innerHTML = `<div class="users-container mt-16"> 
                      <div class="flex flex-col justify-center w-4/5 mx-auto">
                        <table id="searchTable" class="text-left"> 
                          <thead>
                            <tr id="tableHeader" class="border-b">
                              <th class="pr-10">Photo</th> 
                              <th>Description</th>
                              <th>Prix</th>
                              <th>Type</th>
                              <th>État de l'objet</th> 
                            </tr> 
                          </thead> 
                          <tbody id="object-list">
                          </tbody> 
                        </table>
                        <div id="searchError">
                        </div>
                      </div>
                    </div>
                    `;
  main.appendChild(body);

}

const createRow = (objet) => {
  const body = document.querySelector('#object-list');
  const row = document.createElement('tr');
  row.setAttribute("class", "w-4/5 border-roze border-t  transition-colors duration-500 hover:bg-red-50");
  row.innerHTML = `
           <td class="py-4 pr-10"><img class="max-h-40 min-h-40 p-1" src="${"/api/picture/getPicture/".concat(
      objet.picture.url)}" alt="image de l'objet"</td>
           <td class="text-left py-4 pr-52">${objet.description}</td> 
           <td class="test-left py-4 pr-52">${objet.price}€</td>
           <td class="text-left py-4 pr-52">${objet.type.typeName}</td>
           <td class="text-left py-4 pr-52">${objet.state}</td> 
  `
  row.addEventListener('click', () => {
    Navigate(`/object?id=${objet.id}`);
  });
  body.appendChild(row);
}

window.navToNotif = async function navToObjectPage(id, objID){
  console.log(id)
  console.log(objID)
  console.log("f")
  const optionRead = {
    method: "PATCH",
    headers: {
      "Content-type": "application/json",
      "Authorization": sessionStorage.getItem("token"),
    },
    body: JSON.stringify({
      // object-shorthand
      "idNotification": id,
    })
  }
  fetch("api/notificationMember/MarkAsRead", optionRead)
  Navigate(`/object?id=${objID}`)
}
const buildNotificationList = async () => {


  const bodyNotif = document.createElement("div")

  const optionNotif = {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      "Authorization": sessionStorage.getItem("token"),
    },
  };

  const listNotif = await
      fetch("api/users/getAllNotifications/".concat(JSON.parse(sessionStorage.getItem("user")).id), optionNotif)
          .then(r => r.json());

  if (listNotif != null && listNotif.length !== 0){
    bodyNotif.setAttribute("class","h-64 w-4/6 mx-auto overflow-auto border space-y-3 rounded-3xl mb-3 grid place-items-center " );
    listNotif.forEach(notif => {
      console.log(notif)
      const notifCard = document.createElement("button")
      notifCard.setAttribute("class", "w-full h-32 p-5 border-t hover:bg-red-50")
      notifCard.setAttribute("onclick", `navToNotif(${notif.idNotificationMember}, ${notif.idObject})`)

      if (notif.isRead === false) notifCard.setAttribute("class", "w-full h-32 p-5 border-t  bg-red-100")

      const date = new Date(notif.dateNotifications);
      const options = {year: 'numeric', month: 'long', day: 'numeric'};
      const formattedDate = date.toLocaleDateString('fr-FR', options);

      notifCard.innerHTML = `
      
      <div id="notif${notif.id}">
        <p class="text-2xl"> ${notif.titleNotification}</p>
        <p class="text-lg mr-4"> ${notif.textNotification}</p>
        <p class="mr-4" >${formattedDate}</p>
      </div>
      `
      bodyNotif.appendChild(notifCard)
    })
    main.appendChild(bodyNotif)
  }

}

const HomePage = async () => {

  main.innerHTML=``;
  if (user) await buildNotificationList();
  Carousel();

  const optionTypes = {
    method: "GET",
    headers: {
      "Content-type": "application/json",
    },
  };

  // ------------------------------etat------------------------------------//
  const searchBar = document.createElement("div");
  searchBar.setAttribute("class", "flex flex-col justify-center m-2");

// créer le formulaire contenant les deux boutons
  searchBar.innerHTML = `
  <form class="flex flex-row justify-between w-1/3 mx-auto">
    <div class="flex flex-col justify-start w-1/3 mr-1">
      <select class="bg-white rounded-full border-2 border-roze p-2 w-full" id="state" name="stateSearch">
              <option id="all" value="all">Filtrer par état</option>
        <option value="Mis en vente">Mis en vente</option>
        <option value="En magasin">En magasin</option>
        <option value="Vendu">Vendu</option>
      </select>
    </div>
    <div class="flex flex-col justify-start w-1/3 ml-1">
      <select class="bg-white rounded-full border-2 border-roze p-2 w-full" id="type" name="typeSearch">
              <option id="all" value="all">Filtrer par type</option>
      </select>
    </div>
    <input type="button" id="submit" class="bg-red-300 transition-colors duration-500 hover:bg-red-100 p-2 m-1 my-auto rounded-full" value="Rechercher">
  </form>
`;
  main.appendChild(searchBar);
  const listTypes = await fetch('api/object/allTypes', optionTypes).then(r => r.json());
  const selecte = document.querySelector("#type");
  listTypes.forEach(objectType => {
    const typeSelect = document.createElement("option");
    const {id, typeName} = objectType;
    typeSelect.innerHTML = typeName;
    typeSelect.setAttribute("value", typeName);
    typeSelect.setAttribute("id", id);
    selecte.appendChild(typeSelect);
  });
    // ________________________------------------------------------------------------_//

  // ------------afficher la liste des types------------------------------------------//


  // ________________________------------------------------------------------------_//


  const submit = document.querySelector("#submit");

  submit.addEventListener("click", async () => {
    const typeToSearch = document.querySelector("#type").value;
    const stateToSearch = document.querySelector("#state").value;
    let url = "";
    let body = {};

    if (typeToSearch !== "all") {
      url = "api/object/searchByType";
      body = { "typeName": typeToSearch };
    } else if (stateToSearch) {
      url = "api/object/searchByState";
      body = { "state": stateToSearch };
    }

    if (url) {
      const optionsSearch = {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify(body),
      };
      const objetsTrouves = await fetch(url, optionsSearch).then(r => r.json());
      createBody();
      if (!objetsTrouves || objetsTrouves.length === 0) {
        const searchErrorDiv = document.querySelector("#searchError");
        searchErrorDiv.innerHTML = `<div class="flex justify-center w-full m-2 p-2"><p>Aucun objet trouvé.</p></div>`;
      } else {
        objetsTrouves.forEach(object => {
          createRow(object);
        });
      }
    }
  });

}

export default HomePage;
