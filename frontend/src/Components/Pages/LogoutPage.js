import Navigate from '../Router/Navigate';

const LogoutPage = () => {
  localStorage.clear();
  sessionStorage.clear();
  Navigate('/login');
  window.location.reload();
};

export default LogoutPage;