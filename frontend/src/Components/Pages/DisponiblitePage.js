function makeAlert(message) {
  const div = document.createElement("div");
  div.setAttribute("class", "w-full m-2 p-2 bg-red-100 rounded-2xl");
  div.innerHTML = `<p class="w-full h-full text-center align-text-middle">${message}</p>`;

  return div;
}

const DisponibilitePage = () => {

  const dateNow = new Date();
  dateNow.setDate(dateNow.getDate() + (6 - dateNow.getDay()));
  const [usableDate] = dateNow.toISOString().split("T");
  const body = document.querySelector('main');

  body.innerHTML = `

    <div class="grid place-items-center pt-15">
      <div id="alert" class="w-1/3">
      </div>
      <div class="w-1/3" id="formDiv">
        <h1 class="pb-10 text-4xl font-bold text-center">Ajouter une disponibilité</h1>
      </div>
    </div>
  `;

  const form = document.createElement('form');
  form.setAttribute("class", "m-2 p-2 flex flex-col justify-center");
  const alertDiv = document.getElementById('alert');
  const formDiv = document.querySelector("#formDiv");

  const dateDiv = document.createElement("div");
  dateDiv.setAttribute("class", "w-full flex flex-row justify-between");

  const datePicker = document.createElement("input");
  datePicker.setAttribute("id", "date");
  datePicker.setAttribute("type", "date");
  datePicker.setAttribute("class",
      "cursor-pointer bg-white w-full m-2 p-2 rounded-full border-2 border-roze "
      + " transition-colors duration-500 hover:bg-red-100");
  datePicker.setAttribute("min", usableDate);
  datePicker.setAttribute("step", "7");
  datePicker.valueAsDate = dateNow;


  dateDiv.appendChild(datePicker);

  const submitButton = document.createElement("input");
  submitButton.setAttribute("type", "button");
  submitButton.setAttribute("value", "ajouter")
  submitButton.setAttribute("class",
      "m-2 p-2 bg-red-300 cursor-pointer rounded-full"
      + " transition-colors duration-500 hover:bg-red-100");

  submitButton.addEventListener('click', async () => {
    alertDiv.innerHTML = ``;
    let valid = true;
    const date = datePicker.valueAsDate;

    if (date < new Date()) {
      alertDiv.appendChild(
          makeAlert("La date ne peut pas être dans le passé."));
      valid = false;
    }
    if (date.getDay() !== 6) {
      alertDiv.appendChild(makeAlert("La date doit être un samedi."));
      valid = false;
    }

    if (valid) {
      const options = {
        method: "POST",
        headers: {
          "Authorization": sessionStorage.getItem("token")
              ? sessionStorage.getItem("token") : localStorage.getItem("token"),
          "content-type": "application/json"
        },
        body: JSON.stringify({
          "date": date,
          "timeSlot": "AM"
        })
      };
      const request = await fetch("api/users/putAvailability", options);
      if (!request.ok) {
        alertDiv.appendChild(
            makeAlert("Une erreur est survenue pendant la requête."));
      } else {
        const jsonned = await request.json();
        if (!jsonned) {
          alertDiv.appendChild(
              makeAlert("Une erreur est survenue pendant la requête."));
        } else {
          window.location.reload();
        }
      }
    }

  });

  form.appendChild(dateDiv);
  form.appendChild(submitButton);
  formDiv.appendChild(form);

}

export default DisponibilitePage;
