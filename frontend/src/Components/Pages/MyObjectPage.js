import Navigate from "../Router/Navigate";

const MyObjects = () => {

  const user = JSON.parse(sessionStorage.getItem("user"));
  const body = document.querySelector('main');

  body.innerHTML = `
 <div class="flex w-full justify-center">
  <div class="w-4/5 h-[45rem]">
    <div class="flex justify-center" style="padding-bottom: 50px;">
      <h1 class="text-3xl font-semibold">Mes objets proposés</h1>
    </div>
    <div id="alert"></div>
    <table id="table" class="w-4/5 text-left"></table>
  </div>
</div>
`;

  const alertDiv = document.getElementById("alert");
  const table = document.getElementById("table");

  if (user.role === "R" || user.role === "A" || user.role === 'M') {

    const options = {
      method: 'GET',
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        'Content-type': 'application/json',
      },
    };

    const userId = user.id; // Récupérer l'identifiant de l'utilisateur
    fetch(`/api/object/MyObjects/${userId}`, options)
    .then(response => response.json())
    .then(objects => {
      if (objects.length === 0) {
        alertDiv.innerHTML = `<div class="text-red-600" >Vous n'avez aucun objet lié à ce compte </div>`
      } else {

        table.innerHTML = `
        <thead>
                        <tr class="border-b">
                          <th>Photo</th> 

                          <th>Description</th>

                          <th>Prix</th>
                          <th>Type</th>
                          <th>État de l'objet</th> 
                          
                        </tr> 
                      </thead> 
                      <tbody id="object-list"></tbody> 
        `
        const userList = document.getElementById('object-list');

        objects.forEach(objet => {

          const row = document.createElement('tr');
          row.setAttribute("class", "border-roze border-t cursor-pointer"
              + " transition-colors duration-500 hover:bg-red-50");
          row.innerHTML = `
  <td class="py-4 pr-10"><img class="max-h-40 min-h-40 p-1" src="${"/api/picture/getPicture/".concat(
              objet.picture.url)}" alt="image de l'objet"></td>
  <td class="text-left py-4 pr-52 text-xl">${objet.description}</td> 
  <td class="text-left py-4 pr-52 text-xl">${objet.price}€</td>
  <td class="text-left py-4 pr-52 text-xl">${objet.type.typeName}</td>
  <td class="text-left py-4 pr-52 pl-4 pr-4 text-xl">${objet.state}</td> 
`;

          row.addEventListener('click', () => {
            Navigate(`/object?id=${objet.id}`);
          });
          userList.appendChild(row);

        });
      }
    })
  } else {
    Navigate('/')
  }
}

window.navToObj = async function navToObjPage(user) {
  const userId = user.id;
  Navigate(`/object?id=${userId}`);
};

export default MyObjects;