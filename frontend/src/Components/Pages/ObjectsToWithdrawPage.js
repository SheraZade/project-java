import {clearPage} from '../../utils/render';

const ObjectsToWithdrawPage = () => {
  clearPage();

  const body = document.querySelector('main');

  body.innerHTML = `<div class="users-container mt-16"> 
                      <div class="flex flex-col justify-center w-4/5 mx-auto">
<h1 class="text-2xl font-bold mb-6 text-center" style="display: inline-block;">
  Les objets retirés le
  <span class="text-2xl font-bold mb-6" style="display: inline-block;">
   ${new Date().toLocaleDateString('fr-FR',
      {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'})}
  </span>
</h1>
                        <table id="searchTable" class="text-left"> 
                          <thead>
                            <tr id="tableHeader" class="border-b">
                              <th class="pr-10">Photo</th> 
                              <th>Description</th>
                              <th>Prix</th>
                              <th>Type</th>
                              <th>État de l'objet</th> 
                            </tr> 
                          </thead> 
                          <tbody id="objects-list">
                          </tbody> 
                        </table>

                        <div id="empty-message" class="text-center mt-6 text-lg font-medium text-gray-500">Aucun objet à retirer.</div>
                      </div>
                    </div> `;

  const objectList = document.getElementById('objects-list');
  const emptyMessage = document.getElementById('empty-message');
  const options = {
    method: 'GET',
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      'Content-type': 'application/json',
    },
  };
  fetch('api/object/ObjectsToWithdraw', options)
  .then(response => response.json())
  .then(objects => {
    if (objects.length === 0) {
      emptyMessage.style.display = 'block';
    } else {
      objects.forEach(object => {
        const row = document.createElement('tr');
        row.innerHTML = `
            <td><img src="${object.image}" alt="${object.description}" height="100" width="100"></td>
            <td>${object.description}</td>
            <td>${object.price}</td>
            <td>${object.type.typeName}</td>
            <td>${object.state}</td>`
        objectList.appendChild(row);
      });
    }
  })
  .catch(error => error(error));

};

export default ObjectsToWithdrawPage;
