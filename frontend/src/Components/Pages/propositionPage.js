import Swal from "sweetalert2";
import Navigate from "../Router/Navigate";

const PropositionPage = () => {

  const body = document.querySelector("main");

  body.innerHTML = `
    <div class="object-container flex justify-center">
        <div id="object-list" class="grid grid-cols-4 pt-8 pb-28 gap-16"></div>
    </div>
  `;

  const objectList = document.getElementById("object-list");
  const option = {
    method: "GET",
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      "Content-type": "application/json",
    },
  };
  const user = sessionStorage.getItem("user");
  if (user.role !== "R") {
    Navigate('/');
  }

  fetch('/api/object/offeredObjects', option)
  .then(response => response.json())
  .then(objects => {
    objects.forEach((object) => {
      const objectElement = document.createElement("div");
      objectElement.innerHTML = `
          <div class="object-card bg-white rounded-lg shadow-lg w-72 h-96 ">
            <img class="h-1/2 w-full rounded-t-lg object-cover" src="${"/api/picture/getPicture/".concat(
          object.picture.url)}" alt="image de l'object">
            <h3 class="text-xl font-bold pt-2 pl-2">${object.type.typeName}</h3>
            <div class="px-3 pt-1 whitespace-normal overflow-auto overflow-ellipsis h-28">${object.description}</div>
            <div class="flex justify-between mx-10 pt-1">
              <button class="btn px-3 py-1 rounded-2xl bg-green-400" onclick="acceptObject('${object.id}')">Accepter</button>
              <button class="btn px-3 py-1 rounded-2xl bg-red-400" onclick="refuseObject('${object.id}')">Refuser</button>
            </div>
          </div>
        `;
      objectList.appendChild(objectElement);
    });
  })


  window.acceptObject = async function acceptProp(id) {
    const options = {
      method: "POST",
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        "Content-Type": "application/json",
      },
    };
    const apiUrl = ("/api/object/".concat(id)).concat("/validateOffer")
    const request = await fetch(apiUrl, options);

    if (request.ok) {
      Navigate('/object?id='.concat(id));
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: "Erreur lors de la validation de l'objet",
      });

    }
  }

  window.refuseObject = async function refuseProp(id) {
    const {value: reason} = await Swal.fire({
      title: "Raison du refus de l'objet :",
      input: "text",
      inputPlaceholder: "Saisissez une raison",
      inputAttributes: {
        maxlength: 255,
        required: true,
      },
      showCancelButton: true,
      confirmButtonText: "Refuser",
      cancelButtonText: "Annuler",
      allowOutsideClick: false,
      allowEscapeKey: false,
      allowEnterKey: true,
      customClass: {
        input: "form-control",
      },
    });
    if (reason) {
      const options = {
        method: "POST",
        headers: {
          "Authorization": sessionStorage.getItem("token")
              ? sessionStorage.getItem("token") : localStorage.getItem("token"),
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id,
          reasonForRefusal: reason,
        }),
      };
      const request = await fetch(`/api/object/refuseOffer`, options);

      if (request.ok) {
        window.location.reload();
      } else {
        await Swal.fire({
          icon: 'error',
          title: 'Erreur',
          text: "Erreur lors du refus de l'objet",
        });

      }
    }
  }

};
export default PropositionPage;
