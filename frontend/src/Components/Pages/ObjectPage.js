import Navigate from "../Router/Navigate";
import editPicto from "../../img/editPicto.svg"

const ObjectPage = async () => {

  const main = document.querySelector("main");
  const urlParam = new URL(window.location.toLocaleString()).searchParams;
  const objectId = urlParam.get("id");
  const user = JSON.parse(sessionStorage.getItem("user"));

  if (!user) {
    Navigate('/');
  }
  if (!Number.isInteger(objectId) && objectId < 0) {
    Navigate('/')
  }

  window.navEdit = async function navToEditPage() {
    Navigate('/updateObject?id='.concat(objectId));
  };

  const option = {
    method: "GET",
    headers: {
      "Authorization": sessionStorage.getItem("token"),
      "content-type": "application/json",
    }
  }
  const apiUrl = ("/api/object/".concat(objectId)).concat("/getObject");
  const request = await fetch(apiUrl, option);
  if (request.ok) {
    const response = await request.json();
    let owner;
    if (!response.offeringMember) {
      owner = "Anonyme";
    } else {
      owner = (response.offeringMember.firstname).concat(" ").concat(
          response.offeringMember.lastname)

    }
    const date = new Date(response.dateOfReceipt.date);
    const options = {year: 'numeric', month: 'long', day: 'numeric'};

    const formattedDate = date.toLocaleDateString('fr-FR', options);

    main.innerHTML = `
      <div class="flex mt-10 justify-center w-full h-[40rem]">
        <div class="flex bg-white border rounded-2xl h-full w-3/5 ">
          <img alt="Objet presenter" class="rounded-l-2xl w-1/2 h-full object-contain" src="${"/api/picture/getPicture/".concat(
        response.picture.url)}">          
          <div class="w-1/2 flex h-full flex-col">
              <div class="flex justify-between items-center">
                <p class="text-4xl font-semibold p-5">
                   ${response.type.typeName}
                </p>
                
                <div id="editBtn"></div>
              
              </div>
              <p class="flex-1 text-xl p-5 ">
                 ${(response.description != null) ? response.description : '' }
              </p>
              <p class="flex-1 text-xl p-5 ">
                 ${(response.reasonForRefusal != null) ? "Raison de refus : ".concat(response.reasonForRefusal) : '' }
              </p>
              <div class="p-5">
                <div id="pirceChip"></div>
                
                <p class="text-2xl font ">
                   ${response.state}
                </p>

                <p>
                   Publié le ${formattedDate}
                </p>
<p>${(user.role === 'R' || user.role === 'A') && response.offeringMember != null ? `<a href="/pr?id=${response.offeringMember.id}">${owner}</a>` : `Publié par ${owner}`}</p>
            </div>
          </div>
        </div>
      </div>
  `

    if (user.role === 'R' || user.role === 'A') {
      const editBtn = document.getElementById("editBtn");
      editBtn.innerHTML = `
            <button type="button" class="h-fit w-fit flex flex-box" onclick="navEdit()">
            <img src="${editPicto}" class="h-7 mr-3" alt="logo">
          </button>
          `
    }

    if (response.price) {
      const priceChip = document.getElementById("pirceChip");
      priceChip.innerHTML = `
                <p class="p-3 w-fit text-3xl  font-bold bg-roze rounded-full ">
                   ${response.price}€
                </p>
            `
    }

  }

}

export default ObjectPage;