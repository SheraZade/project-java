import Navigate from "../Router/Navigate";

let carPosition ;
let carLength ;

const Carousel = () => {
    const body = document.querySelector('main');
  // const user = JSON.parse(sessionStorage.getItem("user"));

  const carou = `
<div class="w-full flex justify-center pb-10">
<div id="controls-carousel" class="relative w-8/12 ">
    <!-- Carousel wrapper -->
    <div class="relative rounded-2xl border border-gray-400 " id="carouselWrapper">
        
    </div>
    <!-- Slider controls -->
    <button type="button" onclick="onPrevBtn()" class="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" id="carousel-prev">
        <span class="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 group-hover:bg-white/50">
            <svg aria-hidden="true" class="w-6 h-6 text-white dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>
        </span>
    </button>
    <button type="button" onclick="onNextBtn()" class="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" id="carousel-next">
        <span class="inline-flex items-center justify-center w-10 h-10 rounded-full bg-white/30 group-hover:bg-white/50">
            <svg aria-hidden="true" class="w-6 h-6 text-white dark:text-gray-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
        </span>
    </button>
</div>
  </div>
  `

    body.innerHTML = body.innerHTML.concat(carou)
    const carWrapper = document.getElementById("carouselWrapper");



    const option = {
        method: "GET",
        headers: {
            "Content-type": "application/json",
        },
    };
    fetch("api/object/getCarouselObjects", option)
        .then(response => response.json())
        .then( objects => {
            carLength = objects.length;
            carPosition = 0;
            objects.forEach( object =>{
                    const divCar = document.createElement('Div');
                    divCar.classList.toggle('hidden');
                    divCar.id = carPosition;
                    divCar.innerHTML = `
<div class="relative">
    ${object.state === 'Vendu' ? '<div class="absolute top-0 right-0 bg-red-500 px-14 py-2 rounded-lg"><p class="text-white text-lg font-bold">Vendu</p></div>' : ''}
       <img src="${"/api/picture/getPicture/".concat(object.picture.url)}" 
     class="bg-gray-300 object-contain rounded-t-2xl w-full h-[32rem]" 
     alt="..."
    >
    <div class="flex m-3 justify-between items-center">
        <div>
            <p class="text-3xl"> ${object.type.typeName} </p>
            <p class="text-lg"> ${object.description} </p>
        </div>   
        <p class="text-lg">${object.price}€</p>  
    </div>
</div>


              `;
                    divCar.addEventListener('click', () => {Navigate(`/object?id=${object.id}`)});
          carWrapper.appendChild(divCar);
          carPosition += 1;
        }
    )
    document.getElementById("0").classList.toggle("hidden")
    carPosition = 0;
  })
  window.onPrevBtn = function onPrev() {
    const from = document.getElementById(carPosition);
    from.classList.toggle("hidden");
    if (carPosition === 0) {
      carPosition = carLength - 1;
    } else {
      carPosition -= 1;
    }
    const too = document.getElementById(carPosition);
    too.classList.toggle("hidden");
  }

  window.onNextBtn = function onNext() {
    document.getElementById(carPosition).classList.toggle("hidden")
    if (carPosition === carLength - 1) {
      carPosition = 0;
    } else {
      carPosition += 1;
    }
    document.getElementById(carPosition).classList.toggle("hidden")
  }
 // if(user.role==='R' ||user.role==='A')
   // window.navigateTo = function(id) {
   //   Navigate('/object?id='.concat(id));
 //   };
}

export default Carousel;