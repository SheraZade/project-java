import Swal from "sweetalert2";
import navigate from "../Router/Navigate";

const body = document.querySelector("main");
const user = JSON.parse(sessionStorage.getItem("user"));
const urlParam = new URL(window.location.toLocaleString()).searchParams;
const objectId = urlParam.get("id");

const UpdateObject = () => {

  if (user.role === "R" || user.role === "A") {

    if (!Number.isInteger(objectId) && objectId < 0) {
      navigate('/')
    }
    const option = {
      method: "GET",
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        "content-type": "application/json",
      }
    }
    const apiUrl = ("/api/object/".concat(objectId)).concat("/getObject");

    fetch(apiUrl, option).then(response => response.json()).then(
        objet => {
          body.innerHTML = `
  <div class="flex w-full justify-center">
    <div class="w-4/5 h-[45rem]">
      <h2 class="flex justify-center text-3xl font-semibold">Mettre à jour l'objet</h2>
      <div id="error" class="text-red-600 text-xl"></div>
      <form class="flex flex-wrap bg-white w-full rounded-2xl my-10">
        <div class="w-1/2 rounded-l-3xl bg-gray-300 flex justify-center">
          <img alt="image obj" class="w-full max-h-[40rem] object-contain" src="${"/api/picture/getPicture/".concat(
              objet.picture.url)}" id="picture">
        </div>
        <div class="w-1/2 mt-3 space-y-5">
          <div class="flex items-center">
            <label class="m-2 text-xl cursor-pointer rounded-l-lg" for="file_input">Photo :</label>
            <input class="my-2 text-lg border border-gray-400 border-1 rounded-lg cursor-pointer" id="file_input" type="file" accept="image/png, image/jpg" >
          </div>
          <div class="flex items-center">
            <label class="m-2 text-xl rounded-l-lg">Type :</label>
            <select id="selectType" class="w-1/3 rounded-lg text-xl border border-gray-400 bg-gray-100">
            </select>
          </div>
          <div>
            <label class="m-2 w-full text-xl rounded-l-lg">Description :</label>
            <textarea id="description" maxlength="120" rows="3" class="resize-none justify-self-center p-2 mx-5 my-2 w-11/12 text-lg bg-gray-100 rounded-lg border border-gray-400 placeholder-gray-400">${objet.description}</textarea>
          </div>
          <div class="flex items-center">
            <label class="m-2 text-xl rounded-l-lg" >État de l'objet :</label>
            <p id="textState" class="m-2 text-xl rounded-l-lg">${objet.state}</p>
            <button type="button" class="px-4 py-2 text-white bg-blue-500 rounded-lg hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-400 focus:ring-opacity-75" onclick="updateState('${objectId}', '${objet.state}')">
              Passer à l'état suivant
            </button>
          </div>
          <button id="stateBtn" type="button" onclick="updateObj('${objectId}')" class="px-4 py-2 text-white bg-green-500 rounded-lg hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-green-400 focus:ring-opacity-75">
            Mettre à jour l'objet
          </button>
          <div id="alertUpd"></div>
        </div>       
      </form>
    </div>
  </div>
`;

          const typeList = document.getElementById("selectType");

          const option2 = {
            method: "GET",
            headers: {
              "Authorization": sessionStorage.getItem("token")
                  ? sessionStorage.getItem("token") : localStorage.getItem(
                      "token"),
              "Content-type": "application/json",
            },
          };
          fetch('/api/object/allTypes', option2)
          .then(response2 => response2.json())
          .then(types => {
            types.forEach((type) => {
                  const typeElement = document.createElement("option");
                  typeElement.innerHTML = `
                  ${type.typeName} 
        `;
                  typeElement.setAttribute('value', type.typeName);
                  if (type.typeName === objet.type.typeName) {
                    typeElement.setAttribute('selected',
                        "");
                  }
                  typeList.appendChild(typeElement);
                }
            )
          })
        })

  }

}
window.chooseOption = async function chooseOption(action) {
  const alertDivLocal = document.getElementById("alertUpd");

  try {
    const response = await action();

    if (!response.ok) {
      alertDivLocal.innerHTML = `
        <div class="font-bold text-red-600 p-2" role="alert">
          L'état que vous avez choisi ne peut pas être mis 
        </div>
      `;
    }
  } catch (error) {
    alertDivLocal.innerHTML = `
      <div class="font-bold text-red-600 p-2" role="alert">
        Une erreur est survenue : ${error.message}
      </div>
    `;
  }
}

window.updateState = async function updState(id, currentState) {
  const alertDiv = document.getElementById("alertUpd");

  const option = {
    method: "POST",
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      "Content-type": "application/json",
    },
  };

  const optionsByState = {
    "Propos\u00E9": [
      {
        label: "Accepter l'objet",
        action: () => fetch(('api/object/'.concat(id)).concat('/validateOffer'),
            option),
      },
      {
        label: "Refuser l'objet",
        action: () => refuseProp(id),
      },
    ],
    "En atelier": [
      {
        label: "Envoyer en magasin",
        action: () => fetch(
            ('api/object/'.concat(id)).concat('/depositAtStore'), option),
      },
    ],
    "Accept\u00E9": [
      {
        label: "Envoyer en magasin",
        action: () => fetch(
            ('api/object/'.concat(id)).concat('/depositAtStore'), option),
      },
      {
        label: "Envoyer en atelier",
        action: () => fetch(
            ('api/object/'.concat(id)).concat('/depositAtWorkshop'), option),
      },
    ],
    "En magasin": [
      {
        label: "Mettre en vente",
        action: () => putToSell(id, "/updatePrice"),
      },
      {
        label: "Marquer comme vendu",
        action: () => putToSell(id, "/moveToSold"),
        ROnly: true,
      },
    ],
    "Mis en vente": [
      {
        label: "Marquer comme vendu",
        action: () => fetch(('api/object/'.concat(id)).concat('/sold'), option),
      },
      {
        label: "Retirer l'objet de la vente",
        action: () => fetch(('api/object/'.concat(id)).concat('/withdrawn'),
            option),
      },
    ],
  };

  const stateOptions = optionsByState[currentState];

  if (!stateOptions) {
    alertDiv.innerHTML = `
      <div class="font-bold text-red-600 p-2" role="alert">
        L'état ne peut plus être modifié
      </div>
    `;
    return;
  }
  alertDiv.innerHTML = `
    <div class="font-bold p-2">
      Que voulez-vous faire avec cet objet ?
    </div>
  `;

  const dialog = document.createElement("div");
  dialog.classList.add("flex", "justify-center", "space-x-4");

  stateOptions.forEach(({label, action, ROnly}) => {

    if (ROnly === true && user.role !== "R") {
      return;
    }

    const button = document.createElement("button");
    button.type = "button";
    button.classList.add("bg-blue-500", "hover:bg-blue-700", "text-white",
        "font-bold", "py-2", "px-4", "rounded");
    button.textContent = label;
    button.addEventListener("click", async () => {
      const response = await action();
      if (response.ok) {
        navigate("/listeObjet");
      } else {
        alertDiv.innerHTML = `
          <div class="font-bold text-red-600 p-2" role="alert">
            L'état que vous avez choisi ne peut pas être mis 
          </div>
        `;
      }
    });
    dialog.appendChild(button);
  });

  alertDiv.appendChild(dialog);
};

function putToSell(id, action) {
  Swal.fire({
    title: 'Indiquer le prix voulu:',
    input: 'number',
    inputAttributes: {
      step: 'any',
      min: '0',
    },
    showCancelButton: true,
    confirmButtonText: 'Valider',
    cancelButtonText: 'Annuler',
    showLoaderOnConfirm: true,
    preConfirm: (price) => {
      const option = {
        method: "POST",
        headers: {
          "Authorization": sessionStorage.getItem("token")
              ? sessionStorage.getItem("token") : localStorage.getItem("token"),
          "Content-type": "application/json",
        },
        body: JSON.stringify({
          'price': parseFloat(price),
        }),
      };

      return fetch(('api/object/'.concat(id)).concat(action), option)
      .then((response) => {
        if (!response.ok) {
          throw new Error(response.statusText);
        }
      })
      .catch((error) => {
        Swal.showValidationMessage(
            `Erreur : ${error}`
        );
      });
    },
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    if (result.isConfirmed) {
      navigate('/listeObjet');
    }
  });
}

window.updateObj = async function updObj(id) {
  let fileName = "";

  if (document.getElementById("file_input").files[0] != null) {
    const sendPictureOptions = {
      method: "POST",
      body: document.getElementById("file_input").files[0],
    }

    fileName = await fetch('api/picture/upload', sendPictureOptions)
    .then(r => r.json());
  }

  const alertDiv = document.getElementById("alertUpd");
  const type = document.getElementById("selectType").value;
  const updOption = {
    method: "POST",
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      "Content-type": "application/json"
    },
    body: JSON.stringify({
          "description": document.getElementById("description").value,
          "type": {
            "typeName": type,
          },
          "picture": {
            "url": fileName.fileName,
          }
        }
    )
  }

  fetch('/api/object/'.concat(id).concat("/Update"), updOption)
  .then(responseUpd => responseUpd.json())
  .then(done => {
    if (done) {
      window.location.reload();
    } else {
      alertDiv.innerHTML = `
              <div class="font-bold text-red-600 p-2" role="alert">
                Mise à jour de l'objet non réussie
              </div>
              `
    }
  })

}

async function refuseProp(id) {
  const {value: reason} = await Swal.fire({
    title: "Raison du refus de l'objet :",
    input: "text",
    inputPlaceholder: "Saisissez une raison",
    inputAttributes: {
      maxlength: 255,
      required: true,
    },
    showCancelButton: true,
    confirmButtonText: "Refuser",
    cancelButtonText: "Annuler",
    allowOutsideClick: false,
    allowEscapeKey: false,
    allowEnterKey: true,
    customClass: {
      input: "form-control",
    },
  });
  if (reason) {
    const options = {
      method: "POST",
      headers: {
        "Authorization": sessionStorage.getItem("token")
            ? sessionStorage.getItem("token") : localStorage.getItem("token"),
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id,
        reasonForRefusal: reason,
      }),
    };
    const request = await fetch(`/api/object/refuseOffer`, options);

    if (request.ok) {
      window.location.reload();
    } else {
      await Swal.fire({
        icon: 'error',
        title: 'Erreur',
        text: "Erreur lors du refus de l'objet",
      });

    }
  }
}

export default UpdateObject;

