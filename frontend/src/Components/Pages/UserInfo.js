import Navigate from "../Router/Navigate";

const createRow = (objet) => {
  //    const body = document.querySelector('#object-list');
      const row = document.createElement('tr');
      row.setAttribute("class",
          "w-4/5 border-roze border-t  transition-colors duration-500 hover:bg-red-50");
      row.innerHTML = `
           <td class="py-4 pr-10"><img class="max-h-40 min-h-40 p-1" src="${"/api/picture/getPicture/".concat(
          objet.picture.url)}" alt="image de l'objet"</td>
           <td class="text-left py-4 pr-52">${objet.description}</td> 
           <td class="test-left py-4 pr-52">${objet.price}€</td>
           <td class="text-left py-4 pr-52">${objet.type.typeName}</td>
           <td class="text-left py-4 pr-52">${objet.state}</td> 
  `
      row.addEventListener('click', () => {
        Navigate(`/object?id=${objet.id}`);
      });
     // body.appendChild(row);
    }
;

const UserInfo = async () => {
  const main = document.querySelector("main");
  const urlParam = new URL(window.location).searchParams;
  const unknownUserId = urlParam.get("id");
  const user = JSON.parse(sessionStorage.getItem("user"));

  if (!user) {
    Navigate('/');
    return;
  }

  const option = {
    method: "GET",
    headers: {
      "Authorization": sessionStorage.getItem("token")
          ? sessionStorage.getItem("token") : localStorage.getItem("token"),
      "content-type": "application/json",
    }
  };

  // Get user info
  const userInfoUrl = `/api/users/getOne/${unknownUserId}`;
  const userInfoRequest = await fetch(userInfoUrl, option);

  if (!userInfoRequest.ok) {
    main.innerHTML = `
      <div class="grid place-items-center pt-15">
        <div class="max-w-sm">
          <h1 class="pb-10 text-4xl font-bold">Error</h1>
          <p class="text-red-500">${userInfoRequest.statusText}</p>
        </div>
      </div>
    `;
    return;
  }

  const userInfo = await userInfoRequest.json();

  // Get user's offered objects
  const offeredObjectsUrl = `/api/object/MyObjects/${userInfo.id}`;
  const offeredObjectsRequest = await fetch(offeredObjectsUrl, option);

  if (!offeredObjectsRequest.ok) {
    main.innerHTML = `
      <div class="grid place-items-center pt-15">
        <div class="max-w-sm">
          <h1 class="pb-10 text-4xl font-bold">Error</h1>
          <p class="text-red-500">${offeredObjectsRequest.statusText}</p>
        </div>
      </div>
    `;
    return;
  }

  const offeredObjects = await offeredObjectsRequest.json();

  main.innerHTML = `
    <div class="flex justify-center items-center">
      <div class="w-1/2">
        <h1 class="text-4xl font-bold mb-6">Les informations personnelles ${userInfo.role === 'A' ? "d'un aidant" : 'du membre'}</h1>
        <div class="mt-1 relative rounded-md shadow-sm">
          <input
            type="text"
            name="firstname"
            id="firstname"
            value="${userInfo.firstname}"
            class="m-2 p-3 rounded-full border-2 border-roze w-full pl-10"
            readonly
          />
        </div>
        <div class="mt-1 relative rounded-md shadow-sm">
          <input
            type="text"
            name="lasname"
            id="lastname"
            value="${userInfo.lastname}"
            class="m-2 p-3 rounded-full border-2 border-roze w-full pl-10"
            readonly
          />
        </div>
        <div class="mt-1 relative rounded-md shadow-sm">
          <input
            type="text"
            name="phoneNumber"
            id="phoneNumber"
            value="${userInfo.phoneNumber}"
            class="m-2 p-3 rounded-full border-2 border-roze w-full pl-10"
            readonly
          />
        </div>
        <div class="mt-1 relative rounded-md shadow-sm">
          <input
            type="text"
            name="email"
            id="email"
            value="${userInfo.email}"
            class="m-2 p-3 rounded-full border-2 border-roze transition-colors duration-500 hover:bg-red-100 w-full pl-10"
              disabled
            />
      </div>
      <div class="mt-1 relative rounded-md shadow-sm">
  <input
    name="role"
    id="role"
    class="m-2 p-3 rounded-full border-2 border-roze w-full pl-10"
    value="${
      (() => {
        if (userInfo.role === 'M') {
          return 'Membre';
        } if (userInfo.role === 'R') {
          return 'Responsable';
        } if (userInfo.role === 'A') {
          return 'Aidant';
        }
        return '';

      })()
  }"
    readonly
  />
</div>



<div class="mt-5">
  <h1 class="text-3xl font-bold mb-3">Les offres du membre</h1>
  <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-20 gap-4">
    ${offeredObjects.length > 0 ?
        offeredObjects.map(offer => `
        <div class="rounded-lg border-2 border-gray-200">
          <div class="relative">
            <img alt="Objet presenter" class="w-full h-auto object-cover rounded-t-lg" src="${"/api/picture/getPicture/".concat(offer.picture.url)}">   
            <div class="absolute bottom-0 left-0 right-0 p-4 bg-gradient-to-t from-black">
              <h2 class="text-2xl font-bold text-white">${offer.type.typeName}</h2>
              <p class="text-lg text-white">${offer.description}</p>
            </div>
          </div>
        </div>
      `).join("") :
        `<p class="text-lg">Cet utilisateur n'a pas encore proposé d'objets.</p>`
    }
  </div>
</div>

`;
  offeredObjects.forEach((object) => {
    createRow(object);
  });
};

export default UserInfo;