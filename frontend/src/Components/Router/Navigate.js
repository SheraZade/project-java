/**
 * Navigate to a URI by triggering the popstate event in order for the router to deal with
 * a change of browser history.
 * NB : this solution is to avoid circular dependencies : if Navigate() had to import, directly or
 * indirectly, the pages, then there would be a circular reference because the router
 * has to import all the pages to render them.
 */

const Navigate = (toUri) => {
    const fromUri = window.location.pathname;
    if (fromUri === toUri) return;

    window.history.pushState({}, '', window.location.origin + toUri);
    window.location.replace(toUri)
};
export default Navigate;