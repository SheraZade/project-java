import HomePage from '../Pages/HomePage';
import LoginPage from "../Pages/LoginPage";
import LogoutPage from "../Pages/LogoutPage";
import UpdateObject from "../Pages/ObjectUpdatePage";
import ObjectPage from "../Pages/ObjectPage";
import RegisterPage from "../Pages/RegisterPage";
import PropositionPage from "../Pages/propositionPage";
import ListObjects from "../Pages/ObjectListPage";
import MyObjects from "../Pages/MyObjectPage";
import ProfilePage from "../Pages/ProfilePage";
import makeOffer from "../Pages/MakeOffer";
import DisponibilitePage from "../Pages/DisponiblitePage"
import UserInfo from "../Pages/UserInfo"
import ObjectsToWithdrawPage from "../Pages/ObjectsToWithdrawPage"
import {UsersPage} from "../Pages/UsersPage"
import tableauDeBord from "../Pages/TableauDeBord";


const routes = {
  '/': HomePage,
  '/login': LoginPage,
  '/logout': LogoutPage,
  '/updateObject' : UpdateObject,
  '/object': ObjectPage,
  '/register': RegisterPage,
  '/users': UsersPage,
  '/proposition': PropositionPage,
  '/listeObjet': ListObjects ,
  '/mesObjet': MyObjects,
  '/profile': ProfilePage,
  '/offer' : makeOffer,
  '/dispo' : DisponibilitePage,
  '/pr' : UserInfo,
  '/refuse' : ObjectsToWithdrawPage,
  '/tableauDeBord' : tableauDeBord,

};

export default routes;
