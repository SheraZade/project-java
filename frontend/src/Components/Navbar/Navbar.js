import {clearPage} from "../../utils/render";
import logo from "../../img/logo.svg";
import Navigate from "../Router/Navigate";

/**
 * Render the Navbar which is styled by using Bootstrap
 * Each item in the Navbar is tightly coupled with the Router configuration :
 * - the URI associated to a page shall be given in the attribute "data-uri" of the Navbar
 * - the router will show the Page associated to this URI when the user click on a nav-link
 */

const Navbar = () => {
  clearPage()
  renderNavbar()
};

async function renderNavbar() {
  const navbarWrapper = document.querySelector('#navbarWrapper');
  const user = JSON.parse(sessionStorage.getItem("user"));
  window.navToHome = async function navToHomePage() {
    Navigate('/');
  };

  if (!user) {
    navbarWrapper.innerHTML = `<nav class="border-b-2 border-grey-500 fixed w-full top-0 z-50 bg-bgColor px-4 py-2.5">
        <div class="container flex flex-wrap items-center justify-between mx-auto">
          <button type="button" class="h-fit w-fit flex flex-box" onclick="navToHome()">
            <img src="${logo}" class="h-15 mr-3" alt="logo">
            <p class="self-center font-semibold whitespace-nowrap text-left text-3xl">
               <span class="font-Blackadder">RessourceRie</br></span>
               <span class="text-lg">Rue de Heuseux 77ter, 4671 Blégny</span>
            </p>
          </button>
          <div class="block w-auto" >
            <ul class="flex py-4 flex-row space-x-6 font-medium">
              <li>
                <a class="nav-link flex w-40 justify-center py-2 bg-roze border-roze border-2 rounded-3xl
                 transition-colors duration-500 hover:bg-red-100 " aria-current="page" href="#" data-uri="/offer">Proposer un Objet</a>
              </li>
              <li>
                <a class="nav-link flex w-36 justify-center py-2 border-roze border-2 rounded-3xl
                 transition-colors duration-500 hover:bg-red-100" href="#" data-uri="/login">Connexion</a>
              </li> 
              <li>
                <a class="nav-link flex w-36 justify-center py-2 border-roze border-2 rounded-3xl transition-colors duration-500 hover:bg-red-100" href="#" data-uri="/register">S'inscrire</a>
              </li>                        
            </ul>
          </div>
        </div>
      </nav>`;
  } else {

    const authUserNavbar = `<nav class="border-b-2 border-grey-500 fixed w-full top-0 z-50 bg-bgColor px-4 py-2.5">
        <div class="container flex flex-wrap items-center justify-between mx-auto">
          <button type="button" class="h-fit w-fit flex flex-box" onclick="navToHome()">
            <img src="${logo}" class="h-15 mr-3" alt="logo">
            <p class="self-center font-semibold whitespace-nowrap text-left text-3xl">
               <span class="font-Blackadder">RessourceRie</br></span>
               <span class="text-lg">Rue de Heuseux 77ter, 4671 Blégny</span>
            </p>
          </button>
          <div class="block w-auto" >
            <ul class="flex py-4 flex-row space-x-6 font-medium">
              <li>
                <a class="nav-link block px-3 py-2 bg-roze border-roze border-2 rounded-3xl transition-colors duration-500 hover:bg-red-100" aria-current="page" href="#" data-uri="/offer">Proposer un Objet</a>
              </li>  
              <li>
                <div class="relative inline-block text-left">
                  <button type="button" onclick="toggleUserBox()" class="inline-flex justify-center rounded-full border " id="btnUser">
                    <img src="${"/api/picture/getPicture/".concat(
        user.picture.url)}" class="rounded-full h-10 w-10" alt="UserImg" id="pp">
                  </button>
                
                  <div class="userBox hidden origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg ring-1 ring-roze bg-bgColor">
                    <div class="py-1">
                      <a href="#" data-uri="/profile" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Mon profil</a>
                      <a href="#" data-uri="/mesObjet" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Mes objets</a>
                      <a href="#" data-uri="/logout" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100">Déconnexion</a>
                    </div>
                  </div>
                </div> 
              </li>                     
            </ul> 
        </div>
      </nav>`;

    const adminUserNavbar =
        `<nav class="border-b-2 border-grey-500 fixed w-full top-0 z-50 bg-bgColor px-4 py-2.5">
        <div class="container flex flex-wrap items-center justify-between mx-auto">
          <button type="button" class="h-fit w-fit flex flex-box" onclick="navToHome()">
            <img src="${logo}" class="h-15 mr-3" alt="logo">
            <p class="self-center font-semibold whitespace-nowrap text-left text-3xl">
               <span class="font-Blackadder">RessourceRie</br></span>
               <span class="text-lg">Rue de Heuseux 77ter, 4671 Blégny</span>
            </p>
          </button>
          <div class="block w-auto" >
            <ul class="flex py-4 flex-row space-x-6 font-medium">
                <li>
                <a class="nav-link block px-3 py-2 bg-roze border-roze border-2 rounded-3xl transition-colors duration-500 hover:bg-red-100" aria-current="page" href="#" data-uri="/dispo">Ajouter une disponibilité</a>
              </li> 
              <li>
                <a class="nav-link block px-3 py-2 bg-roze border-roze border-2 rounded-3xl transition-colors duration-500 hover:bg-red-100" aria-current="page" href="#" data-uri="/offer">Proposer un Objet</a>
              </li>  
              <li>  
                <div class="relative inline-block text-left">
                  <button type="button" onclick="toggleUserBox()"  class="inline-flex justify-center rounded-full border " id="btnUser">
                    <img src="${"/api/picture/getPicture/".concat(
            user.picture.url)}" class="rounded-full h-10 w-10" alt="UserImg" id="pp">
                  </button>
                
                  <div class="userBox hidden origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg ring-1 ring-roze bg-bgColor">
                    <div class="py-1">
                      <a href="#" data-uri="/mesObjet" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Mes objets</a>
                       <a href="#" data-uri="/profile" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Mon profil</a>
                      <a href="#" data-uri="/tableauDeBord" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Tableau de bord</a>
                      <a href="#" data-uri="/users" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Liste des membres</a>
                      <a href="#" data-uri="/listeObjet" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Liste des objets</a>
                      <a href="#" data-uri="/logout" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100">Déconnexion</a>
                    </div>
                  </div>
                </div>     
              </li>                     
            </ul> 
        </div>
      </nav>`;

    const ownerUserNavbar =
        `<nav class="border-b-2 border-grey-500 fixed w-full top-0 z-50 bg-bgColor px-4 py-2.5 ">
        <div class="container flex flex-wrap items-center justify-between mx-auto">
          <button type="button" class="h-fit w-fit flex flex-box" onclick="navToHome()">
            <img src="${logo}" class="h-15 mr-3" alt="logo">
            <p class="self-center font-semibold whitespace-nowrap text-left text-3xl">
               <span class="font-Blackadder">RessourceRie</br></span>
               <span class="text-lg">Rue de Heuseux 77ter, 4671 Blégny</span>
            </p>
          </button>
          <div class="block w-auto" >
            <ul class="flex py-4 flex-row space-x-6 font-medium">
                <li>
                <a class="nav-link block px-3 py-2 bg-roze border-roze border-2 rounded-3xl transition-colors duration-500 hover:bg-red-100" aria-current="page" href="#" data-uri="/dispo">Ajouter une disponibilité</a>
              </li> 
              <li>
                <a class="nav-link block px-3 py-2 bg-roze border-roze border-2 rounded-3xl transition-colors duration-500 hover:bg-red-100" aria-current="page" href="#" data-uri="/offer">Proposer un Objet</a>
              </li>  
              <li>
                <div class="relative inline-block text-left">
                  <button type="button" onclick="toggleUserBox()" class="inline-flex justify-center rounded-full border " id="btnUser">
                    <img src="${"/api/picture/getPicture/".concat(
            user.picture.url)}" class="rounded-full h-10 w-10" alt="UserImg" id="pp">
                  </button>
                
                  <div class="userBox hidden origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg ring-1 ring-roze bg-bgColor">
                    <div class="py-1">
                      <a href="#" data-uri="/mesObjet" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Mes objets</a>
                      <a href="#" data-uri="/profile" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Mon profil</a>
                      <a href="#" data-uri="/tableauDeBord" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Tableau de bord</a>
                      <a href="#" data-uri="/refuse" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Objets retirés de la vente </a>
                      <a href="#" data-uri="/proposition" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Les propositions</a>
                      <a href="#" data-uri="/users" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Liste des membres</a>
                      <a href="#" data-uri="/listeObjet" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100" >Liste des objets</a>
                      <a href="#" data-uri="/logout" class="nav-link block px-4 py-2 text-sm transition-colors duration-500 hover:bg-red-100">Déconnexion</a>     
                    </div>
                  </div>
                </div>
              </li>                     
            </ul> 
        </div>
      </nav>`;

    if (user.role === "M") {
      navbarWrapper.innerHTML = authUserNavbar;
    } else if (user.role === "A") {
      navbarWrapper.innerHTML = adminUserNavbar;
    } else if (user.role === "R") {
      navbarWrapper.innerHTML = ownerUserNavbar;
    }
    const userBox = document.querySelector(".userBox")

    window.toggleUserBox = function toggleBox() {
      userBox.classList.toggle('hidden');
    }

    document.querySelector("#btnUser").addEventListener('blur', () => {
      window.setTimeout(() => {
        userBox.classList.toggle('hidden', true);
      }, 200);
    });
  }

}

export default Navbar;

