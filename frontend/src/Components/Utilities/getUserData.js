const getUserData = async () => {

  let jwtToken = sessionStorage.getItem("token");
  if (!jwtToken) {
    jwtToken = localStorage.getItem("token");
  }
  if (jwtToken && !sessionStorage.getItem("user")) {
    const options = {
      method: "GET",
      headers: {
        "Content-type": "application/json",
        "Authorization": jwtToken

      },
    }

    const request = await fetch('/api/auths/user', options);
    if (request.ok) {
      const response = await request.json();
      sessionStorage.setItem("token", response.token);
      sessionStorage.setItem("user", JSON.stringify(response.user));
    }
  }
}

export default getUserData;