import './stylesheets/main.css';

import Navbar from './Components/Navbar/Navbar';
import Router from './Components/Router/Router';
import getUserData from "./Components/Utilities/getUserData";

Navbar();

Router();

getUserData();