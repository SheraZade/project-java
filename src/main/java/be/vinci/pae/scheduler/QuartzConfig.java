package be.vinci.pae.scheduler;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;

/**
 * Class that configures the scheduler.
 */
public class QuartzConfig {

  /**
   * Configures the job and the trigger.
   *
   * @param scheduler the scheduler
   * @throws SchedulerException the exception when error occurs.
   */
  public void scheduleJobs(Scheduler scheduler) throws SchedulerException {
    JobDetail jobDetail = JobBuilder.newJob(CheckDepositJobImpl.class)
        .withIdentity("checkDepositJob", "group1")
        .build();

    CronTrigger trigger = TriggerBuilder.newTrigger()
        .withIdentity("checkDepositTrigger", "group1")
        .withSchedule(CronScheduleBuilder.cronSchedule("0 0 0 ? * MON-FRI *"))
        .build();
    scheduler.scheduleJob(jobDetail, trigger);
  }
}
