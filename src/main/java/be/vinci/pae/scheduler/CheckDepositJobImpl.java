package be.vinci.pae.scheduler;

import be.vinci.pae.services.object.ObjectDTO;
import be.vinci.pae.services.object.ObjectUCC;
import be.vinci.pae.utils.ApplicationBinder;
import jakarta.inject.Inject;
import java.util.ArrayList;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

/**
 * Class that give a task for the scheduler.
 */
public class CheckDepositJobImpl implements Job {

  ServiceLocator locator = ServiceLocatorUtilities.bind(new ApplicationBinder());

  @Inject
  ObjectUCC objectUCC = locator.getService(ObjectUCC.class);

  @Override
  public void execute(JobExecutionContext context) {
    ArrayList<ObjectDTO> objects = objectUCC.checkObjectToWithdraw();
    // context.getJobDetail().getJobDataMap().put("result", objects);
    for (ObjectDTO object : objects) {
      objectUCC.withdrawObject(object);
    }
  }
}
