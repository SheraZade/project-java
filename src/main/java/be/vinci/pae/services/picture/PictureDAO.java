package be.vinci.pae.services.picture;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * the interface PictureDAO.
 *
 * @author Sasha van Rossum.
 */
public interface PictureDAO {

  /**
   * get all Users Picture.
   *
   * @return list of user picture.
   */
  List<PictureDTO> getAllPictureUser();

  /**
   * get all Object Picture.
   *
   * @return list of object picture.
   */
  List<PictureDTO> getAllPictureObject();

  /**
   * Get the picture of an object.
   *
   * @param id of the object.
   * @return img from the object.
   */
  PictureDTO getPictureObj(int id);

  /**
   * Get the profile picture of a user.
   *
   * @param id of the user.
   * @return img from the user.
   */
  PictureDTO getPictureUser(int id);

  /**
   * Update the picture of the object into the database.
   *
   * @param picture the picture to update into the database.
   * @return the picture injected into the database.
   */
  PictureDTO updatePictureObjet(PictureDTO picture);

  /**
   * Update the picture of the member into the database.
   *
   * @param picture the picture to update into the database.
   * @return the picture injected into the database.
   */
  PictureDTO updatePictureMember(PictureDTO picture);

  /**
   * fill a new picture with the rs.
   *
   * @param picture to fill.
   * @param rs      rs with the picture.
   * @throws SQLException Error at fill.
   */
  void fillPicture(PictureDTO picture, ResultSet rs) throws SQLException;

  /**
   * add a new picture profile picture in db.
   *
   * @param picture to fill.
   * @return the picture added as a profile picture.
   */
  PictureDTO addPicturePP(PictureDTO picture);
}
