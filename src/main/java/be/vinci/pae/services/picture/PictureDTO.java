package be.vinci.pae.services.picture;

import be.vinci.pae.domain.picture.PictureImpl;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * interface PictureDTO.
 *
 * @author van Rossum Sasha.
 */
@JsonDeserialize(as = PictureImpl.class)
public interface PictureDTO {

  /**
   * Getter for id.
   *
   * @return the Picture ID.
   */
  int getId();

  /**
   * setter of id.
   *
   * @param id the id of the picture.
   */
  void setId(int id);

  /**
   * Getter for url.
   *
   * @return the Picture url.
   */
  String getUrl();

  /**
   * setter of url.
   *
   * @param url the url of the picture.
   */
  void setUrl(String url);

  /**
   * Getter for object id the picture is linked to.
   *
   * @return the object ID.
   */
  int getObjectId();

  /**
   * setter of object id the picture is linked to.
   *
   * @param id the object id the picture is linked to.
   */
  void setObjectId(int id);

  /**
   * Getter for member id the picture is linked to.
   *
   * @return the member ID.
   */
  int getMemberId();

  /**
   * setter of member id the picture is linked to.
   *
   * @param id the member id the picture is linked to.
   */
  void setMemberId(int id);

  /**
   * Getter of the version of the picture.
   *
   * @return the number version of the picture.
   */
  int getVersion();

  /**
   * setter of the version.
   *
   * @param version the version of the picture.
   */
  void setVersion(int version);

}
