package be.vinci.pae.services.picture;

import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.exceptions.OptimisticException;
import be.vinci.pae.services.dal.DalBackendServices;
import jakarta.inject.Inject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * the implementation of the interface PictureDAO.
 *
 * @author Sasha van Rossum
 */
public class PictureDAOImpl implements PictureDAO {
  @Inject
  private Factory myFactory;

  @Inject
  private DalBackendServices dalBackendServices;

  @Override
  public List<PictureDTO> getAllPictureUser() {
    List<PictureDTO> listPicture = new ArrayList<>();
    String query = "SELECT * FROM ressourcerie.pictures WHERE member IS NOT NULL";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        do {
          PictureDTO pictureDTO = myFactory.getPicture();
          fillPicture(pictureDTO, rs);
          listPicture.add(pictureDTO);
        } while (rs.next());
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e.getCause());
    }
    return listPicture;
  }

  @Override
  public List<PictureDTO> getAllPictureObject() {
    List<PictureDTO> listPicture = new ArrayList<>();
    String query = "SELECT * FROM ressourcerie.pictures WHERE object_id IS NOT NULL";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        do {
          PictureDTO pictureDTO = myFactory.getPicture();
          fillPicture(pictureDTO, rs);
          listPicture.add(pictureDTO);
        } while (rs.next());
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e.getCause());
    }
    return listPicture;
  }

  @Override
  public PictureDTO getPictureObj(int id) {
    String query = "SELECT * FROM RESSOURCERIE.PICTURES WHERE object_id = ? "
        + "ORDER BY id_picture DESC LIMIT 1";
    PictureDTO picture = myFactory.getPicture();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, id);
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        fillPicture(picture, rs);
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return picture;
  }

  @Override
  public PictureDTO getPictureUser(int id) {
    String query = "SELECT * FROM RESSOURCERIE.PICTURES WHERE member = ? "
        + "ORDER BY id_picture DESC LIMIT 1";
    PictureDTO picture = myFactory.getPicture();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, id);
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        fillPicture(picture, rs);
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return picture;
  }

  /**
   * Update the picture of the object into the database.
   *
   * @param picture the picture to update into the database.
   * @return the picture injected into the database.
   */
  @Override
  public PictureDTO updatePictureObjet(PictureDTO picture) {
    String query = "UPDATE RESSOURCERIE.PICTURES SET URL = ?, VERSION_NUMBER = VERSION_NUMBER + 1 "
        + " WHERE OBJECT_ID = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, picture.getUrl());
      ps.setInt(2, picture.getObjectId());
      ps.setInt(3, picture.getVersion());
      int rowsUpdated = ps.executeUpdate();
      if (rowsUpdated == 0) {
        checkOptimisticOrUnfoundError(picture);
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return picture;
  }

  /**
   * Update the picture of the member into the database.
   *
   * @param picture the picture to update into the database.
   * @return the picture injected into the database.
   */
  @Override
  public PictureDTO updatePictureMember(PictureDTO picture) {
    String query = "UPDATE RESSOURCERIE.PICTURES SET URL = ?, VERSION_NUMBER = VERSION_NUMBER + 1 "
        + " WHERE MEMBER = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, picture.getUrl());
      ps.setInt(2, picture.getMemberId());
      ps.setInt(3, picture.getVersion());
      int rowsUpdated = ps.executeUpdate();
      if (rowsUpdated == 0) {
        checkOptimisticOrUnfoundError(picture);
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
    return picture;
  }

  @Override
  public void fillPicture(PictureDTO picture, ResultSet rs) throws SQLException {
    picture.setId(rs.getInt(1));
    picture.setUrl(rs.getString(2));
    picture.setObjectId(rs.getInt(3));
    picture.setMemberId(rs.getInt(4));
  }

  private void checkOptimisticOrUnfoundError(PictureDTO pictureDTO) {
    if (isUnfound(pictureDTO.getId())) {
      throw new OptimisticException(); //lancer un 409 Conflicts
    } else if (isInvalidVersion(pictureDTO.getId(), pictureDTO.getVersion())) {
      throw new FatalException(); //404 error not found
    }
  }

  private boolean isUnfound(int idObject) {
    String query = "SELECT * FROM RESSOURCERIE.PICTURES WHERE ID_OBJECT = ?";
    try (PreparedStatement prepareStatement = dalBackendServices.getPrepareStatement(query)) {
      prepareStatement.setInt(1, idObject);
      ResultSet rs = prepareStatement.executeQuery();
      return !rs.next();
    } catch (SQLException e) {
      throw new FatalException("problem occurs with isUnfound.", e.getCause());
    }
  }

  private boolean isInvalidVersion(int idPicture, int versionPicture) {
    String query = "SELECT * FROM RESSOURCERIE.pictures WHERE version_number = ? AND ID_OBJECT = ?";
    try (PreparedStatement prepareStatement = dalBackendServices.getPrepareStatement(query)) {
      prepareStatement.setInt(2, idPicture);
      prepareStatement.setInt(1, versionPicture);
      ResultSet res = prepareStatement.executeQuery();
      return !res.next();
    } catch (SQLException e) {
      throw new FatalException("problem occurs with the versionPicture", e.getCause());
    }
  }

  /**
   * Adds a profile picture for a user, probably.
   *
   * @param picture the user's profile picture.
   * @return the added picture.
   */
  public PictureDTO addPicturePP(PictureDTO picture) {
    String query = "INSERT INTO ressourcerie.pictures (url, member, version_number)"
        + " VALUES (?, ?, ?)";
    try (PreparedStatement prepareStatement = dalBackendServices.getPrepareStatement(query)) {
      prepareStatement.setString(1, picture.getUrl());
      prepareStatement.setInt(2, picture.getMemberId());
      prepareStatement.setInt(3, 1);

      int executeLine = prepareStatement.executeUpdate();
      if (executeLine == 0) {
        checkOptimisticOrUnfoundError(picture);
      }

      return picture;
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e.getCause());
    }
  }


}
