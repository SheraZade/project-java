package be.vinci.pae.services.type;

import be.vinci.pae.domain.type.TypeImpl;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * The interface TypeDTO.
 */
@JsonDeserialize(as = TypeImpl.class)
public interface TypeDTO {

  /**
   * Getter of the id of the type.
   *
   * @return the id of the type
   */
  int getId();

  /**
   * Getter of the name of the type.
   *
   * @return the name of the type.
   */
  String getTypeName();

  /**
   * Setter of the id of the type.
   *
   * @param id the new value of the id.
   */
  void setId(int id);

  /**
   * Setter of the type.
   *
   * @param type the new value of the type.
   */
  void setTypeName(String type);
}
