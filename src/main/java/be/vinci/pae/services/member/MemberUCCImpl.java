package be.vinci.pae.services.member;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.member.Member;
import be.vinci.pae.exceptions.BizException;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.availability.AvailabilityDAO;
import be.vinci.pae.services.dal.DalServices;
import be.vinci.pae.services.notification.NotificationDAO;
import be.vinci.pae.services.notification.NotificationDTO;
import be.vinci.pae.services.picture.PictureDAO;
import be.vinci.pae.services.picture.PictureDTO;
import jakarta.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * implementation of the interface  UserUcc.
 *
 * @author Dorcas KUEZE
 */
public class MemberUCCImpl implements MemberUCC {


  @Inject
  private MemberDAO memberDAO;
  @Inject
  private DalServices dalServices;
  @Inject
  private PictureDAO pictureDAO;
  @Inject
  private NotificationDAO notificationDAO;
  @Inject
  private AvailabilityDAO availabilityDAO;


  /**
   * Empty constructor.
   */
  public MemberUCCImpl() {
  }

  /**
   * get user for checkPassword.
   *
   * @param email    email of user.
   * @param password password of user.
   * @return userDTO if the password is good if not.
   * @throws BizException   when user is not found.
   * @throws FatalException when problem occurs in DAO.
   */

  public MemberDTO login(String email, String password) {
    try {
      dalServices.start();

      Member memberDTO = (Member) memberDAO.getMemberByEmail(email);
      if (memberDTO == null) {
        throw new BizException("The user wasn't found.");
      }

      dalServices.commit();
      return memberDTO.isValidPassword(password) ? memberDTO : null;

    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
  }


  /**
   * gets member from id for refresh and remember me.
   *
   * @param id the user's ID
   * @return a memberDTO object with the requested user's information.
   * @throws DAOException   when the user wasn't found.
   * @throws FatalException when problem occurs in DAO.
   */
  public MemberDTO getMember(int id) {

    try {
      dalServices.start();

      Member memberDTO = (Member) memberDAO.getMemberByID(id);
      if (memberDTO == null) {
        throw new DAOException("The user wasn't found.");
      }
      dalServices.commit();
      return memberDTO;
    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Registers a new member with the given member information.
   *
   * @param member the member information to register
   * @return the registered member information
   * @throws BizException   if the member's email is already registered
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when problem with query.
   */
  public MemberDTO register(MemberDTO member) {
    try {
      dalServices.start();

      Member theMember = (Member) member;
      Member memberAlreadyExists = (Member) memberDAO.getMemberByEmail(theMember.getEmail());
      if (memberAlreadyExists != null) {
        throw new BizException("This email is already registered.");
      }

      MemberDTO newMember = theMember.createMember(member);
      MemberDTO memberRegistered = memberDAO.register(newMember);
      if (memberRegistered == null) {
        throw new DAOException(
            "Error when executing the query when registering a new member.");
      }

      memberRegistered.getPicture().setMemberId(memberRegistered.getId());
      PictureDTO pictureRegistered = pictureDAO.addPicturePP(memberRegistered.getPicture());

      if (pictureRegistered == null) {
        throw new DAOException(
            "Error when executing the query when registering a new member.");
      }

      dalServices.commit();
      return memberRegistered;

    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Retrieves all members stored in the Ressourceries' database by calling the appropriate method
   * in the member DAO.
   *
   * @return A list of memberDTOs representing all registered members.
   * @throws FatalException when problem occurs in DAO.
   */
  @Override
  public List<MemberDTO> getAllMember() {
    try {
      dalServices.start();

      List<MemberDTO> listAllMembers = memberDAO.getAllMembers();
      dalServices.commit();
      return listAllMembers;

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * implementation of confirmRegister.
   *
   * @param id to be confirmed as a staff.
   * @return returns the new assistant or null if problem occurs.
   * @throws BizException   when the role of the given id isn't allowed to confirm as assistant.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the member wasn't found or problem with query.
   */
  @Override
  public MemberDTO updateRoleAsAssistant(int id) {
    try {
      dalServices.start();
      Member member = (Member) memberDAO.getMemberByID(id);
      if (member == null) {
        throw new DAOException("The member wasn't found.");
      }

      MemberDTO memberDTO = member.confirmAssistantRole();
      if (memberDTO == null) {
        throw new BizException("The member must have an account to be to confirm as a member "
            + "of the staff.");
      }

      MemberDTO newMember = memberDAO.updateRole(memberDTO);
      if (newMember == null) {
        throw new DAOException("Error when executing the query.");
      }

      dalServices.commit();
      return newMember;

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Mark the member as responsable.
   *
   * @param id of the member to be confirmed as a responsable.
   * @return returns the new assistant or null if problem occurs.
   * @throws BizException   when the role of the given id isn't allowed to confirm as responsable.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the member wasn't found or problem with query.
   */
  public MemberDTO updateRoleAsResponsable(int id) {
    try {
      dalServices.start();

      Member member = (Member) memberDAO.getMemberByID(id);
      if (member == null) {
        throw new DAOException("The user wasn't found.");
      }

      MemberDTO theMember = member.confirmResponsableRole();
      if (theMember == null) {
        throw new BizException("The member must have an account to be to confirm as responsable.");
      }

      MemberDTO newMember = memberDAO.updateRole(member);
      if (newMember == null) {
        throw new DAOException("Error when executing the query.");
      }

      dalServices.commit();
      return newMember;

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }


  /**
   * Check if the member exist in  DB and  delegate the job to DAO for setting data object member.
   *
   * @param member the member that contain the new data of the member.
   * @return the member with new data.
   */
  public MemberDTO editMyProfile(MemberDTO member) {
    try {
      dalServices.start();

      Member memberToFound = (Member) memberDAO.getMemberByID(member.getId());
      if (memberToFound == null) {
        throw new DAOException("The member wasn't found.");
      }

      MemberDTO memberModified = memberToFound.modifiedMember(member);
      MemberDTO memberEdited = memberDAO.editMyProfile(memberModified);
      if (memberEdited == null) {
        throw new DAOException("impossible to execute the query ");
      }

      PictureDTO pictureDTO = pictureDAO.updatePictureMember(memberEdited.getPicture());
      if (pictureDTO == null) {
        throw new DAOException(
            "Error when executing the query to update the picture of the member.");
      }
      dalServices.commit();
      return memberEdited;

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }


  /**
   * Get the member who offered the object.
   *
   * @param id the id of the object.
   * @return the information of the person.
   */
  @Override
  public MemberDTO getOfferer(int id) {
    try {

      dalServices.start();

      MemberDTO member = memberDAO.getOfferer(id);
      if (member == null) {
        throw new DAOException("whe didn't find the person who offered this object  ");
      }
      dalServices.commit();
      return member;

    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Insert a new date of availability into the database.
   *
   * @param dateAvailable the availability of one of the staff.
   * @return the date of availability.
   * @throws BizException   when an error has occurred while inserting the availability.
   * @throws FatalException when problem occurs in DAO.
   */
  public Availability addAvailability(Availability dateAvailable) {
    try {
      dalServices.start();

      int idAvailability = availabilityDAO.getAvailability(dateAvailable);
      int id = 0;
      if (idAvailability == -1) {
        id = memberDAO.addAvailability(dateAvailable);
      }
      if (id == -1) {
        throw new BizException(
            "An error has occurred while getting the new id of the new availability.");
      }

      dalServices.commit();
      return dateAvailable;

    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
  }

  @Override
  public List<MemberDTO> search(String name, String role) {
    List<MemberDTO> membersFound;
    try {
      dalServices.start();
      membersFound = memberDAO.getAllMembers();
      dalServices.commit();
    } catch (FatalException exc) {
      dalServices.rollback();
      throw exc;
    }

    if (name != null && !name.isBlank()) {
      membersFound = membersFound.stream()
          .filter((member) -> member.getFirstName().equalsIgnoreCase(name)
              || member.getLastName().equalsIgnoreCase(name))
          .collect(Collectors.toList());
    }
    if (role.equalsIgnoreCase("Y")) {
      membersFound = membersFound.stream()
          .filter((member) -> member.getRole().equalsIgnoreCase("R")
              || member.getRole().equalsIgnoreCase("A"))
          .collect(Collectors.toList());
    }
    if (role.equalsIgnoreCase("N")) {
      membersFound = membersFound.stream()
          .filter((member) -> member.getRole().equalsIgnoreCase("M"))
          .collect(Collectors.toList());
    }

    return membersFound;
  }

  /**
   * Gets the list of all the notifications of the member.
   *
   * @param idMember the id of the member
   * @return the list of the notifications of the member
   */
  @Override
  public List<NotificationDTO> allNotificationsMember(int idMember) {
    try {
      dalServices.start();

      List<NotificationDTO> listNotifications = notificationDAO.listNotificationsMember(idMember);
      if (listNotifications == null) {
        throw new DAOException(
            "Problem occurred while trying to get the list of all notifications of a member");
      }
      dalServices.commit();
      return listNotifications;

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Gets the list of unread of notifications of the member.
   *
   * @param idMember the id of the member
   * @return the list of unread notifications of the member
   */
  @Override
  public List<NotificationDTO> unreadNotificationsMember(int idMember) {
    try {
      dalServices.start();

      List<NotificationDTO> listNotifications = notificationDAO.listUnreadNotificationsMember(
          idMember);
      if (listNotifications == null) {
        throw new DAOException("Problem occurred while trying to get the list of all unread "
            + "notifications of a member");
      }
      dalServices.commit();
      return listNotifications;

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }
}
