package be.vinci.pae.services.member;


import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.exceptions.BizException;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.notification.NotificationDTO;
import java.util.List;

/**
 * implementation of the interface  UserUcc.
 *
 * @author Dorcas KUEZE
 */

public interface MemberUCC {

  /**
   * get user for checkPassword.
   *
   * @param email    email of user
   * @param passWord password of user
   * @return userDTO if the password is good if not
   */
  MemberDTO login(String email, String passWord);

  /**
   * gets user from id for refresh and remember me.
   *
   * @param id the user's ID
   * @return a userDTO object with the requestedd user's information.
   */
  MemberDTO getMember(int id);

  /**
   * Registers a new user with the given user information.
   *
   * @param user the user information to register
   * @return the registered user information
   * @throws IllegalArgumentException if the user's email is already registered
   */
  MemberDTO register(MemberDTO user);

  /**
   * Gets all members stored in the Ressourcerie's database.
   *
   * @return A list of members DTOs representing all registered users.
   */
  List<MemberDTO> getAllMember();

  /**
   * Mark the member as responsable.
   *
   * @param id the id of the member.
   * @return the member that was leveled up.
   * @throws BizException   when the role of the given id isn't allowed to confirm as responsable.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the member wasn't found or problem with query.
   */
  MemberDTO updateRoleAsResponsable(int id);


  /**
   * Mark the member as staff member.
   *
   * @param id the id of the member.
   * @return the member that was leveled up.
   * @throws BizException   when the role of the given id isn't allowed to confirm as assistant.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the member wasn't found or problem with query.
   */
  MemberDTO updateRoleAsAssistant(int id);

  /**
   * Insert the new date of availability of one of the staff or responsable.
   *
   * @param dateAvailable the availability of one of the staff.
   * @return the date of availability.
   * @throws BizException   when problem occurs in the insert into the database.
   * @throws FatalException when problem occurs in DAO.
   */
  Availability addAvailability(Availability dateAvailable);

  /**
   * Get the person who offered the object.
   *
   * @param id the id of the object.
   * @return the information of the person.
   */
  MemberDTO getOfferer(int id);


  /**
   * Edit the profile of the user.
   *
   * @param user the user that contain the new data of the user.
   * @return the user with new data.
   */
  MemberDTO editMyProfile(MemberDTO user);


  /**
   * Search for a member by name and/or role.
   *
   * @param name the firstname or lastname of the member.
   * @param role "Y" if the member is aidant, "N" if he is a plain member and "0" for both.
   * @return a list of all members who match the criterias provided.
   */
  List<MemberDTO> search(String name, String role);

  /**
   * Gets the list of all the notifications of the member.
   *
   * @param idMember the id of the member
   * @return the list of the notifications of the member
   */
  List<NotificationDTO> allNotificationsMember(int idMember);

  /**
   * Gets the list of unread of notifications of the member.
   *
   * @param idMember the id of the member
   * @return the list of unread notifications of the member
   */
  List<NotificationDTO> unreadNotificationsMember(int idMember);

}