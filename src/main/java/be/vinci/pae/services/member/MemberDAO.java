package be.vinci.pae.services.member;

import be.vinci.pae.domain.availability.Availability;
import java.util.List;

/**
 * the interface of UserDAO.
 *
 * @author Loubna ELjattari
 */
public interface MemberDAO {

  /**
   * Get a user by their email .
   *
   * @param email the user's email
   * @return the user with the specified email
   */
  MemberDTO getMemberByEmail(String email);

  /**
   * gets user from id for refresh and remember me.
   *
   * @param id the user's ID
   * @return the requested user's data
   */
  MemberDTO getMemberByID(int id);

  /**
   * Registers a new user in the database.
   *
   * @param user the UserDTO object representing the user to register.
   * @return the registered UserDTO object if registration was successful, null otherwise.
   */
  MemberDTO register(MemberDTO user);

  /**
   * Retrieves all members stored in the Ressourcerie's database.
   *
   * @return A list of members DTOs representing all registered users.
   */
  List<MemberDTO> getAllMembers();

  /**
   * confirme inscription of the member of staff.
   *
   * @param user the member to be confirmed as a staff.
   * @return returns true if the member has been confirmed as a helper and an error if not.
   */
  MemberDTO updateRole(MemberDTO user);

  /**
   * Get the person who offered the object.
   *
   * @param id the id of the object.
   * @return the information of the person.
   */
  MemberDTO getOfferer(int id);

  /**
   * Edit the profile of the user in DB and set new user data object.
   *
   * @param user the user that contain the new data of the user.
   * @return the user with new data.
   */
  MemberDTO editMyProfile(MemberDTO user);

  /**
   * put the object in state 'Withdrawn'.
   *
   * @param availability the availability to insert into the database.
   * @return the id of the new insert
   */
  int addAvailability(Availability availability);

}
