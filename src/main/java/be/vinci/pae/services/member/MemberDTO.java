package be.vinci.pae.services.member;

import be.vinci.pae.domain.member.MemberImpl;
import be.vinci.pae.domain.picture.Picture;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.time.LocalDate;

/**
 * interface UserDTO.
 *
 * @author Chehrazad Ouazzani
 */
@JsonDeserialize(as = MemberImpl.class)
public interface MemberDTO {


  /**
   * getter of picture url.
   *
   * @return the picture url.
   */
  Picture getPicture();

  /**
   * setter of picture.
   *
   * @param picture the picture.
   */
  void setPicture(Picture picture);

  /**
   * getter of id.
   *
   * @return the id.
   */
  int getId();

  /**
   * setter of id.
   *
   * @param id the user's id.
   */
  void setId(int id);

  /**
   * getter of version.
   *
   * @return the version.
   */
  int getVersion();

  /**
   * setter of the version.
   *
   * @param version the version.
   */
  void setVersion(int version);

  /**
   * getter of firstname.
   *
   * @return the firstname.
   */
  @JsonIgnore
  String getFirstName();

  /**
   * Setter of firstname.
   *
   * @param firstname the first name to give to the user.
   */
  void setFirstName(String firstname);

  /**
   * getter of lastname.
   *
   * @return the lastname.
   */
  @JsonIgnore
  String getLastName();

  /**
   * setter of lastname.
   *
   * @param lastname the last name to give to the user.
   */
  void setLastName(String lastname);

  /**
   * getter of email.
   *
   * @return the email.
   */
  String getEmail();

  /**
   * setter of email.
   *
   * @param mail the email received
   */
  void setEmail(String mail);

  /**
   * getter of the encrypted password.
   *
   * @return the encrypted password.
   */
  String getEncryptedPassword();

  /**
   * setter of password encrypted.
   *
   * @param passwordMember the encrypted password received
   */
  void setEncryptedPassword(String passwordMember);

  /**
   * getter of role.
   *
   * @return the role.
   */
  String getRole();

  /**
   * setter of role as a string.
   *
   * @param roleMember the role of the user.
   */
  void setRole(String roleMember);

  /**
   * setter of role as enum.
   *
   * @param roleMember the enum of type role of the user.
   */
  void setRole(RolesRessourceries roleMember);

  /**
   * getter of phone number.
   *
   * @return a string containing the phone number.
   */
  String getPhoneNumber();

  /**
   * setter of phoneNumber.
   *
   * @param phoneNumber the user's phone number
   */
  void setPhoneNumber(String phoneNumber);

  /**
   * getter of subscribe date.
   *
   * @return to subscribe date.
   */
  LocalDate getRegistrationDate();

  /**
   * setter of subscribeDate.
   *
   * @param registrationDate the date of the user's subscription.
   */
  void setRegistrationDate(LocalDate registrationDate);

  /**
   * enum of the role in the Ressourcerie.
   */
  enum RolesRessourceries {
    /**
     * Responsable role.
     */
    RESPONSABLE("R"),

    /**
     * staff role: 'A' for aidant.
     */
    STAFF("A"),

    /**
     * public role.
     */
    PUBLIC("P"),

    /**
     * member role: a user who is connected.
     */
    MEMBER("M");

    private String role;

    /**
     * constructor of the enum.
     *
     * @param role the role.
     */
    RolesRessourceries(String role) {
      this.role = role;
    }

    /**
     * get of the role.
     *
     * @return the role.
     */
    public String getRole() {
      return role;
    }
  }

}
