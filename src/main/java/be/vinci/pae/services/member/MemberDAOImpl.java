package be.vinci.pae.services.member;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.exceptions.OptimisticException;
import be.vinci.pae.services.dal.DalBackendServices;
import jakarta.inject.Inject;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * the implementation of the interface UserDAO.
 *
 * @author Loubna Eljattari
 */
public class MemberDAOImpl implements MemberDAO {

  @Inject
  private Factory myFactory;
  @Inject
  private DalBackendServices dalBackendServices;

  /**
   * Get a member by their email .
   *
   * @param email the user's email
   * @return the user with the specified email
   */
  @Override
  public MemberDTO getMemberByEmail(String email) {
    MemberDTO user = myFactory.getUser();
    String query = "SELECT m.*, "
        + "p.id_picture, p.url, p.object_id, p.member, p.version_number AS picture_version_number "
        + "FROM ressourcerie.members m, ressourcerie.pictures p "
        + "WHERE email = ? AND m.id_member = p.member";

    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, email);
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        fillMember(user, rs);
        return user;
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e);
    }
  }

  /**
   * gets user from id for refresh and remember me.
   *
   * @param id the user's ID
   * @return the requested user's data
   */
  public MemberDTO getMemberByID(int id) {
    MemberDTO user = myFactory.getUser();
    String query = "SELECT m.*, p.id_picture, p.url, p.object_id, p.member, "
        + "p.version_number AS picture_version_number "
        + "FROM ressourcerie.members m, ressourcerie.pictures p "
        + "WHERE m.id_member = ? AND m.id_member = p.member";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, id);
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        fillMember(user, rs);
        return user;
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e.getCause());
    }

  }


  /**
   * Sets the user's data in the user object.
   *
   * @param user whose data is to be filled
   * @param rs   the data to fill the user with
   * @throws SQLException if there's an SQL exception.
   */
  private void fillMember(MemberDTO user, ResultSet rs) throws SQLException {
    user.setId(rs.getInt("id_member"));
    user.setEmail(rs.getString("email"));
    user.setEncryptedPassword(rs.getString("password_member"));
    user.setFirstName(rs.getString("firstname"));
    user.setLastName(rs.getString("lastname"));
    user.setRole(rs.getString("role_member"));
    user.setPhoneNumber(rs.getString("phone_number"));
    user.setRegistrationDate(rs.getDate("registration_date").toLocalDate());
    Picture picture = myFactory.getPicture();
    picture.setUrl(rs.getString("url"));
    picture.setId(rs.getInt("id_picture"));
    picture.setMemberId(rs.getInt("member"));
    picture.setVersion(rs.getInt("picture_version_number"));
    user.setPicture(picture);
    user.setVersion(rs.getInt("version_number"));
  }

  /**
   * Register a new member in the database.
   *
   * @param member the UserDTO object representing the member to register.
   * @return the registered UserDTO object if registration was successful, null otherwise.
   */
  public MemberDTO register(MemberDTO member) {
    String query = "INSERT INTO ressourcerie.members (lastname, firstname, email, password_member, "
        + "registration_date, role_member, phone_number, version_number) VALUES (?,?,?,?,?,?,?,?)";
    try (PreparedStatement ps = dalBackendServices.getPreparedStatementRGK(query)) {
      ps.setString(1, member.getLastName());
      ps.setString(2, member.getFirstName());
      ps.setString(3, member.getEmail());
      ps.setString(4, member.getEncryptedPassword());
      ps.setDate(5, Date.valueOf(member.getRegistrationDate()));
      ps.setString(6, member.getRole());
      ps.setString(7, member.getPhoneNumber());
      ps.setInt(8, member.getVersion());

      int executeLine = ps.executeUpdate();
      if (executeLine == 0) {
        checkOptimisticOrUnfoundError(member);
      }

      try (ResultSet rs = ps.getGeneratedKeys()) {
        if (rs.next()) {
          member.setId(rs.getInt(1));
        }
      }

      return member;
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e.getCause());
    }
  }

  /**
   * Retrieves all members stored in the Ressourcerie's database.
   *
   * @return A list of members DTOs representing all registered users.
   */
  @Override
  public List<MemberDTO> getAllMembers() {
    List<MemberDTO> memberDTOList = new ArrayList<>();
    String query = "SELECT m.*, "
        + "p.id_picture, p.url, p.object_id, p.member, p.version_number AS picture_version_number "
        + "FROM ressourcerie.pictures p, ressourcerie.members m "
        + "WHERE m.id_member = p.member";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          MemberDTO memberDto = myFactory.getUser();
          fillMember(memberDto, rs);
          memberDTOList.add(memberDto);
        }
      }
      return memberDTOList;
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e.getCause());
    }
  }

  /**
   * Confirm a member to a staff member. Change the role from member to staff member. Give the
   * access to the new staff member to a more options.
   *
   * @param member the member to be confirmed as a staff.
   * @return the member that has been confirmed as a member of the staff.
   */
  @Override
  public MemberDTO updateRole(MemberDTO member) {
    String query = "UPDATE ressourcerie.members SET role_member = ?, "
        + "VERSION_NUMBER = VERSION_NUMBER + 1 WHERE id_member = ? AND VERSION_NUMBER = ? ";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, member.getRole());
      ps.setInt(2, member.getId());
      ps.setInt(3, member.getVersion());
      int updateRows = ps.executeUpdate();
      if (updateRows == 0) {
        checkOptimisticOrUnfoundError(member);
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e.getCause());
    }
    return member;
  }

  /**
   * put the availability of the staff and responsable into the database.
   *
   * @param availability the availability to insert into the database.
   * @return the id of the new insert or -1 if problem occurs.
   */
  public int addAvailability(Availability availability) {
    String query = "INSERT INTO ressourcerie.availabilities(availability, time_slot) "
        + "VALUES (?, ?) RETURNING id_availability";
    int newId;

    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setDate(1, Date.valueOf(availability.getDate()));
      ps.setString(2, availability.getTimeSlot());

      try (ResultSet rs = ps.executeQuery()) {
        if (rs.next()) {
          newId = rs.getInt("id_availability");
          return newId;
        } else {
          return -1;
        }
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to insert into the database",
          e.getCause());
    }
  }

  /**
   * Get the member who offered the object in the database.
   *
   * @param id the id of the object.
   * @return the information of the person.
   */
  @Override
  public MemberDTO getOfferer(int id) {
    MemberDTO user;
    String query = "SELECT member FROM ressourcerie.objects WHERE id_object = ? ";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, id);
      ResultSet rs = ps.executeQuery();
      if (!rs.next()) {
        return null;
      }
      user = getMemberByID(rs.getInt(1));
      return user;
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e.getCause());
    }
  }

  /**
   * Edit the profile of the member in DB and set new member data object.
   *
   * @param member the member that contain the new data of the member.
   * @return the member with new data.
   */
  @Override
  public MemberDTO editMyProfile(MemberDTO member) {
    String query = "UPDATE ressourcerie.members SET lastname = ? , firstname = ?"
        + ", email = ?, phone_number = ?, password_member = ?, VERSION_NUMBER = VERSION_NUMBER + 1"
        + " where id_member = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, member.getLastName());
      ps.setString(2, member.getFirstName());
      ps.setString(3, member.getEmail());
      ps.setString(4, member.getPhoneNumber());
      ps.setString(5, member.getEncryptedPassword());
      ps.setInt(6, member.getId());
      ps.setInt(7, member.getVersion());

      if (ps.executeUpdate() == 0) {
        checkOptimisticOrUnfoundError(member);
      }
      return member;
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database", e.getCause());
    }
  }

  private void checkOptimisticOrUnfoundError(MemberDTO member) {
    if (isInvalidVersion(member.getId(), member.getVersion())) {
      throw new OptimisticException();
    } else if (isUnfound(member.getId())) {
      throw new FatalException();
    }
  }

  private boolean isInvalidVersion(int idMember, int version) {
    String query = "SELECT * FROM RESSOURCERIE.MEMBERS WHERE version_number = ? AND ID_MEMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, version);
      ps.setInt(2, idMember);
      ResultSet res = ps.executeQuery();
      return !res.next();
    } catch (SQLException e) {
      throw new FatalException("problem occurs with the version", e.getCause());
    }
  }

  private boolean isUnfound(int idMember) {
    String query = "SELECT * FROM RESSOURCERIE.MEMBERS WHERE ID_MEMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, idMember);
      ResultSet res = ps.executeQuery();
      return !res.next();
    } catch (SQLException e) {
      throw new FatalException("problem occurs with isUnfound.", e.getCause());
    }
  }
}
