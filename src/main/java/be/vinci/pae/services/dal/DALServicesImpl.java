package be.vinci.pae.services.dal;

import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.utils.Config;
import be.vinci.pae.utils.Logs;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 * the implementation of the interface DalService.
 *
 * @author Loubna Eljattari
 */
public class DALServicesImpl implements DalBackendServices, DalServices {

  static {
    Config.load("dev.properties");
  }

  private static final String url = Config.getProperty("DatabaseUrl");
  private static final String user = Config.getProperty("DatabaseUser");
  private static final String password = Config.getProperty("DatabasePassword");
  private BasicDataSource dataSource;
  private ThreadLocal<Connection> connectionHolder;
  private static final Logs logger = new Logs();


  {
    dataSource = new BasicDataSource();
    connectionHolder = new ThreadLocal<Connection>();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUrl(url);
    dataSource.setUsername(user);
    dataSource.setPassword(password);
  }

  /**
   * The method receives a request, then it manages the preparation of this request.
   *
   * @param query The query to prepare
   * @return returns a prepared statement
   */
  @Override
  public PreparedStatement getPrepareStatement(String query) {
    PreparedStatement ps;
    Connection connection = connectionHolder.get();
    try {
      ps = connection.prepareStatement(query);
    } catch (SQLException e) {
      throw new FatalException("Error while preparing the PreparedStatement.", e.getCause());
    }
    return ps;
  }

  @Override
  public PreparedStatement getPreparedStatementRGK(String query) {

    PreparedStatement ps;
    Connection connection = connectionHolder.get();
    try {
      ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
    } catch (SQLException e) {
      //logger.log(Level.SEVERE, e.getMessage());
      throw new FatalException("Error while preparing the PreparedStatement.", e.getCause());
    }
    return ps;
  }

  @Override
  public void start() {
    Connection connection = null;
    try {
      connection = dataSource.getConnection();
      connection.setAutoCommit(false);
      connectionHolder.set(connection);
    } catch (SQLException e) {
      if (connection != null) {
        try {
          connection.close();
        } catch (SQLException ex) {
          logger.error("Error while closing the connection in start()");
        }
      }
      connectionHolder.remove();
      connectionHolder.set(null);
      throw new FatalException("Error while starting a connection into the database", e);
    }
  }


  @Override
  public void commit() {
    Connection connection = connectionHolder.get();
    if (connection != null) {
      try {
        connection.commit();
      } catch (SQLException e) {
        throw new FatalException("Error during commit", e);
      } finally {
        try {
          connection.close();
        } catch (SQLException ex) {
          logger.error("Error while closing the connection in commit()");
        } finally {
          connectionHolder.remove();
          connectionHolder.set(null);
        }
      }
    }
  }

  @Override
  public void rollback() {
    Connection connection = connectionHolder.get();
    try {
      connection.rollback();
      connection.setAutoCommit(true);
    } catch (SQLException e) {
      throw new FatalException("Error during rollback", e);
    } finally {
      try {
        connection.close();
      } catch (SQLException ex) {
        logger.error("Error while closing the connection in rollback()");
      } finally {
        connectionHolder.remove();
        connectionHolder.set(null);
      }
    }
  }

}
