package be.vinci.pae.services.dal;

/**
 * the interface that manages the transaction.
 */
public interface DalServices {

  /**
   * Start the transaction.
   */
  void start();

  /**
   * Commit the transaction.
   */
  void commit();

  /**
   * Rollback the transaction.
   */
  void rollback();
}
