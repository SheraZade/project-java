package be.vinci.pae.services.dal;

import java.sql.PreparedStatement;

/**
 * the interface of DalService.
 *
 * @author Loubna ELjattari
 */
public interface DalBackendServices {

  /**
   * The method receives a request, then it manages the preparation of this request.
   *
   * @param query The query to prepare
   * @return returns a prepared statement
   */
  PreparedStatement getPrepareStatement(String query);

  /**
   * Create a prepared statement with option RETURN GENERATED KEYS enabeled.
   *
   * @param query the query.
   * @return a prepared statement.
   */
  PreparedStatement getPreparedStatementRGK(String query);

}
