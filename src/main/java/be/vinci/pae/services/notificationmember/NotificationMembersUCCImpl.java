package be.vinci.pae.services.notificationmember;

import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.notificationmember.NotificationMember;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.dal.DalServices;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.member.MemberDTO.RolesRessourceries;
import be.vinci.pae.services.member.MemberUCC;
import be.vinci.pae.services.notification.NotificationDAO;
import jakarta.inject.Inject;
import java.util.List;

/**
 * the implementation of the interface NotificationMembersUCC.
 *
 * @author Loubna
 */
public class NotificationMembersUCCImpl implements NotificationMembersUCC {

  @Inject
  DalServices dalServices;

  @Inject
  NotificationMembersDAO notificationsMemberDAO;

  @Inject
  NotificationDAO notificationDAO;
  @Inject
  MemberUCC memberUCC;

  @Inject
  Factory myFactory;


  /**
   * Updates the database to mark a notification as read.
   *
   * @param idNotification the notification to be marked as read.
   * @return true if the notification was successfully marked as read, false otherwise.
   * @throws FatalException if there is a problem accessing the database.
   */
  @Override
  public boolean markNotificationAsRead(int idNotification) {
    try {
      dalServices.start();
      List<NotificationMembersDTO> listAllNotificationMembersDTO = notificationsMemberDAO
          .getAllNotificationMembers(idNotification);
      if (listAllNotificationMembersDTO == null) {
        throw new DAOException("The id notification member was not found to marked them as read");
      }

      for (NotificationMembersDTO notificationMembersDTO : listAllNotificationMembersDTO) {
        notificationsMemberDAO.markNotificationAsRead(notificationMembersDTO);
      }

      dalServices.commit();
      return true;
    } catch (FatalException e) {
      dalServices.rollback();
      throw new FatalException("Fatal error occurred while updating the notification as read", e);
    }
  }


  /**
   * get the notification Members with id.
   *
   * @param id of the notification members
   * @return a list of NotificationMembersDTO with the requested notification's information.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the object is not found.
   */
  @Override
  public List<NotificationMembersDTO> getNotificationMembers(int id) {
    try {
      dalServices.start();
      List<NotificationMembersDTO> notification =
          notificationsMemberDAO.getAllNotificationMembers(id);
      if (notification == null) {
        throw new DAOException("The notification doesn't exist.");
      }
      dalServices.commit();
      return notification;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Get all the Notification of a person.
   *
   * @param id the id of the member.
   * @return return a list of theirs  notifications.
   */
  @Override
  public List<NotificationMembersDTO> getAllNotificationsOfMember(int id) {
    try {
      dalServices.start();
      List<NotificationMembersDTO> list = notificationsMemberDAO
          .allNotifications()
          .stream()
          .filter(notificationMembersDTO -> notificationMembersDTO.getMember() == id)
          .toList();
      dalServices.commit();
      return list;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }


  /**
   * Adds a notification for all staff members.
   *
   * @param idNotification the id of the notification.
   * @return the id of the new notificationMember injected into the database.
   * @throws FatalException if a SQLException is thrown during the execution of the method.
   * @throws DAOException   if problem occurs in the database.
   */
  public int addNotificationToStaffMembers(int idNotification) {
    try {
      int id = 0;
      List<MemberDTO> memberDTOList = memberUCC
          .getAllMember()
          .stream()
          .filter(member -> member.getRole().contains(RolesRessourceries.RESPONSABLE.getRole()))
          .toList();

      dalServices.start();

      for (MemberDTO memberDTO : memberDTOList) {
        NotificationMember notificationMember = myFactory.getMembersNotification();
        notificationMember.setNotificationMember(idNotification, memberDTO.getId());

        id = notificationsMemberDAO.addNotificationMember(
            notificationMember.getIdNotification(),
            notificationMember.getMember());
        if (id == -1) {
          throw new DAOException(
              "Problem occurs while inserting notificationMember into the database.");

        }
        notificationMember.setIdNotificationMember(id);
      }
      dalServices.commit();
      return id;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Adds a notification for the member who made an offer.
   *
   * @param idNotification the id of the notification.
   * @param idMember       the id of the member who made the offer
   * @return the id of the new notificationMember injected into the database.
   * @throws FatalException if a SQLException is thrown during the execution of the method.
   */
  @Override
  public int addNotificationForMember(int idNotification, int idMember) {
    int id;
    try {
      dalServices.start();
      id = notificationsMemberDAO.addNotificationMember(idNotification, idMember);
      if (id == -1) {
        throw new DAOException(
            "problem occur when we trying to "
                + "execute update query of  addNotificationMember in DAO ");
      }
      dalServices.commit();
      return id;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

}
