package be.vinci.pae.services.notificationmember;

import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import java.util.List;

/**
 * the interface NotificationMembersUCC.
 *
 * @author Loubna
 */
public interface NotificationMembersUCC {

  /**
   * Mark the notification as read.
   *
   * @param idNotification The notification to be marked as read.
   * @return true if the notification was successfully marked as read, false otherwise.
   * @throws FatalException if there is a problem accessing the database.
   */
  boolean markNotificationAsRead(int idNotification);

  /**
   * get the notification Members with id.
   *
   * @param id of the notification members
   * @return a list NotificationMembersDTO with the requested notification's information.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the object is not found.
   */
  List<NotificationMembersDTO> getNotificationMembers(int id);

  /**
   * Get all the Notification of a person.
   *
   * @param id the id of the member.
   * @return return a list of theirs  notifications.
   */
  List<NotificationMembersDTO> getAllNotificationsOfMember(int id);


  /**
   * Adds a notification for all staff members.
   *
   * @param idNotification the id of the notification.
   * @return the id of the new notificationMember injected into the database.
   * @throws FatalException if a SQLException is thrown during the execution of the method.
   */
  int addNotificationToStaffMembers(int idNotification);

  /**
   * Adds a notification for the member who made an offer.
   *
   * @param idNotification the id of the notification.
   * @param idMember       the id of the member.
   * @return the id of the new notificationMember injected into the database.
   * @throws FatalException if a SQLException is thrown during the execution of the method.
   */
  int addNotificationForMember(int idNotification, int idMember);
}
