package be.vinci.pae.services.notificationmember;

import be.vinci.pae.domain.notificationmember.NotificationMemberImpl;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * interface NotificationMembersDTO.
 *
 * @author Loubna
 */
@JsonDeserialize(as = NotificationMemberImpl.class)
public interface NotificationMembersDTO {


  /**
   * Getter for id.
   *
   * @return the id of the notification member.
   */
  int getIdNotificationMember();

  /**
   * setter for id.
   *
   * @param id the id of the notification to set
   */
  void setIdNotificationMember(int id);

  /**
   * Getter for notification.
   *
   * @return the id of the notification.
   */
  int getIdNotification();

  /**
   * setter for notification.
   *
   * @param idNotification the id notification to set.
   */
  void setIdNotification(int idNotification);

  /**
   * getter the id of the member.
   *
   * @return the id of the member.
   */
  int getMember();

  /**
   * setter of the id member.
   *
   * @param idMember the id member to set.
   */
  void setMember(int idMember);

  /**
   * getter for isRead.
   *
   * @return the notification.
   */
  boolean isIsRead();

  /**
   * setter for isRead.
   *
   * @param isRead parameter boolean to set isRead variable.
   */
  void setIsRead(boolean isRead);

  /**
   * Get the version.
   *
   * @return the version
   */
  int getVersion();

  /**
   * Set the version.
   *
   * @param version the new value of version
   */
  void setVersion(int version);


}
