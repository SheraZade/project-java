package be.vinci.pae.services.notificationmember;

import be.vinci.pae.exceptions.FatalException;
import java.util.List;

/**
 * the interface NotificationMembersDAO.
 *
 * @author Loubna
 */
public interface NotificationMembersDAO {

  /**
   * Updates the database to mark a notificationMember as read.
   *
   * @param notificationMember The notification member to be marked as read.
   * @throws FatalException if there is a problem accessing the database.
   */
  void markNotificationAsRead(NotificationMembersDTO notificationMember);

  /**
   * Get the list of notifications of members.
   *
   * @param idNotification the idNotification.
   * @return the notification.
   */
  List<NotificationMembersDTO> getAllNotificationMembers(int idNotification);

  /**
   * Get all the Notification of a person.
   *
   * @return return a list of theirs  notifications.
   */
  List<NotificationMembersDTO> allNotifications();

  /**
   * Add a notification for a member.
   *
   * @param idNotification the id of the notification
   * @param idMember       the id of the member
   * @return the id of the new notification member
   */
  int addNotificationMember(int idNotification, int idMember);
}
