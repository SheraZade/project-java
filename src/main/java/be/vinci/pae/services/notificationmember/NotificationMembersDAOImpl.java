package be.vinci.pae.services.notificationmember;

import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.exceptions.OptimisticException;
import be.vinci.pae.services.dal.DalBackendServices;
import jakarta.inject.Inject;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * the implementation of the interface NotificationMembersDAO.
 *
 * @author Loubna
 */
public class NotificationMembersDAOImpl implements NotificationMembersDAO {
  @Inject
  private DalBackendServices dalBackendServices;
  @Inject
  private Factory myFactory;

  /**
   * Updates the database to mark a notification as read.
   *
   * @param notificationMember the notification to be marked as read.
   * @throws FatalException if there is a problem accessing the database.
   */
  @Override
  public void markNotificationAsRead(NotificationMembersDTO notificationMember) {
    String query = "UPDATE ressourcerie.notifications_members SET is_read = true, "
        + " VERSION_NUMBER = VERSION_NUMBER + 1 "
        + "WHERE notification = ? AND member = ? AND version_number = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, notificationMember.getIdNotification());
      ps.setInt(2, notificationMember.getMember());
      ps.setInt(3, notificationMember.getVersion());
      int updateRows = ps.executeUpdate();
      if (updateRows == 0) {
        checkOptimisticOrUnfoundError(notificationMember);
      }
    } catch (SQLException e) {
      throw new FatalException("Problem occurs when we trying to access database", e.getCause());
    }
  }


  /**
   * get the notification members with idNotification.
   *
   * @param idNotification of the notification members
   * @return a list of notificationMembersDTO with the requested notification's information.
   */
  @Override
  public List<NotificationMembersDTO> getAllNotificationMembers(int idNotification) {
    String query = "SELECT * FROM ressourcerie.notifications_members WHERE notification = ?";
    List<NotificationMembersDTO> listAllNotificationsMembers = new ArrayList<>();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, idNotification);
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          NotificationMembersDTO notificationMember = myFactory.getMembersNotification();
          fillNotificationMember(notificationMember, rs);
          listAllNotificationsMembers.add(notificationMember);
        }
      }
      return listAllNotificationsMembers;
    } catch (SQLException e) {
      throw new FatalException("error not found object with the specific idNotification "
          + "to get all notifications of members", e.getCause());
    }
  }

  @Override
  public List<NotificationMembersDTO> allNotifications() {
    String query = "SELECT * FROM ressourcerie.notifications_members ";
    List<NotificationMembersDTO> list = new ArrayList<>();

    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        do {
          NotificationMembersDTO notificationMembersDTO = myFactory.getMembersNotification();
          fillNotificationMember(notificationMembersDTO, rs);
          list.add(notificationMembersDTO);
        } while (rs.next());
      }

    } catch (SQLException e) {
      throw new FatalException("error not found object with the specific id", e.getCause());
    }
    return list;
  }

  /**
   * fill notification members passed in parameter by the resultSet in parameters.
   *
   * @param notificationMembersDTO whose data is to be filled.
   * @param rs                     the data to fill the object with.
   */
  private void fillNotificationMember(NotificationMembersDTO notificationMembersDTO, ResultSet rs)
      throws SQLException {
    notificationMembersDTO.setIdNotificationMember(rs.getInt("id_notifier_member"));
    notificationMembersDTO.setIsRead(rs.getBoolean("is_read"));
    notificationMembersDTO.setMember(rs.getInt("member"));
    notificationMembersDTO.setIdNotification(rs.getInt("notification"));
    notificationMembersDTO.setVersion(rs.getInt("version_number"));
  }

  /**
   * Create a notification for all staff member.
   *
   * @param idNotification the id of the notification
   * @param idMember       the id of the member
   * @throws FatalException if a SQLException is thrown during the execution of the method.
   */
  @Override
  public int addNotificationMember(int idNotification, int idMember) {
    String query = "INSERT INTO ressourcerie.notifications_members "
        + "(notification, member, is_read, version_number) VALUES (?, ?, ?, ?)";

    try (PreparedStatement ps = dalBackendServices.getPreparedStatementRGK(query)) {
      ps.setInt(1, idNotification);
      ps.setInt(2, idMember);
      ps.setBoolean(3, false);
      ps.setInt(4, 1);

      int executeLine = ps.executeUpdate();
      if (executeLine == 0) {
        return -1;
      }
      try (ResultSet rs = ps.getGeneratedKeys()) {
        if (rs.next()) {
          return rs.getInt(1);
        }
      }
      throw new FatalException(
          "Problem occurred while attempting to get the new id of the new notification member");
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
  }

  private void checkOptimisticOrUnfoundError(NotificationMembersDTO notificationMembersDTO) {
    if (isInvalidVersion(notificationMembersDTO.getIdNotificationMember(),
        notificationMembersDTO.getVersion())) {
      throw new OptimisticException();
    } else if (isUnfound(notificationMembersDTO.getIdNotificationMember())) {
      throw new FatalException();
    }
  }

  private boolean isInvalidVersion(int idNotificationMember, int version) {
    String query = "SELECT * FROM RESSOURCERIE.NOTIFICATIONS_MEMBERS WHERE version_number = ? "
        + "AND ID_NOTIFIER_MEMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, version);
      ps.setInt(2, idNotificationMember);
      ResultSet res = ps.executeQuery();
      return !res.next();
    } catch (SQLException e) {
      throw new FatalException("problem occurs with the version into the notification member",
          e.getCause());
    }
  }

  private boolean isUnfound(int idNotificationMember) {
    String query = "SELECT * FROM RESSOURCERIE.NOTIFICATIONS_MEMBERS WHERE ID_NOTIFIER_MEMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, idNotificationMember);
      ResultSet res = ps.executeQuery();
      return !res.next();
    } catch (SQLException e) {
      throw new FatalException("problem occurs the notification member, unfound.", e.getCause());
    }
  }
}


