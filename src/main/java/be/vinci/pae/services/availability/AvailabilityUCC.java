package be.vinci.pae.services.availability;

import be.vinci.pae.domain.availability.Availability;
import java.util.List;

/**
 * The availabilityUCC.
 */
public interface AvailabilityUCC {

  /**
   * Gets all the availabilities from database.
   *
   * @return the list of availabilities
   */
  List<AvailabilityDTO> getAllAvailabilities();

  /**
   * Add a new availability if not existing in database, nothing if not.
   *
   * @param dateAvailable the availablity to insert in database
   * @return the availability with its new id in the database
   */
  AvailabilityDTO addAvailability(Availability dateAvailable);

  /**
   * Gets the ID of an availability by it's date and time slot.
   *
   * @param a the availability with it's date and time slot.
   * @return the id of the availability or -1 if it does not exist.
   */
  int getAvailabilityId(Availability a);
}
