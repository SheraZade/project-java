package be.vinci.pae.services.availability;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.dal.DalBackendServices;
import jakarta.inject.Inject;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of Availability interface.
 */
public class AvailabilityDAOImpl implements AvailabilityDAO {

  @Inject
  private Factory myFactory;
  @Inject
  private DalBackendServices dalBackendServices;


  private void fillAvailability(AvailabilityDTO availabilityDTO, ResultSet rs) throws SQLException {
    availabilityDTO.setId(rs.getInt("id_availability"));
    availabilityDTO.setDate(rs.getDate("availability").toLocalDate());
    availabilityDTO.setTimeSlot(rs.getString("time_slot"));

  }

  /**
   * Gets all the availabilities from database.
   *
   * @return the list of availabilities
   */
  public List<AvailabilityDTO> getAllAvailabilities() {
    List<AvailabilityDTO> listObject = new ArrayList<>();
    String query = "SELECT * FROM RESSOURCERIE.availabilities";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        do {
          AvailabilityDTO dispoDTO = myFactory.getAvailability();
          fillAvailability(dispoDTO, rs);
          listObject.add(dispoDTO);
        } while (rs.next());
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
    return listObject;
  }

  /**
   * get the ID for a known availability.
   *
   * @param availability the availability.
   * @return the id of the availability found, -1 if not.
   */
  public boolean isNotExistantInDb(Availability availability) {
    String query = "SELECT * FROM ressourcerie.availabilities "
        + "WHERE availability = DATE(?) AND time_slot = ?;";

    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setDate(1, Date.valueOf(availability.getDate()));
      ps.setString(2, availability.getTimeSlot());
      try (ResultSet rs = ps.executeQuery()) {
        return !rs.next();
      }
    } catch (SQLException exc) {
      throw new FatalException(exc.getMessage(), exc);
    }
  }

  /**
   * put the availability of the staff and responsable into the database.
   *
   * @param availability the availability to insert into the database.
   * @return the id of the new insert or -1 if problem occurs.
   */
  public AvailabilityDTO addAvailability(Availability availability) {
    String query = "INSERT INTO ressourcerie.availabilities(availability, time_slot) VALUES (?, ?)";

    try (PreparedStatement ps = dalBackendServices.getPreparedStatementRGK(query)) {
      ps.setDate(1, Date.valueOf(availability.getDate()));
      ps.setString(2, availability.getTimeSlot());
      int affectedRows = ps.executeUpdate();
      if (affectedRows == 0) {
        return null;
      }

      try (ResultSet rs = ps.getGeneratedKeys()) {
        if (rs.next()) {
          availability.setId(rs.getInt("id_availability"));
          return availability;
        }
        return null;
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to insert into the database",
          e.getCause());
    }
  }

  /**
   * get the ID for a known availability.
   *
   * @param availability the availability.
   * @return the id of the availability found, -1 if not.
   */
  public int getAvailability(Availability availability) {
    int availabilityID;
    String query = "SELECT id_availability FROM ressourcerie.availabilities "
        + "WHERE availability = DATE(?) AND time_slot = ?;";

    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setDate(1, Date.valueOf(availability.getDate()));
      ps.setString(2, availability.getTimeSlot());
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return -1;
        }
        availabilityID = rs.getInt("id_availability");
      }
    } catch (SQLException exc) {
      throw new FatalException(exc.getMessage(), exc);
    }
    return availabilityID;
  }
}
