package be.vinci.pae.services.availability;

import be.vinci.pae.domain.availability.Availability;
import java.util.List;

/**
 * The DAO of Availability.
 */
public interface AvailabilityDAO {

  /**
   * Gets all the availabilities from database.
   *
   * @return the list of availabilities
   */
  List<AvailabilityDTO> getAllAvailabilities();

  /**
   * Add a new availability if not existing in database, nothing if not.
   *
   * @param availability the availablity to insert in database
   * @return the availability with its new id in the database
   */
  AvailabilityDTO addAvailability(Availability availability);

  /**
   * Check if the given availability is existing in database.
   *
   * @param availability the availability to check.
   * @return true if not existing, false if it is existing.
   */
  boolean isNotExistantInDb(Availability availability);

  /**
   * Search for the id of the given availability.
   *
   * @param availability the availability to look for in the database.
   * @return the id of the availability found, -1 if not.
   */
  int getAvailability(Availability availability);

}
