package be.vinci.pae.services.availability;

import be.vinci.pae.domain.availability.AvailabilityImpl;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.time.LocalDate;

/**
 * interface AvailabilityDTO.
 *
 * @author Chehrazad Ouazzani
 */
@JsonDeserialize(as = AvailabilityImpl.class)
public interface AvailabilityDTO {

  /**
   * Getter of the id of the availability.
   *
   * @return the id
   */
  int getId();

  /**
   * Setter of the id of the availability.
   *
   * @param id the new value of the id.
   */
  void setId(int id);

  /**
   * Getter for the Availability's date.
   *
   * @return the date of the Availability.
   */
  LocalDate getDate();

  /**
   * Setter for Availability's date.
   *
   * @param date the date of the availability.
   */
  void setDate(LocalDate date);

  /**
   * Getter for the Availability's shift.
   *
   * @return AM for the morning or PM for the afternoon.
   */
  String getTimeSlot();

  /**
   * Setter for the Availability's shift.
   *
   * @param timeSlot the time slot.
   */
  void setTimeSlot(String timeSlot);
}
