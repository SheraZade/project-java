package be.vinci.pae.services.availability;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.dal.DalServices;
import jakarta.inject.Inject;
import java.util.List;

/**
 * The implementation of AvailabilityUCC.
 *
 * @author Chehrazad Ouazzani.
 */
public class AvailabilityUCCImpl implements AvailabilityUCC {

  @Inject
  AvailabilityDAO availabilityDAO;

  @Inject
  DalServices dalServices;


  /**
   * Gets all the availabilities from database.
   *
   * @return the list of availabilities
   */
  @Override
  public List<AvailabilityDTO> getAllAvailabilities() {
    try {
      dalServices.start();
      List<AvailabilityDTO> listObjects = availabilityDAO.getAllAvailabilities();
      dalServices.commit();
      return listObjects;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Add a new availability if not existing in database, nothing if not.
   *
   * @param dateAvailable the availablity to insert in database
   * @return the availability with its new id in the database
   * @throws FatalException when problem occurs in DAO.
   */
  public AvailabilityDTO addAvailability(Availability dateAvailable) {
    try {
      dalServices.start();
      AvailabilityDTO availabilityDTO = null;
      if (availabilityDAO.isNotExistantInDb(dateAvailable)) {
        availabilityDTO = availabilityDAO.addAvailability(dateAvailable);
        if (availabilityDTO == null) {
          throw new DAOException("An error has occurred while inserting a new availability.");
        }
      }
      dalServices.commit();
      return availabilityDTO;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  @Override
  public int getAvailabilityId(Availability a) {
    int id;
    try {
      dalServices.start();
      id = availabilityDAO.getAvailability(a);
      dalServices.commit();
    } catch (FatalException exc) {
      dalServices.rollback();
      throw exc;
    }
    return id;
  }
}
