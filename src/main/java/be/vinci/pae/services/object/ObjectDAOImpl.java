package be.vinci.pae.services.object;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.member.Member;
import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.domain.statechange.StateChange;
import be.vinci.pae.domain.type.Type;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.exceptions.OptimisticException;
import be.vinci.pae.services.dal.DalBackendServices;
import be.vinci.pae.services.member.MemberDAO;
import be.vinci.pae.services.object.ObjectDTO.ObjectState;
import be.vinci.pae.services.type.TypeDTO;
import jakarta.inject.Inject;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * the implementation of the interface ObjectDAO.
 *
 * @author Dorcas KUEZE
 */
public class ObjectDAOImpl implements ObjectDAO {

  @Inject
  private Factory myFactory;

  @Inject
  private DalBackendServices dalBackendServices;

  @Inject
  private MemberDAO memberDAO;


  /**
   * get the list of all the objects.
   *
   * @return la liste des objets.
   */

  @Override
  public List<ObjectDTO> getAllObject() {
    List<ObjectDTO> listObject = new ArrayList<>();
    String query = "SELECT * FROM ressourcerie.objects o\n"
        + "LEFT OUTER JOIN ressourcerie.pictures p ON p.object_id = o.id_object";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        do {
          ObjectDTO objectDTO = myFactory.getObject();
          fillObject(objectDTO, rs);
          listObject.add(objectDTO);
        } while (rs.next());
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
    return listObject;
  }

  /**
   * fill object passed in parameter by the resultSet in parameters.
   *
   * @param object whose data is to be filled.
   * @param rs     the data to fill the object with.
   */

  @Override
  public void fillObject(ObjectDTO object, ResultSet rs) throws SQLException {
    object.setId(rs.getInt(1));
    String typeName = getNameType(rs.getInt(2));
    Type type = myFactory.getType();
    type.setTypeName(typeName);
    type.setId(getTypeByName(typeName));
    object.setType(type);

    Date tmpDate = rs.getDate("acceptance_date");
    if (tmpDate != null) {
      object.setAcceptanceDate(tmpDate.toLocalDate());
    }
    object.setDescription(rs.getString("description"));
    Availability availability = getAvailability(rs);
    object.setDateOfReceipt(availability);
    tmpDate = rs.getDate("market_withdrawal_date");
    if (tmpDate != null) {
      object.setMarketWithdrawalDate(tmpDate.toLocalDate());
    }
    object.setPhoneNumber(rs.getString("phone_number"));
    tmpDate = rs.getDate("selling_date");
    if (tmpDate != null) {
      object.setSellingDate(tmpDate.toLocalDate());
    }
    int idMember = rs.getInt("member");
    if (idMember > 0) {
      Member user = (Member) memberDAO.getMemberByID(idMember);
      object.setOfferingMember(user);
    }
    String msg = rs.getString("reason_for_refusal");
    if (msg != null) {
      object.setReasonForRefusal(msg);
    }
    tmpDate = rs.getDate("store_deposit_date");
    if (tmpDate != null) {
      object.setStoreDepositDate(tmpDate.toLocalDate());
    }
    msg = rs.getString("state");
    if (msg != null) {
      object.setState(msg);
    }
    object.setPrice(rs.getDouble("selling_price"));
    object.setVersionNumberObject(rs.getInt("version_number"));

    Picture picture = myFactory.getPicture();
    picture.setId(rs.getInt("id_picture"));
    picture.setUrl(rs.getString("url"));
    picture.setVersion(rs.getInt("version_number"));
    picture.setObjectId(rs.getInt("id_object"));
    object.setPicture(picture);

  }

  /**
   * get availability from id.
   *
   * @param rs a resultset containing the id of an availability.
   * @return the availability if it exists, an availability for the current day's morning otherwise.
   * @throws FatalException in case of an SQL error.
   */
  private Availability getAvailability(ResultSet rs) {
    int idAvailability;
    String query = "SELECT * FROM ressourcerie.availabilities WHERE id_availability = ?";
    try (PreparedStatement ps1 = dalBackendServices.getPrepareStatement(query)) {
      idAvailability = rs.getInt("id_date_of_receipt");
      ps1.setInt(1, idAvailability);
      try (ResultSet rs1 = ps1.executeQuery()) {
        LocalDate date;
        String timeSlot = null;

        if (!rs1.next()) {
          date = LocalDate.now();
        } else {
          date = rs1.getDate("availability").toLocalDate();
          timeSlot = rs1.getString("time_slot");
        }
        Availability availability = myFactory.getAvailability();
        availability.setDate(date);
        availability.setTimeSlot(timeSlot);
        return availability;
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
  }

  /**
   * get the name of type by its id.
   *
   * @return the string corresponding to the id passed in parameter.
   */

  @Override
  public String getNameType(int id) {
    String typeToReturn;
    String query = "select label_type from ressourcerie.types where id_type = ? ";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, id);
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          throw new DAOException("Name of Type is Not found ");
        }
        typeToReturn = rs.getString("label_type");
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
    return typeToReturn;
  }

  /**
   * get the date of the receipt.
   *
   * @param id of the object.
   * @return the Date of the receipt of the object.
   */
  @Override
  public LocalDate getDateReceipt(int id) {
    String query = "SELECT AVAILABILITY, TIME_SLOT "
        + "FROM RESSOURCERIE.OBJECTS O, RESSOURCERIE.AVAILABILITIES A "
        + "WHERE O.ID_OBJECT = ? AND O.ID_DATE_OF_RECEIPT = A.ID_AVAILABILITY";
    LocalDate dateReceipt = null;
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, id);
      try (ResultSet rs = ps.executeQuery()) {
        if (rs.next()) {
          dateReceipt = LocalDate.parse(rs.getString("AVAILABILITY"));
        }
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
    return dateReceipt;
  }


  /**
   * get the object with id.
   *
   * @param id of the object
   * @return an objectDTO with the requested object's information.
   */
  @Override
  public ObjectDTO getObject(int id) {
    String query = "SELECT o.*, p.* FROM ressourcerie.objects o "
        + "LEFT OUTER JOIN ressourcerie.pictures p ON p.object_id = o.id_object "
        + "WHERE o.id_object = ?";
    ObjectDTO object = myFactory.getObject();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, id);
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        fillObject(object, rs);
      }
    } catch (SQLException e) {
      throw new FatalException("error not found object with the specific id", e.getCause());
    }
    return object;
  }

  /**
   * add a price to the object.
   *
   * @param objectUpdated the object update.
   * @return the object with updates.
   */
  @Override
  public ObjectDTO addPriceToObject(ObjectDTO objectUpdated) {
    String query = "UPDATE RESSOURCERIE.OBJECTS SET SELLING_PRICE = ?, STATE = ? ,"
        + "VERSION_NUMBER = VERSION_NUMBER + 1 WHERE ID_OBJECT = ?"
        + "AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setDouble(1, objectUpdated.getPrice());
      ps.setString(2, objectUpdated.getState().getNameState());
      ps.setInt(3, objectUpdated.getId());
      ps.setInt(4, objectUpdated.getVersionNumberObject());
      int rowsUpdated = ps.executeUpdate();
      if (rowsUpdated == 0) {
        checkOptimisticOrUnfoundError(objectUpdated);
      }
      return objectUpdated;
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());

    }
  }

  /**
   * put the object as sold.
   *
   * @param object the object to modify
   * @return the object with updates.
   */
  @Override
  public ObjectDTO putObjectAsSold(ObjectDTO object) {
    String query = "UPDATE RESSOURCERIE.OBJECTS SET STATE = ?, SELLING_DATE = ?, "
        + "VERSION_NUMBER = VERSION_NUMBER + 1 WHERE ID_OBJECT = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, ObjectState.SOLD.getNameState());
      ps.setDate(2, Date.valueOf(object.getSellingDate()));
      ps.setInt(4, object.getVersionNumberObject());
      ps.setInt(3, object.getId());
      int rowsUpdated = ps.executeUpdate();
      if (rowsUpdated == 0) {
        checkOptimisticOrUnfoundError(object);
      }
      return object;
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
  }


  /**
   * moving an object from the stored state to the sold  state.
   *
   * @param object      the object to put to the sold state.
   * @param objectPrice the price at which the object was sold
   * @return returns the object whose state has been changed.
   */
  @Override
  public Object putToSoldState(ObjectDTO object, double objectPrice) {
    String query =
        "UPDATE RESSOURCERIE.OBJECTS SET STATE = ?, SELLING_DATE = ?, selling_price = ?, "
            + "VERSION_NUMBER = VERSION_NUMBER + 1 WHERE ID_OBJECT = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, ObjectState.SOLD.getNameState());
      ps.setDate(2, Date.valueOf(object.getSellingDate()));
      ps.setInt(4, object.getId());
      ps.setDouble(3, objectPrice);
      ps.setInt(5, object.getVersionNumberObject());
      int rowsUpdated = ps.executeUpdate();
      if (rowsUpdated == 0) {
        checkOptimisticOrUnfoundError(object);
      }
      return object;
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database 12",
          e.getCause());
    }
  }

  /**
   * Updates an object in the database.
   *
   * @param object the object to be updated.
   * @return the updated objectDTO.
   */

  public ObjectDTO acceptOffer(ObjectDTO object) {
    String query = "UPDATE ressourcerie.objects SET state = ?, acceptance_date = ?,"
        + "VERSION_NUMBER = VERSION_NUMBER + 1 WHERE ID_OBJECT = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, object.getState().getNameState());
      ps.setInt(3, object.getId());
      ps.setInt(4, object.getVersionNumberObject());
      ps.setDate(2, Date.valueOf(object.getAcceptanceDate()));

      int updateRows = ps.executeUpdate();
      if (updateRows == 0) {
        checkOptimisticOrUnfoundError(object);
      }
      return object;
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }

  }

  /**
   * update the state of the object to 'Denied'.
   *
   * @param object the object to update
   * @return the object with update or null if error occurs
   */
  public ObjectDTO updateRefusedObject(ObjectDTO object) {
    String query = "UPDATE ressourcerie.objects SET reason_for_refusal = ?, STATE = ?, "
        + "VERSION_NUMBER = VERSION_NUMBER + 1 WHERE ID_OBJECT = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(2, object.getState().getNameState());
      ps.setString(1, object.getReasonForRefusal());
      ps.setInt(3, object.getId());
      ps.setInt(4, object.getVersionNumberObject());
      int updateRows = ps.executeUpdate();
      if (updateRows == 0) {
        checkOptimisticOrUnfoundError(object);
      }
      return object;
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
  }

  /**
   * put the object in state 'In store'.
   *
   * @param objectToUpdate the object to update
   * @return the object updated
   */
  public ObjectDTO depositAtStore(ObjectDTO objectToUpdate) {
    String query = "UPDATE ressourcerie.objects SET state = ?, store_deposit_date = ?, "
        + "VERSION_NUMBER = VERSION_NUMBER + 1 WHERE ID_OBJECT = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, objectToUpdate.getState().getNameState());
      ps.setDate(2, Date.valueOf(objectToUpdate.getStoreDepositDate()));
      ps.setInt(3, objectToUpdate.getId());
      ps.setInt(4, objectToUpdate.getVersionNumberObject());
      int updateRows = ps.executeUpdate();
      if (updateRows == 0) {
        checkOptimisticOrUnfoundError(objectToUpdate);
      }
      return objectToUpdate;
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
  }

  /**
   * put the object in state 'In workshop'.
   *
   * @param objectUpdated the object to update
   * @return the object updated
   */
  @Override
  public Object depositAtWorkshop(ObjectDTO objectUpdated) {
    String query = "UPDATE ressourcerie.objects SET state = ?, VERSION_NUMBER = VERSION_NUMBER + 1 "
        + "WHERE ID_OBJECT = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, objectUpdated.getState().getNameState());
      ps.setInt(2, objectUpdated.getId());
      ps.setInt(3, objectUpdated.getVersionNumberObject());
      int updateRows = ps.executeUpdate();
      if (updateRows == 0) {
        checkOptimisticOrUnfoundError(objectUpdated);
      }
      return objectUpdated;
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
  }

  /**
   * put the object as withdrawn.
   *
   * @param object the object to modify
   * @return the object with updates.
   */
  @Override
  public ObjectDTO withdrawObject(ObjectDTO object) {
    String query = "UPDATE RESSOURCERIE.OBJECTS SET STATE = ?, market_withdrawal_date = ?,"
        + " VERSION_NUMBER = VERSION_NUMBER + 1 WHERE ID_OBJECT = ? AND VERSION_NUMBER = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, ObjectState.WITHDRAWN.getNameState());
      ps.setDate(2, Date.valueOf(object.getMarketWithdrawalDate()));
      ps.setInt(3, object.getId());
      ps.setInt(4, object.getVersionNumberObject());
      int rowsUpdated = ps.executeUpdate();
      if (rowsUpdated == 0) {
        checkOptimisticOrUnfoundError(object);
      }
      return object;
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
  }

  /**
   * Gets a list of objects in state of withdrawn with the date given in argument.
   *
   * @param date the day of the withdrawn
   * @return list of objects in state of withdraw
   */
  @Override
  public ArrayList<ObjectDTO> listObjectWithdraw(LocalDate date) {
    String requete =
        "SELECT * from ressourcerie.objects o, ressourcerie.pictures p "
            + "WHERE o.id_object = p.object_id AND O.STATE = ? AND O.MARKET_WITHDRAWAL_DATE = ? ";
    ArrayList<ObjectDTO> listeObjetsWithdrawn = new ArrayList<>();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(requete)) {
      ps.setString(1, ObjectState.WITHDRAWN.getNameState());
      ps.setDate(2, Date.valueOf(date));
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          ObjectDTO objectDTO = myFactory.getObject();
          fillObject(objectDTO, rs);
          listeObjetsWithdrawn.add(objectDTO);
        }
        return listeObjetsWithdrawn;
      }
    } catch (SQLException e) {
      throw new FatalException(
          "problem occurs while trying to access to the database for list of objects withdrawn",
          e.getCause());
    }
  }

  /**
   * This method retrieves a list of ObjectDTO objects associated with a specific user.
   *
   * @param idUser The user ID.
   * @return A list of ObjectDTO objects.
   */
  public List<ObjectDTO> memberObjects(int idUser) {
    String requete =
        "SELECT * from ressourcerie.objects o, ressourcerie.pictures p, ressourcerie.members m "
            + "WHERE o.member=m.id_member AND m.id_member= ? AND o.id_object = p.object_id";
    List<ObjectDTO> listeObjets = new ArrayList<>();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(requete)) {
      ps.setInt(1, idUser);
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          ObjectDTO objectD = myFactory.getObject();
          fillObject(objectD, rs);
          listeObjets.add(objectD);
        }
        return listeObjets;
      }
    } catch (SQLException e) {
      throw new FatalException(
          "problem occurs while trying to access to the database memberObjects()",
          e.getCause());
    }
  }

  /**
   * This method retrieves a list of objects in state "offered".
   *
   * @return A list of objects in state "offered".
   */
  @Override
  public List<ObjectDTO> offeredObjects() {
    String query = "SELECT * FROM RESSOURCERIE.OBJECTS o, ressourcerie.pictures p "
        + "WHERE O.STATE = ? AND o.id_object = p.object_id";
    List<ObjectDTO> listOfferedObjects = new ArrayList<>();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, ObjectState.OFFERED_ITEM.getNameState());
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          ObjectDTO objectD = myFactory.getObject();
          fillObject(objectD, rs);
          listOfferedObjects.add(objectD);
        }
        return listOfferedObjects;
      }
    } catch (SQLException e) {
      throw new FatalException(
          "problem occurs while trying to access to the database for: offeredObjects",
          e.getCause());
    }
  }

  /**
   * Get the list of all types in database.
   *
   * @return A list of all types in database.
   */
  @Override
  public List<TypeDTO> listTypesObjects() {
    String query = "SELECT * FROM RESSOURCERIE.TYPES T";
    List<TypeDTO> listTypesObjects = new ArrayList<>();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          String name = rs.getString("label_type");
          int id = rs.getInt("id_type");
          TypeDTO type = myFactory.getType();
          type.setId(id);
          type.setTypeName(name);
          listTypesObjects.add(type);
        }
        return listTypesObjects;
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to get the list of types of objects",
          e.getCause());
    }
  }


  /**
   * research all  objects corresponding to the criteria passed in parameter in db.
   *
   * @param minPrice  the minimum price of the objects.
   * @param maxPrice2 the maximum price of the objects.
   * @param idType    the type of the objects.
   * @return the list of objects.
   */

  @Override
  public List<ObjectDTO> searchObjects(double minPrice, double maxPrice2, int idType) {

    List<ObjectDTO> listObject = new ArrayList<>();
    String query;
    PreparedStatement ps;

    try {
      if (maxPrice2 == 0 || minPrice == 0) {
        query = "SELECT * FROM RESSOURCERIE.OBJECTS WHERE object_type = ?";
        ps = dalBackendServices.getPrepareStatement(query);
        ps.setInt(1, idType);

      } else if (maxPrice2 > 0 || minPrice >= 0) {
        query = "SELECT * FROM RESSOURCERIE.OBJECTS  "
            + "WHERE selling_price >= ? AND selling_price <= ? ";
        ps = dalBackendServices.getPrepareStatement(query);
        ps.setDouble(1, minPrice);
        ps.setDouble(2, maxPrice2);

      } else {
        query = " SELECT * FROM ressourcerie.objects o " + " WHERE o.object_type = ?  "
            + "AND ( o.selling_price >= ? AND o.selling_price <= ? ) ";
        ps = dalBackendServices.getPrepareStatement(query);
        ps.setInt(1, idType);
        ps.setDouble(2, minPrice);
        ps.setDouble(3, maxPrice2);
      }

      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        ObjectDTO objectD = myFactory.getObject();
        fillObject(objectD, rs);
        listObject.add(objectD);
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to get the list of types of objects",
          e.getCause());
    }

    return listObject;

  }

  /**
   * This methode get a list of object that the date of deposit in container park is the date in
   * parameters.
   *
   * @param dateReceipt the reception date of object to the container park.
   * @return the list of objects.
   */
  @Override
  public List<ObjectDTO> searchObjectsByDate(LocalDate dateReceipt) {

    List<ObjectDTO> listObject = new ArrayList<>();
    String query = " SELECT * FROM ressourcerie.objects o, ressourcerie.availabilities a "
        + "WHERE o.id_date_of_receipt = a.id_availability  AND a.availability = ? ";

    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setDate(1, Date.valueOf(dateReceipt));
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        ObjectDTO objectD = myFactory.getObject();
        fillObject(objectD, rs);
        listObject.add(objectD);
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to get the list of types of objects",
          e.getCause());
    }
    return listObject;
  }

  /**
   * Insert the new offer of the object by a member into the database.
   *
   * @param object         the object to insert
   * @param availabilityID the id of the availability
   * @return the id of the new insert
   */
  public int offerMember(ObjectDTO object, int availabilityID) {
    int id = -1;
    String query = "INSERT INTO ressourcerie.objects(object_type, description, "
        + "id_date_of_receipt, member, phone_number, state, version_number) VALUES (?,?,?,?,?,?,?)";
    try (PreparedStatement ps = dalBackendServices.getPreparedStatementRGK(query)) {
      ps.setInt(1, object.getType().getId());
      ps.setString(2, object.getDescription());
      ps.setInt(3, availabilityID);
      ps.setInt(4, object.getOfferingMember().getId());
      ps.setString(5, object.getPhoneNumber());
      ps.setString(6, object.getState().getNameState());
      ps.setInt(7, object.getVersionNumberObject());

      int executeLine = ps.executeUpdate();
      if (executeLine == 0) {
        return -1;
      }
      try (ResultSet rs = ps.getGeneratedKeys()) {
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      throw new FatalException(exc.getMessage(), exc);
    }
    return id;
  }

  /**
   * Insert the new offer of the object with a phone number into the database.
   *
   * @param object         the object to insert
   * @param availabilityID the id of the availability
   * @return the id of the new insert
   */
  public int offerPhoneNumber(ObjectDTO object, int availabilityID) {
    int id = -1;
    String query = "INSERT INTO ressourcerie.objects(object_type, description, "
        + "id_date_of_receipt, phone_number, state, version_number) VALUES (?,?,?,?,?,?)";
    try (PreparedStatement ps = dalBackendServices.getPreparedStatementRGK(query)) {
      ps.setInt(1, object.getType().getId());
      ps.setString(2, object.getDescription());
      ps.setInt(3, availabilityID);
      ps.setString(4, object.getPhoneNumber());
      ps.setString(5, object.getState().getNameState());
      ps.setInt(6, object.getVersionNumberObject());

      int executeLine = ps.executeUpdate();
      if (executeLine == 0) {
        return -1;
      }
      try (ResultSet rs = ps.getGeneratedKeys()) {
        if (rs.next()) {
          id = rs.getInt(1);
        }
      }
    } catch (SQLException exc) {
      throw new FatalException(exc.getMessage(), exc);
    }
    return id;
  }

  /**
   * Update the object into the database.
   *
   * @param objectUpdate the object with the updates.
   * @return the object that was updated.
   */
  @Override
  public ObjectDTO updateObject(ObjectDTO objectUpdate) {
    String query = "UPDATE RESSOURCERIE.OBJECTS SET DESCRIPTION = ?, "
        + "OBJECT_TYPE = ?, VERSION_NUMBER = VERSION_NUMBER + 1 "
        + "WHERE ID_OBJECT = ? AND VERSION_NUMBER = ?";

    //  int idType = getTypeByName();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, objectUpdate.getDescription());
      ps.setInt(2, objectUpdate.getType().getId());
      ps.setInt(3, objectUpdate.getId());
      ps.setInt(4, objectUpdate.getVersionNumberObject());
      int rowsUpdated = ps.executeUpdate();
      if (rowsUpdated == 0) {
        checkOptimisticOrUnfoundError(objectUpdate);
      }
      return objectUpdate;

    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
  }

  /**
   * Gets the id of the given type in argument.
   *
   * @param type the name of the type
   * @return the id of the type in database
   */
  public int getTypeByName(String type) {
    int id;
    String query = "SELECT id_type FROM RESSOURCERIE.TYPES WHERE LABEL_TYPE = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, type);
      ResultSet rs = ps.executeQuery();
      if (!rs.next()) {
        return -1;
      }
      id = rs.getInt("id_type");
    } catch (SQLException e) {
      throw new FatalException(e.getMessage(), e);
    }
    return id;
  }

  private void checkOptimisticOrUnfoundError(ObjectDTO object) {
    if (isInvalidVersion(object.getId(), object.getVersionNumberObject())) {
      throw new OptimisticException(); //lancer un 409 Conflicts
    } else if (isUnfound(object.getId())) {
      throw new FatalException(); //404 error not found
    }
  }

  private boolean isInvalidVersion(int idObject, int version) {
    String query = "SELECT * FROM RESSOURCERIE.OBJECTS WHERE version_number = ? AND ID_OBJECT = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(2, idObject);
      ps.setInt(1, version);
      ResultSet res = ps.executeQuery();
      return !res.next();
    } catch (SQLException e) {
      throw new FatalException("problem occurs with the version", e.getCause());
    }
  }

  private boolean isUnfound(int idObject) {
    String query = "SELECT * FROM RESSOURCERIE.OBJECTS WHERE ID_OBJECT = ?";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, idObject);
      ResultSet res = ps.executeQuery();
      return !res.next();
    } catch (SQLException e) {
      throw new FatalException("problem occurs with isUnfound.", e.getCause());
    }
  }


  /**
   * This method retrieves a list of objects in state "In store", "Available for sale" and "Sold".
   * That will be used for the carousel in the home page.
   *
   * @return An ArrayList of objects in state "In store", "Available for sale" and "Sold".
   */
  @Override
  public ArrayList<ObjectDTO> getObjectsForCarousel() {
    String query = "SELECT O.*, p.* FROM RESSOURCERIE.OBJECTS O, ressourcerie.pictures p "
        + "WHERE (O.STATE = ? OR O.STATE = ? OR O.STATE = ? ) AND O.id_object = p.object_id ";
    ArrayList<ObjectDTO> listObjects = new ArrayList<>();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, ObjectState.IN_STORE.getNameState());
      ps.setString(2, ObjectState.AVAILABLE_FOR_SALE.getNameState());
      ps.setString(3, ObjectState.SOLD.getNameState());
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          ObjectDTO objectD = myFactory.getObject();
          fillObject(objectD, rs);
          listObjects.add(objectD);
        }
        return listObjects;
      }
    } catch (SQLException e) {
      throw new FatalException(
          "problem occurs while trying to access to the database for the carousel",
          e.getCause());
    }
  }

  @Override
  public void setPicture(int id, String filePath) {
    String query = "INSERT INTO ressourcerie.pictures (object_id, url, version_number)"
        + " VALUES (?, ?, ?)";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, id);
      ps.setString(2, filePath);
      ps.setInt(3, 1);
      try {
        ps.execute();
      } catch (SQLException exc) {
        throw new FatalException(exc.getMessage(), exc.getCause());
      }
    } catch (SQLException exc) {
      throw new FatalException(exc.getMessage(), exc.getCause());
    }
  }

  /**
   * Get a list of objects in the state 'available for sale'.
   *
   * @return a list of objects in the state 'available for sale'
   */
  @Override
  public ArrayList<ObjectDTO> getObjectsInSell() {
    String query = "SELECT O.*, p.* FROM RESSOURCERIE.OBJECTS O, ressourcerie.pictures p "
        + "WHERE O.STATE = ? AND O.id_object = p.object_id ";
    ArrayList<ObjectDTO> listObjects = new ArrayList<>();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, ObjectState.AVAILABLE_FOR_SALE.getNameState());
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          ObjectDTO objectD = myFactory.getObject();
          fillObject(objectD, rs);
          listObjects.add(objectD);
        }
        return listObjects;
      }
    } catch (SQLException e) {
      throw new FatalException(
          "problem occurs while trying to access to the database for the carousel",
          e.getCause());
    }
  }

  /**
   * Searches for objects in the database based on the given state.
   *
   * @param state the state of the objects to search for
   * @return a list of ObjectDTO objects that match the given state
   * @throws FatalException if an error occurs while trying to access the database
   */
  @Override
  public List<ObjectDTO> searchObjectsByState(String state) {
    List<ObjectDTO> listObject = new ArrayList<>();
    String query = "SELECT o.*, p.* FROM ressourcerie.objects o , ressourcerie.pictures p "
        + "WHERE STATE = ? AND o.id_object = p.object_id";

    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setString(1, state);
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        ObjectDTO objectD = myFactory.getObject();
        fillObject(objectD, rs);
        listObject.add(objectD);
      }
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to get the list of types of objects",
          e.getCause());
    }
    return listObject;
  }

  @Override
  public List<StateChange> getStateChanges() {
    List<StateChange> changes = new ArrayList<StateChange>();
    String query = "SELECT * FROM ressourcerie.state_changes";

    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          StateChange sc = myFactory.getStateChange();
          String nameState = rs.getString("state");
          ObjectState state = null;
          for (ObjectState value : ObjectState.values()) {
            if (value.getNameState().equalsIgnoreCase(nameState)) {
              state = value;
            }
          }
          sc.setState(state);
          sc.setDate(rs.getDate("date").toLocalDate());
          sc.setObjectID(rs.getInt("object_id"));
          changes.add(sc);

        }
      }

    } catch (SQLException exc) {
      throw new FatalException(exc.getMessage(), exc.getCause());
    }
    return changes;
  }


  @Override
  public void addStateChange(int objectId, ObjectState state) {
    String query = "INSERT INTO ressourcerie.state_changes (object_id, date, state)\n"
        + "VALUES (?,?,?)";

    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, objectId);
      ps.setDate(2, Date.valueOf(LocalDate.now()));
      ps.setString(3, state.getNameState());
      ps.execute();
    } catch (SQLException exc) {
      throw new FatalException(exc.getMessage(), exc.getCause());
    }
  }
}
