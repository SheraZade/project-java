package be.vinci.pae.services.object;

import be.vinci.pae.domain.statechange.StateChange;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.object.ObjectDTO.ObjectState;
import be.vinci.pae.services.type.TypeDTO;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * the interface ObjectUCC.
 *
 * @author Dorcas KUEZE
 */

public interface ObjectUCC {

  /**
   * get the list.
   *
   * @return list all objects
   */
  List<ObjectDTO> allObjectsList();

  /**
   * get the object with id.
   *
   * @param id of the object
   * @return an objectDTO with the requested object's information.
   */
  ObjectDTO getObject(int id);

  /**
   * add a price in the object.
   *
   * @param id    the id of the object.
   * @param price the price of the object.
   * @return the object with updates.
   */
  ObjectDTO addPriceObject(int id, double price);

  /**
   * put the object as sold.
   *
   * @param id the id of the object.
   * @return the object with updates.
   */
  ObjectDTO putAsSold(int id);

  /**
   * Accepts a proposal for an object and updates the object's state in the database.
   *
   * @param idObject the id of the object to accept the proposal for
   * @return the updated object after accepting the proposal
   */
  ObjectDTO acceptOfferObject(int idObject);

  /**
   * Refuse a proposal for an object, updates the object's state and the reason for refusal if given
   * in the database.
   *
   * @param id             the id of the object
   * @param messageRefusal the reason for the refusal
   * @return the updated object after accepting the proposal
   */
  ObjectDTO refuseOfferObject(int id, String messageRefusal);

  /**
   * Change the state of the object as deposit at store.
   *
   * @param idObject the id of the object to update.
   * @return the object with the updates.
   */
  ObjectDTO putDepositAtStore(int idObject);

  /**
   * Change the state of the object as deposit at the workshop.
   *
   * @param idObject the id of the object to update.
   * @return the object with the updates.
   */
  ObjectDTO putAtWorkshop(int idObject);

  /**
   * Change the state of the object as 'Withdraw'.
   *
   * @param objectDTO the object to update.
   * @return the object with the updates.
   */
  ObjectDTO withdrawObject(ObjectDTO objectDTO);

  /**
   * Gets a list of the objects that had been withdrawn for the day.
   *
   * @return the list of objects withdraw.
   */
  ArrayList<ObjectDTO> listObjectsWithdraw();

  /**
   * This method retrieves a list of ObjectDTO objects associated with a specific user.
   *
   * @param idUser The user ID.
   * @return A list of ObjectDTO objects.
   */
  List<ObjectDTO> memberObjects(int idUser);

  /**
   * This method retrieves a list of objects in state "offered".
   *
   * @return A list of ObjectDTO objects.
   */
  List<ObjectDTO> offeredObjects();

  /**
   * Get the list of all types in database.
   *
   * @return A list of all types in database.
   */
  List<TypeDTO> getAllTypes();

  /**
   * Get the list of all states in domain.
   *
   * @return A list of all states in domain.
   */
  ArrayList<String> getAllStates();

  /**
   * get the list of objects corresponding to the values passed in parameter.
   *
   * @param minPrice   the minimum price of the objects.
   * @param maxPrice   the maximum price of the objects.
   * @param typeObject the type of the objects.
   * @return return a list of ObjectDTO
   */
  List<ObjectDTO> searchObjects(double minPrice, double maxPrice, int typeObject);

  /**
   * Get the list of objects with correspondant date of deposit in container park.
   *
   * @param dateReceipt the reception date of object to the container park.
   * @return the list of objects.
   */
  List<ObjectDTO> searchObjectsByDate(LocalDate dateReceipt);

  /**
   * Offer an object.
   *
   * @param newObject the object to offer.
   * @return -1 if the methode fails, the id of the object added otherwise.
   * @throws DAOException if problem occurs in database while inserting the new object.
   */
  int offerObject(ObjectDTO newObject);

  /**
   * Update an object.
   *
   * @param objectUpdated the object with the updates.
   * @return true if the object was correctly updated, false otherwise.
   * @throws DAOException if problem occurs in database while inserting the new object.
   */
  boolean updateObject(ObjectDTO objectUpdated);

  /**
   * Sort the objects by their type.
   *
   * @param type the type selected.
   * @return a list of all objects of that type.
   */
  List<ObjectDTO> searchByType(String type);

  //  /**
  //   * Get type by it's ID.
  //   *
  //   * @param id the id of the type in the DB.
  //   * @return the type.
  //   */
  //  TypeDTO getTypeByID(int id);

  /**
   * perform a search on minimum price, maximum price, date of receipt and/or type.
   *
   * @param minPrice the minimum price.
   * @param maxPrice the maximum price.
   * @param date     the date of receipt.
   * @param type     the type of the object.
   * @param state    the state of the object.
   * @param sbd      search by date.
   * @param sbp      search by price ascending or descending.
   * @return a list of all objects matching the search parameters.
   */
  List<ObjectDTO> search(double minPrice, double maxPrice, LocalDate date, List<String> type,
      String state, String sbd, String sbp);

  /**
   * Get all the object in state : "In store", "Available for sale" and "Sold".
   *
   * @return the list of the objects.
   */
  List<ObjectDTO> getObjectForCarousel();

  /**
   * Set picture for an object.
   *
   * @param id       the id of the object.
   * @param filePath the path to the picture.
   */
  void setPicture(int id, String filePath);

  /**
   * Searches for objects with the given state.
   *
   * @param state the state of the objects to search for
   * @return a List of ObjectDTOs with the given state
   * @throws FatalException if an SQLException occurs while querying the database
   */
  List<ObjectDTO> searchObjectsByState(String state);

  /**
   * Check the objects in state 'available for sale' that are up than 30 open days to withdraw.
   *
   * @return a list of the objects to withdraw
   */
  ArrayList<ObjectDTO> checkObjectToWithdraw();

  /**
   * Get the count of objects during a time period by state.
   * If an object has had multiple states during a time period, their last state in that time
   * period is counted, the others are ignored.
   *
   * @param dateMax the latest date for the object to be in a given state.
   * @return A map of all states and their corresponding count
   */
  Map<ObjectState, Integer> getStats(LocalDate dateMax);

  /**
   * Get the count of objects by state.
   * If an object has had multiple states during a time period, their last state in that time
   * period is counted, the others are ignored.
   *
   * @return A map of all states and their corresponding count
   */
  Map<ObjectState, Integer> getStats();

  /**
   * Gets the total number of objects present during a certain period. If an object has been pulled
   * from sale or sold before the given period, it will not be taken into account.
   * If either or both the date given are null, that parameter will be ignored and the statistics
   * will cover all objects matching the remaining parameter.
   *
   * @param dateMin the minimum date.
   * @param dateMax the maximum date.
   * @return the number of objects present during a certain period.
   */
  int getCount(LocalDate dateMin, LocalDate dateMax);

  /**
   * Get the statistics of object of a certain state, over a certain period of time.
   * if eiter or both dates are null, the statistics will covert the remaining parameters.
   *
   * @param state the state for which the statistics are needed.
   * @param dateMin the minimum date to be in the given state.
   * @param dateMax the maximum date to be in the given state.
   * @return a list of all state changes to the given state that occured during the given period.
   */
  List<StateChange> getStateStats(ObjectState state, LocalDate dateMin, LocalDate dateMax);

  /**
   * moving an object from the stored state to the sold  state.
   *
   * @param id          the id of the object to put to the sold state.
   * @param objectPrice the price at which the object was sold
   * @return returns the object whose state has been changed.
   */
  ObjectDTO moveToSold(int id, double objectPrice);
}
