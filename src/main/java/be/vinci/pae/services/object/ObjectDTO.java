package be.vinci.pae.services.object;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.object.ObjectImpl;
import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.domain.type.Type;
import be.vinci.pae.services.member.MemberDTO;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.time.LocalDate;

/**
 * interface ObjectDTO.
 *
 * @author Dorcas KUEZE
 */
@JsonDeserialize(as = ObjectImpl.class)
public interface ObjectDTO {

  /**
   * Getter for id.
   *
   * @return the object's ID.
   */
  int getId();

  /**
   * setter of id.
   *
   * @param id the id of the object.
   */
  void setId(int id);

  /**
   * Getter for price.
   *
   * @return the object's price.
   */
  double getPrice();

  /**
   * Getter for the object picture.
   *
   * @return the object picture.
   */
  Picture getPicture();

  /**
   * setter for object picture.
   *
   * @param picture picture of the object.S
   */
  void setPicture(Picture picture);

  /**
   * Getter for type.
   *
   * @return the object's type.
   */

  Type getType();

  /**
   * setter of the type of the object.
   *
   * @param type the type of the object.
   */
  void setType(Type type);

  /**
   * Getter for description.
   *
   * @return the object's description.
   */
  String getDescription();

  /**
   * setter of description. Update description.
   *
   * @param description the description of object.
   */
  void setDescription(String description);

  /**
   * Getter for the reason for refusal.
   *
   * @return the reason for the object's refusal.
   */
  String getReasonForRefusal();

  /**
   * setter of the reason for refusal.
   *
   * @param reasonForRefusal the reason for refusal an object.
   */
  void setReasonForRefusal(String reasonForRefusal);

  /**
   * Getter for state.
   *
   * @return the object's state.
   */
  ObjectState getState();

  /**
   * setter of state.
   *
   * @param state the state of object.
   */
  void setState(String state);

  /**
   * setter of state.
   *
   * @param state the state of object.
   */
  void setState(ObjectState state);

  /**
   * Getter for the phone number.
   *
   * @return the phone number of the offering member.
   */
  String getPhoneNumber();

  /**
   * setter of the phone number. set the phone number of the user
   *
   * @param phoneNumber the phone number.
   */
  void setPhoneNumber(String phoneNumber);


  /**
   * Getter for the receipt date.
   *
   * @return the date at which the object was received.
   */
  Availability getDateOfReceipt();

  /**
   * setter of receipt date. update the date of reception.
   *
   * @param dateOfReceipt the date receipt of object.
   */

  void setDateOfReceipt(Availability dateOfReceipt);

  /**
   * Getter for the acceptation date.
   *
   * @return the date at which the object was accepted.
   */
  LocalDate getAcceptanceDate();

  /**
   * setter of acceptation date.Update the date of acceptation.
   *
   * @param acceptanceDate the acceptation date of object.
   */
  void setAcceptanceDate(LocalDate acceptanceDate);

  /**
   * Getter for the sale date.
   *
   * @return the date at which the object was put up for sale.
   */
  LocalDate getSellingDate();

  /**
   * setter of forSaleDate. update de date.
   *
   * @param sellingDate the sale date of object.
   */
  void setSellingDate(LocalDate sellingDate);

  /**
   * Getter for end of sale date.
   *
   * @return the date at which the object was withdrawn from the storefront.
   */
  LocalDate getMarketWithdrawalDate();

  /**
   * setter of end of sale date. update the date.
   *
   * @param marketWithdrawalDate the end of sale date.
   */
  void setMarketWithdrawalDate(LocalDate marketWithdrawalDate);

  /**
   * Getter for store deposit date.
   *
   * @return the date at which the object was deposited at the store.
   */
  LocalDate getStoreDepositDate();

  /**
   * setter of store deposit date. update the date of deposit in the store.
   *
   * @param storeDepositDate the store description of the object.
   */
  void setStoreDepositDate(LocalDate storeDepositDate);

  /**
   * getter for the offering member.
   *
   * @return the member who offered the object.
   */
  MemberDTO getOfferingMember();

  /**
   * setter of OfferingMember. Update the offering Member.
   *
   * @param offeringMember the offering member.
   */
  void setOfferingMember(MemberDTO offeringMember);


  /**
   * setter of the price.
   *
   * @param price the price of the object.
   */
  void setPrice(double price);


  /**
   * Getter of version.
   *
   * @return the version
   */
  int getVersionNumberObject();

  /**
   * Setter of version.
   *
   * @param version of the object
   */
  void setVersionNumberObject(int version);

  /**
   * enum of the different state of the object.
   */
  enum ObjectState {
    /**
     * state offered item.
     */
    OFFERED_ITEM("Proposé"),
    /**
     * state valid item.
     */
    ACCEPT_OFFER("Accepté"),
    /**
     * state deny offer.
     */
    DENY_OFFER("Refusé"),
    /**
     * state in worshop.
     */
    IN_WORKSHOP("En atelier"),
    /**
     * state in store.
     */
    IN_STORE("En magasin"),
    /**
     * state in available for sale.
     */
    AVAILABLE_FOR_SALE("Mis en vente"),
    /**
     * state sold.
     */
    SOLD("Vendu"),
    /**
     * state withdraw.
     */
    WITHDRAWN("Retiré");

    private String nameState;

    /**
     * Constructor for the ObjectState class.
     *
     * @param nameState The initial state of the object.
     */
    ObjectState(String nameState) {
      this.nameState = nameState;
    }

    /**
     * Getter for state.
     *
     * @return the object's state.
     */
    @JsonValue
    public String getNameState() {
      return nameState;
    }

    @Override
    public String toString() {
      return nameState;
    }
  }
}

