package be.vinci.pae.services.object;

import be.vinci.pae.domain.statechange.StateChange;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.object.ObjectDTO.ObjectState;
import be.vinci.pae.services.type.TypeDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * the interface ObjectDAO.
 *
 * @author Dorcas KUEZE
 */

public interface ObjectDAO {

  /**
   * get the list of all the objects.
   *
   * @return la liste des objets.
   */

  List<ObjectDTO> getAllObject();

  /**
   * fill object passed in parameter by the resultSet in parameters.
   *
   * @param object whose data is to be filled.
   * @param rs     the data to fill the object with.
   * @throws SQLException the exception.
   */

  void fillObject(ObjectDTO object, ResultSet rs) throws SQLException;

  /**
   * get the name of type by its id.
   *
   * @param id id of the object.
   * @return the string corresponding to the id passed in parameter.
   */

  String getNameType(int id);


  /**
   * get the date of the receipt.
   *
   * @param id of the object.
   * @return the Date of the receipt of the object.
   */
  LocalDate getDateReceipt(int id);

  /**
   * Gets the id of the given type in argument.
   *
   * @param type the name of the type
   * @return the id of the type in database
   */
  int getTypeByName(String type);

  /**
   * get the object with id.
   *
   * @param id of the object
   * @return an objectDTO with the requested object's information.
   */
  ObjectDTO getObject(int id);

  /**
   * add a price to the object.
   *
   * @param objectUpdated the object with the new change.
   * @return the object with updates.
   */
  ObjectDTO addPriceToObject(ObjectDTO objectUpdated);

  /**
   * put the object as sold.
   *
   * @param object the object to modify
   * @return the object with updates.
   */
  ObjectDTO putObjectAsSold(ObjectDTO object);

  /**
   * Updates an object in the database.
   *
   * @param object the object to be updated.
   * @return the updated objectDTO.
   */
  ObjectDTO acceptOffer(ObjectDTO object);

  /**
   * add a refusal reason to the object in the database.
   *
   * @param object the object to update.
   * @return the updated objectDTO.
   */
  ObjectDTO updateRefusedObject(ObjectDTO object);

  /**
   * put the object in state 'In store'.
   *
   * @param objectToUpdate the object to update
   * @return the object updated
   */
  ObjectDTO depositAtStore(ObjectDTO objectToUpdate);

  /**
   * put the object in state 'In workshop'.
   *
   * @param objectUpdate the object to update
   * @return the object updated
   */
  Object depositAtWorkshop(ObjectDTO objectUpdate);

  /**
   * put the object in state 'Withdrawn'.
   *
   * @param objectUpdate the object to update
   * @return the object updated
   */
  Object withdrawObject(ObjectDTO objectUpdate);

  /**
   * Gets a list of objects in state of withdrawn with the date given in argument.
   *
   * @param date the day of the withdrawn
   * @return list of objects in state of withdraw
   */
  ArrayList<ObjectDTO> listObjectWithdraw(LocalDate date);

  /**
   * This method retrieves a list of ObjectDTO objects associated with a specific user.
   *
   * @param idUser The user ID.
   * @return A list of ObjectDTO objects.
   */
  List<ObjectDTO> memberObjects(int idUser);

  /**
   * This method retrieves a list of objects in state "offered".
   *
   * @return A list of objects in state "offered".
   */
  List<ObjectDTO> offeredObjects();

  /**
   * research all  objects corresponding to the criteria passed in parameter in db.
   *
   * @param minPrice  the minimum price of the objects.
   * @param maxPrice2 the maximum price of the objects.
   * @param idType    the type of the objects.
   * @return the list of objects.
   */
  List<ObjectDTO> searchObjects(double minPrice, double maxPrice2, int idType);

  /**
   * This methode get a list of object that the date of deposit in container park is the date in
   * parameters.
   *
   * @param dateReceipt the reception date of object to the container park.
   * @return the list of objects.
   */
  List<ObjectDTO> searchObjectsByDate(LocalDate dateReceipt);

  /**
   * Get the list of all types in database.
   *
   * @return A list of all types in database.
   */
  List<TypeDTO> listTypesObjects();

  /**
   * Insert the new offer of the object by a member into the database.
   *
   * @param object         the object to insert
   * @param availabilityID the id of the availability
   * @return the id of the new insert
   */
  int offerMember(ObjectDTO object, int availabilityID);

  /**
   * Insert the new offer of the object with a phone number into the database.
   *
   * @param object         the object to insert
   * @param availabilityID the id of the availability
   * @return the id of the new insert
   */
  int offerPhoneNumber(ObjectDTO object, int availabilityID);

  /**
   * Update the object into the database.
   *
   * @param objectUpdate the object with the updates.
   * @return the object that was updated.
   */
  ObjectDTO updateObject(ObjectDTO objectUpdate);

  /**
   * This method retrieves a list of objects in state "In store", "Available for sale" and "Sold".
   * That will be used for the carousel in the home page.
   *
   * @return An arraylist of objects in state "In store", "Available for sale" and "Sold".
   */
  ArrayList<ObjectDTO> getObjectsForCarousel();

  /**
   * Link picture to object in the DB.
   *
   * @param id       the id of the object.
   * @param filePath the path to the picture.
   */
  void setPicture(int id, String filePath);

  /**
   * Get a list of objects in the state 'available for sale'.
   *
   * @return a list of objects in the state 'available for sale'
   */
  ArrayList<ObjectDTO> getObjectsInSell();

  /**
   * Searches for objects in the database based on the given state.
   *
   * @param state the state of the objects to search for
   * @return a list of ObjectDTO objects that match the given state
   * @throws FatalException if an error occurs while trying to access the database
   */
  List<ObjectDTO> searchObjectsByState(String state);

  /**
   * Gets all changes in states.
   *
   * @return a list of all changes for that object.
   */
  List<StateChange> getStateChanges();

  /**
   * Add a state change to the DB.
   *
   * @param objectId the id of the object whose state has changed.
   * @param state    the state it has changed to.
   */
  void addStateChange(int objectId, ObjectState state);

  /**
   * moving an object from the stored state to the sold  state.
   *
   * @param object      the object to put to the sold state.
   * @param objectPrice the price at which the object was sold
   * @return returns the object whose state has been changed.
   */
  Object putToSoldState(ObjectDTO object, double objectPrice);
}
