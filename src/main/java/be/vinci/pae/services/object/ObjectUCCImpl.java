package be.vinci.pae.services.object;

import be.vinci.pae.domain.statechange.StateChange;
import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.object.Object;
import be.vinci.pae.exceptions.BizException;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.availability.AvailabilityUCC;
import be.vinci.pae.services.dal.DalServices;
import be.vinci.pae.services.member.MemberDAO;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.notification.NotificationUCC;
import be.vinci.pae.services.object.ObjectDTO.ObjectState;
import be.vinci.pae.services.picture.PictureDAO;
import be.vinci.pae.services.picture.PictureDTO;
import be.vinci.pae.services.type.TypeDTO;
import jakarta.inject.Inject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * implementation of the interface  ObjetUcc.
 *
 * @author Dorcas KUEZE
 */

public class ObjectUCCImpl implements ObjectUCC {

  @Inject
  ObjectDAO objectDAO;

  @Inject
  MemberDAO memberDAO;

  @Inject
  DalServices dalServices;
  @Inject
  PictureDAO pictureDAO;

  @Inject
  AvailabilityUCC availabilityUCC;

  @Inject
  NotificationUCC notificationUCC;

  @Inject
  Factory myFactory;
  private final int nbBusinessDays = 30;

  /**
   * Retrieves a list of DTO objects representing all the objects stored in the database.
   *
   * @return a list of DTO objects representing all the objects stored in the database.
   * @throws FatalException when problem occurs in DAO.
   */
  @Override
  public List<ObjectDTO> allObjectsList() {
    try {
      dalServices.start();
      List<ObjectDTO> listObjects = objectDAO.getAllObject();
      dalServices.commit();
      return listObjects;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }

  }


  /**
   * get the object with id.
   *
   * @param id of the object
   * @return an objectDTO with the requested object's information.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the object is not found.
   */
  @Override
  public ObjectDTO getObject(int id) {
    try {
      dalServices.start();
      ObjectDTO object = objectDAO.getObject(id);
      if (object == null) {
        throw new DAOException("The object doesn't exist.");
      }
      dalServices.commit();
      return object;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * add a price in the object.
   *
   * @param id    the id of the object.
   * @param price the price of the object.
   * @return the object with updates.
   * @throws FatalException when problem occurs in DAO.
   * @throws BizException   when the state of the object or the price are invalid.
   * @throws DAOException   when problem occurs with the query.
   */
  @Override
  public ObjectDTO addPriceObject(int id, double price) {
    try {
      dalServices.start();
      Object objectInDB = (Object) objectDAO.getObject(id);
      if (objectInDB == null) {
        throw new DAOException("the object is not find in the db");
      }
      ObjectDTO objetDTO = objectInDB.checkAndSetToPrice(price);
      if (objetDTO == null) {
        throw new BizException("the object is not in the required state"
            + " or the price must be higher than 0.");
      }
      ObjectDTO objectUpdated = objectDAO.addPriceToObject(objetDTO);
      if (objectUpdated == null) {
        throw new DAOException("Error when executing the query for adding a price");
      }
      objectDAO.addStateChange(id, ObjectState.AVAILABLE_FOR_SALE);
      dalServices.commit();
      return objectUpdated;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * put the object as sold.
   *
   * @param id the id of the object.
   * @return the object with updates.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when problem occurs with the query.
   * @throws BizException   when the state of the object is invalid.
   */
  @Override
  public ObjectDTO putAsSold(int id) {
    try {
      dalServices.start();
      Object objectInDb = (Object) objectDAO.getObject(id);
      if (objectInDb == null) {
        throw new DAOException("The object doesn't exist.");
      }
      ObjectDTO objectDTO = objectInDb.checkAndSetSold();
      if (objectDTO == null) {
        throw new BizException("The object must be in state dropped off in store ");
      }
      ObjectDTO objectUpdated = objectDAO.putObjectAsSold(objectDTO);
      if (objectUpdated == null) {
        throw new DAOException("The object wasn't updated cause of the query.");
      }
      objectDAO.addStateChange(id, ObjectState.SOLD);
      dalServices.commit();
      return objectUpdated;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }


  /**
   * moving an object from the stored state to the sold  state.
   *
   * @param id          the id of the object to put to the sold state.
   * @param objectPrice the price at which the object was sold
   * @return returns the object whose state has been changed .
   */
  @Override
  public ObjectDTO moveToSold(int id, double objectPrice) {
    try {
      dalServices.start();
      Object objectInDb = (Object) objectDAO.getObject(id);
      if (objectInDb == null) {
        throw new DAOException("The object doesn't exist.");
      }

      ObjectDTO objectDomainUpdate = objectInDb.moveToSoldState();
      if (objectDomainUpdate == null) {
        throw new BizException("The object isn't in the required state.");
      }

      objectInDb = (Object) objectDAO.putToSoldState(objectDomainUpdate, objectPrice);
      if (objectInDb == null) {
        throw new DAOException("The object wasn't updated cause of the query.");
      }

      dalServices.commit();
      return objectInDb;

    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
  }


  /**
   * Accepts a proposal for an object and updates the object's state in the database.
   *
   * @param idObject the id of the object to accept the proposal for
   * @return the updated object after accepting the proposal
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when problem occurs with the query.
   * @throws BizException   when the state of the object is invalid.
   */
  public ObjectDTO acceptOfferObject(int idObject) {
    try {
      dalServices.start();
      Object objectInDb = (Object) objectDAO.getObject(idObject);
      if (objectInDb == null) {
        throw new DAOException("The object doesn't exist.");
      }

      ObjectDTO objectDTO = objectInDb.acceptOfferObject();
      if (objectDTO == null) {
        throw new BizException("The object isn't in the required state.");
      }

      objectInDb = (Object) objectDAO.acceptOffer(objectDTO);
      if (objectInDb == null) {
        throw new DAOException("The object wasn't updated cause of the query.");
      }

      objectDAO.addStateChange(idObject, ObjectState.ACCEPT_OFFER);

      dalServices.commit();
      if (objectInDb.getOfferingMember() != null) {
        notificationUCC.addNotificationForAcceptOffer(objectInDb.getId(),
            objectInDb.getOfferingMember().getId());
      }

      return objectInDb;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Refuse a proposal for an object, updates the object's state and the reason for refusal if given
   * in the database.
   *
   * @param id             the id of the object
   * @param messageRefusal the reason for the refusal
   * @return the updated object after accepting the proposal
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when problem occurs with the query.
   * @throws BizException   when the state of the object is invalid.
   */
  public ObjectDTO refuseOfferObject(int id, String messageRefusal) {
    try {
      dalServices.start();
      Object objectInDb = (Object) objectDAO.getObject(id);
      if (objectInDb == null) {
        throw new DAOException("The object doesn't exist.");
      }

      ObjectDTO objectToUpdate = objectInDb.refuseOffer(messageRefusal);
      if (objectToUpdate == null) {
        throw new BizException("The object isn't in the required state.");
      }

      objectInDb = (Object) objectDAO.updateRefusedObject(objectToUpdate);
      if (objectInDb == null) {
        throw new DAOException("The object wasn't updated cause of the query.");
      }

      objectDAO.addStateChange(id, ObjectState.DENY_OFFER);
      dalServices.commit();
      if (objectInDb.getOfferingMember() != null) {
        notificationUCC.addNotificationForRefuseOffer(objectInDb.getId(),
            objectInDb.getOfferingMember().getId());
      }
      return objectInDb;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Change the state of the object as deposit at store.
   *
   * @param idObject the id of the object to update the state as sold.
   * @return the object with the updates.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when problem occurs with the query.
   * @throws BizException   when the state of the object is invalid.
   */
  @Override
  public ObjectDTO putDepositAtStore(int idObject) {
    try {
      dalServices.start();
      Object objectInDb = (Object) objectDAO.getObject(idObject);
      if (objectInDb == null) {
        throw new DAOException("The object doesn't exist.");
      }

      ObjectDTO objectUpdate = objectInDb.depositObjectAtStore();
      if (objectUpdate == null) {
        throw new BizException("The object isn't in the required state.");
      }

      objectInDb = (Object) objectDAO.depositAtStore(objectUpdate);
      if (objectInDb == null) {
        throw new DAOException("The object wasn't updated cause of the query.");
      }

      objectDAO.addStateChange(idObject, ObjectState.IN_STORE);
      dalServices.commit();
      return objectInDb;

    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Change the state of the object as deposit at the workshop.
   *
   * @param idObject the id of the object to update.
   * @return the object with the updates.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when problem occurs with the query.
   * @throws BizException   when the state of the object is invalid.
   */
  @Override
  public ObjectDTO putAtWorkshop(int idObject) {
    try {
      dalServices.start();
      Object objectInDb = (Object) objectDAO.getObject(idObject);
      if (objectInDb == null) {
        throw new DAOException("The object doesn't exist.");
      }

      ObjectDTO objectUpdate = objectInDb.putAtWorkshop();
      if (objectUpdate == null) {
        throw new BizException("The object isn't in the required state.");
      }

      objectInDb = (Object) objectDAO.depositAtWorkshop(objectUpdate);
      if (objectInDb == null) {
        throw new DAOException(
            "Error when executing the query to update the state to 'In workshop'.");
      }

      objectDAO.addStateChange(idObject, ObjectState.IN_WORKSHOP);
      dalServices.commit();
      return objectInDb;

    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Change the state of the object as 'Withdraw'.
   *
   * @param objectDTO the object to update.
   * @return the object with the updates.
   */
  @Override
  public ObjectDTO withdrawObject(ObjectDTO objectDTO) {
    try {
      dalServices.start();
      //      Object objectInDb = (Object) objectDAO.getObject(idObject);
      //      if (objectInDb == null) {
      //        throw new DAOException("The object doesn't exist.");
      //      }
      Object objectInDb = (Object) objectDTO;
      ObjectDTO objectUpdate = objectInDb.withdrawObject();
      if (objectUpdate == null) {
        throw new BizException("The object isn't in the required state.");
      }

      objectInDb = (Object) objectDAO.withdrawObject(objectUpdate);
      if (objectInDb == null) {
        throw new DAOException(
            "Error when executing the query to update the state to 'Withdrawn'.");
      }

      objectDAO.addStateChange(objectDTO.getId(), ObjectState.WITHDRAWN);
      dalServices.commit();
      return objectInDb;

    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Gets a list of the objects that had been withdrawn for the day.
   *
   * @return the list of objects withdraw.
   */
  @Override
  public ArrayList<ObjectDTO> listObjectsWithdraw() {
    try {
      dalServices.start();
      ArrayList<ObjectDTO> listObjects = objectDAO.listObjectWithdraw(LocalDate.now());
      if (listObjects == null) {
        throw new DAOException(
            "Error when executing the query to get a list of the objects in state of withdraw.");
      }
      dalServices.commit();
      return listObjects;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Offer an object.
   *
   * @param newObject the object to offer.
   * @return the id of the new object injected into the database.
   * @throws DAOException   if problem occurs in database while inserting the new object.
   * @throws FatalException if the availability does not exist.
   */
  @Override
  public int offerObject(ObjectDTO newObject) {

    try {
      int availabilityID = availabilityUCC.getAvailabilityId(newObject.getDateOfReceipt());
      if (availabilityID < 0) {
        throw new FatalException("Availability does not exist.");
      }
      dalServices.start();

      Object object = (Object) newObject;
      int idObject;

      if (newObject.getOfferingMember() != null && newObject.getOfferingMember().getId() != -1) {

        object.offerObjectWithMember(newObject.getOfferingMember(),
            newObject.getOfferingMember().getPhoneNumber(), newObject.getDescription(),
            newObject.getType(), newObject.getDateOfReceipt());
        idObject = objectDAO.offerMember(object, availabilityID);

      } else {

        object.offerObjectWithPhoneNumber(newObject.getPhoneNumber(),
            newObject.getDescription(), newObject.getType(), newObject.getDateOfReceipt());
        idObject = objectDAO.offerPhoneNumber(object, availabilityID);
      }

      if (idObject <= 0) {
        throw new DAOException("Error while inserting the new object to offer.");
      }

      objectDAO.addStateChange(idObject, ObjectState.OFFERED_ITEM);
      dalServices.commit();
      if (newObject.getOfferingMember() != null) {
        notificationUCC.addNotificationForResponsables(idObject);
      }
      return idObject;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * This method retrieves a list of ObjectDTO objects associated with a specific user.
   *
   * @param idMember The user ID.
   * @return A list of ObjectDTO objects.
   * @throws DAOException   when user is not found.
   * @throws FatalException when problem occurs in DAO.
   */
  @Override
  public List<ObjectDTO> memberObjects(int idMember) {
    try {
      dalServices.start();
      MemberDTO user = memberDAO.getMemberByID(idMember);
      if (user == null) {
        throw new DAOException("Error user not found");
      }
      List<ObjectDTO> listeObjets = objectDAO.memberObjects(idMember);
      dalServices.commit();
      return listeObjets;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * This method retrieves a list of objects in state "offered".
   *
   * @return A list of ObjectDTO objects.
   * @throws FatalException when problem occurs in DAO.
   */
  @Override
  public List<ObjectDTO> offeredObjects() {
    try {
      dalServices.start();
      List<ObjectDTO> listOfferedObjects = objectDAO.offeredObjects();
      dalServices.commit();
      return listOfferedObjects;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }


  /**
   * Get the list of all types in database.
   *
   * @return A list of all types in database.
   * @throws FatalException when problem occurs in DAO.
   */
  @Override
  public List<TypeDTO> getAllTypes() {
    try {
      dalServices.start();
      List<TypeDTO> listAllTypes = objectDAO.listTypesObjects();
      dalServices.commit();
      return listAllTypes;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Get the list of all states in ObjectState.
   *
   * @return A list of all states in ObjectState.
   */
  @Override
  public ArrayList<String> getAllStates() {
    ArrayList<String> listStates = new ArrayList<>();
    for (ObjectState state : ObjectState.values()) {
      listStates.add(state.getNameState());
    }
    return listStates;
  }


  /**
   * get the list of objects corresponding to the values given in the parameters.
   *
   * @param minPrice the minimum price of the objects.
   * @param maxPrice the maximum price of the objects.
   * @param idType   the id of the type of the object.
   * @return the list of objects found.
   */
  @Override
  public List<ObjectDTO> searchObjects(double minPrice, double maxPrice, int idType) {
    List<ObjectDTO> list;

    try {
      dalServices.start();
      list = objectDAO.searchObjects(minPrice, maxPrice, idType);
      dalServices.commit();

    } catch (FatalException e) {

      dalServices.rollback();
      throw e;
    }
    return list;
  }

  /**
   * Get the list of objects with correspondant date of deposit in container park.
   *
   * @param dateReceipt the reception date of object to the container park.
   * @return the list of objects.
   */
  @Override
  public List<ObjectDTO> searchObjectsByDate(LocalDate dateReceipt) {
    List<ObjectDTO> list;

    try {
      dalServices.start();
      list = objectDAO.searchObjectsByDate(dateReceipt);
      dalServices.commit();
      return list;

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  @Override
  public List<ObjectDTO> searchByType(String type) {
    List<ObjectDTO> list = getAllObject();

    if (!type.equalsIgnoreCase("all")) {
      list = list.stream()
          .filter(o -> o.getType().getTypeName().equals(type))
          .collect(Collectors.toList());
    }

    ObjectState[] allowedStates = {
        ObjectState.AVAILABLE_FOR_SALE,
        ObjectState.SOLD,
        ObjectState.IN_STORE
    };

    list = list.stream()
        .filter((object) ->
            object.getState().equals(allowedStates[0])
                || object.getState().equals(allowedStates[1])
                || object.getState().equals(allowedStates[2]))
        .toList();

    return list;
  }

  /**
   * Update an object.
   *
   * @param objectUpdated the object with the updates.
   * @return true if the object was correctly updated, false otherwise.
   * @throws DAOException if problem occurs in database while inserting the new object.
   */
  @Override
  public boolean updateObject(ObjectDTO objectUpdated) {
    try {
      dalServices.start();
      Object objectInDb = (Object) objectDAO.getObject(objectUpdated.getId());
      if (objectInDb == null) {
        throw new DAOException("The object doesn't exist.");
      }

      TypeDTO type = objectUpdated.getType();
      type.setId(objectDAO.getTypeByName(type.getTypeName()));

      ObjectDTO objectUpdate = objectInDb.updateObject(objectUpdated);

      objectUpdate = objectDAO.updateObject(objectUpdate);
      if (objectUpdate == null) {
        throw new DAOException("Error when executing the query to update the object.");
      }

      PictureDTO pictureDTO = pictureDAO.updatePictureObjet(objectInDb.getPicture());
      if (pictureDTO == null) {
        throw new DAOException(
            "Error when executing the query to update the picture of the object.");
      }
      objectDAO.addStateChange(objectUpdated.getId(), objectUpdate.getState());
      dalServices.commit();
      return true;

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  private List<ObjectDTO> getAllObject() {

    List<ObjectDTO> list;
    try {
      dalServices.start();
      list = objectDAO.getAllObject();
      dalServices.commit();
    } catch (FatalException exc) {
      dalServices.rollback();
      throw exc;
    }
    return list;
  }

  @Override
  public List<ObjectDTO> search(double minPrice, double maxPrice, LocalDate date, List<String> type,
      String state, String sbd, String sbp) {

    List<ObjectDTO> list = getAllObject();

    if (!sbp.equalsIgnoreCase("0")) {
      list = list.stream()
          .filter(object -> object.getPrice() >= minPrice)
          .collect(Collectors.toList());

      list = list.stream()
          .filter(object -> object.getPrice() <= maxPrice)
          .collect(Collectors.toList());
    }

    if (state != null && !state.isBlank()) {
      list = list.stream()
          .filter((object) -> object.getState().getNameState().equalsIgnoreCase(state))
          .collect(Collectors.toList());
    }

    if (date != null && sbd.equalsIgnoreCase("Y")) {
      list = list.stream()
          .filter(object -> object.getDateOfReceipt().getDate().equals(date))
          .collect(Collectors.toList());
    }
    if (type != null && !type.isEmpty() && !type.contains("all")) {
      list = list.stream()
          .filter(object -> type.contains(object.getType().getTypeName()))
          .collect(Collectors.toList());
    }
    if (sbp.equalsIgnoreCase("A")) {
      list = list.stream()
          .sorted((o1, o2) -> (int) Math.round(o1.getPrice() - o2.getPrice()))
          .toList();
    } else if (sbp.equalsIgnoreCase("D")) {
      list = list.stream()
          .sorted((o1, o2) -> (int) Math.round(o2.getPrice() - o1.getPrice()))
          .toList();
    }

    ObjectState[] allowedStates = {
        ObjectState.AVAILABLE_FOR_SALE,
        ObjectState.SOLD,
        ObjectState.IN_STORE
    };

    if (state == null || state.isBlank()) {
      list = list.stream()
          .filter((object) ->
              object.getState().equals(allowedStates[0])
                  || object.getState().equals(allowedStates[1])
                  || object.getState().equals(allowedStates[2]))
          .toList();

    }

    return list;
  }

  /**
   * Get all the object in state : "In store", "Available for sale" and "Sold".
   *
   * @return the list of the objects.
   */
  @Override
  public List<ObjectDTO> getObjectForCarousel() {
    try {
      dalServices.start();
      ArrayList<ObjectDTO> objects = objectDAO.getObjectsForCarousel();

      dalServices.commit();

      return objects;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Check the objects in state 'available for sale' that are up than 30 open days to withdraw.
   *
   * @return a list of the objects to withdraw
   */
  @Override
  public ArrayList<ObjectDTO> checkObjectToWithdraw() {
    ArrayList<ObjectDTO> objects;
    try {
      dalServices.start();
      objects = objectDAO.getObjectsInSell();
      dalServices.commit();
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
    ArrayList<ObjectDTO> objectsToWithdraw = new ArrayList<>();
    for (ObjectDTO objectDTO : objects) {
      Object object = (Object) objectDTO;
      LocalDate depositDate = object.getStoreDepositDate();
      if (depositDate != null
          && object.getWorkingDaysBetweenDates(depositDate, LocalDate.now()) > nbBusinessDays) {
        objectsToWithdraw.add(objectDTO);
      }
    }
    return objectsToWithdraw;
  }


  @Override
  public void setPicture(int id, String filePath) {
    try {
      dalServices.start();
      objectDAO.setPicture(id, filePath);
      dalServices.commit();
    } catch (FatalException exc) {
      dalServices.rollback();
      throw exc;
    }
  }

  /**
   * Searches for objects with the given state.
   *
   * @param state the state of the objects to search for
   * @return a List of ObjectDTOs with the given state
   * @throws FatalException if an SQLException occurs while querying the database
   */
  @Override
  public List<ObjectDTO> searchObjectsByState(String state) {
    List<ObjectDTO> list;

    try {
      dalServices.start();
      list = objectDAO.searchObjectsByState(state);
      dalServices.commit();
      return list;

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  @Override
  public Map<ObjectState, Integer> getStats(LocalDate dateMax) {
    Map<ObjectState, Integer> stats = new HashMap<>();
    List<ObjectDTO> objects = getAllObject();

    try {
      dalServices.start();
      for (ObjectDTO o : objects) {
        List<StateChange> changes = objectDAO.getStateChanges();

        StateChange change = changes.stream()
            .filter((change1) -> change1.getObjectID() == o.getId())
            .filter((change1) -> change1.getDate().isBefore(dateMax))
            .reduce((change1, change2) ->
                change1.getDate().isAfter(change2.getDate()) ? change1 : change2)
            .orElse(null);

        if (change != null) {
          ObjectState state = change.getState();
          stats.put(state, stats.getOrDefault(state, 0) + 1);
        }
      }
      dalServices.commit();

    } catch (FatalException exc) {
      dalServices.rollback();
      throw exc;
    }

    return stats;
  }

  public Map<ObjectState, Integer> getStats() {
    return getStats(LocalDate.now());
  }

  @Override
  public int getCount(LocalDate dateMin, LocalDate dateMax) {
    int count = 0;
    List<ObjectDTO> objects = getAllObject();

    for (ObjectDTO object : objects) {
      if ((dateMax == null || object.getDateOfReceipt().getDate().isBefore(dateMax))
          && (dateMin == null
          || !(object.getSellingDate() != null
          && object.getSellingDate().isBefore(dateMin)
          || object.getMarketWithdrawalDate() != null
          && object.getMarketWithdrawalDate().isBefore(dateMin)
          || object.getAcceptanceDate() != null
          && object.getAcceptanceDate().isBefore(dateMin)
          && object.getReasonForRefusal() != null))) {
        count++;
      }
    }
    return count;
  }

  @Override
  public List<StateChange> getStateStats(ObjectState state, LocalDate dateMin, LocalDate dateMax) {
    List<StateChange> changes = null;
    try {
      dalServices.start();
      changes = objectDAO.getStateChanges();
      dalServices.commit();
    } catch (FatalException exc) {
      dalServices.rollback();
      throw exc;
    }

    if (state != null) {
      changes = changes.stream()
          .filter((change) -> change.getState() != null && change.getState().equals(state))
          .collect(Collectors.toList());
    } else {
      List<StateChange> newChanges = new ArrayList<>();
      for (StateChange sc1 : changes) {
        boolean addToList = true;
        for (StateChange sc2 : newChanges) {
          if (sc1.getObjectID() == sc2.getObjectID()) {
            addToList = false;
          }
        }

        if (addToList) {
          newChanges.add(sc1);
        }

      }
      changes = newChanges;
    }
    if (dateMax != null) {
      changes = changes.stream()
          .filter((change) -> change.getDate().isBefore(dateMax))
          .collect(Collectors.toList());
    }
    if (dateMin != null) {
      changes = changes.stream()
          .filter((change) -> change.getDate().isAfter(dateMin))
          .collect(Collectors.toList());
    }

    return changes;
  }
}
