package be.vinci.pae.services.notification;

import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.notification.Notification;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.dal.DalServices;
import be.vinci.pae.services.notificationmember.NotificationMembersUCC;
import be.vinci.pae.services.object.ObjectDAO;
import be.vinci.pae.services.object.ObjectDTO;
import jakarta.inject.Inject;
import java.util.List;


/**
 * implementation of the interface  NotificationUCC.
 *
 * @author Loubna
 */

public class NotificationUCCImpl implements NotificationUCC {


  @Inject
  NotificationDAO notificationDAO;

  @Inject
  NotificationMembersUCC notificationMembersUCC;

  @Inject
  DalServices dalServices;
  @Inject
  Factory myFactory;

  @Inject
  private ObjectDAO objectDAO;

  /**
   * get the notification with id.
   *
   * @param id of the notification
   * @return an NotificationDTO with the requested notification's information.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the object is not found.
   */
  @Override
  public NotificationDTO getNotification(int id) {
    try {
      dalServices.start();
      NotificationDTO notif = notificationDAO.getNotification(id);
      if (notif == null) {
        throw new DAOException("The notification doesn't exist.");
      }
      dalServices.commit();
      return notif;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * get the notification with idObject.
   *
   * @param idObject of the object for the notification.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the object is not found.
   */
  @Override
  public void addNotificationForResponsables(int idObject) {
    Notification notification = myFactory.getNotification();
    notification.setNotification("Nouvelle proposition d'objet !",
        "Cliquez pour voir le détail.", idObject);
    try {
      dalServices.start();

      NotificationDTO notificationDTO = notificationDAO.addNotification(notification);
      if (notificationDTO == null) {
        throw new DAOException("Problem occurred when inserting a notification.");
      }
      dalServices.commit();

      int idNotificationMember = notificationMembersUCC.addNotificationToStaffMembers(
          notificationDTO.getId());

      notificationDTO.setId(idNotificationMember);
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * get the notification with idObject.
   *
   * @param idObject of the object for the notification.
   * @param idMember the id of the member who made the offer
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when problem occurs with the database.
   */
  @Override
  public void addNotificationForAcceptOffer(int idObject, int idMember) {
    Notification notification = myFactory.getNotification();
    notification.setNotification("Félicitation votre proposition a été acceptée !",
        "On vous attend avec plaisir le samedi choisi pour déposer votre objet.",
        idObject);
    try {
      dalServices.start();
      NotificationDTO notificationDTO = notificationDAO.addNotification(notification);
      if (notificationDTO == null) {
        throw new DAOException(
            "Problem occurred when inserting a notification for accepting offer.");
      }
      dalServices.commit();

      int idNotificationMember = notificationMembersUCC.addNotificationForMember(
          notificationDTO.getId(), idMember);

      notificationDTO.setId(idNotificationMember);
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Trigger a new notification for refuse offer.
   *
   * @param idObject of the object.
   * @param idMember the id of the member who made the offer
   */
  @Override
  public void addNotificationForRefuseOffer(int idObject, int idMember) {
    Notification notification = myFactory.getNotification();
    notification.setNotification("Malheureusement votre proposition a été refusée !",
        "Cliquez ici pour voir la raison du refus.",
        idObject);
    try {
      dalServices.start();
      NotificationDTO notificationDTO = notificationDAO.addNotification(notification);
      if (notificationDTO == null) {
        throw new DAOException(
            "Problem occurred when inserting a notification for refusing offer.");
      }
      dalServices.commit();

      int idNotificationMember = notificationMembersUCC.addNotificationForMember(
          notificationDTO.getId(), idMember);

      notificationDTO.setId(idNotificationMember);
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  //  /**
  //   * get the notification with the specified ID.
  //   *
  //   * @param idObject the ID of the notification to retrieve.
  //   * @return a NotificationDTO object with the requested notification's information.
  //   * @throws DAOException   if the specified notification is not found in the database.
  //   * @throws BizException   if the notification is not in the required state.
  //   * @throws FatalException if an error occurs while accessing the database.
  //   */
  //
  //  @Override
  //  public NotificationDTO getNotificationsOfferedObjects(int idObject) {
  //    try {
  //      dalServices.start();
  //
  //      Notification notifInDB = (Notification) notificationDAO.getNotification(idObject);
  //      if (notifInDB == null) {
  //        throw new DAOException("the notification is not find in the db");
  //      }
  //      NotificationDTO notifDTO = notifInDB.checkNotif();
  //      if (notifDTO == null) {
  //        throw new BizException("the notification is not in the required state");
  //      }
  //      NotificationDTO notifications = notificationDAO.getNotificationsOfferedObjects(idObject);
  //      dalServices.commit();
  //      return notifications;
  //    } catch (FatalException e) {
  //      dalServices.rollback();
  //    }
  //    return null;
  //  }


  /**
   * Adds a notification for a specified member to the database.
   *
   * @param objectDTO the object for which a notification is to be created.
   * @param title     the title of the notification.
   * @param texte     the content of the notification.
   * @throws FatalException if a SQLException is thrown during the execution of the method.
   */
  public void addNotificationToMember(ObjectDTO objectDTO, String title, String texte) {
    try {
      dalServices.start();

      Notification notification = myFactory.getNotification();

      NotificationDTO notificationDomain = notification.setNotification(title, texte,
          objectDTO.getId());

      NotificationDTO notificationDTO = notificationDAO.addNotification(notificationDomain);

      NotificationDTO notificationDB = notificationDAO.addNotificationToMember(notificationDTO,
          objectDTO.getOfferingMember().getId());

      if (notificationDB == null) {
        throw new DAOException(
            "Error when executing the query when registering a new user.");
      }

      dalServices.commit();

    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }

  /**
   * Get all the Notification of a person.
   *
   * @param id the id of the member.
   * @return return a list of theirs  notifications.
   */
  @Override
  public List<NotificationDTO> getAllNotificationsMember(int id) {
    try {
      dalServices.start();
      List<NotificationDTO> list = notificationDAO.listNotificationsMember(id);

      dalServices.commit();
      return list;
    } catch (FatalException e) {
      dalServices.rollback();
      throw e;
    }
  }
}
