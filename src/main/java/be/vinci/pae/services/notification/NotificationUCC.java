package be.vinci.pae.services.notification;

import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.object.ObjectDTO;
import java.util.List;

/**
 * the interface NotificationUCC.
 *
 * @author Loubna
 */
public interface NotificationUCC {

  /**
   * get the notification with id.
   *
   * @param id of the notification
   * @return an NotificationDTO with the requested notification's information.
   * @throws FatalException when problem occurs in DAO.
   * @throws DAOException   when the object is not found.
   */
  NotificationDTO getNotification(int id);

  //  /**
  //   * get the notification with the specified ID.
  //   *
  //   * @param idObject the ID of the notification to retrieve.
  //   * @return a NotificationDTO object with the requested notification's information.
  //   * @throws DAOException   if the specified notification is not found in the database.
  //   * @throws BizException   if the notification is not in the required state.
  //   * @throws FatalException if an error occurs while accessing the database.
  //   */
  //

  //  NotificationDTO getNotificationsOfferedObjects(int idObject);

  /**
   * Adds a notification for a specified member to the database.
   *
   * @param objectDTO the object for which a notification is to be created.
   * @param title     the title of the notification.
   * @param texte     the content of the notification.
   * @throws FatalException if a SQLException is thrown during the execution of the method.
   */
  void addNotificationToMember(ObjectDTO objectDTO, String title, String texte);

  /**
   * Trigger a new notification to be created.
   *
   * @param idObject of the object.
   */
  void addNotificationForResponsables(int idObject);

  /**
   * Trigger a new notification for accept offer.
   *
   * @param idObject of the object.
   * @param idMember the id of the member who made the offer
   */
  void addNotificationForAcceptOffer(int idObject, int idMember);

  /**
   * Trigger a new notification for refuse offer.
   *
   * @param idObject of the object.
   * @param idMember the id of the member who made the offer
   */
  void addNotificationForRefuseOffer(int idObject, int idMember);

  /**
   * Get all the Notification of a person.
   *
   * @param idMember the id of the member.
   * @return return a list of theirs  notifications.
   */
  List<NotificationDTO> getAllNotificationsMember(int idMember);
}
