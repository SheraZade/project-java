package be.vinci.pae.services.notification;

import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.object.Object;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.dal.DalBackendServices;
import be.vinci.pae.services.member.MemberDAO;
import be.vinci.pae.services.object.ObjectDAO;
import jakarta.inject.Inject;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * the implementation of the interface NotificationDAO.
 *
 * @author Loubna
 */
public class NotificationDAOImpl implements NotificationDAO {

  @Inject
  private Factory myFactory;
  @Inject
  private DalBackendServices dalBackendServices;
  @Inject
  private ObjectDAO objectDAO;

  @Inject
  private MemberDAO memberDAO;

  /**
   * fill notification passed in parameter by the resultSet in parameters.
   *
   * @param notif whose data is to be filled.
   * @param rs    the data to fill the object with.
   */
  private void fillNotification(NotificationDTO notif, ResultSet rs) throws SQLException {
    notif.setId(rs.getInt("id_notification"));
    notif.setDateNotifications(rs.getDate("notification_date").toLocalDate());
    notif.setTitleNotification(rs.getString("notification_title"));
    notif.setTextNotification(rs.getString("notification_text"));
    notif.setTextNotification(rs.getString("notification_text"));
    int idObject = rs.getInt("object_id");
    if (idObject > 0) {
      Object obj = (Object) objectDAO.getObject(idObject);
      notif.setIdObject(obj.getId());
    }
  }

  /**
   * get the notification with id.
   *
   * @param id of the notification
   * @return an notificationDTO with the requested notification's information.
   */
  @Override
  public NotificationDTO getNotification(int id) {
    String query = "SELECT * FROM RESSOURCERIE.notifications WHERE id_notification = ?";
    NotificationDTO notif = myFactory.getNotification();
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, id);
      try (ResultSet rs = ps.executeQuery()) {
        if (!rs.next()) {
          return null;
        }
        fillNotification(notif, rs);
      }
    } catch (SQLException e) {
      throw new FatalException("error not found object with the specific id", e.getCause());
    }
    return notif;
  }

  /**
   * Adds a notification for a specified member to the database.
   *
   * @param notification a NotificationDTO object containing the information of the notification to
   *                     insert.
   * @param memberId     the ID of the member for which the notification should be associated.
   * @return the notification object, or null if the insertion failed.
   * @throws FatalException if a SQLException is thrown during the execution of the method.
   */
  public NotificationDTO addNotificationToMember(NotificationDTO notification, int memberId) {
    String query = "INSERT INTO ressourcerie.notifications_members (notification, member, is_read)"
        + " VALUES (?, ?, ?)";
    try (PreparedStatement ps2 = dalBackendServices.getPrepareStatement(query)) {

      ps2.setInt(1, notification.getId());
      ps2.setInt(2, memberId);
      ps2.setBoolean(3, false);
      ps2.executeUpdate();

      return notification;
    } catch (SQLException e) {
      throw new FatalException("problem occurs while trying to access to the database",
          e.getCause());
    }
  }

  /**
   * Create a new notification and set it in data Base.
   *
   * @param notificationDTO notificationDTO to be set in DB.
   * @return return the new notificationDTO with the id in database.
   */
  @Override
  public NotificationDTO addNotification(NotificationDTO notificationDTO) {
    String query = "insert into ressourcerie.notifications( notification_date, notification_title, "
        + "notification_text, object_id) VALUES(?,?,?,?)";

    try (PreparedStatement ps = dalBackendServices.getPreparedStatementRGK(query)) {
      ps.setDate(1, Date.valueOf(notificationDTO.getDateNotifications()));
      ps.setString(2, notificationDTO.getTitleNotification());
      ps.setString(3, notificationDTO.getTextNotification());
      ps.setInt(4, notificationDTO.getIdObject());
      int executeLine = ps.executeUpdate();
      if (executeLine == 0) {
        return null;
      }
      try (ResultSet rs = ps.getGeneratedKeys()) {
        if (rs.next()) {
          notificationDTO.setId(rs.getInt(1));
        }
      }
    } catch (SQLException e) {
      throw new FatalException(
          "problem occurs while trying to insert a new notification into the database.",
          e.getCause());
    }
    return notificationDTO;
  }

  /**
   * Gets a list of all the notifications of the member given in argument.
   *
   * @param idMember the id of the member
   * @return a list of all notifications of the member
   */
  @Override
  public List<NotificationDTO> listNotificationsMember(int idMember) {
    List<NotificationDTO> listNotifications = new ArrayList<>();
    String query =
        "SELECT n.*, nm.is_read, nm.id_notifier_member FROM ressourcerie.notifications n, "
            + "ressourcerie.notifications_members nm "
            + "WHERE nm.member = ? AND n.id_notification = nm.notification "
            + "ORDER BY id_notification DESC ";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, idMember);
      try (ResultSet rs = ps.executeQuery()) {
        while (rs.next()) {
          NotificationDTO notificationDTO = myFactory.getNotification();
          fillNotification(notificationDTO, rs);
          notificationDTO.setIsRead(rs.getBoolean("is_read"));
          notificationDTO.setIdNotificationMember(rs.getInt("id_notifier_member"));
          listNotifications.add(notificationDTO);
        }
      }
      return listNotifications;
    } catch (SQLException e) {
      throw new FatalException("problem occurs when we trying to access database in getting all "
          + "the notifications member", e.getCause());
    }
  }

  /**
   * Gets a list of all unread notifications of the member given in argument.
   *
   * @param idMember the id member
   * @return a list of all unread notifications of the member
   */
  @Override
  public List<NotificationDTO> listUnreadNotificationsMember(int idMember) {
    List<NotificationDTO> listUnreadNotifications = new ArrayList<>();
    String query =
        "SELECT n.* FROM ressourcerie.notifications n, ressourcerie.notifications_members nm "
            + "WHERE nm.member = ? AND n.id_notification = nm.notification AND nm.is_read = FALSE";
    try (PreparedStatement ps = dalBackendServices.getPrepareStatement(query)) {
      ps.setInt(1, idMember);
      try (ResultSet resultSet = ps.executeQuery()) {
        while (resultSet.next()) {
          NotificationDTO notification = myFactory.getNotification();
          fillNotification(notification, resultSet);
          listUnreadNotifications.add(notification);
        }
      }
      return listUnreadNotifications;
    } catch (SQLException e) {
      throw new FatalException(
          "Problem occurs when we trying to access database for unread notifications",
          e.getCause());
    }
  }
}
