package be.vinci.pae.services.notification;

import be.vinci.pae.domain.notification.NotificationImpl;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.time.LocalDate;

/**
 * interface ObjectDTO.
 *
 * @author Loubna
 */
@JsonDeserialize(as = NotificationImpl.class)
public interface NotificationDTO {

  /**
   * Getter for id.
   *
   * @return the notification's ID.
   */
  int getId();

  /**
   * Getter for date.
   *
   * @return the notification's date.
   */
  LocalDate getDateNotifications();

  /**
   * Getter for title.
   *
   * @return the notification's title.
   */
  String getTitleNotification();

  /**
   * Getter for text.
   *
   * @return the notification's text.
   */
  String getTextNotification();

  /**
   * Getter of the id of the object.
   *
   * @return the id of the object.
   */
  int getIdObject();


  /**
   * Getter of the id member of the notification.
   *
   * @return the id of the object.
   */
  int getIdNotificationMember();

  /**
   * Setter of the id notification of a member .
   *
   * @param idNotificationMember the id notification member
   */
  void setIdNotificationMember(int idNotificationMember);

  /**
   * setter of text.
   *
   * @param text the text of the notification.
   */
  void setTextNotification(String text);

  /**
   * Get the value of is read.
   *
   * @return the value.
   */
  boolean getIsRead();

  /**
   * Set the new value of is read.
   *
   * @param isRead the new value.
   */
  void setIsRead(boolean isRead);

  /**
   * setter of object.
   *
   * @param object the object of the notification.
   */
  void setIdObject(int object);

  /**
   * setter of if.
   *
   * @param id the id of the notification.
   */
  void setId(int id);

  /**
   * setter of date.
   *
   * @param date the date of the notification.
   */
  void setDateNotifications(LocalDate date);

  /**
   * setter of title.
   *
   * @param title the tite of the notification.
   */
  void setTitleNotification(String title);

}

