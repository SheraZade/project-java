package be.vinci.pae.services.notification;

import java.util.List;

/**
 * the interface NotificationDAO.
 *
 * @author Loubna
 */

public interface NotificationDAO {

  /**
   * get the notification with id.
   *
   * @param id of the notification
   * @return an notificationDTO with the requested notification's information.
   */
  NotificationDTO getNotification(int id);

  /**
   * Adds a notification for a specified member to the database.
   *
   * @param notification a NotificationDTO object containing the information of the notification to
   *                     insert.
   * @param memberId     the ID of the member for which the notification should be associated.
   * @return the notification object, or null if the insertion failed.
   */
  NotificationDTO addNotificationToMember(NotificationDTO notification, int memberId);

  /**
   * Create a new notification and set it in data Base.
   *
   * @param notificationDTO notificationDTO to be set in dataBase.
   * @return return the new notificationDTO.
   */
  NotificationDTO addNotification(NotificationDTO notificationDTO);

  /**
   * Gets a list of all the notifications of the member given in argument.
   *
   * @param idMember the id of the member
   * @return a list of all notifications of the member
   */
  List<NotificationDTO> listNotificationsMember(int idMember);

  /**
   * Gets a list of all unread notifications of the member given in argument.
   *
   * @param idMember the id member
   * @return a list of all unread notifications of the member
   */
  List<NotificationDTO> listUnreadNotificationsMember(int idMember);


}
