package be.vinci.pae.exceptions;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * this exception class concern the exceptions related to the parameter tests invalid or null.
 *
 * @author Dorcas KUEZE.
 */

public class WebParametersException extends RuntimeException {

  /**
   * The response.
   */
  private Response response;

  /**
   * The status code.
   */
  private Status status;

  /**
   * Constructor of  WebParametersException.
   *
   * @param message message description exception.
   */
  public WebParametersException(String message) {
    super(message);
  }

  /**
   * Constructor with Response.
   *
   * @param message  message of exception.
   * @param response response launched.
   */
  public WebParametersException(String message, Response response) {
    this(message);
    this.response = response;
  }

  /**
   * Constructor with Status.
   *
   * @param message message of exception.
   * @param status  status code launched
   */
  public WebParametersException(String message, Response.Status status) {
    this(message);
    this.status = status;
  }

  /**
   * Getter for response.
   *
   * @return the response to be sent to the browser.
   */
  public Response getResponse() {
    return response;
  }

  /**
   * Getter for status.
   *
   * @return the status code to be sent to the browser.
   */
  public Status getStatus() {
    return status;
  }

}
