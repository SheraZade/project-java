package be.vinci.pae.exceptions;

/**
 * This is a custom exception class that represents a fatal error in the application. It is used to
 * indicate that an unrecoverable error(SqlException for example) has occurred and that the
 * application cannot continue running.
 *
 * @author Loubna Eljattari
 */

public class FatalException extends RuntimeException {

  /**
   * Exception.
   */
  public FatalException() {
    super();
  }

  /**
   * Exception with just a message.
   *
   * @param message the message.
   */
  public FatalException(String message) {
    super(message);
  }

  /**
   * Exception.
   *
   * @param message message.
   * @param cause   cause.
   */
  public FatalException(String message, Throwable cause) {
    super(message, cause);
  }
}
