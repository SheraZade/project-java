package be.vinci.pae.exceptions;


/**
 * This is a custom exception class that represents an unauthorized action in the business logic. It
 * is used when a user tries to perform an action for which they do not have the necessary
 * permissions.
 *
 * @author Loubna Eljattari
 */

public class BizException extends RuntimeException {

  /**
   * Exception.
   */
  public BizException() {
    super();
  }


  /**
   * Exception.
   *
   * @param message message.
   */
  public BizException(String message) {
    super(message);
  }

}
