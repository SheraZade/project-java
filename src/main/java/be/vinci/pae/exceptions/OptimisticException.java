package be.vinci.pae.exceptions;

/**
 * This is a custom exception class that is thrown when there is a problem of version when updating
 * an object into the database.
 *
 * @author Chehrazad Ouazzani
 */

public class OptimisticException extends RuntimeException {

  /**
   * Constructs a new runtime exception.
   */
  public OptimisticException() {
    super();
  }

  /**
   * Constructs a new runtime exception with a message.
   *
   * @param message the detail message.
   */
  public OptimisticException(String message) {
    super(message);
  }
}
