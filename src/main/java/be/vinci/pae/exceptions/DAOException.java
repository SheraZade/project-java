package be.vinci.pae.exceptions;

/**
 * DAOException  is a customized exception class. It's concerns the case of invalid parameters. and
 * the case of methods DAO that return null.
 *
 * @author Dorcas
 */

public class DAOException extends RuntimeException {

  /**
   * constructor of DAOException.
   *
   * @param message the message that give an explication about exception
   */
  public DAOException(String message) {
    super(message);
  }


}
