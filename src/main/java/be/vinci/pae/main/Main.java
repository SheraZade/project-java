package be.vinci.pae.main;


import be.vinci.pae.scheduler.QuartzConfig;
import be.vinci.pae.utils.ApplicationBinder;
import be.vinci.pae.utils.Config;
import be.vinci.pae.utils.Logs;
import be.vinci.pae.utils.WebExceptionMapper;
import java.io.IOException;
import java.net.URI;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Main class.
 */
public class Main {


  /**
   * field that gets the URL.
   */
  public static String BASE_URI;


  static {
    Config.load("dev.properties");
    BASE_URI = Config.getProperty("BaseUri");
  }

  /**
   * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
   *
   * @return Grizzly HTTP server.
   */
  public static HttpServer startServer() {
    final ResourceConfig rc = new ResourceConfig().packages("be.vinci.pae.api")
        .register(ApplicationBinder.class)
        .register(WebExceptionMapper.class);

    return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
  }

  /**
   * Main method.
   *
   * @param args command line arguments.
   * @throws IOException        the IOException.
   * @throws SchedulerException the scheduler exception.
   */
  public static void main(String[] args) throws IOException, SchedulerException {
    Logs logger = new Logs();
    logger.info("API server starting up");

    final HttpServer server = startServer();
    try {
      Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
      QuartzConfig quartzConfig = new QuartzConfig();
      quartzConfig.scheduleJobs(scheduler);
      scheduler.start();
    } catch (SchedulerException se) {
      logger.exception(se.getCause().getMessage(), se.getClass().getSimpleName());
    }
    System.out.printf("Jersey app started with WADL available at "
        + "%sapplication.wadl\nHit enter to stop it...%n", BASE_URI);
    System.in.read();
    logger.info("API server shutting down");
    server.shutdownNow();
  }
}