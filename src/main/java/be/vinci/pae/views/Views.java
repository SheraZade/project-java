package be.vinci.pae.views;

/**
 * the implementation of the classe Views.
 *
 * @author Loubna Eljattari
 */
public class Views {

  /**
   * empty classe.
   *
   * @author Loubna Eljattari
   */
  public static class Public {

  }

  /**
   * empty classe.
   *
   * @author Loubna Eljattari
   */
  public static class Internal extends Public {

  }

}
