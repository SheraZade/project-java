package be.vinci.pae.domain.statechange;

import be.vinci.pae.services.object.ObjectDTO.ObjectState;
import java.time.LocalDate;

/**
 * Changes in an object's state.
 *
 * @author Gabriel Tomson.
 */
public interface StateChange {

  /**
   * getter for state.
   *
   * @return the state.
   */
  ObjectState getState();

  /**
   * getter for date.
   *
   * @return the date.
   */
  LocalDate getDate();

  /**
   * getter for object ID.
   *
   * @return the object's ID.
   */
  int getObjectID();

  /**
   * setter for state.
   *
   * @param state the state.
   */
  void setState(ObjectState state);

  /**
   * setter for date.
   *
   * @param date the date.
   */
  void setDate(LocalDate date);

  /**
   * Setter for object ID.
   *
   * @param id the object's id.
   */
  void setObjectID(int id);

}
