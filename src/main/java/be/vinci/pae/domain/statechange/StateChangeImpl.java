package be.vinci.pae.domain.statechange;

import be.vinci.pae.services.object.ObjectDTO.ObjectState;
import java.time.LocalDate;

/**
 * Implementation of StateChange interface.
 *
 * @author Gabriel Tomson.
 */
public class StateChangeImpl implements  StateChange {

  private ObjectState state;
  private LocalDate date;
  private int objectID;

  /**
   * Constructeur pour StateChangeImpl.
   */
  public StateChangeImpl() {}

  @Override
  public LocalDate getDate() {
    return date;
  }

  @Override
  public ObjectState getState() {
    return state;
  }

  @Override
  public void setState(ObjectState state) {
    this.state = state;
  }

  @Override
  public void setDate(LocalDate date) {
    this.date = date;
  }

  @Override
  public int getObjectID() {
    return objectID;
  }

  @Override
  public void setObjectID(int objectID) {
    this.objectID = objectID;
  }
}
