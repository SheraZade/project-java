package be.vinci.pae.domain.notification;

import be.vinci.pae.services.notification.NotificationDTO;
import java.time.LocalDate;

/**
 * the implementation of the interface Notification.
 *
 * @author Loubna Eljattari
 */
public class NotificationImpl implements Notification {

  private int id;

  private LocalDate dateNotifications;

  private String titleNotification;

  private String textNotification;

  private int idObject;

  private boolean isRead;
  private int idNotificationMember;


  /**
   * Empty constructor.
   */
  public NotificationImpl() {
  }

  /**
   * Getter for id.
   *
   * @return the notification's ID.
   */
  public int getId() {
    return id;
  }

  /**
   * Getter of the id member of the notification.
   *
   * @return the id of the object.
   */
  @Override
  public int getIdNotificationMember() {
    return this.idNotificationMember;
  }

  /**
   * Setter of the id notification of a member .
   *
   * @param idNotificationMember the id notification member
   */
  @Override
  public void setIdNotificationMember(int idNotificationMember) {
    this.idNotificationMember = idNotificationMember;
  }

  /**
   * Getter for date.
   *
   * @return the notification's date.
   */
  public LocalDate getDateNotifications() {
    return dateNotifications;
  }

  /**
   * Getter for title.
   *
   * @return the notification's title.
   */
  public String getTitleNotification() {
    return titleNotification;
  }

  /**
   * Getter for text.
   *
   * @return the notification's text.
   */
  public String getTextNotification() {
    return textNotification;
  }

  /**
   * Getter for the offeringObject variable.
   *
   * @return the value of the offeringObject variable.
   */
  public int getIdObject() {
    return idObject;
  }

  /**
   * Get the value of is read of a notification.
   *
   * @return the boolean.
   */
  public boolean getIsRead() {
    return isRead;
  }

  /**
   * Set the new value of is read.
   *
   * @param isRead the new value.
   */
  @Override
  public void setIsRead(boolean isRead) {
    this.isRead = isRead;
  }

  /**
   * setter of text.
   *
   * @param text the text of the notification.
   */
  @Override
  public void setTextNotification(String text) {
    this.textNotification = text;
  }

  /**
   * setter of idOfferingObject.
   *
   * @param idObject the new value to set for the idOfferingObject variable.
   */
  @Override
  public void setIdObject(int idObject) {
    this.idObject = idObject;
  }

  /**
   * setter of text.
   *
   * @param id the text of the notification.
   */
  @Override
  public void setId(int id) {
    this.id = id;
  }

  /**
   * setter of date.
   *
   * @param date the date of the notification.
   */
  @Override
  public void setDateNotifications(LocalDate date) {
    this.dateNotifications = date;
  }

  /**
   * setter of title.
   *
   * @param title the title of the notification.
   */
  @Override
  public void setTitleNotification(String title) {
    this.titleNotification = title;
  }


  /**
   * Create a new notification.
   *
   * @return return the new notificationDTO.
   */
  @Override
  public NotificationDTO setNotification(String titleNotification, String textNotification,
      int idObject) {

    this.dateNotifications = LocalDate.now();
    this.titleNotification = titleNotification;
    this.textNotification = textNotification;
    this.idObject = idObject;

    return this;
  }
}
