package be.vinci.pae.domain.notification;

import be.vinci.pae.services.notification.NotificationDTO;

/**
 * the interface of Notification.
 *
 * @author Loubna ELjattari
 */
public interface Notification extends NotificationDTO {

  /**
   * Create a new notification.
   *
   * @param titleNotification the title of the notification
   * @param textNotification  the addition information of the notification
   * @param idObject          the id of the object
   * @return the new notificationDTO.
   */
  NotificationDTO setNotification(String titleNotification, String textNotification, int idObject);
}
