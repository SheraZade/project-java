package be.vinci.pae.domain.member;

import be.vinci.pae.services.member.MemberDTO;

/**
 * the interface of User.
 *
 * @author Chehrazad Ouazzani
 */
public interface Member extends MemberDTO {


  /**
   * hash the given password.
   *
   * @param password the password entered by the user.
   * @return the password crypted.
   */
  String hashPassword(String password);

  /**
   * check if the given password is identical to the one in database.
   *
   * @param password the password entered by the user.
   * @return true if identical, false if not.
   */
  boolean isValidPassword(String password);

  /**
   * Confirm the assistant role to a member.
   *
   * @return the new assistant with the update.
   */
  MemberDTO confirmAssistantRole();

  /**
   * Confirm the responsable role to a member.
   *
   * @return the new responsable with the update.
   */
  MemberDTO confirmResponsableRole();

  /**
   * Create a new member.
   *
   * @param newMember the new member
   * @return the member registered
   */
  MemberDTO createMember(MemberDTO newMember);

  /**
   * Modified user data.
   *
   * @param user the user to modify.
   * @return a new data user.
   */
  MemberDTO modifiedMember(MemberDTO user);
}
