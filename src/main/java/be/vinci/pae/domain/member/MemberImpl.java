package be.vinci.pae.domain.member;

import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.views.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import java.time.LocalDate;
import org.mindrot.jbcrypt.BCrypt;

/**
 * the implementation of User.
 *
 * @author Chehrazad Ouazzani
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class MemberImpl implements Member {

  @JsonIgnore
  private int idMember;

  @JsonView(Views.Public.class)
  private String firstname;

  @JsonView(Views.Public.class)
  private String lastname;

  @JsonView(Views.Public.class)
  private String email;

  private String passwordMember;

  @JsonIgnore
  private RolesRessourceries roleMember = RolesRessourceries.PUBLIC;

  @JsonView(Views.Internal.class)
  private String phoneNumber;

  @JsonView(Views.Public.class)
  private LocalDate registrationDate;

  @JsonView(Views.Public.class)
  private Picture picture;

  @JsonIgnore
  private int version;


  /**
   * Empty constructor.
   */
  public MemberImpl() {
  }

  @Override
  public String hashPassword(String password) {
    return BCrypt.hashpw(password, BCrypt.gensalt());
  }

  @Override
  public boolean isValidPassword(String password) {
    return BCrypt.checkpw(password, this.passwordMember);
  }

  @Override
  public int getId() {
    return this.idMember;
  }

  @Override
  public void setId(int id) {
    this.idMember = id;
  }

  /**
   * getter of version.
   *
   * @return the version.
   */
  @Override
  public int getVersion() {
    return version;
  }

  /**
   * setter of the version.
   *
   * @param version the version.
   */
  @Override
  public void setVersion(int version) {
    this.version = version;
  }

  @Override
  public String getFirstName() {
    return this.firstname;
  }

  @Override
  public void setFirstName(String firstname) {
    this.firstname = firstname;
  }

  @Override
  public String getLastName() {
    return this.lastname;
  }

  @Override
  public void setLastName(String lastname) {
    this.lastname = lastname;
  }

  @Override
  public String getEmail() {
    return this.email;
  }

  @Override
  public void setEmail(String mail) {
    this.email = mail;
  }

  @Override
  public String getEncryptedPassword() {
    return this.passwordMember;
  }

  @Override
  public void setEncryptedPassword(String cryptedPassword) {
    this.passwordMember = cryptedPassword;
  }

  @Override
  public String getRole() {
    return this.roleMember.getRole();
  }

  @Override
  public void setRole(String role) {
    for (RolesRessourceries value : RolesRessourceries.values()) {
      if (value.getRole().equals(role)) {
        this.roleMember = value;
        break;
      }
    }
    if (this.roleMember == null) {
      throw new IllegalArgumentException("error role not found");
    }
  }

  public void setRole(RolesRessourceries roleMember) {
    this.roleMember = roleMember;
  }

  @Override
  public String getPhoneNumber() {
    return this.phoneNumber;
  }

  @Override
  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @Override
  public LocalDate getRegistrationDate() {
    return this.registrationDate;
  }

  @Override
  public void setRegistrationDate(LocalDate subscribeDate) {
    this.registrationDate = subscribeDate;
  }

  @Override
  public Picture getPicture() {
    return this.picture;
  }

  @Override
  public void setPicture(Picture picture) {
    this.picture = picture;
  }

  @Override
  public MemberDTO confirmAssistantRole() {
    if (!this.getRole().equals(RolesRessourceries.MEMBER.getRole())
        && !this.getRole().equals(RolesRessourceries.RESPONSABLE.getRole())) {
      return null;
    }
    this.roleMember = RolesRessourceries.STAFF;
    return this;
  }

  @Override
  public MemberDTO confirmResponsableRole() {
    if (!this.getRole().equals(RolesRessourceries.MEMBER.getRole())
        && !this.getRole().equals(RolesRessourceries.STAFF.getRole())) {
      return null;
    }
    this.roleMember = RolesRessourceries.RESPONSABLE;
    return this;
  }


  @Override
  public MemberDTO createMember(MemberDTO newMember) {
    setFirstName(newMember.getFirstName());
    setLastName(newMember.getLastName());
    setEmail(newMember.getEmail());
    setPhoneNumber(newMember.getPhoneNumber());
    setEncryptedPassword(hashPassword(newMember.getEncryptedPassword()));
    setRegistrationDate(LocalDate.now());
    setRole(RolesRessourceries.MEMBER);
    setPicture(newMember.getPicture());
    setVersion(1);
    return this;
  }

  @Override
  public MemberDTO modifiedMember(MemberDTO user) {
    setFirstName(user.getFirstName());
    setLastName(user.getLastName());
    setEmail(user.getEmail());
    setPhoneNumber(user.getPhoneNumber());
    if (!user.getPicture().getUrl().isBlank()) {
      this.picture.setUrl(user.getPicture().getUrl());
    }
    if (!user.getEncryptedPassword().isBlank()) {
      setEncryptedPassword(hashPassword(user.getEncryptedPassword()));
    }
    return this;
  }
}
