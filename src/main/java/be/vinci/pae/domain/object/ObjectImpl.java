package be.vinci.pae.domain.object;


import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.domain.type.Type;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.object.ObjectDTO;
import be.vinci.pae.utils.Logs;
import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * Implementation of Object interface.
 *
 * @author Gabriel Tomson.
 */
public class ObjectImpl implements Object {

  private static final Logs logger = new Logs();
  private int id;
  private double price;
  private Type type;
  private String description;
  private String reasonForRefusal;
  private ObjectState state;
  private String phoneNumber;
  private Availability dateOfReceipt;
  private LocalDate acceptanceDate;
  private LocalDate sellingDate;
  private LocalDate marketWithdrawalDate;
  private LocalDate storeDepositDate;
  private MemberDTO offeringMember;
  private Picture picture;
  private int version;


  /**
   * Constructor for ObjectImpl.
   */
  public ObjectImpl() {
  }

  @Override
  public void offerObjectWithMember(MemberDTO offeringMember, String phoneNumber,
      String description, Type type, Availability dateOfReceipt) {
    setOfferingMember(offeringMember);
    setPhoneNumber(phoneNumber);
    setDescription(description);
    setType(type);
    setDateOfReceipt(dateOfReceipt);
    setState(ObjectState.OFFERED_ITEM);
    setVersionNumberObject(1);
  }

  @Override
  public void offerObjectWithPhoneNumber(String phoneNumber, String description, Type type,
      Availability dateOfReceipt) {
    offerObjectWithMember(null, phoneNumber, description, type, dateOfReceipt);
  }

  @Override
  public ObjectDTO depositObjectAtStore() {
    if (this.state != ObjectState.ACCEPT_OFFER && this.state != ObjectState.IN_WORKSHOP) {
      return null;
    }
    setStoreDepositDate(LocalDate.now());
    logger.info("object " + id + " sent to storefront");
    setState(ObjectState.IN_STORE);
    return this;
  }

  /**
   * Put the state of the object at the workshop.
   *
   * @return the object updated
   */
  @Override
  public ObjectDTO putAtWorkshop() {
    if (this.state != ObjectState.ACCEPT_OFFER) {
      return null;
    }
    setState(ObjectState.IN_WORKSHOP);
    logger.info("object " + id + " sent to workshop");
    return this;
  }

  /**
   * setter of the price of the object.
   *
   * @param price the price of the object.
   * @return the object updated.
   */
  @Override
  public ObjectDTO checkAndSetToPrice(double price) {

    if (price <= 0) {
      return null;
    }
    if (!this.getState().equals(ObjectState.IN_STORE)) {
      return null;
    }
    setPrice(price);
    setSellingDate(LocalDate.now());
    logger.info("price of object " + id + " set to " + price);
    setState(ObjectState.AVAILABLE_FOR_SALE);
    return this;
  }

  /**
   * check the state of the object and put it as sold.
   *
   * @return if the state is valid then set the object or returns null
   */
  @Override
  public ObjectDTO checkAndSetSold() {
    if (!this.state.equals(ObjectState.AVAILABLE_FOR_SALE)) {
      return null;
    }
    setState(ObjectState.SOLD);
    logger.info("object " + id + " sold for " + price);
    setSellingDate(LocalDate.now());
    return this;
  }

  /**
   * Getter of version.
   *
   * @return the version
   */
  @Override
  public int getVersionNumberObject() {
    return version;
  }

  /**
   * Setter of version.
   *
   * @param version of the object
   */
  @Override
  public void setVersionNumberObject(int version) {
    this.version = version;
  }

  @Override
  public ObjectDTO refuseOffer(String message) {
    if (this.state != ObjectState.OFFERED_ITEM) {
      return null;
    }
    setReasonForRefusal(message);
    logger.info("object " + id + " refused for the following reason : " + message);
    setState(ObjectState.DENY_OFFER);
    return this;
  }

  /**
   * accept the object.
   *
   * @return the object updated
   */
  @Override
  public ObjectDTO acceptOfferObject() {
    if (this.state != ObjectState.OFFERED_ITEM) {
      return null;
    }
    setAcceptanceDate(LocalDate.now());
    logger.info("object " + id + " accepted");
    setState(ObjectState.ACCEPT_OFFER);
    return this;
  }

  /**
   * Withdraw the object.
   *
   * @return the object updated
   */
  @Override
  public ObjectDTO withdrawObject() {
    if (this.state != ObjectState.AVAILABLE_FOR_SALE) {
      return null;
    }
    setState(ObjectState.WITHDRAWN);
    setMarketWithdrawalDate(LocalDate.now());
    return this;
  }


  /**
   * Getter for id. To be removed once ObjectDTO is created.
   *
   * @return the object's ID.
   */
  public int getId() {
    return id;
  }

  /**
   * setter of id.
   *
   * @param id the id of the object.
   */
  @Override
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Getter for price. To be removed once ObjectDTO is created.
   *
   * @return the object's price.
   */
  public double getPrice() {
    return price;
  }

  /**
   * Getter for picture.
   *
   * @return the object's picture.
   */
  @Override
  public Picture getPicture() {
    return picture;
  }

  /**
   * setter for picture.
   *
   * @param picture the object's picture.
   */
  @Override
  public void setPicture(Picture picture) {
    this.picture = picture;
  }

  /**
   * setter of the price.
   *
   * @param price the price of the object.
   */
  @Override
  public void setPrice(double price) {

    this.price = price;
  }

  /**
   * Getter for type. To be removed once ObjectDTO is created.
   *
   * @return the object's type.
   */
  public Type getType() {
    return type;
  }

  /**
   * setter of the type.
   */
  public void setType(Type type) {
    this.type = type;
    logger.info("type of object " + id + " set to " + type);
  }

  /**
   * Getter for description. To be removed once ObjectDTO is created.
   *
   * @return the object's description.
   */
  public String getDescription() {
    return description;
  }

  /**
   * setter of description. Update description.
   */

  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Getter for reasonOfRefusal. To be removed once ObjectDTO is created.
   *
   * @return the reason for the object's refusal.
   */
  public String getReasonForRefusal() {
    return reasonForRefusal;
  }

  /**
   * setter of reasonForRefusal.
   */
  public void setReasonForRefusal(String reasonForRefusal) {
    this.reasonForRefusal = reasonForRefusal;
  }

  /**
   * Getter for state. To be removed once ObjectDTO is created.
   *
   * @return the object's state.
   */
  public ObjectState getState() {
    return state;
  }

  /**
   * setter of state.
   *
   * @param state the state of object.
   */
  @Override
  public void setState(ObjectState state) {
    this.state = state;
    logger.info("state of object " + id + " set to " + state.getNameState());
  }

  /**
   * setter of state.
   */
  public void setState(String state) {
    for (ObjectState value : ObjectState.values()) {
      if (value.getNameState().equals(state)) {
        this.state = value;
      }
    }
    if (this.state == null) {
      throw new IllegalArgumentException("error state not found");
    }
  }

  /**
   * Getter for phoneNumber. To be removed once ObjectDTO is created.
   *
   * @return the phone number of the offering member.
   */
  public String getPhoneNumber() {
    return phoneNumber;
  }

  /**
   * setter of phoneNumber. set the phone number of the user
   */
  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  /**
   * Getter for dateOfreceipt. To be removed once ObjectDTO is created.
   *
   * @return the date at which the object was received.
   */
  public Availability getDateOfReceipt() {
    return dateOfReceipt;
  }

  /**
   * setter of receipt date. update the date of reception.
   *
   * @param dateOfReceipt the date receipt of object.
   */
  @Override
  public void setDateOfReceipt(Availability dateOfReceipt) {
    this.dateOfReceipt = dateOfReceipt;
  }

  /**
   * Getter for acceptationDate. To be removed once ObjectDTO is created.
   *
   * @return the date at which the object was accepted.
   */
  public LocalDate getAcceptanceDate() {
    return acceptanceDate;
  }

  /**
   * setter of acceptationDate.Update the date of acceptation.
   */
  public void setAcceptanceDate(LocalDate acceptanceDate) {
    this.acceptanceDate = acceptanceDate;
  }

  /**
   * Getter for forSaleDate. To be removed once ObjectDTO is created.
   *
   * @return the date at which the object was put up for sale.
   */
  public LocalDate getSellingDate() {
    return sellingDate;
  }

  /**
   * setter of the sale date. update de date.
   */
  public void setSellingDate(LocalDate sellingDate) {
    this.sellingDate = sellingDate;
  }

  /**
   * Getter for endOfSaleDate. To be removed once ObjectDTO is created.
   *
   * @return the date at which the object was withdrawn from the storefront.
   */
  public LocalDate getMarketWithdrawalDate() {
    return marketWithdrawalDate;
  }

  /**
   * setter of the end sale date. update the date.
   */
  public void setMarketWithdrawalDate(LocalDate marketWithdrawalDate) {
    this.marketWithdrawalDate = marketWithdrawalDate;
  }

  /**
   * Getter for storeDepositDate. To be removed once ObjectDTO is created.
   *
   * @return the date at which the object was deposited at the store.
   */
  public LocalDate getStoreDepositDate() {
    return storeDepositDate;
  }

  /**
   * setter of storeDepositDate. update the date of deposit in te store.
   */
  public void setStoreDepositDate(LocalDate storeDepositDate) {
    this.storeDepositDate = storeDepositDate;
  }

  /**
   * getter for offeringMember. To be removed once ObjectDTO is created.
   *
   * @return the member who offered the object.
   */
  public MemberDTO getOfferingMember() {
    return offeringMember;
  }

  /**
   * setter of OfferingMember. Update the offering Member.
   *
   * @param offeringMember the offering member.
   */
  @Override
  public void setOfferingMember(MemberDTO offeringMember) {
    this.offeringMember = offeringMember;
  }

  /**
   * Update the object.
   *
   * @param objectUpdated the object with the updates
   * @return the object updated
   */
  @Override
  public ObjectDTO updateObject(ObjectDTO objectUpdated) {
    if (objectUpdated.getDescription() != null) {
      setDescription(objectUpdated.getDescription());
    }
    if (objectUpdated.getType() != null) {
      setType(objectUpdated.getType());
    }
    if (objectUpdated.getPicture().getUrl() != null) {
      this.getPicture().setUrl(objectUpdated.getPicture().getUrl());
    }
    return this;
  }

  /**
   * Calculates the number of working days (weekdays) between two dates, excluding weekends.
   *
   * @param startDate the start date
   * @param endDate   the end date
   * @return the number of working days between the start and end dates
   */
  @Override
  public int getWorkingDaysBetweenDates(LocalDate startDate, LocalDate endDate) {
    int workingDays = 0;
    LocalDate date = startDate;
    while (date.isBefore(endDate) || date.isEqual(endDate)) {
      DayOfWeek dayOfWeek = date.getDayOfWeek();
      if (!(dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY)) {
        workingDays++;
      }
      date = date.plusDays(1);
    }
    return workingDays;
  }

  /**
   * check if the object is in stored state and set it in sold state.
   *
   * @return the id of the new object that was injected.
   */
  @Override
  public ObjectDTO moveToSoldState() {
    if (!this.state.equals(ObjectState.IN_STORE)) {
      return null;
    }
    setState(ObjectState.SOLD);
    logger.info("object " + id + " sold for " + price);
    setSellingDate(LocalDate.now());
    return this;

  }
}
