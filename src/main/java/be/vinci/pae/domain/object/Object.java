package be.vinci.pae.domain.object;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.type.Type;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.object.ObjectDTO;
import java.time.LocalDate;

/**
 * Interface for Objects.
 *
 * @author Gabriel Tomson.
 */
public interface Object extends ObjectDTO {

  /**
   * method to offer a new object with both the User's account and phone number.
   *
   * @param offeringMember the person who offered the object.
   * @param phoneNumber    their phone number.
   * @param description    a short description of the object.
   * @param type           the type of the object.
   * @param dateOfReceipt  the time at which the object will be received at the container park.
   */
  void offerObjectWithMember(MemberDTO offeringMember, String phoneNumber, String description,
      Type type, Availability dateOfReceipt);

  /**
   * method to offer a new object with a phone number.
   *
   * @param phoneNumber   their phone number.
   * @param description   a short description of the object.
   * @param type          the type of the object.
   * @param dateOfReceipt the time at which the object will be received at the container park.
   */
  void offerObjectWithPhoneNumber(String phoneNumber, String description, Type type,
      Availability dateOfReceipt);

  /**
   * deposit object at the store.
   *
   * @return the object with the updates.
   */
  ObjectDTO depositObjectAtStore();

  /**
   * Put the state of the object at the workshop.
   *
   * @return the object updated
   */
  ObjectDTO putAtWorkshop();

  /**
   * check the state of the object, if OK, then set price and put it up for sale.
   *
   * @param price the price of the object.
   * @return the object updated
   */
  ObjectDTO checkAndSetToPrice(double price);


  /**
   * check the state of the object and put it as sold.
   *
   * @return if the state is valid then set the object or returns null
   */
  ObjectDTO checkAndSetSold();

  /**
   * reject the object.
   *
   * @param reason the reason for refusal.
   * @return the object updated
   */
  ObjectDTO refuseOffer(String reason);

  /**
   * accept the object.
   *
   * @return the object updated
   */
  ObjectDTO acceptOfferObject();

  /**
   * Withdraw the object.
   *
   * @return the object updated
   */
  ObjectDTO withdrawObject();

  /**
   * Update the object.
   *
   * @param objectUpdated the object with the updates
   * @return the object updated
   */
  ObjectDTO updateObject(ObjectDTO objectUpdated);

  /**
   * Calculates the number of working days (weekdays) between two dates, excluding weekends.
   *
   * @param startDate the start date
   * @param endDate   the end date
   * @return the number of working days between the start and end dates
   */
  int getWorkingDaysBetweenDates(LocalDate startDate, LocalDate endDate);


  /**
   * check if the object is in stored state and set it in sold state.
   *
   * @return the id of the new object that was injected.
   */
  ObjectDTO moveToSoldState();
}
