package be.vinci.pae.domain.availability;

import be.vinci.pae.services.availability.AvailabilityDTO;

/**
 * The interface of Availability.
 *
 * @author Chehrazad Ouazzani
 */
public interface Availability extends AvailabilityDTO {

}
