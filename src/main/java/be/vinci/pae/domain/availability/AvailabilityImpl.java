package be.vinci.pae.domain.availability;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.Locale;

/**
 * Equivalent of availability table in DB.
 *
 * @author Gabriel Tomson.
 */
public class AvailabilityImpl implements Availability {

  private int id;
  private LocalDate date;
  private String timeSlot;

  /**
   * Constructor for Availability.
   */

  public AvailabilityImpl() {
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Getter for the Availability's date.
   *
   * @return the date of the Availability.
   */
  @Override
  public LocalDate getDate() {
    return date;
  }

  /**
   * Setter for Availability's date.
   *
   * @param date the date of the availability.
   */
  @Override
  public void setDate(LocalDate date) {
    this.date = date;
  }

  /**
   * Getter for the Availability's shift.
   *
   * @return AM for the morning or PM for the afternoon.
   */
  @Override
  public String getTimeSlot() {
    return timeSlot;
  }

  /**
   * Setter for the Availability's shift.
   *
   * @param timeSlot the time slot.
   */
  @Override
  public void setTimeSlot(String timeSlot) {
    this.timeSlot = timeSlot;
  }

  @Override
  public String toString() {

    return date.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.FRANCE) + " "
        + date.getDayOfMonth() + "/"
        + date.getMonth().getValue() + "/" + date.getYear() + " "
        + (timeSlot.equals("AM") ? "matin" : "après-midi");
  }
}
