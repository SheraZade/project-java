package be.vinci.pae.domain.picture;

import be.vinci.pae.services.picture.PictureDTO;

/**
 * Interface for Picture.
 *
 * @author Sasha van Rossum.
 */
public interface Picture extends PictureDTO {

}
