package be.vinci.pae.domain.picture;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Implementation of picture interface.
 *
 * @author Sasha van Rossum
 */
public class PictureImpl implements Picture {

  private int id;
  private String url;
  private int objectId;
  private int memberId;
  private int version;

  /**
   * impl of the Picture.
   */
  public PictureImpl() {
  }

  /**
   * Get the picture.
   *
   * @return the file name.
   */
  public static String getObjectPictureFileName() {
    return "object_" + LocalDateTime.now()
        .format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-SSSS")) + ".jpg";
  }

  @Override
  public String getUrl() {
    return url;
  }

  @Override
  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public int getObjectId() {
    return objectId;
  }

  @Override
  public void setObjectId(int objectId) {
    this.objectId = objectId;
  }


  @Override
  public int getMemberId() {
    return memberId;
  }

  @Override
  public void setMemberId(int memberId) {
    this.memberId = memberId;
  }

  @Override
  public int getId() {
    return id;
  }

  @Override
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Getter of the version of the picture.
   *
   * @return the number version of the picture.
   */
  @Override
  public int getVersion() {
    return this.version;
  }

  /**
   * setter of the version.
   *
   * @param version the version of the picture.
   */
  @Override
  public void setVersion(int version) {
    this.version = version;
  }
}
