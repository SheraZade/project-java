package be.vinci.pae.domain.factory;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.notification.Notification;
import be.vinci.pae.domain.notificationmember.NotificationMember;
import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.domain.statechange.StateChange;
import be.vinci.pae.domain.type.Type;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.object.ObjectDTO;

/**
 * the interface of Factory.
 *
 * @author Loubna ELjattari
 */
public interface Factory {

  /**
   * getUser().
   *
   * @return an instance of the UserImpl
   */
  MemberDTO getUser();

  /**
   * Factory for Object.
   *
   * @return a new instance of Object
   */
  ObjectDTO getObject();

  /**
   * Factory of the availability.
   *
   * @return a new instance of Availability
   */
  Availability getAvailability();

  /**
   * Factory of the type.
   *
   * @return a new instance of Type
   */
  Type getType();

  /**
   * Factory of the Notification.
   *
   * @return a new instance of Notification
   */
  Notification getNotification();

  /**
   * return an instance of Notification members.
   *
   * @return a new instance of Notification members
   */
  NotificationMember getMembersNotification();

  /**
   * return an instance of Picture.
   *
   * @return a new instance of Picture
   */
  Picture getPicture();

  /**
   * return an instance of StateChange.
   *
   * @return a new instance of StateChange.
   */
  StateChange getStateChange();
}
