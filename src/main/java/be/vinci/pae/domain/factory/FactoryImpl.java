package be.vinci.pae.domain.factory;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.availability.AvailabilityImpl;
import be.vinci.pae.domain.member.MemberImpl;
import be.vinci.pae.domain.notification.Notification;
import be.vinci.pae.domain.notification.NotificationImpl;
import be.vinci.pae.domain.notificationmember.NotificationMember;
import be.vinci.pae.domain.notificationmember.NotificationMemberImpl;
import be.vinci.pae.domain.object.Object;
import be.vinci.pae.domain.object.ObjectImpl;
import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.domain.picture.PictureImpl;
import be.vinci.pae.domain.statechange.StateChange;
import be.vinci.pae.domain.statechange.StateChangeImpl;
import be.vinci.pae.domain.type.Type;
import be.vinci.pae.domain.type.TypeImpl;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.utils.Logs;

/**
 * the implementation of the interface Factory.
 *
 * @author Loubna Eljattari
 */
public class FactoryImpl implements Factory {

  private static final Logs logger = new Logs();

  /**
   * getUser().
   *
   * @return an instance of the UserImpl
   */
  @Override
  public MemberDTO getUser() {
    logger.factory("new User");
    return new MemberImpl();
  }

  /**
   * return an instance of ObjectImpl.
   *
   * @return a new instance of ObjectImpl
   */
  @Override
  public Object getObject() {
    logger.factory("new object");
    return new ObjectImpl();
  }

  /**
   * return an instance of Availability.
   *
   * @return a new instance of Availability
   */
  @Override
  public Availability getAvailability() {
    logger.factory("new Availability");
    return new AvailabilityImpl();
  }

  /**
   * Factory of the type.
   *
   * @return a new instance of Type
   */
  @Override
  public Type getType() {
    return new TypeImpl();
  }

  /**
   * return an instance of Notification.
   *
   * @return a new instance of Notification
   */
  @Override
  public Notification getNotification() {
    return new NotificationImpl();
  }

  /**
   * return an instance of Notification members.
   *
   * @return a new instance of Notification members
   */
  @Override
  public NotificationMember getMembersNotification() {
    return new NotificationMemberImpl();
  }

  /**
   * return an instance of Picture.
   *
   * @return a new instance of Picture
   */
  @Override
  public Picture getPicture() {
    return new PictureImpl();
  }

  @Override
  public StateChange getStateChange() {
    return new StateChangeImpl();
  }
}
