package be.vinci.pae.domain.type;

import be.vinci.pae.services.type.TypeDTO;

/**
 * The interface of Type.
 */
public interface Type extends TypeDTO {


}
