package be.vinci.pae.domain.type;

/**
 * The implementation of Type.
 */
public class TypeImpl implements Type {

  private int id;
  private String typeName;

  /**
   * Empty constructor.
   */
  public TypeImpl() {
  }

  /**
   * Getter of the id of the type.
   *
   * @return the id of the type
   */
  @Override
  public int getId() {
    return this.id;
  }

  /**
   * Getter of the name of the type.
   *
   * @return the name of the type.
   */
  @Override
  public String getTypeName() {
    return this.typeName;
  }

  /**
   * Setter of the id of the type.
   *
   * @param id the new value of the id.
   */
  @Override
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Setter of the type.
   *
   * @param type the new value of the type.
   */
  @Override
  public void setTypeName(String type) {
    this.typeName = type;
  }
}
