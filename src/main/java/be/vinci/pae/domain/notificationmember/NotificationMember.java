package be.vinci.pae.domain.notificationmember;

import be.vinci.pae.services.notificationmember.NotificationMembersDTO;

/**
 * the interface NotificationMember.
 *
 * @author Loubna Eljattari
 */
public interface NotificationMember extends NotificationMembersDTO {

  /**
   * Create a new notification.
   *
   * @param idNotification the id of the notification.
   * @param idMember       the id of the member.
   */
  void setNotificationMember(int idNotification, int idMember);
}
