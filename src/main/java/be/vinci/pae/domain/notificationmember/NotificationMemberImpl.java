package be.vinci.pae.domain.notificationmember;


/**
 * the implementation of the interface NotificationMember.
 *
 * @author Loubna Eljattari
 */
public class NotificationMemberImpl implements NotificationMember {

  private int idNotificationMember;
  private int idNotification;
  private int idMember;
  private boolean isRead;
  private int version;


  /**
   * Getter for id.
   *
   * @return the notification members ID.
   */
  public int getIdNotificationMember() {
    return idNotificationMember;
  }

  /**
   * setter for id.
   *
   * @param id the id of the notificationMember.
   */
  public void setIdNotificationMember(int id) {
    this.idNotificationMember = id;
  }

  /**
   * Getter for notification.
   *
   * @return the notification.
   */
  public int getIdNotification() {
    return idNotification;
  }

  /**
   * setter for notification.
   *
   * @param idNotification the notification to set
   */
  public void setIdNotification(int idNotification) {
    this.idNotification = idNotification;
  }

  /**
   * getter for notification's user.
   *
   * @return the notification.
   */
  public int getMember() {
    return this.idMember;
  }

  /**
   * setter for notification's user.
   *
   * @param idMember the id of member to set.
   */
  public void setMember(int idMember) {
    this.idMember = idMember;
  }

  /**
   * getter for isRead.
   *
   * @return the notification.
   */
  public boolean isIsRead() {
    return isRead;
  }

  /**
   * setter for isRead.
   *
   * @param isRead the boolean to set if notification is read.
   */
  public void setIsRead(boolean isRead) {
    this.isRead = isRead;
  }

  @Override
  public int getVersion() {
    return this.version;
  }

  @Override
  public void setVersion(int version) {
    this.version = version;
  }

  /**
   * Create a new notification.
   *
   * @param idNotification the id of the notification.
   * @param idMember       the id of the member.
   */
  @Override
  public void setNotificationMember(int idNotification, int idMember) {
    this.idNotification = idNotification;
    this.idMember = idMember;
  }
}
