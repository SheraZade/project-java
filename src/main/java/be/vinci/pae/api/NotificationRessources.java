package be.vinci.pae.api;

import be.vinci.pae.api.filters.Authorize;
import be.vinci.pae.services.notification.NotificationDTO;
import be.vinci.pae.services.notification.NotificationUCC;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

/**
 * class giving access to api operations.
 *
 * @author Loubna
 */
@Singleton
@Path("/notifications")
public class NotificationRessources {


  @Inject
  NotificationUCC notificationsUCC;

  /**
   * This method retrieves the notifications associated with a proposed object identified by its
   * ID.
   *
   * @param id the id of the member
   * @return the list of notifications associated with the proposed object.
   */
  @GET
  @Path("AllNotifications/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public List<NotificationDTO> getNotificationsOfMember(@PathParam("id") int id) {
    return notificationsUCC.getAllNotificationsMember(id);
  }
}

