package be.vinci.pae.api;

import be.vinci.pae.api.filters.Authorize;
import be.vinci.pae.domain.statechange.StateChange;
import be.vinci.pae.exceptions.WebParametersException;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.object.ObjectDTO;
import be.vinci.pae.services.object.ObjectDTO.ObjectState;
import be.vinci.pae.services.object.ObjectUCC;
import be.vinci.pae.services.type.TypeDTO;
import be.vinci.pae.utils.Logs;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * class giving access to api operations.
 *
 * @author Dorcas KUEZE
 */
@Singleton
@Path("/object")
public class ObjectRessources {

  private static final Logs logger = new Logs();
  private final ObjectMapper jsonMapper = new ObjectMapper();
  @Inject
  ObjectUCC objectUCC;

  /**
   * Gets all the object.
   *
   * @return list objects.
   */
  @GET
  @Authorize(roles = {"R", "A"})
  @Produces(MediaType.APPLICATION_JSON)
  @Path("getAllObjects")
  public List<ObjectDTO> getAllObjects() {
    return objectUCC.allObjectsList();
  }

  /**
   * add the price at the object with the id received.
   *
   * @param objectReceived the object with updates
   * @param id             the id of the object.
   * @return the object with updates
   */
  @POST
  @Path("/{id}/updatePrice")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A"})
  public ObjectDTO putPriceObject(ObjectDTO objectReceived, @PathParam("id") int id) {
    if (id <= 0) {
      throw new WebParametersException("Error id", Status.BAD_REQUEST);
    }
    if (objectReceived.getPrice() <= 0) {
      throw new WebParametersException("Error price must be higher than 0", Status.BAD_REQUEST);
    }
    ObjectDTO object = objectUCC.addPriceObject(id, objectReceived.getPrice());
    if (object == null) {
      throw new WebApplicationException(" an error has occurs while we trying to add price ",
          Status.INTERNAL_SERVER_ERROR);
    }
    logger.info("Price for object " + id + " set to " + objectReceived.getPrice() + ".");
    return object;
  }

  /**
   * put the object as sold.
   *
   * @param id the id of the object
   * @return the object with updates
   */
  @POST
  @Path("/{id}/sold")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public ObjectDTO putAsSold(@PathParam("id") int id) {
    if (id <= 0) {
      throw new WebParametersException("Error id", Status.BAD_REQUEST);
    }
    ObjectDTO objectDTO = objectUCC.putAsSold(id);
    if (objectDTO == null) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }
    logger.info("Object " + id + " sold.");
    return objectDTO;
  }

  /**
   * Updates the state of the object to "Validate".
   *
   * @param id the id of the object to be accepted.
   * @return The updated object DTO.
   */

  @POST
  @Path("{id}/validateOffer")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R"})
  public ObjectDTO acceptOffer(@PathParam("id") int id) {
    ObjectDTO objectDTO = objectUCC.acceptOfferObject(id);
    if (objectDTO == null) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }
    logger.info("Object " + id + " Accepted.");
    return objectDTO;
  }

  /**
   * Updates the state of the object to "Denied".
   *
   * @param objectToRefuse the object with the message why denied
   * @return The updated objectDTO.
   */
  @POST
  @Path("refuseOffer")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R"})
  public ObjectDTO refuseProposal(ObjectDTO objectToRefuse) {
    if (objectToRefuse.getId() <= 0) {
      throw new WebParametersException("Error invalid id", Status.BAD_REQUEST);
    }
    if (objectToRefuse.getReasonForRefusal() == null || objectToRefuse.getReasonForRefusal()
        .isBlank()) {
      throw new WebParametersException("Error empty message refusal", Status.BAD_REQUEST);
    }
    ObjectDTO objectUpdated = objectUCC.refuseOfferObject(objectToRefuse.getId(),
        objectToRefuse.getReasonForRefusal());
    if (objectUpdated == null) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }

    logger.info("Object " + objectToRefuse.getId() + " refused.");
    return objectUpdated;
  }

  /**
   * give all the info of the object passed by id.
   *
   * @param id the id of the object wanted
   * @return a JSON node with the object info
   */
  @GET
  @Path("{id}/getObject")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public ObjectDTO getObjectInfo(@PathParam("id") int id) {
    if (id <= 0) {
      throw new WebParametersException("Error invalid id", Status.BAD_REQUEST);
    }
    ObjectDTO object = objectUCC.getObject(id);
    if (object == null) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }
    return object;
  }

  /**
   * Sets the object's state to the state : "In store".
   *
   * @param id the id of the object to update
   * @return The updated object if everything works, null otherwise.
   * @throws WebApplicationException with error code 404 if the object not found.
   */
  @POST
  @Path("{id}/depositAtStore")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A"})
  public ObjectDTO depositAtStore(@PathParam("id") int id) {
    if (id <= 0) {
      throw new WebParametersException("Object id cannot be null!", Status.BAD_REQUEST);
    }
    ObjectDTO objectDTO = objectUCC.putDepositAtStore(id);
    if (objectDTO == null) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }
    return objectDTO;
  }

  /**
   * Sets the object's state to the state : "In workshop".
   *
   * @param id the id of the object to be updated
   * @return The updated object if everything works, null otherwise.
   * @throws WebApplicationException with error code 404 if the object not found.
   */
  @POST
  @Path("{id}/depositAtWorkshop")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A"})
  public ObjectDTO depositAtWorkshop(@PathParam("id") int id) {
    if (id <= 0) {
      throw new WebParametersException("Object id cannot be null!", Status.BAD_REQUEST);
    }
    ObjectDTO objectDTO = objectUCC.putAtWorkshop(id);
    if (objectDTO == null) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }
    return objectDTO;
  }

  /**
   * moving an object from the stored state to the sold  state.
   *
   * @param id   the id of the object to put to the sold state.
   * @param json a JSON object.
   * @return returns the object whose state has been changed .
   * @throws WebApplicationException with code 400 if the id of the object is invalid.
   */
  @POST
  @Path("{id}/moveToSold")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R"})
  public ObjectDTO moveToSold(@PathParam("id") int id, JsonNode json) {
    double objectPrice = json.get("price").asDouble();
    if (id <= 0) {
      throw new WebParametersException("Object id cannot be null!", Status.BAD_REQUEST);
    }
    if (objectPrice <= 0) {
      throw new WebParametersException("Object price cannot be empty ", Status.BAD_REQUEST);
    }

    ObjectDTO objectDTO = objectUCC.moveToSold(id, objectPrice);
    if (objectDTO == null) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }
    return objectDTO;
  }

  //  /**
  //   * Sets the object's state to the state : "Withdrawn".
  //   *
  //   * @param id the id of the object to be updated
  //   * @return The updated object if everything works, null otherwise.
  //   * @throws WebApplicationException with error code 404 if the object not found.
  //   */
  //  @POST
  //  @Path("{id}/withdrawn")
  //  @Consumes(MediaType.APPLICATION_JSON)
  //  @Produces(MediaType.APPLICATION_JSON)
  //  @Authorize(roles = {"R", "A"})
  //  public ObjectDTO withdrawObject(@PathParam("id") int id) {
  //    if (id <= 0) {
  //      throw new WebParametersException("Object id can't be null!", Status.BAD_REQUEST);
  //    }
  //    ObjectDTO objectDTO = objectUCC.withdrawObject(id);
  //    if (objectDTO == null) {
  //      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
  //    }
  //    return objectDTO;
  //  }

  /**
   * This method is a RESTful HTTP GET API that returns a list of objects as JSON for the connected
   * user. It requires the user to be connected and have a role of "R", "A", or "M".
   *
   * @param id the id of the member for getting its list of objects
   * @return A list of ObjectDTO objects as JSON.
   * @throws WebApplicationException If the user is not connected.
   */
  @GET
  @Path("MyObjects/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public List<ObjectDTO> getMembersObjects(@PathParam("id") int id) {
    if (id <= 0) {
      throw new WebParametersException("Object id cannot be negative!", Status.BAD_REQUEST);
    }
    return objectUCC.memberObjects(id);
  }

  /**
   * Returns a list of offered objects.
   *
   * @param request the ContainerRequestContext object that represents the request.
   * @return a List of ObjectDTO, each representing an offered object.
   * @throws WebApplicationException if the user is not authorized to access the resource, with the
   *                                 status code UNAUTHORIZED.
   */
  @GET
  @Path("offeredObjects")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R"})
  public List<ObjectDTO> offeredObjects(@Context ContainerRequestContext request) {
    MemberDTO userAConnected = (MemberDTO) request.getProperty("user");
    if (userAConnected == null) {
      throw new WebApplicationException(Status.UNAUTHORIZED);
    }

    return objectUCC.offeredObjects();
  }

  /**
   * Returns a list of all the types of objects in the system as TypeDTO.
   *
   * @return a List of ObjectNodes, each representing a type of object.
   */
  @GET
  @Path("allTypes")
  @Produces(MediaType.APPLICATION_JSON)
  public List<TypeDTO> listTypes() {
    return objectUCC.getAllTypes();
  }

  /**
   * Returns a list of all the states of objects in the system as JSON objects, provided the user
   * has the roles "R" or "A".
   *
   * @param request the ContainerRequestContext object that represents the request.
   * @return a List of ObjectNodes, each representing a state of object in the system.
   * @throws WebApplicationException if the user is not authorized to access the resource, with the
   *                                 status code UNAUTHORIZED.
   */
  @GET
  @Path("allStates")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A"})
  public List<ObjectNode> listStates(@Context ContainerRequestContext request) {
    MemberDTO userConnected = (MemberDTO) request.getProperty("user");
    if (userConnected == null) {
      throw new WebApplicationException(Status.UNAUTHORIZED);
    }
    List<String> listStatesString = objectUCC.getAllStates();
    List<ObjectNode> listStates = new ArrayList<>();
    for (String state : listStatesString) {
      ObjectNode objectNode = jsonMapper.createObjectNode();
      objectNode.put("State", state);
      listStates.add(objectNode);
    }
    return listStates;
  }


  /**
   * Get the list of objects with correspondant date of deposit in container park.
   *
   * @param jsonNode jsonNode that contains the date of receipt object in container park.
   * @return the list of object that corresponds to the date in parameter.
   */
  @POST
  @Path("/searchByDate")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A"})
  public List<ObjectDTO> searchListByDate(JsonNode jsonNode) {
    String dateStr = jsonNode.get("dateDeposit").asText();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    LocalDate dateReceipt = LocalDate.from(dtf.parse(dateStr));

    return objectUCC.searchObjectsByDate(dateReceipt);
  }

  /**
   * Get the list of objects of a certain type.
   *
   * @param type the type selected by the user.
   * @return a list of all objects of that type.
   */
  @POST
  @Path("/searchByType")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public List<ObjectDTO> searchByType(TypeDTO type) {
    return objectUCC.searchByType(type.getTypeName());
  }

  /**
   * connected user search.
   *
   * @param json a JSON object containing the search parameters.
   * @return a list of all objects matching the search parameters.
   */
  @POST
  @Path("/search")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public List<ObjectDTO> search(JsonNode json) {

    String state = null;
    if (json.hasNonNull("state")) {
      state = json.get("state").asText();
    }

    LocalDate date = null;
    if (json.hasNonNull("date")) {
      String dateString = json.get("date").asText();
      if (!dateString.isEmpty() && !dateString.isBlank()) {
        date = LocalDate.parse(dateString);
      }
    }

    List<String> type = new ArrayList<>();
    if (json.hasNonNull("type")) {
      JsonNode jn = json.get("type");
      for (JsonNode typeName : jn) {
        type.add(typeName.asText());
      }
    }

    String sbd = json.get("SBD").asText();
    String sbp = json.get("SBP").asText();
    double minPrice = json.get("minPrice").asDouble();
    double maxPrice = json.get("maxPrice").asDouble();

    return objectUCC.search(minPrice, maxPrice, date, type, state, sbd, sbp);
  }

  /**
   * offer a new object.
   *
   * @param objectToAdd the new object to add into the database.
   * @return the id of the new object that was injected.
   * @throws WebApplicationException with code 400 if the object's data is invalid.
   * @throws WebApplicationException with code 500 if the offer fails.
   */
  @POST
  @Path("offer")
  @Consumes(MediaType.APPLICATION_JSON)
  public int offerObject(ObjectDTO objectToAdd) {
    if (objectToAdd == null || objectToAdd.getDescription() == null
        || objectToAdd.getDescription().isBlank() || objectToAdd.getDateOfReceipt() == null) {
      throw new WebApplicationException("Fields can't be empty!", Status.BAD_REQUEST);
    }

    if (objectToAdd.getDescription().length() > 120) {
      throw new WebApplicationException("Description can't be higher than 120 characters !",
          Status.BAD_REQUEST);
    }
    if (objectToAdd.getDateOfReceipt().getDate().isBefore(LocalDate.now())) {
      throw new WebApplicationException("Choose a later date !");
    }

    if (objectToAdd.getDateOfReceipt().getDate().getDayOfWeek() != DayOfWeek.SATURDAY) {
      throw new WebApplicationException("Select a date that corresponds to Saturday !",
          Status.BAD_REQUEST);
    }
    int idObject = objectUCC.offerObject(objectToAdd);
    if (idObject == -1) {
      throw new WebApplicationException("Something went wrong with the offer.",
          Status.INTERNAL_SERVER_ERROR);
    }
    return idObject;
  }


  /**
   * offer a new object.
   *
   * @param id            the id of the object
   * @param objectUpdated the object with the new updates.
   * @return true if the object was updated in the database, false if something went wrong.
   * @throws WebApplicationException with code 400 if the object's data is invalid.
   * @throws WebApplicationException with code 500 if the offer fails.
   */
  @POST
  @Path("{id}/Update")
  @Consumes(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R"})
  public boolean updateObject(@PathParam("id") int id, ObjectDTO objectUpdated) {
    if (id <= 0) {
      throw new WebParametersException("Object id can't be null!", Status.BAD_REQUEST);
    }
    if (objectUpdated == null || objectUpdated.getDescription() == null
        || objectUpdated.getDescription().isBlank() || objectUpdated.getType() == null) {
      throw new WebApplicationException("Fields can't be empty!", Status.BAD_REQUEST);
    }
    if (objectUpdated.getDescription().length() > 120) {
      throw new WebApplicationException("Description can't be higher than 120 characters !",
          Status.BAD_REQUEST);
    }
    objectUpdated.setId(id);
    if (!objectUCC.updateObject(objectUpdated)) {
      throw new WebApplicationException("Something went wrong when updating the object.",
          Status.INTERNAL_SERVER_ERROR);
    }
    return true;
  }


  /**
   * Gets all the object in the state of "In store", "Available for sale" and "Sold".
   *
   * @return list objects.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/getCarouselObjects")
  public List<ObjectDTO> getCarouselObjects() {
    return objectUCC.getObjectForCarousel();
  }

  /**
   * Gets the list of the objects to withdraw, that passed 30 business days.
   *
   * @return the list of objects to withdraw.
   */
  @GET
  @Path("/ObjectsToWithdraw")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A"})
  public ArrayList<ObjectDTO> listObjectsToWithdraw() {
    return objectUCC.listObjectsWithdraw();
  }

  /**
   * Set image for an object.
   *
   * @param json a JSON object with the object's id and the path to the image.
   */
  @POST
  @Path("/setImage")
  @Consumes(MediaType.APPLICATION_JSON)
  public void setImage(JsonNode json) {
    String filePath = json.get("fileName").asText();
    int id = json.get("id").asInt();
    objectUCC.setPicture(id, filePath);
  }

  /**
   * Searches for objects by state.
   *
   * @param jsonNode JsonNode object containing a "state" field specifying the state to search for
   * @return A List of ObjectDTOs representing the objects found
   */
  @POST
  @Path("/searchByState")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public List<ObjectDTO> searchByState(JsonNode jsonNode) {
    String state = jsonNode.get("state").asText();
    return objectUCC.searchObjectsByState(state);
  }


  /**
   * Get the object statistics.
   *
   * @param json a json object containing the search parameters.
   * @return an array of JSON objects containing a state and the number of objects in that state.
   */
  @POST
  @Path("/getStats")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public ArrayNode stats(JsonNode json) {
    LocalDate dateMin = null;
    LocalDate dateMax = null;

    if (json.hasNonNull("dateMin") && !json.get("dateMin").asText().isBlank()) {
      dateMin = LocalDate.parse(json.get("dateMin").asText());
    }
    if (json.hasNonNull("dateMax") && !json.get("dateMax").asText().isBlank()) {
      dateMax = LocalDate.parse(json.get("dateMax").asText());
    }

    ObjectNode totalObject = jsonMapper.createObjectNode();
    totalObject.put("stateName", "total");
    totalObject.put("count", objectUCC.getCount(dateMin, dateMax));

    ArrayNode listStats = jsonMapper.createArrayNode();
    listStats.add(totalObject);

    Map<ObjectState, Integer> stats;

    if (dateMax != null) {
      stats = objectUCC.getStats(dateMax);
    } else {
      stats = objectUCC.getStats();
    }

    for (ObjectState os : ObjectState.values()) {
      int count = stats.getOrDefault(os, 0);
      ObjectNode on = jsonMapper.createObjectNode();
      on.put("stateName", os.getNameState());
      on.put("count", count);
      listStats.add(on);
    }

    return listStats;
  }

  /**
   * Get the statistics by object state.
   *
   * @param json a JSON object containing the search parameters.
   * @return a list of all updates to that state that occured during the specified period.
   */
  @POST
  @Path("/getStateStats")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public List<StateChange> getStateStats(JsonNode json) {
    if (!json.hasNonNull("state")) {
      throw new WebApplicationException("invalid state", Status.BAD_REQUEST);
    }

    LocalDate dateMin = null;
    LocalDate dateMax = null;

    String stateName = json.get("state").asText();
    ObjectState state = null;
    for (ObjectState os : ObjectState.values()) {
      if (os.getNameState().equalsIgnoreCase(stateName)) {
        state = os;
        break;
      }
    }

    if (json.hasNonNull("dateMin") && !json.get("dateMin").asText().isBlank()) {
      dateMin = LocalDate.parse(json.get("dateMin").asText());
    }
    if (json.hasNonNull("dateMax") && !json.get("dateMax").asText().isBlank()) {
      dateMax = LocalDate.parse(json.get("dateMax").asText());
    }

    List<StateChange> changes = objectUCC.getStateStats(state, dateMin, dateMax);

    if (changes == null) {
      changes = new ArrayList<>();
    }
    return changes;

  }
}