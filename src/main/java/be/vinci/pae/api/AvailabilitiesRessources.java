package be.vinci.pae.api;

import be.vinci.pae.services.availability.AvailabilityDTO;
import be.vinci.pae.services.availability.AvailabilityUCC;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

/**
 * class giving access to api operations.
 *
 * @author Loubna
 */
@Singleton
@Path("/availabilities")
public class AvailabilitiesRessources {


  @Inject
  AvailabilityUCC availabilityUCC;

  /**
   * This method retrieves the notifications associated with a proposed object identified by its
   * ID.
   *
   * @return the list of notifications associated with the proposed object.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("getAll")
  public List<AvailabilityDTO> getAllObjects() {
    return availabilityUCC.getAllAvailabilities();
  }
}

