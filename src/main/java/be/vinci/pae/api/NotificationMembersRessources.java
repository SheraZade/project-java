package be.vinci.pae.api;

import be.vinci.pae.api.filters.Authorize;
import be.vinci.pae.services.notificationmember.NotificationMembersDTO;
import be.vinci.pae.services.notificationmember.NotificationMembersUCC;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PATCH;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

/**
 * class giving access to api operations.
 *
 * @author Loubna
 */
@Path("/notificationMember")
public class NotificationMembersRessources {


  @Inject
  NotificationMembersUCC notificationMembersUCC;

  /**
   * Marks a list of notifications as read by updating it in the database.
   *
   * @param notificationMembersDTO the notification of the member with the id of notification.
   * @return true if the notification was successfully marked as read, false otherwise.
   */
  @PATCH
  @Path("/MarkAsRead")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public boolean markNotificationAsRead(NotificationMembersDTO notificationMembersDTO) {
    return notificationMembersUCC.markNotificationAsRead(
        notificationMembersDTO.getIdNotification());
  }

  /**
   * Gets all the notifications read and unread of given member.
   *
   * @param id the id of the member
   * @return a list of notifications of a member.
   */
  @GET
  @Path("AllNotificationsMember/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public List<NotificationMembersDTO> getNotificationsOfMember(@PathParam("id") int id) {
    return notificationMembersUCC.getAllNotificationsOfMember(id);
  }
}
