package be.vinci.pae.api.filters;

import jakarta.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * class annotation.
 *
 * @author Dorcas Nounga
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorize {

  /**
   * get the roles allowed of the request.
   *
   * @return an array of roles allowed
   */
  String[] roles();
}
