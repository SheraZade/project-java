package be.vinci.pae.api.filters;

import be.vinci.pae.domain.member.Member;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.member.MemberUCC;
import be.vinci.pae.utils.Config;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.container.ResourceInfo;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * class which allows to manage the access to the operations of the api.
 *
 * @author Dorcas Kueze Nounga
 */
@Singleton
@Provider
@Authorize(roles = {"R", "A", "M"})
public class AuthorizationRequestFilter implements ContainerRequestFilter {

  private final Algorithm jwtAlgorithm = Algorithm.HMAC256(Config.getProperty("JWTSecret"));
  private final JWTVerifier jwtVerifier = JWT.require(this.jwtAlgorithm).withIssuer("auth0")
      .build();
  @Inject
  private MemberUCC memberUCC;
  @Context
  private ResourceInfo resourceInfo;

  /**
   * Method that retrieves the user's data to know if he has access to a resource or not.
   *
   * @param requestContext request context.
   */
  @Override
  public void filter(ContainerRequestContext requestContext) {
    String token = requestContext.getHeaderString("Authorization");
    Member authenticatedMember = (Member) getAuthenticatedMember(token);

    if (authenticatedMember == null) {
      requestContext.abortWith(Response.status(Status.FORBIDDEN)
          .entity("You are forbidden to access this resource").build());
    }
    Method method = resourceInfo.getResourceMethod();
    Authorize rolesAnnotation = method.getAnnotation(Authorize.class);
    if (rolesAnnotation == null) {
      return;
    }
    if (!Arrays.asList(rolesAnnotation.roles()).contains(authenticatedMember.getRole())) {
      requestContext.abortWith(Response.status(Status.FORBIDDEN)
          .entity("You are forbidden to access this resource").build());
      return;
    }
    requestContext.setProperty("user", authenticatedMember);
  }


  /**
   * Get the authenticated member based on the provided token.
   *
   * @param token the token provided in the Authorization header
   * @return the authenticated member or null if the token is invalid or the user doesn't exist
   */
  private MemberDTO getAuthenticatedMember(String token) {
    if (token == null) {
      return null;
    }
    DecodedJWT decodedToken;
    try {
      decodedToken = this.jwtVerifier.verify(token);
    } catch (Exception e) {
      throw new TokenDecodingException(e);
    }
    int idMember = decodedToken.getClaim("userId").asInt();
    MemberDTO member = memberUCC.getMember(idMember);
    if (member == null) {
      return null;
    }
    return member;
  }

}