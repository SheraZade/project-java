package be.vinci.pae.api.filters;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

/**
 * class allowing to manage exceptions.
 *
 * @author DORCAS NOUNGA
 */
public class TokenDecodingException extends WebApplicationException {

  /**
   * Constructs a new TokenDecodingException with default message and HTTP status code.
   * the HTTP status code to be returned
   */
  public TokenDecodingException() {
    super(Response.Status.UNAUTHORIZED);
  }

  /**
   * constructor with parameter string.
   *
   * @param message the error message.
   */
  public TokenDecodingException(String message) {
    super(message, Response.Status.UNAUTHORIZED);
  }

  /**
   * constructor with parameter Throwable.
   *
   * @param cause which is saved for later retrieval by the getCause() method.
   */

  public TokenDecodingException(Throwable cause) {
    super(cause.getMessage(), Response.Status.UNAUTHORIZED);
  }
}
