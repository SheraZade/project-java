package be.vinci.pae.api;

import be.vinci.pae.domain.picture.PictureImpl;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.utils.Config;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import jakarta.inject.Singleton;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * class giving access to api operations.
 *
 * @author Sasha van Rossum
 */
@Singleton
@Path("/picture")
public class PictureRessources {

  private final ObjectMapper jsonMapper = new ObjectMapper();

  /**
   * give all the info of the picture passed by user id.
   *
   * @param url the url of the picture wanted.
   * @return the picture.
   */
  @GET
  @Path("/getPicture/{url}")
  public File getMemberPictureInfo(@PathParam("url") String url) {
    return new File(Config.getProperty("PictureUrl").concat(url));
  }


  /**
   * upload the picture to the server.
   *
   * @param is the picture.
   * @return the picture name
   */
  @POST
  @Path("upload")
  @Produces(MediaType.APPLICATION_JSON)
  public ObjectNode storeImage(InputStream is) {
    ObjectNode on = jsonMapper.createObjectNode();
    String fileName = PictureImpl.getObjectPictureFileName();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    byte[] buff = new byte[1024];
    int n;
    try {
      while ((n = is.read(buff)) != -1) {
        out.write(buff, 0, n);
      }
      byte[] finalImage = out.toByteArray();
      File file = new File(Config.getProperty("PictureUrl").concat(fileName));
      file.createNewFile();
      FileOutputStream fos = new FileOutputStream(file);
      fos.write(finalImage);
      fos.close();
      out.close();
      is.close();
    } catch (IOException exc) {
      throw new FatalException("exception while downloading picture.", exc);
    }
    on.put("fileName", fileName);
    return on;
  }
}
