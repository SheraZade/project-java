package be.vinci.pae.api;

import be.vinci.pae.api.filters.Authorize;
import be.vinci.pae.exceptions.WebParametersException;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.member.MemberUCC;
import be.vinci.pae.utils.Config;
import be.vinci.pae.utils.Logs;
import be.vinci.pae.utils.Logs.UserActions;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import org.glassfish.jersey.server.ContainerRequest;


/**
 * Ressources for Auth route. Responsible for authenticating users.
 *
 * @author Gabriel Tomson.
 */

@Singleton
@Path("/auths")
public class AuthRessources {

  private static final Logs logger = new Logs();
  final int validityHours = 8;
  private final ObjectMapper jsonMapper = new ObjectMapper();
  private final Algorithm jwtAlgorithm = Algorithm.HMAC256(Config.getProperty("JWTSecret"));
  @Inject
  private MemberUCC memberUCC;

  /**
   * Verifies the user's credentials and returns an object with the user's informations if the
   * Verifies the user's credentials and returns an object with the user's information if the
   * credentials are correct.
   *
   * @param json an object containing the user's email as well as the password hashed by b-crypt.
   * @return An ObjectNode containing the user's information.
   * @throws WebApplicationException with error code 401 if password or email field is empty.
   * @throws WebApplicationException with error code 401 if userUCC's login method returns null.
   */
  @POST
  @Path("login")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public ObjectNode login(JsonNode json) {
    if (!json.hasNonNull("email") || !json.hasNonNull("password")) {
      throw new WebParametersException(" login or password required ", Status.UNAUTHORIZED);
    }

    String passwordHashed = json.get("password").asText();
    String email = json.get("email").asText();

    MemberDTO user = memberUCC.login(email, passwordHashed);
    if (user == null) {
      throw new WebApplicationException("An error has occurred while trying to login.",
          Status.UNAUTHORIZED);
    }

    Date expirationTime = Date.from(
        LocalDateTime.now().plusHours(validityHours).toInstant(ZoneOffset.UTC));

    String token = JWT.create()
        .withIssuer("auth0")
        .withClaim("userId", user.getId())
        .withExpiresAt(expirationTime)
        .sign(this.jwtAlgorithm);

    ObjectNode returned = jsonMapper.createObjectNode();
    returned.put("token", token);
    returned.putPOJO("user", user);
    logger.user(UserActions.LOGIN, "user " + user.getId() + " Logged in.");
    return returned;
  }


  /**
   * Temporarly authentify a user by its phone number to offer an object.
   *
   * @param json a JSON object containing the phone number
   * @return a JSON object containing a token identifying the user.
   */
  @POST
  @Path("phoneNumber")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public ObjectNode tempLoginPhoneNumber(JsonNode json) {
    ObjectNode returnNode = jsonMapper.createObjectNode();

    String phoneNumber = json.get("phoneNumber").asText();
    Date expirationTime = Date.from(
        LocalDateTime.now().plusHours(validityHours).toInstant(ZoneOffset.UTC));

    String token = JWT.create().withIssuer("auth0").withClaim("phoneNumber", phoneNumber)
        .withExpiresAt(expirationTime).sign(this.jwtAlgorithm);

    returnNode.put("token", token);
    return returnNode;
  }

  /**
   * Gets user from JWT upon page refresh or remember me.
   *
   * @param request containerRequest.
   * @return The user's data.
   * @throws WebApplicationException with error code 403 (forbidden) when jwt is expired or
   *                                 invalid.
   * @throws WebApplicationException with error code 400 (bad request) when authorization header is
   *                                 missing.
   * @throws WebApplicationException with error code 500 (internal server error) when
   *                                 userUCC.getMember returns null.
   */
  @GET
  @Path("user")
  @Authorize(roles = {"R", "A", "M"})
  @Produces(MediaType.APPLICATION_JSON)
  public ObjectNode refresh(@Context ContainerRequest request) {
    MemberDTO authenticatedMember = (MemberDTO) request.getProperty("user");
    String token = request.getHeaderString("Authorization");
    ObjectNode userReturn = jsonMapper.createObjectNode();
    userReturn.put("token", token);
    userReturn.putPOJO("user", authenticatedMember);

    logger.user(UserActions.LOGIN, "user " + authenticatedMember.getId()
        + " Logged in from JWT.");

    return userReturn;
  }

  /**
   * register a user in the DB.
   *
   * @param user the user
   * @return the user information
   */
  @PUT
  @Path("register")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public MemberDTO register(MemberDTO user) {
    if (user == null || user.getFirstName() == null || user.getLastName() == null
        || user.getEmail() == null || user.getEncryptedPassword() == null
        || user.getPhoneNumber() == null || user.getFirstName().isBlank()
        || user.getLastName().isBlank() || user.getEmail().isBlank()
        || user.getEncryptedPassword().isBlank() || user.getEncryptedPassword().isBlank()
        || user.getPicture().getUrl().isBlank()) {
      throw new WebParametersException("All fields are required", Status.BAD_REQUEST);
    }
    MemberDTO theMember = memberUCC.register(user);
    if (theMember == null) {
      throw new WebApplicationException(" an error has occurs while we trying to register",
          Status.INTERNAL_SERVER_ERROR);
    }

    logger.user(UserActions.REGISTER, "user " + theMember.getId() + "registered");
    return theMember;
  }
}
