package be.vinci.pae.api;

import be.vinci.pae.api.filters.Authorize;
import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.exceptions.WebParametersException;
import be.vinci.pae.services.availability.AvailabilityDTO;
import be.vinci.pae.services.availability.AvailabilityUCC;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.member.MemberUCC;
import be.vinci.pae.services.notification.NotificationDTO;
import be.vinci.pae.services.notificationmember.NotificationMembersUCC;
import com.fasterxml.jackson.databind.node.ObjectNode;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;


/**
 * Ressources for user route.
 *
 * @author Loubna el jattari.
 */

@Singleton
@Path("/users")
public class MemberRessources {

  @Inject
  private MemberUCC memberUCC;

  @Inject
  private AvailabilityUCC availabilityUCC;

  @Inject
  private NotificationMembersUCC notificationMembersUCC;


  /**
   * Retrieves all users stored in the Ressourcerie  database and returns their information as a
   * list of JSON nodes.
   *
   * @return A list of JSON nodes representing all registered users.
   * @throws WebApplicationException If no users are found in the database.
   */
  @GET
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A"})
  public List<MemberDTO> getAllMembers() {
    return memberUCC.getAllMember();
  }

  /**
   * Confirm that a user has the role "A".
   *
   * @param id the id of the user whose role is to be updated.
   * @return the new assistant or null if problem occurs.
   */
  @POST()
  @Path("/confirmAssistant/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R"})
  public MemberDTO confirmAssistant(@PathParam("id") int id) {
    if (id <= 0) {
      throw new WebParametersException("Error id", Status.BAD_REQUEST);
    }
    MemberDTO memberDTO = memberUCC.updateRoleAsAssistant(id);
    if (memberDTO == null) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }
    return memberDTO;
  }

  /**
   * Mark a member as 'Responsable'.
   *
   * @param id the id of the member whose role is to be updated.
   * @return the new responsable or null if problem occurs.
   */
  @POST()
  @Path("/confirmResponsable/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R"})
  public MemberDTO confirmResponsable(@PathParam("id") int id) {
    if (id <= 0) {
      throw new WebParametersException("Error id", Status.BAD_REQUEST);
    }
    MemberDTO memberDTO = memberUCC.updateRoleAsResponsable(id);
    if (memberDTO == null) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }
    return memberDTO;
  }

  /**
   * Insert an availability into the database.
   *
   * @param availability the availability to insert into the database
   * @return the availability if not already existed in database
   */
  @POST()
  @Path("/putAvailability")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A"})
  public AvailabilityDTO putAvailability(Availability availability) {
    if (availability.getDate().isBefore(LocalDate.now())) {
      throw new WebApplicationException("Choose a later date !", Status.BAD_REQUEST);
    }

    if (availability.getDate().getDayOfWeek() != DayOfWeek.SATURDAY) {
      throw new WebApplicationException("Select a date that corresponds to Saturday !",
          Status.BAD_REQUEST);
    }

    availability.setTimeSlot("AM");
    AvailabilityDTO retour = availabilityUCC.addAvailability(availability);
    if (retour == null) {
      return null;
    }
    availability.setTimeSlot("PM");
    retour = availabilityUCC.addAvailability(availability);
    return retour;
  }

  /**
   * Gets the member by its id.
   *
   * @param id the id of the member
   * @return the member found
   */
  @GET
  @Path("getOne/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public MemberDTO getsMemberById(@PathParam("id") int id) {
    return memberUCC.getMember(id);
  }


  /**
   * Gets the list of all the notifications of a given member.
   *
   * @param id of the member
   * @return the list of all notifications specific for the member given in argument
   */
  @GET
  @Path("getAllNotifications/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public List<NotificationDTO> getAllNotificationsMember(@PathParam("id") int id) {
    return memberUCC.allNotificationsMember(id);
  }

  /**
   * Gets the list of all the notifications of a given member.
   *
   * @param id of the member
   * @return the list of all notifications specific for the member given in argument
   */
  @GET
  @Path("AllUnreadNotifications/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public List<NotificationDTO> allUnreadNotifications(@PathParam("id") int id) {
    return memberUCC.unreadNotificationsMember(id);
  }


  /**
   * Edit his personal profile.
   *
   * @param memberDTO the object user that contain all new data of user.
   * @return the user edited.
   */
  @PUT
  @Path("editMyProfile")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A", "M"})
  public MemberDTO editMyProfile(MemberDTO memberDTO) {
    return memberUCC.editMyProfile(memberDTO);
  }

  /**
   * Get the person who offered the object.
   *
   * @param id the id of the object.
   * @return the information of the person.
   */
  @GET
  @Path("/offerer/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  @Authorize(roles = {"R", "A"})
  public MemberDTO getOfferer(@PathParam("id") int id) {
    return memberUCC.getOfferer(id);
  }

  /**
   * Search members according to name and/or role.
   *
   * @param json a JSON object containing the search parameters.
   * @return a list of all members matching the search criterias.
   */
  @POST
  @Path("search")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public List<MemberDTO> search(ObjectNode json) {
    String name = json.get("name").asText();
    String role = json.get("role").asText();

    return memberUCC.search(name, role);
  }


}
