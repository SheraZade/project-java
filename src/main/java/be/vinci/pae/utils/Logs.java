package be.vinci.pae.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Logger class to simplify logging.
 *
 * @author Gabriel Tomson
 */
public class Logs {

  private Logger infoLogger;
  private Logger errorLogger;
  private Logger userLogger;

  /**
   * Constructor for logs.
   */
  public Logs() {
    infoLogger = LogManager.getLogger("infoLogger");
    errorLogger = LogManager.getLogger("errorLogger");
    userLogger = LogManager.getLogger("userLogger");

  }

  /**
   * Logs general information to general log file.
   *
   * @param message the message to be logged.
   */
  public void info(String message) {
    infoLogger.info(message);
  }

  /**
   * Logs general errors to general log file.
   *
   * @param message the message to be logged/
   */
  public void error(String message) {
    errorLogger.error(message);
  }

  /**
   * Logs user's actions to user log file.
   *
   * @param action  the action attempted by the user.
   * @param message the message to be logged.
   */
  public void user(UserActions action, String message) {
    userLogger.info("[{}] {}", action.getName(), message);
  }

  /**
   * Logs general exceptions that occur.
   *
   * @param msg  the exception message.
   * @param type the exception class.
   */
  public void exception(String msg, String type) {
    errorLogger.fatal("[exception] [{}] {}", type, (msg == null) ? msg : "no message");
  }

  /**
   * Logs WebApplicationExceptions.
   *
   * @param statusCode the status code sent to the user.
   * @param msg        the message of the exception.
   */
  public void webAppException(int statusCode, String msg) {
    errorLogger.warn("[EXCEPTION] [WEB APPLICATION] [{}] {}", statusCode,
        (msg == null) ? msg : "no message");
  }

  /**
   * Logs WebParamExceptions.
   *
   * @param statusCode the status code sent to the user.
   * @param msg        the message of the exception.
   */
  public void webParamException(int statusCode, String msg) {
    errorLogger.warn("[EXCEPTION] [WEB PARAMETERS] [{}] {}", statusCode,
        (msg == null) ? msg : "no message");
  }

  /**
   * Logs WebBizExceptions.
   *
   * @param msg the message of the exception.
   */
  public void webBizException(String msg) {
    errorLogger.error("[EXCEPTION] [WEB BUSINESS] {}", (msg == null) ? msg : "no message");
  }

  /**
   * Logs DAOExceptions.
   *
   * @param message the message of the exception.
   */
  public void daoException(String message) {
    errorLogger.warn("[EXCEPTION] [DAO] {}", (message == null) ? message : "no message");
  }

  /**
   * Logs Optimistic locks.
   *
   * @param message the message of the exception.
   */
  public void optimisticLockException(String message) {
    errorLogger.warn("[EXCEPTION] [Optimistic lock] {}",
        (message == null) ? message : "no message");
  }

  /**
   * Logs factory events.
   *
   * @param msg the message to be logged.
   */
  public void factory(String msg) {
    infoLogger.info("[FACTORY] {}", msg);
  }


  /**
   * Actions a user can attempt.
   */
  public enum UserActions {
    /**
     * log-in related logging.
     */
    LOGIN("connexion"),

    /**
     * log-out related logging.
     */
    LOGOUT("déconnexion"),

    /**
     * registering related logging.
     */
    REGISTER("inscription");

    private String name;

    /**
     * Constructor for userActions.
     *
     * @param name the nice name for the action.
     */
    UserActions(String name) {
      this.name = name;
    }

    /**
     * get the nice name of an action.
     *
     * @return the nice name of the action.
     */
    String getName() {
      return this.name;
    }
  }
}
