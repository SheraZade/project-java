package be.vinci.pae.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * the implementation of the classe Config.
 *
 * @author Loubna Eljattari
 */
public class Config {


  private static Properties props;

  /**
   * Loading the properties file.
   *
   * @param file properties file to load
   */
  public static void load(String file) {
    props = new Properties();
    try (InputStream input = new FileInputStream(file)) {
      props.load(input);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Get the value of a key.
   *
   * @param key the key of the property
   * @return returns the value of the property
   */
  public static String getProperty(String key) {
    return props.getProperty(key);
  }

}
