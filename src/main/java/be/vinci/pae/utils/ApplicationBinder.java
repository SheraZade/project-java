package be.vinci.pae.utils;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.availability.AvailabilityImpl;
import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.factory.FactoryImpl;
import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.domain.picture.PictureImpl;
import be.vinci.pae.domain.type.Type;
import be.vinci.pae.domain.type.TypeImpl;
import be.vinci.pae.services.availability.AvailabilityDAO;
import be.vinci.pae.services.availability.AvailabilityDAOImpl;
import be.vinci.pae.services.availability.AvailabilityUCC;
import be.vinci.pae.services.availability.AvailabilityUCCImpl;
import be.vinci.pae.services.dal.DALServicesImpl;
import be.vinci.pae.services.dal.DalBackendServices;
import be.vinci.pae.services.dal.DalServices;
import be.vinci.pae.services.member.MemberDAO;
import be.vinci.pae.services.member.MemberDAOImpl;
import be.vinci.pae.services.member.MemberUCC;
import be.vinci.pae.services.member.MemberUCCImpl;
import be.vinci.pae.services.notification.NotificationDAO;
import be.vinci.pae.services.notification.NotificationDAOImpl;
import be.vinci.pae.services.notification.NotificationUCC;
import be.vinci.pae.services.notification.NotificationUCCImpl;
import be.vinci.pae.services.notificationmember.NotificationMembersDAO;
import be.vinci.pae.services.notificationmember.NotificationMembersDAOImpl;
import be.vinci.pae.services.notificationmember.NotificationMembersUCC;
import be.vinci.pae.services.notificationmember.NotificationMembersUCCImpl;
import be.vinci.pae.services.object.ObjectDAO;
import be.vinci.pae.services.object.ObjectDAOImpl;
import be.vinci.pae.services.object.ObjectUCC;
import be.vinci.pae.services.object.ObjectUCCImpl;
import be.vinci.pae.services.picture.PictureDAO;
import be.vinci.pae.services.picture.PictureDAOImpl;
import jakarta.inject.Singleton;
import jakarta.ws.rs.ext.Provider;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

/**
 * ApplicationBinder configure the binding between factories and their implementations.
 */
@Provider
public class ApplicationBinder extends AbstractBinder {

  /**
   * configure the bind between all the factories and their implementations.
   */
  @Override
  protected void configure() {
    bind(FactoryImpl.class).to(Factory.class).in(Singleton.class);
    bind(DALServicesImpl.class).to(DalServices.class).to(DalBackendServices.class)
        .in(Singleton.class);
    bind(MemberUCCImpl.class).to(MemberUCC.class).in(Singleton.class);
    bind(MemberDAOImpl.class).to(MemberDAO.class).in(Singleton.class);
    bind(ObjectDAOImpl.class).to(ObjectDAO.class).in(Singleton.class);
    bind(ObjectUCCImpl.class).to(ObjectUCC.class).in(Singleton.class);
    bind(NotificationDAOImpl.class).to(NotificationDAO.class).in(Singleton.class);
    bind(NotificationUCCImpl.class).to(NotificationUCC.class).in(Singleton.class);
    bind(NotificationMembersDAOImpl.class).to(NotificationMembersDAO.class).in(Singleton.class);
    bind(NotificationMembersUCCImpl.class).to(NotificationMembersUCC.class).in(Singleton.class);
    bind(AvailabilityImpl.class).to(Availability.class).in(Singleton.class);
    bind(AvailabilityDAOImpl.class).to(AvailabilityDAO.class).in(Singleton.class);
    bind(AvailabilityUCCImpl.class).to(AvailabilityUCC.class).in(Singleton.class);
    bind(TypeImpl.class).to(Type.class).in(Singleton.class);
    bind(PictureImpl.class).to(Picture.class).in(Singleton.class);
    bind(PictureDAOImpl.class).to(PictureDAO.class).in(Singleton.class);
  }
}