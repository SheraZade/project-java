package be.vinci.pae.utils;

import be.vinci.pae.exceptions.BizException;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.OptimisticException;
import be.vinci.pae.exceptions.WebParametersException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * webExceptionMapper is a class allowing to manage exceptions.
 *
 * @author Dorcas KUEZE
 */
@Provider
public class WebExceptionMapper implements ExceptionMapper<Throwable> {

  private static final Logs logger = new Logs();

  /**
   * get user for checkPassword.
   *
   * @param exception exception to be thrown.
   * @return returns the status corresponding to the exception.
   */
  @Override
  public Response toResponse(Throwable exception) {
    if (exception instanceof WebApplicationException) {
      WebApplicationException exc = (WebApplicationException) exception;
      logger.webAppException(exc.getResponse().getStatus(), exc.getMessage());
      return Response.status(((WebApplicationException) exception).getResponse().getStatus())
          .entity(exception.getMessage())
          .build();
    } else if (exception instanceof WebParametersException) {
      WebParametersException exc = (WebParametersException) exception;
      logger.webParamException(exc.getStatus().getStatusCode(), exc.getMessage());
      return Response.status(((WebParametersException) exception).getResponse().getStatus())
          .entity(exception.getMessage())
          .build();
    } else if (exception instanceof BizException) {
      BizException exc = (BizException) exception;
      logger.webBizException(exc.getMessage());
      return Response.status(Status.BAD_REQUEST)
          .entity(exception.getMessage())
          .build();
    } else if (exception instanceof DAOException) {
      DAOException exc = (DAOException) exception;
      logger.daoException(exc.getMessage());
      return Response.status(Status.EXPECTATION_FAILED)
          .entity(exception.getMessage())
          .build();
    } else if (exception instanceof OptimisticException) {
      OptimisticException exc = (OptimisticException) exception;
      logger.optimisticLockException(exc.getMessage());
      return Response.status(Status.CONFLICT)
          .entity(exception.getMessage())
          .build();
    }
    logger.exception(exception.getMessage(), exception.getClass().getSimpleName());
    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
        .entity(exception.getMessage())
        .build();
  }
}
