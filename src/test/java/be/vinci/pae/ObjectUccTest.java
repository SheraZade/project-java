package be.vinci.pae;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.member.Member;
import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.domain.type.Type;
import be.vinci.pae.exceptions.BizException;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.availability.AvailabilityDAO;
import be.vinci.pae.services.availability.AvailabilityUCC;
import be.vinci.pae.services.member.MemberDAO;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.notification.NotificationDAO;
import be.vinci.pae.services.notificationmember.NotificationMembersDAO;
import be.vinci.pae.services.notificationmember.NotificationMembersUCC;
import be.vinci.pae.services.object.ObjectDAO;
import be.vinci.pae.services.object.ObjectDTO;
import be.vinci.pae.services.object.ObjectDTO.ObjectState;
import be.vinci.pae.services.object.ObjectUCC;
import be.vinci.pae.services.type.TypeDTO;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;


/**
 * This is the test class for the Object Use Case Controller (UCC).
 *
 * @author Loubna
 */

public class ObjectUccTest {

  private static ObjectDAO objectDAO;
  private static MemberDAO memberDAO;
  private static MemberDTO memberDTO;
  private static ObjectUCC objectUCC;
  private static ServiceLocator locator;
  private static ObjectDTO objectDTO;
  private static Availability availability;
  private static NotificationDAO notificationDAO;
  private static NotificationMembersDAO notificationMembersDAO;

  private static NotificationMembersUCC notificationMembersUCC;
  private static Factory myFactory;
  private static AvailabilityDAO availabilityDAO;
  private static AvailabilityUCC availabilityUCC;

  @BeforeAll
  static void initAll() {
    locator = ServiceLocatorUtilities.bind(new ApplicationBinderTest());
    objectDAO = locator.getService(ObjectDAO.class);
    objectUCC = locator.getService(ObjectUCC.class);
    objectDTO = locator.getService(ObjectDTO.class);
    memberDAO = locator.getService(MemberDAO.class);
    availability = locator.getService(Availability.class);
    notificationDAO = locator.getService(NotificationDAO.class);
    notificationMembersDAO = locator.getService(NotificationMembersDAO.class);
    myFactory = locator.getService(Factory.class);
    availabilityDAO = locator.getService(AvailabilityDAO.class);
    availabilityUCC = locator.getService(AvailabilityUCC.class);
  }

  /**
   * set up the fields.
   */
  @BeforeEach
  public void setup() {
    Mockito.reset(objectDAO);
    Factory f = locator.getService(Factory.class);
    objectDTO = f.getObject();
    availability.setDate(LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY)));
    availability.setTimeSlot("AM");
    objectDTO.setDateOfReceipt(availability);
    Type type = f.getType();
    type.setTypeName("Chaise");
    type.setId(1);
    objectDTO.setType(type);
    objectDTO.setPhoneNumber("1234567890");
    objectDTO.setId(1);
    objectDTO.setDescription("description");
    objectDTO.setState(ObjectState.IN_STORE);
    Picture picture = myFactory.getPicture();
    picture.setUrl("chaise.png");
    picture.setId(1);
    picture.setObjectId(objectDTO.getId());
    objectDTO.setPicture(picture);
    memberDTO = f.getUser();
    memberDTO.setId(1);
    memberDTO.setEmail("bas.ile@vinci.be");
    memberDTO.setPhoneNumber("048756235");
    memberDTO.setFirstName("Base");
    memberDTO.setLastName("Ile");
  }


  @Test
  @DisplayName("should return the object with the given id")
  void testGetObjectOK() {
    int id = 1;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    assertEquals(objectDTO, objectUCC.getObject(id));
    verify(objectDAO).getObject(id);
  }

  /**
   * Test getObject with the following invalid ID's: - ID inferior to 0. - ID that does not exist in
   * the database.
   */
  @ParameterizedTest
  @ValueSource(ints = {-1, 0})
  @DisplayName("should throw exception when given invalid id of object <= 0")
  public void testGetObjectInvalidID(int id) {
    assertThrows(DAOException.class, () -> objectUCC.getObject(id));
  }

  /**
   * Test of getObject() when problem with DAO occurs. Should throw FatalException.
   */
  @Test
  @DisplayName("should throw exception when given a null parameter to getObject()")
  public void testGetObjectRollback() {
    int id = 1;
    when(objectDAO.getObject(id)).thenThrow(new FatalException());
    assertThrows(FatalException.class, () -> objectUCC.getObject(id));
  }

  /**
   * Test of addPriceToObject with correct parameters. Should return the object with the new state
   * and the given price.
   */
  @Test
  @DisplayName("should return the object with the new state and the given price")
  public void testAddPriceToObjectOK() {
    int id = 1;
    double price = 3.50;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.addPriceToObject(objectDTO)).thenReturn(objectDTO);

    assertAll(
        () -> assertNotNull(objectUCC.addPriceObject(id, price)),
        () -> assertEquals(objectDTO.getPrice(), price),
        () -> assertEquals(objectDTO.getState(), ObjectState.AVAILABLE_FOR_SALE)
    );
  }

  /**
   * Test of addPriceObject with invalid id. Should throw DAOException when given invalid id of
   * object <= 0
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw DAOException when given invalid id of object <= 0")
  public void testAddPriceObjectInvalidID(int id) {
    double price = 3.50;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    assertThrows(DAOException.class, () -> objectUCC.addPriceObject(id, price));
  }

  /**
   * Test of addPriceObject, should throw DAOException when given a null parameter to getObject().
   */
  @Test
  @DisplayName("should throw DAOException when given invalid parameter to getObject()")
  public void testAddPriceObjectInvalidParameter() {
    int id = 1;
    double price = 3.50;
    when(objectDAO.getObject(id)).thenReturn(null);
    assertThrows(DAOException.class, () -> objectUCC.addPriceObject(id, price));
  }

  /**
   * Test addPriceObject with the following invalid prices : 0, -1, -10.
   */
  @ParameterizedTest
  @ValueSource(ints = {-10, -1, 0})
  @DisplayName("should throw BizExceptionUnauthorized exception for invalid price <=0.")
  public void testAddPriceObjectInvalidPrice(int price) {
    int id = 1;
    double expectedPrice = 0;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    objectDTO.setState(ObjectState.IN_STORE);

    assertAll(
        () -> assertThrows(BizException.class, () -> objectUCC.addPriceObject(id, price)),
        () -> assertEquals(expectedPrice, objectDTO.getPrice()),
        () -> assertEquals(ObjectState.IN_STORE, objectDTO.getState())
    );
  }

  /**
   * Test addPriceObject with an invalid states should throw BizException.
   */
  @ParameterizedTest
  @EnumSource(
      value = ObjectState.class,
      names = {"IN_STORE", "AVAILABLE_FOR_SALE"},
      mode = EnumSource.Mode.EXCLUDE)
  @DisplayName("should throw BizException when given an invalid state for the object.")
  public void testAddPriceObjectInvalidState(ObjectState state) {
    int id = 1;
    double price = 3.50;
    objectDTO.setState(state);
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    assertThrows(BizException.class, () -> objectUCC.addPriceObject(id, price));
  }

  /**
   * Test of addPriceObject() when problem with DAO occurs. Should throw FatalException.
   */
  @Test
  @DisplayName("should throw FatalException when problem with DAO occurs to addPriceObject()")
  public void testAddPriceObjectRollback() {
    objectDTO.setState(ObjectState.IN_STORE);
    int id = 1;

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.addPriceToObject(objectDTO)).thenThrow(new FatalException());

    double price = 3.5;
    assertThrows(FatalException.class, () -> objectUCC.addPriceObject(id, price));
  }

  /**
   * Test putAtWorkshop with correct parameters.
   */
  @Test
  @DisplayName("should update the state of the object as 'In workshop'")
  public void testPutObjectAtWorkshopOK() {
    when(objectDAO.getObject(1)).thenReturn(objectDTO);
    when(objectDAO.depositAtWorkshop(objectDTO)).thenReturn(objectDTO);
    objectDTO.setState(ObjectState.ACCEPT_OFFER);

    assertNotNull(objectUCC.putAtWorkshop(1));
    assertEquals(objectDTO.getState(), ObjectState.IN_WORKSHOP);
  }

  /**
   * Test putAtWorkshop with an invalid id of the object, should throw DAOException.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw DAOException when given invalid id of the object to update")
  public void testPutObjectAtWorkshopInvalidID(int id) {
    when(objectDAO.getObject(id)).thenReturn(null);
    assertThrows(DAOException.class, () -> objectUCC.putAtWorkshop(id));
  }

  /**
   * Test putAtWorkshop with the invalid states, should throw BizException.
   */
  @ParameterizedTest
  @EnumSource(
      value = ObjectState.class,
      names = {"ACCEPT_OFFER"},
      mode = EnumSource.Mode.EXCLUDE)
  @DisplayName("should throw BizException for invalid states given in parameter.")
  public void testPutAtWorkshopInvalidState(ObjectState state) {
    int id = 1;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.depositAtWorkshop(objectDTO)).thenReturn(objectDTO);
    objectDTO.setState(state);
    assertThrows(BizException.class, () -> objectUCC.putAtWorkshop(id));
  }

  /**
   * Test of putAtWorkshop() when problem with DAO occurs. Should throw FatalException.
   */
  @Test
  @DisplayName("should throw FatalException when problem occurs in DAO.")
  public void testPutAtWorkshopRollback() {
    int id = 1;
    objectDTO.setState(ObjectState.ACCEPT_OFFER);

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.depositAtWorkshop(objectDTO)).thenThrow(new FatalException());

    assertThrows(FatalException.class, () -> objectUCC.putAtWorkshop(id));
  }

  /**
   * Test of PutAtWorkshop() when given an invalid parameter. Should throw DAOException.
   */
  @Test
  @DisplayName("should throw DAOException when given a null parameter to depositAtWorkshop()")
  public void testPutAtWorkshopInvalidParameter() {
    int id = 1;
    ObjectState immutableStateExpected = ObjectState.ACCEPT_OFFER;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    objectDTO.setState(immutableStateExpected);

    when(objectDAO.depositAtWorkshop(objectDTO)).thenReturn(null);
    assertThrows(DAOException.class, () -> objectUCC.putAtWorkshop(id));
  }


  /**
   * Test of putAsSold when a valid object is given, should update the state and the selling date of
   * the object.
   */
  @Test
  @DisplayName("should update the state of the object as sold and put selling date of today.")
  public void testPutAsSoldOK() {
    objectDTO.setState(ObjectState.AVAILABLE_FOR_SALE);

    when(objectDAO.getObject(1)).thenReturn(objectDTO);
    when(objectDAO.putObjectAsSold(objectDTO)).thenReturn(objectDTO);

    ObjectDTO objectResult = objectUCC.putAsSold(objectDTO.getId());
    ObjectState stateExpected = ObjectState.SOLD;

    assertEquals(objectResult.getState(), stateExpected);
    assertEquals(objectResult.getSellingDate(), LocalDate.now());
  }


  /**
   * Test of putAsSold with invalid id of the object, should throw DAOException.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw DAOException when given invalid id of object <= 0")
  public void testPutAsSoldInvalidID(int id) {
    assertThrows(DAOException.class, () -> objectUCC.putAsSold(id));
  }

  /**
   * Test of putAsSold with invalid states of the object, should throw BizException, excluding
   * 'Available for sale'.
   */
  @ParameterizedTest
  @EnumSource(
      value = ObjectState.class,
      names = {"AVAILABLE_FOR_SALE"},
      mode = EnumSource.Mode.EXCLUDE)
  @DisplayName("should throw BizException when given invalid state of object: 'Offered item'")
  public void testPutAsSoldInvalidState(ObjectState state) {
    int id = 1;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.depositAtWorkshop(objectDTO)).thenReturn(objectDTO);
    objectDTO.setState(state);
    assertThrows(BizException.class, () -> objectUCC.putAsSold(id));
  }

  /**
   * Test of putAsSold() when problem with DAO occurs. Should throw FatalException.
   */
  @Test
  @DisplayName("should throw exception when given a null parameter to putAsSold()")
  public void testPutAsSoldRollback() {
    int id = 1;
    objectDTO.setState(ObjectState.AVAILABLE_FOR_SALE);

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.putObjectAsSold(objectDTO)).thenReturn(objectDTO);
    when(objectDAO.putObjectAsSold(objectDTO)).thenThrow(new FatalException());

    assertThrows(FatalException.class, () -> objectUCC.putAsSold(id));
  }

  /**
   * Test of PutAsSold() when given an invalid parameter. Should throw DAOException.
   */
  @Test
  @DisplayName("should throw DAOException when given a null parameter to putObjectAsSold()")
  public void testPutAsSoldInvalidParameter() {
    int id = 1;
    objectDTO.setState(ObjectState.AVAILABLE_FOR_SALE);
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.putObjectAsSold(objectDTO)).thenReturn(null);

    assertThrows(DAOException.class, () -> objectUCC.putAsSold(id));
  }


  /**
   * Test of refuseOfferObject with invalid id of the object, should throw DAOException.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw DAOException when given invalid id of object <= 0")
  public void testRefuseOfferObjectInvalidID(int id) {
    assertThrows(DAOException.class, () ->
        objectUCC.refuseOfferObject(id, "not clean enough"));
  }

  /**
   * Test of refuseOfferObject with invalid object, should throw DAOException.
   */
  @Test
  @DisplayName("should throw DAOException when given invalid object.")
  public void testRefuseOfferObjectInvalid() {
    int id = 1;
    objectDTO.setState(ObjectState.OFFERED_ITEM);
    when(objectDAO.getObject(id)).thenReturn(objectDTO);

    assertThrows(DAOException.class, () ->
        objectUCC.refuseOfferObject(id, "not clean enough"));
  }


  /**
   * Test of refuseOfferObject, when called with an object in the correct state. Should update the
   * refusal message and the state of the object.
   */
  @Test
  @DisplayName("Should update the refusal message and the state of the object.")
  public void testRefuseOfferObjectOK() {
    //    int idObject = 2;
    //    objectDTO.setState(ObjectState.OFFERED_ITEM);
    //    when(objectDAO.getObject(idObject)).thenReturn(objectDTO);
    //    when(objectDAO.updateRefusedObject(objectDTO)).thenReturn(objectDTO);
    //
    //    String messageExpected = "not interested";
    //    ObjectState objectStateExpected = ObjectState.DENY_OFFER;
    //    ObjectDTO resultObject = objectUCC.refuseOfferObject(idObject, messageExpected);
    //
    //    assertEquals(messageExpected, resultObject.getReasonForRefusal());
    //    assertEquals(objectStateExpected, resultObject.getState());
  }

  /**
   * Test of RefusedObject when given invalid state excluding 'offered item'.
   */
  @ParameterizedTest
  @EnumSource(
      value = ObjectState.class,
      names = {"OFFERED_ITEM"},
      mode = EnumSource.Mode.EXCLUDE)
  @DisplayName("should throw exception when given invalid state for refuse offer"
      + " excluding 'Offered item'")
  public void testRefuseOfferObjectInvalidStates(ObjectState state) {
    int id = 1;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.updateRefusedObject(objectDTO)).thenReturn(objectDTO);

    objectDTO.setState(state);
    String msg = "not interested";
    assertThrows(BizException.class, () -> objectUCC.refuseOfferObject(id, msg));
  }

  /**
   * Test of refusedObject() when problem with DAO occurs. Should throw FatalException.
   */
  @Test
  @DisplayName("should throw FatalException when problem occurs in DAO to refusedObject()")
  public void testRefuseOfferObjectRollback() {
    int id = 1;
    objectDTO.setState(ObjectState.OFFERED_ITEM);

    Mockito.when(objectDAO.getObject(id)).thenReturn(objectDTO);
    Mockito.when(objectDAO.updateRefusedObject(objectDTO)).thenThrow(new FatalException());

    assertThrows(FatalException.class,
        () -> objectUCC.refuseOfferObject(id, "not interested"));
  }


  /**
   * Test of acceptOfferObject when given invalid id of the object, should throw DAOException.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw DAOException as invalid id of the object")
  public void testAcceptOfferObjectInvalidID(int id) {
    assertThrows(DAOException.class, () -> objectUCC.acceptOfferObject(id));
  }

  /**
   * Test of acceptOfferObject when given invalid state to validate the offer. Should throw
   * BizException.
   */
  @ParameterizedTest
  @EnumSource(
      value = ObjectState.class,
      names = {"OFFERED_ITEM"},
      mode = EnumSource.Mode.EXCLUDE)
  @DisplayName("should throw BizException when given invalid state for validate offer excluding "
      + "'Offered item'")
  public void testAcceptOfferObjectInvalidState(ObjectState state) {
    int id = 1;
    objectDTO.setState(state);
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.acceptOffer(objectDTO)).thenReturn(null);

    assertThrows(BizException.class, () -> objectUCC.acceptOfferObject(id));
  }

  /**
   * Test of acceptOfferObject when given invalid state to validate the offer. Should throw
   * DAOException.
   */
  @Test
  @DisplayName("should throw DAOException when given invalid state for validate offer excluding "
      + "'Offered item'")
  public void testAcceptOfferObjectInvalidQuery() {
    int id = 1;
    objectDTO.setState(ObjectState.OFFERED_ITEM);

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.acceptOffer(objectDTO)).thenReturn(null);

    assertThrows(DAOException.class, () -> objectUCC.acceptOfferObject(id));
  }

  /**
   * Test of acceptOfferObject(), should update the object.

   @Test
   @DisplayName("should update the object's state and register the date of today.")
   public void testAcceptOfferObjectOK() {
   int id = 1;
   Mockito.when(objectDAO.getObject(id)).thenReturn(objectDTO);
   Mockito.when(objectDAO.acceptOffer(objectDTO)).thenReturn(objectDTO);
   //Mockito.when(memberDAO.getOfferer(objectDTO.getId())).thenReturn(memberDTO);

   objectDTO.setState(ObjectState.OFFERED_ITEM);
   objectDTO.setOfferingMember(memberDTO);
   ObjectDTO resultObject = objectUCC.acceptOfferObject(id);
   ObjectState stateExpected = ObjectState.ACCEPT_OFFER;
   LocalDate dateExpected = LocalDate.now();

   assertAll(
   () -> assertNotNull(resultObject),
   () -> assertEquals(stateExpected, resultObject.getState()),
   () -> assertEquals(dateExpected, resultObject.getAcceptanceDate()),
   () -> assertEquals(resultObject, objectDTO)
   );
   //  notificationMembersUCC.getNotificationMembers(memberDTO.getId());

   }
   */

  /**
   * Test of acceptOfferObject() when problem with DAO occurs. Should throw FatalException.
   */
  @Test
  @DisplayName("should throw FatalException when problem with DAO occurs to acceptOfferObject().")
  public void testAcceptOfferObjectRollback() {
    int id = 1;
    objectDTO.setState(ObjectState.OFFERED_ITEM);

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.acceptOffer(objectDTO)).thenThrow(new FatalException());

    assertThrows(FatalException.class, () -> objectUCC.acceptOfferObject(id));
  }

  /**
   * Test of acceptOfferObject() and send a notification to the offering member if he exist.
   *
   //@Test
   //@DisplayName("should send a notification when the object has been accept")
   public void testAcceptSendNotif() {
   objectDTO.setState(ObjectState.OFFERED_ITEM);
   objectDTO.setOfferingMember(memberDTO);
   int id = 1;
   objectDTO.setId(id);
   ObjectDTO obj = objectDAO.acceptOffer(objectDTO);
   System.out.println(obj);
   Mockito.when(objectDAO.acceptOffer(objectDTO)).thenReturn(objectDTO);
   System.out.println(Arrays.toString(notificationMembersDAO.allNotifications().toArray()));
   }
   */

  /**
   * Test of putDepositAtStore when given valid states. Should update the state of the object.
   */
  @ParameterizedTest
  @EnumSource(
      value = ObjectState.class,
      names = {"IN_WORKSHOP", "ACCEPT_OFFER"})
  @DisplayName("should update the state of the object to 'In store'")
  public void testPutDepositAtStoreOK(ObjectState state) {
    int id = 1;
    objectDTO.setState(state);

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.depositAtStore(objectDTO)).thenReturn(objectDTO);

    ObjectDTO resultObject = objectUCC.putDepositAtStore(id);
    LocalDate dateExpected = LocalDate.now();
    ObjectState stateExpected = ObjectState.IN_STORE;

    assertAll(
        () -> assertNotNull(resultObject),
        () -> assertNotNull(resultObject.getStoreDepositDate()),
        () -> assertEquals(stateExpected, resultObject.getState()),
        () -> assertEquals(dateExpected, resultObject.getStoreDepositDate())
    );
  }


  /**
   * Test of putDepositAtStore when given invalid id of the object. Should throw DAOException.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw DAOException when given invalid id for the object <= 0.")
  public void testPutDepositAtStoreInvalidID(int id) {
    assertThrows(DAOException.class, () -> objectUCC.putDepositAtStore(id));
  }

  /**
   * Test of putDepositAtStore when given invalid state except 'accept offer' and 'in workshop'.
   * Should throw exception.
   */
  @ParameterizedTest
  @EnumSource(
      value = ObjectState.class,
      names = {"ACCEPT_OFFER", "IN_WORKSHOP"},
      mode = EnumSource.Mode.EXCLUDE)
  @DisplayName("should throw exception when given invalid state for depositAtStore"
      + " excluding 'Accept offer' and 'In Workshop'")
  public void testPutDepositAtStoreInvalidStates(ObjectState state) {
    int id = 1;
    objectDTO.setState(state);

    when(objectDAO.getObject(id)).thenReturn(objectDTO);

    assertThrows(BizException.class, () -> objectUCC.putDepositAtStore(id));
  }

  /**
   * Test of depositAtStore() when given an invalid parameter. Should throw DAOException.
   */
  @Test
  @DisplayName("should throw DAOException when given a null parameter to depositAtStore()")
  public void testPutDepositAtStoreInvalidParameter() {
    int id = 1;
    objectDTO.setState(ObjectState.ACCEPT_OFFER);

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.depositAtStore(objectDTO)).thenReturn(null);

    assertThrows(DAOException.class, () -> objectUCC.putDepositAtStore(id));
  }

  /**
   * Test PutDepositAtStore when problem with DAO occurs. Should throw a FatalException.
   */
  @Test
  @DisplayName("should throw a FatalException when problem with DAO for putDepositAtStore().")
  public void testPutDepositAtStoreRollback() {
    int id = 1;
    objectDTO.setState(ObjectState.IN_WORKSHOP);

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.depositAtStore(objectDTO)).thenThrow(new FatalException());

    assertThrows(FatalException.class, () -> objectUCC.putDepositAtStore(id));
  }

  /**
   * Test of withdrawObject when given a valid state of the object. Should update the state and the
   * date.
   */
  @Test
  @DisplayName("should update the state and the date of the object as 'Withdrawn'.")
  public void testWithdrawOK() {
    int id = 1;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.withdrawObject(objectDTO)).thenReturn(objectDTO);
    objectDTO.setState(ObjectState.AVAILABLE_FOR_SALE);

    ObjectDTO resultObject = objectUCC.withdrawObject(objectDTO);
    ObjectState stateExpected = ObjectState.WITHDRAWN;
    LocalDate dateExpected = LocalDate.now();

    assertAll(
        () -> assertNotNull(resultObject),
        () -> assertNotNull(resultObject.getMarketWithdrawalDate()),
        () -> assertEquals(stateExpected, resultObject.getState()),
        () -> assertEquals(dateExpected, resultObject.getMarketWithdrawalDate())
    );
  }

  /**
   * Test of withdrawObject when given invalid states, except for the state excluded. Should throw
   * BizException.
   */
  @ParameterizedTest
  @EnumSource(
      value = ObjectState.class,
      names = {"AVAILABLE_FOR_SALE"},
      mode = EnumSource.Mode.EXCLUDE)
  @DisplayName("should throw BizException when invalid state of the object. "
      + "Except for the state excluded.")
  public void testWithdrawObjectInvalidState(ObjectState state) {
    int id = 1;
    objectDTO.setState(state);

    when(objectDAO.getObject(id)).thenReturn(objectDTO);

    assertThrows(BizException.class, () -> objectUCC.withdrawObject(objectDTO));
  }


  /**
   * Test of withdrawObject() when problem with DAO occurs. Should throw FatalException.
   */
  @Test
  @DisplayName("should throw a FatalException when problem with DAO to withdrawObject.")
  public void testWithdrawObjectRollback() {
    int id = 1;

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.withdrawObject(objectDTO)).thenThrow(new FatalException());

    objectDTO.setState(ObjectState.AVAILABLE_FOR_SALE);

    assertThrows(FatalException.class, () -> objectUCC.withdrawObject(objectDTO));
  }

  /**
   * Test of withdrawObject() when problem with DAO occurs. Should throw DAOException.
   */
  @Test
  @DisplayName("should throw a DAOException when problem with DAO to withdrawObject.")
  public void testWithdrawObjectErrorInDAO() {
    int id = 1;

    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.withdrawObject(objectDTO)).thenReturn(null);
    objectDTO.setState(ObjectState.AVAILABLE_FOR_SALE);

    assertThrows(DAOException.class, () -> objectUCC.withdrawObject(objectDTO));
  }


  /**
   * Test of getAllObject if returns a complet list of all the objects in database.
   */
  @Test
  @DisplayName("should return a list of all the objects in database.")
  public void testGetAllObjectOK() {
    ObjectDTO mockObject1 = mock(ObjectDTO.class);
    ObjectDTO mockObject2 = mock(ObjectDTO.class);

    List<ObjectDTO> expectedList = new ArrayList<>();
    expectedList.add(mockObject1);
    expectedList.add(mockObject2);

    when(objectDAO.getAllObject()).thenReturn(expectedList);

    List<ObjectDTO> actualList = objectUCC.allObjectsList();

    assertAll(
        () -> assertEquals(expectedList.size(), actualList.size()),
        () -> assertEquals(expectedList.get(0), actualList.get(0)),
        () -> assertEquals(expectedList.get(1), actualList.get(1))
    );
  }

  /**
   * Test getAllObject when problem with DAO occurs. Should throw a FatalException.
   */
  @Test
  @DisplayName("should throw a FatalException when problem with DAO for getAllObject().")
  public void testGetAllObjectRollback() {
    when(objectDAO.getAllObject()).thenThrow(new FatalException());
    assertThrows(FatalException.class, () -> objectUCC.allObjectsList());
  }

  /**
   * Test of getAllTypes, if return a complet list of the all types in database.
   */
  @Test
  @DisplayName("should return a list of all object's type")
  public void testGetAllTypesOK() {
    TypeDTO type1 = myFactory.getType();
    type1.setTypeName("Tableau");
    type1.setId(1);
    TypeDTO type2 = myFactory.getType();
    type2.setTypeName("Meuble");
    type2.setId(2);
    TypeDTO type3 = myFactory.getType();
    type3.setTypeName("Nounours");
    type3.setId(1);
    List<TypeDTO> expectedTypes = Arrays.asList(type1, type2, type3);

    when(objectDAO.listTypesObjects()).thenReturn(expectedTypes);
    List<TypeDTO> actualTypes = objectUCC.getAllTypes();

    assertEquals(expectedTypes, actualTypes);
  }

  /**
   * Test getAllTypes when problem with DAO occurs. Should throw a FatalException.
   */
  @Test
  @DisplayName("should throw a FatalException when problem with DAO for getAllTypes().")
  public void testGetAllTypesRollback() {
    when(objectDAO.listTypesObjects()).thenThrow(new FatalException());
    assertThrows(FatalException.class, () -> objectUCC.getAllTypes());
  }

  /**
   * Test of offeredObjects when there isn't any offered object, should return empty list.
   */
  @Test
  @DisplayName("should return an empty list if there isn't any object offered")
  public void testOfferedObjectsEmpty() {
    List<ObjectDTO> expectedlist = new ArrayList<>();
    List<ObjectDTO> returnedList = objectUCC.offeredObjects();

    when(objectDAO.offeredObjects()).thenReturn(expectedlist);

    assertTrue(returnedList.isEmpty());
  }

  /**
   * Test of offeredObjects when there is a list of offered object, should return a complet list.
   */
  @Test
  @DisplayName("should return a list of objects in the state 'Offered item'")
  public void testOfferedObjects() {
    List<ObjectDTO> expectedList = new ArrayList<>();
    ObjectDTO mockObjectDTO1 = mock(ObjectDTO.class);
    ObjectDTO mockObjectDTO2 = mock(ObjectDTO.class);
    expectedList.add(mockObjectDTO1);
    expectedList.add(mockObjectDTO2);

    when(objectDAO.offeredObjects()).thenReturn(expectedList);
    List<ObjectDTO> resultList = objectUCC.offeredObjects();

    assertEquals(expectedList, resultList);
  }

  /**
   * Test of OfferedObjects should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to offeredObjects.")
  public void testOfferedObjectsRollback() {
    when(objectDAO.offeredObjects()).thenThrow(new FatalException());
    assertThrows(FatalException.class, () -> objectUCC.offeredObjects());
  }


  /**
   * Test of getUserObject when given invalid id of user. Should throw DAOException.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw DAOException when given invalid id of user.")
  public void testGetUserObjectsInvalidID(int id) {
    assertThrows(DAOException.class, () -> objectUCC.memberObjects(id));
  }

  /**
   * Test of getUserObjects when given a valid id user, should return a list of objects.
   */
  @Test
  @DisplayName("should return a list of all the objects of the given user.")
  public void testGetUserObjectsOK() {
    List<ObjectDTO> expectedObjects = new ArrayList<>();
    expectedObjects.add(objectDTO);
    MemberDTO user = myFactory.getUser();
    objectDTO.setOfferingMember((Member) user);
    int userId = 1;
    user.setId(userId);

    when(objectDAO.memberObjects(userId)).thenReturn(expectedObjects);
    when(memberDAO.getMemberByID(1)).thenReturn(user);

    List<ObjectDTO> resultObjects = objectUCC.memberObjects(userId);

    assertEquals(expectedObjects, resultObjects);
  }

  /**
   * Test offerObject, should add a new object as Offered for a member.
   */
  @Test
  @DisplayName("should add a new object as Offered for a member.")
  public void testOfferObjectSuccessWithMember() {
    //    objectDTO.setOfferingMember((Member) memberDTO);
    //    int idExpected = 2;
    //    NotificationDTO notificationDTO = myFactory.getNotification();
    //    when(objectDAO.getAvailability(objectDTO.getDateOfReceipt())).thenReturn(1);
    //    when(objectDAO.offerMember(objectDTO, 1)).thenReturn(idExpected);
    //    when(notificationDAO.addNotification(any(NotificationDTO.class))).thenReturn(
    //        notificationDTO);
    //    assertEquals(idExpected, objectUCC.offerObject(objectDTO));
  }

  /**
   * Test offerObject, should add a new object as Offered for a given phone number.
   */
  @Test
  @DisplayName("should add a new object as Offered for a given phone number.")
  public void testOfferObjectSuccessWithPhoneNumber() {
    memberDTO.setId(-1);
    objectDTO.setOfferingMember((Member) memberDTO);
    objectDTO.setPhoneNumber("048632541");
    int idExpected = 1;

    when(availabilityDAO.getAvailability(objectDTO.getDateOfReceipt())).thenReturn(1);
    when(objectDAO.offerPhoneNumber(objectDTO, 1)).thenReturn(idExpected);
    when(availabilityUCC.addAvailability(availability)).thenReturn(availability);

    // assertEquals(idExpected, objectUCC.offerObject(objectDTO));
  }

  /**
   * Test offerObject, should add a new availability and a new object as offered.
   */
  @Test
  @DisplayName("should add a new availability and a new object as offered.")
  public void testOfferObjectInexistantAvailabilityID() {
    //    objectDTO.setOfferingMember((Member) memberDTO);
    //    int idExpected = 1;
    //
    //    when(objectDAO.getAvailability(objectDTO.getDateOfReceipt())).thenReturn(-1);
    //    when(memberDAO.addAvailability(objectDTO.getDateOfReceipt())).thenReturn(1);
    //    when(objectDAO.offerMember(objectDTO, 1)).thenReturn(idExpected);
    //    assertEquals(idExpected, objectUCC.offerObject(objectDTO));
  }

  /**
   * Test offerObject(), should throw BizException for missing user and phone number.
   */
  @Test
  @DisplayName("should throw DAOException as missing user and phone number")
  public void testOfferObjectMissingUserAndPhone() {
    objectDTO.setOfferingMember((Member) memberDTO);
    objectDTO.setPhoneNumber(null);
    //    when( availabilityDAO.getAvailability(objectDTO.getDateOfReceipt()))
    //        .thenReturn(objectDTO.getDateOfReceipt().getId());
    //    when(objectDAO.offerMember(objectDTO, objectDTO.getDateOfReceipt().getId()))
    //        .thenReturn(objectDTO.getDateOfReceipt().getId());
    //    assertThrows(DAOException.class, () -> objectUCC.offerObject(objectDTO));
  }

  /**
   * test of OfferObject, should throw DAOException as error when inserting the new object into the
   * database.
   */
  @Test
  @DisplayName("should throw DAOException as error when inserting the new object into the database")
  public void testOfferObjectInvalidReturnValue() {
    Member mockUser = Mockito.mock(Member.class);
    objectDTO.setOfferingMember(mockUser);
    when(availabilityUCC.addAvailability(availability)).thenReturn(availability);
    when(objectDAO.offerMember(objectDTO, 1)).thenReturn(-1);
    assertThrows(DAOException.class, () -> objectUCC.offerObject(objectDTO));
  }


  /**
   * Test of OfferObjects should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to offerObjects.")
  public void testOfferObjectsRollback() {
    when(availabilityUCC.getAvailabilityId(availability)).thenThrow(new FatalException());
    assertThrows(FatalException.class, () -> objectUCC.offerObject(objectDTO));
  }

  /**
   * Test getUserObjects when problem with DAO occurs. Should throw a FatalException.
   */
  @Test
  @DisplayName("should throw a FatalException when problem with DAO for getUserObjects().")
  public void testGetUserObjectsRollback() {
    int id = 1;
    Member memberMock = mock(Member.class);

    when(memberDAO.getMemberByID(id)).thenReturn(memberMock);
    when(objectDAO.memberObjects(id)).thenThrow(new FatalException());

    assertThrows(FatalException.class, () -> objectUCC.memberObjects(id));
  }

  /**
   * test of getAllStates, should return a list of all the possible state in the enum.
   */
  @Test
  @DisplayName("should return a list of all the possible state of an object in the enum.")
  public void testGetAllStatesOK() {
    ObjectState[] expectedStates = ObjectState.values();
    ArrayList<String> actualStates = objectUCC.getAllStates();

    assertEquals(expectedStates.length, actualStates.size());

    for (ObjectState state : expectedStates) {
      assertTrue(actualStates.contains(state.getNameState()));
    }
  }

  /**
   * test of getObjectForCarousel, should return a list of all objects in the state of 'Available
   * for sale', 'Sold', 'In store'.
   */
  @Test
  @DisplayName("should return a list of all objects in the state of 'Available for sale', 'Sold', "
      + "'In store'.")
  public void testGetObjectForCarouselOK() {
    ArrayList<ObjectDTO> expectedStates = new ArrayList<>();
    expectedStates.add(objectDTO);

    when(objectDAO.getObjectsForCarousel()).thenReturn(expectedStates);
    List<ObjectDTO> resultList = objectUCC.getObjectForCarousel();

    assertEquals(expectedStates.size(), resultList.size());
    assertEquals(expectedStates, resultList);
  }

  /**
   * Test of getObjectForCarousel should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to "
      + "getObjectForCarousel.")
  public void testGetObjectForCarouselRollback() {
    when(objectDAO.getObjectsForCarousel()).thenThrow(new FatalException());
    assertThrows(FatalException.class, () -> objectUCC.getObjectForCarousel());
  }

  /**
   * Test of UpdateObject should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to UpdateObject.")
  public void testUpdateObjectRollback() {
    doThrow(new FatalException()).when(objectDAO).getObject(objectDTO.getId());
    assertThrows(FatalException.class, () -> objectUCC.updateObject(objectDTO));
  }

  /**
   * Test of UpdateObject should return true when updating a valid object.
   */
  @Test
  @DisplayName("should return true when updating a valid object.")
  public void testUpdateObjectOK() {
    //    int id = 1;
    //    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    //    when(objectDAO.updateObject(objectDTO)).thenReturn(objectDTO);
    //
    //    assertTrue(objectUCC.updateObject(objectDTO));
  }

  /**
   * Test of UpdateObject should throw a DAOException when a null value is returned by DAO to
   * UpdateObject.
   */
  @Test
  @DisplayName("should throw a DAOException when a null value is returned by DAO to UpdateObject.")
  public void testUpdateObjectNullParameter() {
    int id = 1;
    when(objectDAO.getObject(id)).thenReturn(objectDTO);
    when(objectDAO.getTypeByName("Chaise")).thenReturn(1);
    when(objectDAO.updateObject(objectDTO)).thenReturn(null);

    assertThrows(DAOException.class, () -> objectUCC.updateObject(objectDTO));
  }


  /**
   * Test of UpdateObject should throw a DAOException when given invalid id.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw a DAOException when a problem with DAO occurs to UpdateObject.")
  public void testUpdateObjectInvalidID(int id) {
    when(objectDAO.getObject(id)).thenReturn(null);

    assertThrows(DAOException.class, () -> objectUCC.updateObject(objectDTO));
  }

  /**
   * Test of setPicture should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to setPicture.")
  public void testSetPictureRollback() {
    String filePath = "picture.jpg";
    int idPicture = 1;
    doThrow(new FatalException()).when(objectDAO).setPicture(idPicture, filePath);
    assertThrows(FatalException.class, () -> objectUCC.setPicture(idPicture, filePath));
  }

  /**
   * Test of setPicture should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to setPicture.")
  public void testSetPictureOK() {
    String filePath = "picture.jpg";
    int idPicture = 1;
    ObjectUCC objetUcc = Mockito.mock(ObjectUCC.class);
    objetUcc.setPicture(idPicture, filePath);
    Mockito.verify(objetUcc, times(1)).setPicture(idPicture, filePath);
  }

  /**
   * Test of searchObjectsByDate should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to "
      + "searchObjectsByDate.")
  public void testSearchObjectsByDateRollback() {
    doThrow(new FatalException()).when(objectDAO).searchObjectsByDate(availability.getDate());
    assertThrows(FatalException.class, () -> objectUCC.searchObjectsByDate(availability.getDate()));
  }

  /**
   * Test of searchObjects should return a list with object with the availability given.
   */
  @Test
  @DisplayName("should return a list with object with the availability given for SearchObjects.")
  public void testSearchObjectsByDateOK() {
    ObjectDTO objectDTO2 = Mockito.mock(ObjectDTO.class);

    objectDTO.setDateOfReceipt(availability);
    when(objectDTO2.getDateOfReceipt()).thenReturn(availability);

    List<ObjectDTO> listExpected = new ArrayList<>();
    listExpected.add(objectDTO2);

    when(objectDAO.searchObjectsByDate(availability.getDate())).thenReturn(listExpected);
    assertEquals(listExpected, objectUCC.searchObjectsByDate(availability.getDate()));
  }

  /**
   * Test of searchByType should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to searchByType.")
  public void testSearchByTypeRollback() {
    doThrow(new FatalException()).when(objectDAO).getAllObject();
    assertThrows(FatalException.class, () -> objectUCC.searchByType("Chaise"));
  }

  /**
   * Test of searchByType should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to searchByType.")
  public void testSearchByTypeOK() {
    String typeName = "Chaise";
    ObjectDTO objectDTO2 = Mockito.mock(ObjectDTO.class);
    Type type = myFactory.getType();
    type.setTypeName(typeName);
    objectDTO2.setType(type);
    when(objectDTO2.getType()).thenReturn(type);
    when(objectDTO2.getState()).thenReturn(objectDTO.getState());

    List<ObjectDTO> listExpected = new ArrayList<>();
    listExpected.add(objectDTO2);
    when(objectDAO.getAllObject()).thenReturn(listExpected);
    assertEquals(listExpected, objectUCC.searchByType(typeName));
  }

  /**
   * Test of searchObjects should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to SearchObjects.")
  public void testSearchObjectsRollback() {
    double minPrice = 1.0;
    double maxPrice = 4.0;
    int idType = 5;

    ObjectDTO objectDTO2 = Mockito.mock(ObjectDTO.class);
    Type type = Mockito.mock(Type.class);
    when(type.getId()).thenReturn(idType);
    when(type.getTypeName()).thenReturn("Chaise");

    when(objectDTO2.getType()).thenReturn(type);
    when(objectDTO2.getPrice()).thenReturn(minPrice);

    doThrow(new FatalException()).when(objectDAO).searchObjects(minPrice, maxPrice, idType);
    assertThrows(FatalException.class, () -> objectUCC.searchObjects(minPrice, maxPrice, idType));
  }

  /**
   * Test of searchObjects should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to SearchObjects.")
  public void testSearchObjectsInvalidParameters() {
    double minPrice = 1.0;
    double maxPrice = 4.0;
    int idType = 5;

    ObjectDTO objectDTO2 = Mockito.mock(ObjectDTO.class);
    Type type = Mockito.mock(Type.class);
    when(type.getId()).thenReturn(idType);
    when(type.getTypeName()).thenReturn("Chaise");

    when(objectDTO2.getType()).thenReturn(type);
    when(objectDTO2.getPrice()).thenReturn(minPrice);

    doThrow(new FatalException()).when(objectDAO).searchObjects(minPrice, maxPrice, idType);
    assertThrows(FatalException.class, () -> objectUCC.searchObjects(minPrice, maxPrice, idType));
  }

  /**
   * Test of searchObjects should return a list of objects with the given arguments.
   */
  @Test
  @DisplayName("should return a list of objects with the given arguments to SearchObjects.")
  public void testSearchObjectsOK() {
    int idType = 5;

    ObjectDTO objectDTO2 = Mockito.mock(ObjectDTO.class);
    Type type = Mockito.mock(Type.class);
    when(type.getId()).thenReturn(idType);
    when(type.getTypeName()).thenReturn("Chaise");

    double minPrice = 1.0;
    when(objectDTO2.getType()).thenReturn(type);
    when(objectDTO2.getPrice()).thenReturn(minPrice);
    List<ObjectDTO> listExpected = new ArrayList<>();
    listExpected.add(objectDTO2);

    double maxPrice = 4.0;
    when(objectDAO.searchObjects(minPrice, maxPrice, idType)).thenReturn(listExpected);
    assertEquals(listExpected, objectUCC.searchObjects(minPrice, maxPrice, idType));
  }

  /**
   * Test of searchObjects should throw a FatalException when a problem with DAO occurs.
   */
  @Test
  @DisplayName("should throw a FatalException when a problem with DAO occurs to SearchObjects.")
  public void testSearchRollback() {
    double minPrice = 1.0;
    double maxPrice = 4.0;
    List<String> types = List.of(new String[]{"Chaise", "Table"});
    LocalDate date = LocalDate.now();

    doThrow(new FatalException()).when(objectDAO).getAllObject();
    assertThrows(FatalException.class,
        () -> objectUCC.search(minPrice, maxPrice, date, types, "", "", "")
    );
  }

  /**
   * Test if methode throw fatalException when problem occur
   */
  @Test
  @DisplayName("should throw a FatalException when a "
      + "problem with DAO occurs to checkObjectToWithdraw()")
  public void testCheckObjectToWithdrawRollback() {
    when(objectDAO.getObjectsInSell()).thenThrow(new FatalException());

    assertThrows(FatalException.class, () -> objectUCC.checkObjectToWithdraw());
  }

  /**
   * Test methods get the list of checkObjectToWithdraw()
   */
  @Test
  @DisplayName("should get a list of object to withdraw")
  public void testCheckObjectToWithdrawOk() {

  }

}
