package be.vinci.pae;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.notification.Notification;
import be.vinci.pae.domain.type.Type;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.notification.NotificationDAO;
import be.vinci.pae.services.notification.NotificationDTO;
import be.vinci.pae.services.notification.NotificationUCC;
import be.vinci.pae.services.object.ObjectDAO;
import be.vinci.pae.services.object.ObjectDTO;
import java.time.LocalDate;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

/**
 * This is the test class for the Notification  Use Case Controller (UCC).
 *
 * @author Loubna Eljattari
 */

public class NotificationUccTest {


  private static NotificationUCC notifUcc;
  private static NotificationDAO notifDAO;
  private static NotificationDTO notifDTO;
  private static ObjectDAO objectDAO;

  private static MemberDTO memberDTO;
  private static ObjectDTO objectDTO;
  private static Factory myFactory;


  /**
   * Sets up the service locator, the user use case controller, and the user data source before all
   * tests are run.
   */
  @BeforeAll
  static void initAll() {
    ServiceLocator locator = ServiceLocatorUtilities.bind(new ApplicationBinderTest());
    notifUcc = locator.getService(NotificationUCC.class);
    notifDAO = locator.getService(NotificationDAO.class);
    notifDTO = locator.getService(NotificationDTO.class);
    objectDAO = locator.getService(ObjectDAO.class);
    myFactory = locator.getService(Factory.class);
  }

  /**
   * Resets the user data source before each test is run.
   */
  @BeforeEach
  public void setup() {
    Mockito.reset(notifDAO);
    notifDTO = myFactory.getNotification();
    notifDTO.setId(1);
    notifDTO.setTextNotification("lubna a proposé un objet");
    notifDTO.setTitleNotification("objet");
    notifDTO.setDateNotifications(LocalDate.now());
    objectDTO = myFactory.getObject();
    objectDTO.setId(1);
    objectDTO.setDescription("description de l'objet");
    objectDTO.setPrice(11);
    Type type = myFactory.getType();
    type.setTypeName("Décoration");
    type.setId(1);
    objectDTO.setType(type);
    memberDTO = myFactory.getUser();
    memberDTO.setId(1);
    memberDTO.setEmail("bas.ile@vinci.be");
    memberDTO.setPhoneNumber("048756235");
    memberDTO.setFirstName("Base");
    memberDTO.setLastName("Ile");
    objectDTO.setOfferingMember(memberDTO);
  }

  @Test
  @DisplayName("should return the notification with the given id")
  void testGetNotifOK() {
    int id = 1;
    when(notifDAO.getNotification(id)).thenReturn(notifDTO);
    assertEquals(notifDTO, notifUcc.getNotification(id));
    verify(notifDAO).getNotification(id);
  }

  @ParameterizedTest
  @ValueSource(ints = {-100, 0})
  @DisplayName("should throw exception when given invalid id of notification <= 0")
  public void testGetNotifInvalidID(int id) {
    assertThrows(DAOException.class, () -> notifUcc.getNotification(id));
  }

  @Test
  @DisplayName("should throw FatalException when notification retrieval fails due"
      + " to database error.")
  public void testGetNotifFatalException() {
    int id = 1;
    when(notifDAO.getNotification(id)).thenThrow(new FatalException());
    assertThrows(FatalException.class, () -> notifUcc.getNotification(id));
  }

  @Test
  @DisplayName("should throw DAOException when NotificationDTO is null")
  void testGetNotificationNull() {
    int id = 1;
    when(notifDAO.getNotification(id)).thenReturn(null);
    assertThrows(DAOException.class, () -> notifUcc.getNotification(id));
    verify(notifDAO).getNotification(id);
  }

  @Test
  @DisplayName("should throw DAOexception when NotificationDto is null")
  void testaddNotificationForAcceptOfferNull() {
    Notification notification = myFactory.getNotification();
    notification.setNotification("Félicitation votre proposition a été acceptée !",
        "On vous attend avec plaisir le samedi choisi pour déposer votre objet.",
        objectDTO.getId());

    Mockito.when(notifDAO.addNotification(notification)).thenReturn(null);
    assertThrows(DAOException.class, () -> notifUcc.addNotificationForAcceptOffer(objectDTO.getId(),
        objectDTO.getOfferingMember().getId()));
  }

  @Test
  @DisplayName("should throw DAOexception when NotificationDto is null")
  void testaddNotificationForRefuseOfferNull() {
    Notification notification = myFactory.getNotification();
    notification.setNotification("Félicitation votre proposition a été acceptée !",
        "On vous attend avec plaisir le samedi choisi pour déposer votre objet.",
        objectDTO.getId());

    Mockito.when(notifDAO.addNotification(notification)).thenReturn(null);
    assertThrows(DAOException.class, () -> notifUcc.addNotificationForRefuseOffer(objectDTO.getId(),
        objectDTO.getOfferingMember().getId()));
  }

  @Test
  @DisplayName("should throw DAOexception when NotificationDto is null")
  void testaddNotificationResponsableNull() {
    Notification notification = myFactory.getNotification();
    notification.setNotification("Nouvelle proposition d'objet !",
        "Cliquez pour voir le détail.", objectDTO.getId());

    Mockito.when(notifDAO.addNotification(notification)).thenReturn(null);
    assertThrows(DAOException.class,
        () -> notifUcc.addNotificationForResponsables(objectDTO.getId()));
  }

  @Test
  @DisplayName("should throw DAOexception when NotificationDto is null")
  void testAddNotificationToMemberNull() {
    Notification notification = myFactory.getNotification();
    notification.setNotification("Félicitation votre proposition a été acceptée !",
        "On vous attend avec plaisir le samedi choisi pour déposer votre objet.",
        objectDTO.getId());

    Mockito.when(notifDAO.addNotification(notification)).thenReturn(null);
    assertThrows(DAOException.class,
        () -> notifUcc.addNotificationToMember(objectDTO, notification.getTitleNotification(),
            notification.getTextNotification()));
  }

  @Test
  @DisplayName("should throw Daoexception when problem occur in DAO")
  void testAddNotificationToMemberRollback() {
    Notification notification = myFactory.getNotification();
    notification.setNotification("Félicitation votre proposition a été acceptée !",
        "On vous attend avec plaisir le samedi choisi pour déposer votre objet.",
        objectDTO.getId());

    Mockito.when(notifDAO.addNotificationToMember(notification, memberDTO.getId()))
        .thenThrow(new FatalException());
    assertThrows(DAOException.class,
        () -> notifUcc.addNotificationToMember(objectDTO, notification.getTitleNotification(),
            notification.getTextNotification()));

  }


}
