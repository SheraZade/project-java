package be.vinci.pae;

import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.services.member.MemberDAO;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.notification.NotificationDAO;
import be.vinci.pae.services.notification.NotificationDTO;
import be.vinci.pae.services.notificationmember.NotificationMembersDAO;
import be.vinci.pae.services.notificationmember.NotificationMembersDTO;
import be.vinci.pae.services.notificationmember.NotificationMembersUCC;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

/**
 * This is the test class for the Notification Members Use Case Controller (UCC).
 *
 * @author Loubna Eljattari
 */

public class NotificationMemberUccTest {


  private static NotificationMembersUCC notificationMemberUCC;
  private static NotificationMembersDAO notificationMemberDAO;
  private static NotificationMembersDTO notificationMembersDTO;
  private static MemberDAO memberDAO;
  private static NotificationDAO notifDAO;
  private static NotificationDTO notificationDTO;
  private static Factory myFactory;


  /**
   * Sets up the service locator, the user use case controller, and the user data source before all
   * tests are run.
   */
  @BeforeAll
  static void initAll() {
    ServiceLocator locator = ServiceLocatorUtilities.bind(new ApplicationBinderTest());
    notificationMemberUCC = locator.getService(NotificationMembersUCC.class);
    notificationMemberDAO = locator.getService(NotificationMembersDAO.class);
    notificationMembersDTO = locator.getService(NotificationMembersDTO.class);
    notificationDTO = locator.getService(NotificationDTO.class);
    memberDAO = locator.getService(MemberDAO.class);
    notifDAO = locator.getService(NotificationDAO.class);
    myFactory = locator.getService(Factory.class);
  }

  /**
   * Resets the user data source before each test is run.
   */
  @BeforeEach
  public void setup() {
    Mockito.reset(notificationMemberDAO);
    notificationMembersDTO = myFactory.getMembersNotification();
    notificationMembersDTO.setIdNotificationMember(1);
    notificationMembersDTO.setIsRead(false);
    MemberDTO member = myFactory.getUser();
    member.setId(1);
    member.setFirstName("John");
    member.setLastName("Doe");
    member.setEmail("john.doe@example.com");
    //    notificationDTO.setIdNotificationMember(2);
    //    notificationDTO.setId(2);
    //    notificationDTO.setTextNotification("Nouvelle proposition !");
    //    notificationDTO.setTextNotification("Félicitations !");
    //    notificationDTO.setDateNotifications(LocalDate.now());
  }

  @Test
  @DisplayName("should return the notification member with the given id")
  void testGetNotifMemberOK() {
    int id = 1;
    //    when(notificationMemberDAO.getAllNotificationMembers(id)).thenReturn(notifMembersDTO);
    //    assertEquals(notifMembersDTO, notificationMemberUCC.getNotificationMembers(id));
    //    verify(notificationMemberDAO).getAllNotificationMembers(id);
  }

  @ParameterizedTest
  @ValueSource(ints = {-100, 0})
  @DisplayName("should throw exception when given invalid id of notification member <= 0")
  public void testGetNotifMembersInvalidID(int id) {
    //assertThrows(DAOException.class, () -> notificationMemberUCC.getNotificationMembers(id));
  }

  @Test
  @DisplayName("should return true when notification is successfully marked as read")
  void testMarquerNotificationCommeLueOK() {
    //    when(notificationMemberDAO.getNotificationMembers(
    //        notifMembersDTO.getIdNotificationMember())).thenReturn(notifMembersDTO);
    //    assertTrue(notificationMemberUCC.markNotificationAsRead(notifMembersDTO));
    //    verify(notificationMemberDAO).markNotificationAsRead(notifMembersDTO);
  }


  @Test
  @DisplayName("should throw exception when given invalid id of notification <= 0")
  void testMarquerNotificationCommeLueInvalidID() {
    //    List<NotificationMembersDTO> list = new ArrayList<>();
    //    list.add(notificationMembersDTO);
    //    when(notificationMemberDAO.getAllNotificationMembers(notificationDTO.getId())).thenReturn(list);
    //    assertThrows(DAOException.class, () -> notificationMemberUCC.markNotificationAsRead(
    //            notificationMembersDTO.getIdNotificationMember()));
  }
}
