package be.vinci.pae;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.member.Member;
import be.vinci.pae.domain.picture.Picture;
import be.vinci.pae.exceptions.BizException;
import be.vinci.pae.exceptions.DAOException;
import be.vinci.pae.exceptions.FatalException;
import be.vinci.pae.services.availability.AvailabilityDAO;
import be.vinci.pae.services.member.MemberDAO;
import be.vinci.pae.services.member.MemberDTO;
import be.vinci.pae.services.member.MemberDTO.RolesRessourceries;
import be.vinci.pae.services.member.MemberUCC;
import be.vinci.pae.services.picture.PictureDAO;
import be.vinci.pae.services.picture.PictureDTO;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

/**
 * This is the test class for the User Use Case Controller (UCC).
 *
 * @author Loubna Eljattari
 */

public class MemberUccTest {

  private static MemberUCC memberUCC;
  private static MemberDAO memberDAO;
  private static MemberDTO memberDTO;
  private static Availability availability;
  private static PictureDAO pictureDAO;
  private static PictureDTO pictureDTO;
  private static AvailabilityDAO availabilityDAO;
  private static Factory myFactory;



  /**
   * Sets up the service locator, the user use case controller, and the user data source before all
   * tests are run.
   */
  @BeforeAll
  static void initAll() {
    ServiceLocator locator = ServiceLocatorUtilities.bind(new ApplicationBinderTest());
    memberUCC = locator.getService(MemberUCC.class);
    memberDAO = locator.getService(MemberDAO.class);
    pictureDAO = locator.getService(PictureDAO.class);
    availabilityDAO = locator.getService(AvailabilityDAO.class);
    myFactory = locator.getService(Factory.class);
  }

  /**
   * Resets the user data source before each test is run.
   */
  @BeforeEach
  public void setup() {
    Mockito.reset(memberDAO);
    Mockito.reset(pictureDAO);
    memberDTO = myFactory.getUser();
    memberDTO.setId(1);
    memberDTO.setRole(RolesRessourceries.MEMBER);
    memberDTO.setEmail("elisa.bete@vinci.be");
    memberDTO.setFirstName("Elisa");
    memberDTO.setLastName("Bete");
    String hashPassword = ((Member) memberDTO).hashPassword("123");
    memberDTO.setEncryptedPassword(hashPassword);
    memberDTO.setRegistrationDate(LocalDate.now());
    memberDTO.setPhoneNumber("0485789456");
    availability = myFactory.getAvailability();
    availability.setDate(LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY)));
    availability.setTimeSlot("AM");

    Picture picture = myFactory.getPicture();
    picture.setUrl("url");
    picture.setMemberId(1);
    picture.setId(1);
    picture.setVersion(1);

    pictureDTO = picture;
    memberDTO.setPicture(picture);
  }


  /**
   * Test of login when given a valid user.
   */
  @Test
  @DisplayName("should log the user when given a valid user.")
  public void testLoginOK() {
    String password = "123";
    when(memberDAO.getMemberByEmail(memberDTO.getEmail())).thenReturn(memberDTO);
    MemberDTO userActual = memberUCC.login(memberDTO.getEmail(), password);

    assertAll(
        () -> assertNotNull(userActual),
        () -> assertEquals(memberDTO, userActual),
        () -> verify(memberDAO).getMemberByEmail(memberDTO.getEmail())
    );
  }


  /**
   * Test of login when the user provides an incorrect password.
   */
  @Test
  @DisplayName("should return null when given an invalid user.")
  public void testLoginInvalidPassword() {
    String wrongPassword = "Fake.";
    when(memberDAO.getMemberByEmail(memberDTO.getEmail())).thenReturn(memberDTO);

    assertAll(
        () -> assertNull(memberUCC.login(memberDTO.getEmail(), wrongPassword)),
        () -> verify(memberDAO).getMemberByEmail(memberDTO.getEmail())
    );
  }

  /**
   * Test the login with an invalid parameter as the email is null. Should throw a BizException.
   */
  @Test
  @DisplayName("should throw BizException when given a null parameter for the email.")
  public void testLoginInvalidEmail() {
    when(memberDAO.getMemberByEmail(memberDTO.getEmail())).thenReturn(null);
    assertThrows(BizException.class, () ->
        memberUCC.login(memberDTO.getEmail(), memberDTO.getEncryptedPassword()));
  }


  /**
   * Test of login when the parameters are empty. Should throw a BizException.
   */
  @Test
  @DisplayName("Login : test login method when the parameters are empty String")
  public void testLoginIEmptyParameters() {
    assertAll(
        () -> assertThrows(BizException.class,
            () -> memberUCC.login("", "")),
        () -> assertThrows(BizException.class,
            () -> memberUCC.login("", memberDTO.getEncryptedPassword())),
        () -> assertThrows(BizException.class,
            () -> memberUCC.login(memberDTO.getEmail(), ""))
    );
  }

  /**
   * Test the login, should throw FatalException when problem occurs in DAO.
   */
  @Test
  @DisplayName("should throw FatalException when problem occurs in DAO.")
  public void testLoginRollback() {
    when(memberDAO.getMemberByEmail(memberDTO.getEmail())).thenThrow(FatalException.class);
    assertThrows(FatalException.class, () ->
        memberUCC.login(memberDTO.getEmail(), memberDTO.getEncryptedPassword()));
  }


  /**
   * test of getUser method with a valid id of user. Should return the user.
   */
  @Test
  @DisplayName("should return the user with the given a valid id.")
  public void testGetUser() {
    int id = 1;
    when(memberDAO.getMemberByID(id)).thenReturn(memberDTO);
    assertEquals(memberDTO, memberUCC.getMember(id));
  }

  /**
   * Test of getUser when given invalid id. Should throw exception.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("getUser : test getUser method with invalid ID")
  public void testGetUserInvalidID(int id) {
    when(memberDAO.getMemberByID(id)).thenReturn(null);
    assertThrows(DAOException.class, () -> memberUCC.getMember(id));
  }

  /**
   * Test the getUser, should throw FatalException when problem occurs in DAO.
   */
  @Test
  @DisplayName("should throw FatalException when problem occurs in DAO.")
  public void testGetUserRollback() {
    when(memberDAO.getMemberByID(memberDTO.getId())).thenThrow(FatalException.class);
    assertThrows(FatalException.class, () -> memberUCC.getMember(memberDTO.getId()));
  }


  /**
   * test of Register with valid parameter.
   */
  @Test
  @DisplayName("should register a new user when given valid parameters.")
  public void testRegisterOK() {
    when(memberDAO.register(memberDTO)).thenReturn(memberDTO);
    when(pictureDAO.addPicturePP(pictureDTO)).thenReturn(pictureDTO);
    assertNotNull(memberUCC.register(memberDTO));
  }

  /**
   * test register method with an already registered email.
   */
  @Test
  @DisplayName("register : test register method when email is already registered")
  public void testRegisterAlreadyExists() {
    MemberDTO memberDTO = Mockito.mock(Member.class);
    when(memberDTO.getId()).thenReturn(1);
    when(memberDTO.getRole()).thenReturn("M");
    when(memberDTO.getEmail()).thenReturn("email@email.com");
    when(memberDTO.getFirstName()).thenReturn("person");
    when(memberDTO.getLastName()).thenReturn("McPersonFace");
    when(memberDTO.getEncryptedPassword()).thenReturn("123");
    when(memberDTO.getPhoneNumber()).thenReturn("1234567890");
    when(memberDTO.getRegistrationDate()).thenReturn(LocalDate.now());

    when(memberDAO.getMemberByEmail("email@email.com")).thenReturn(memberDTO);

    BizException exc = assertThrows(BizException.class,
        () -> memberUCC.register(memberDTO));
    assertEquals(exc.getMessage(), "This email is already registered.");

  }

  /**
   * test of Register when email is null and empty and finally with invalid mail. Should throw
   * exception.
   */
  @ParameterizedTest
  @ValueSource(strings = {"", "wrong@mail"})
  @DisplayName("should throw exception when given invalid mail")
  public void testRegisterInvalidEmail(String email) {
    MemberDTO mockDTO = Mockito.mock(Member.class);
    when(mockDTO.getId()).thenReturn(1);
    when(mockDTO.getRole()).thenReturn("M");
    when(mockDTO.getFirstName()).thenReturn("person");
    when(mockDTO.getLastName()).thenReturn("McPersonFace");
    when(mockDTO.getEncryptedPassword()).thenReturn("123");
    when(mockDTO.getPhoneNumber()).thenReturn("0487256321");
    when(mockDTO.getRegistrationDate()).thenReturn(LocalDate.now());

    assertAll(
        () -> {
          when(mockDTO.getEmail()).thenReturn(null);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getEmail()).thenReturn(email);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        }
    );
  }


  /**
   * test of Register, should throw exception when given invalid role.
   */
  @ParameterizedTest
  @ValueSource(strings = {"T", "S", ""})
  @DisplayName("should throw exception when given invalid role.")
  public void testRegisterInvalidRole(String role) {
    MemberDTO mockDTO = Mockito.mock(Member.class);
    when(mockDTO.getId()).thenReturn(1);
    when(mockDTO.getEmail()).thenReturn("email@email.com");
    when(mockDTO.getFirstName()).thenReturn("person");
    when(mockDTO.getLastName()).thenReturn("McPersonFace");
    when(mockDTO.getEncryptedPassword()).thenReturn("Ui");
    when(mockDTO.getPhoneNumber()).thenReturn("1234567890");
    when(mockDTO.getRegistrationDate()).thenReturn(LocalDate.now());

    assertAll(
        () -> {
          when(mockDTO.getRole()).thenReturn(null);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getRole()).thenReturn(role);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        }
    );
  }

  /**
   * test the register method with the following invalid firstname and lastname combinations: - both
   * firstname and lastname are empty strings. - both firstname and lastname are null. - firstname
   * is an empty String and lastname is null. - firstname is null and lastname is an empty String. -
   * firstname is valid and lastname is an empty String. - firstname is an empty String and lastname
   * is valid. - firstname is valid and lastname is null. - firstname is null and lastname is
   * valid.
   */
  @Test
  @DisplayName("should throw exception when firstname and/or lastname are invalid")
  public void testRegisterInvalidFirstLastName() {
    MemberDTO mockDTO = Mockito.mock(Member.class);
    when(mockDTO.getId()).thenReturn(100);
    when(mockDTO.getRole()).thenReturn("M");
    when(mockDTO.getEmail()).thenReturn("email@email.com");
    when(mockDTO.getEncryptedPassword()).thenReturn("Ui");
    when(mockDTO.getPhoneNumber()).thenReturn("1234567890");
    when(mockDTO.getRegistrationDate()).thenReturn(LocalDate.now());

    assertAll(
        () -> {
          when(mockDTO.getFirstName()).thenReturn("");
          when(mockDTO.getLastName()).thenReturn("");
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getFirstName()).thenReturn(null);
          when(mockDTO.getLastName()).thenReturn(null);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getFirstName()).thenReturn("");
          when(mockDTO.getLastName()).thenReturn(null);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getFirstName()).thenReturn(null);
          when(mockDTO.getLastName()).thenReturn("");
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getFirstName()).thenReturn("person");
          when(mockDTO.getLastName()).thenReturn("");
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getFirstName()).thenReturn("");
          when(mockDTO.getLastName()).thenReturn("McPersonFace");
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getFirstName()).thenReturn("person");
          when(mockDTO.getLastName()).thenReturn(null);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getFirstName()).thenReturn(null);
          when(mockDTO.getLastName()).thenReturn("McPersonFace");
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        }
    );
  }

  /**
   * test the register method with the following invalid passwords: - password is null. - password
   * is an empty String.
   */
  @Test
  @DisplayName("should throw exception when given invalid password")
  public void testRegisterInvalidPassword() {
    MemberDTO mockDTO = Mockito.mock(Member.class);
    when(mockDTO.getId()).thenReturn(100);
    when(mockDTO.getRole()).thenReturn("M");
    when(mockDTO.getEmail()).thenReturn("email@email.com");
    when(mockDTO.getFirstName()).thenReturn("person");
    when(mockDTO.getLastName()).thenReturn("McPersonFace");
    when(mockDTO.getPhoneNumber()).thenReturn("1234567890");
    when(mockDTO.getRegistrationDate()).thenReturn(LocalDate.now());

    assertAll(
        () -> {
          when(mockDTO.getEncryptedPassword()).thenReturn(null);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getEncryptedPassword()).thenReturn("");
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        }
    );
  }

  /**
   * Test the getUser, should throw FatalException when problem occurs in DAO.
   */
  @Test
  @DisplayName("should throw FatalException when problem occurs in DAO.")
  public void testRegisterRollback() {
    when(memberDAO.getMemberByEmail(memberDTO.getEmail())).thenThrow(FatalException.class);
    assertThrows(FatalException.class, () -> memberUCC.register(memberDTO));
  }

  /**
   * Test the register method with the following invalid phone numbers: - phone number is null. -
   * phone number is an empty String. - phone number is a single digit. - phone number is too
   * short.
   */
  @ParameterizedTest
  @ValueSource(strings = {"", "2", "123456"})
  @DisplayName("should throw exception when given invalid phone number.")
  public void testRegisterInvalidPhoneNumber(String phoneNumber) {
    MemberDTO mockDTO = Mockito.mock(Member.class);
    when(mockDTO.getId()).thenReturn(1);
    when(mockDTO.getRole()).thenReturn("M");
    when(mockDTO.getEmail()).thenReturn("email@email.com");
    when(mockDTO.getFirstName()).thenReturn("person");
    when(mockDTO.getLastName()).thenReturn("McPersonFace");
    when(mockDTO.getEncryptedPassword()).thenReturn("123");
    when(mockDTO.getRegistrationDate()).thenReturn(LocalDate.now());

    assertAll(
        () -> {
          when(mockDTO.getPhoneNumber()).thenReturn(null);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        },
        () -> {
          when(mockDTO.getPhoneNumber()).thenReturn(phoneNumber);
          assertThrows(DAOException.class, () -> memberUCC.register(mockDTO));
        }
    );
  }

  /**
   * Test UpdateRoleAsResponsable  when given valid user. Should update the role of the given user
   * to Staff.
   */
  @Test
  @DisplayName("should update the role of the given user to 'Staff'.")
  public void testUpdateRoleAsResponsableOK() {
    int id = 1;
    when(memberDAO.getMemberByID(id)).thenReturn(memberDTO);
    when(memberDAO.updateRole(memberDTO)).thenReturn(memberDTO);

    assertNotNull(memberUCC.updateRoleAsResponsable(id));
    assertEquals(RolesRessourceries.RESPONSABLE.getRole(), memberDTO.getRole());
  }

  /**
   * Test UpdateRoleAsResponsable method with the following invalid roles. - role is null. - role is
   * an empty String. - "A" - "R" - role that does not exist.
   */
  @ParameterizedTest
  @ValueSource(strings = {"R", "", "S"})
  @DisplayName("should throw BizException when the user has invalid role")
  public void testUpdateRoleAsResponsableInvalidRole(String role) {
    int id = 1;
    MemberDTO mockDTO = Mockito.mock(Member.class);
    when(memberDAO.getMemberByID(id)).thenReturn(mockDTO);

    assertAll(
        () -> {
          when(mockDTO.getRole()).thenReturn(null);
          assertThrows(BizException.class, () -> memberUCC.updateRoleAsResponsable(id));
        },
        () -> {
          when(mockDTO.getRole()).thenReturn(role);
          assertThrows(BizException.class, () -> memberUCC.updateRoleAsResponsable(id));
        }
    );
  }


  /**
   * Test confirm register when given valid user. Should update the role of the given user to
   * Staff.
   */
  @Test
  @DisplayName("should update the role of the given user to 'Staff'.")
  public void testUpdateRoleAsAssistantOK() {
    int id = 1;
    when(memberDAO.getMemberByID(id)).thenReturn(memberDTO);
    when(memberDAO.updateRole(memberDTO)).thenReturn(memberDTO);

    assertNotNull(memberUCC.updateRoleAsAssistant(id));
    assertEquals(RolesRessourceries.STAFF.getRole(), memberDTO.getRole());
  }


  /**
   * Test confirmRegister method with the following invalid roles. - role is null. - role is an
   * empty String. - "A" - "R" - role that does not exist.
   */
  @ParameterizedTest
  @ValueSource(strings = {"R", "", "S"})
  @DisplayName("should throw BizException when the user has invalid role")
  public void testConfirmRegisterInvalidRole(String role) {
    int id = 1;
    MemberDTO mockDTO = Mockito.mock(Member.class);
    when(memberDAO.getMemberByID(id)).thenReturn(mockDTO);

    assertAll(
        () -> {
          when(mockDTO.getRole()).thenReturn(null);
          assertThrows(BizException.class, () -> memberUCC.updateRoleAsAssistant(id));
        },
        () -> {
          when(mockDTO.getRole()).thenReturn(role);
          assertThrows(BizException.class, () -> memberUCC.updateRoleAsAssistant(id));
        }
    );
  }

  /**
   * test confirmRegister method with the following invalid ID's: - ID inferior to 0. - ID not in
   * the database.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("confirmRegister : test confirmRegister method with invalid id")
  public void testConfirmRegisterInvalidID(int id) {
    MemberDTO mockDTO = Mockito.mock(MemberDTO.class);

    when(mockDTO.getRole()).thenReturn(RolesRessourceries.MEMBER.getRole());
    when(memberDAO.getMemberByID(id)).thenReturn(null);

    assertThrows(DAOException.class, () -> memberUCC.updateRoleAsResponsable(id));
  }

  /**
   * Test confirmRegister should throw exception when received a null value.
   */
  @Test
  @DisplayName("should throw exception when received a null value.")
  public void testConfirmRegisterInvalid() {
    int id = 1;
    when(memberDAO.getMemberByID(id)).thenReturn(null);
    assertThrows(DAOException.class, () -> memberUCC.updateRoleAsResponsable(id));
  }

  /**
   * Test confirmRegister should throw exception when problem occurs in DAO.
   */
  @Test
  @DisplayName("should throw exception when received a null object.")
  public void testConfirmRegisterInvalidValue() {
    int id = 1;
    when(memberDAO.getMemberByID(id)).thenReturn(memberDTO);
    when(memberDAO.updateRole(memberDTO)).thenReturn(null);

    assertThrows(DAOException.class, () -> memberUCC.updateRoleAsResponsable(id));
  }

  /**
   * Test confirmRegister should throw FatalException when problem occurs in DAO.
   */
  @Test
  @DisplayName("should throw exception when problem occurs in DAO.")
  public void testConfirmRegisterRollback() {
    int id = 1;
    when(memberDAO.getMemberByID(id)).thenReturn(memberDTO);
    when(memberDAO.updateRole(memberDTO)).thenThrow(FatalException.class);
    assertThrows(FatalException.class, () -> memberUCC.updateRoleAsResponsable(id));
  }

  /**
   * Test of getAllUsers, should throw FatalException when problem occurs in DAO.
   */
  @Test
  @DisplayName("should throw FatalException when problem occurs in DAO.")
  public void testGetAllUsersRollback() {
    when(memberDAO.getAllMembers()).thenThrow(FatalException.class);
    assertThrows(FatalException.class, () -> memberUCC.getAllMember());
  }

  /**
   * Test of getAllUsers, should return null when the list of user is empty.
   */
  @Test
  @DisplayName("should return null when the list of user is empty.")
  public void testGetAllUsersEmpty() {
    when(memberDAO.getAllMembers()).thenReturn(null);
    assertNull(memberUCC.getAllMember());
  }

  /**
   * Test of addAvailability, should throw FatalException when problem occurs in DAO.
   */
  @Test
  @DisplayName("should throw FatalException when problem occurs in DAO to addAvailability().")
  public void testAddAvailabilityRollback() {
    when(availabilityDAO.getAvailability(availability)).thenThrow(FatalException.class);
    assertThrows(FatalException.class, () -> memberUCC.addAvailability(availability));
  }


  /**
   * Test of addAvailability, should throw BizException when DAO return false.
   */
  @Test
  @DisplayName("should throw BizException when the DAO return false into addAvailability().")
  public void testAddAvailabilityInvalidParameter() {
    int invalidResponse = -1;
    when(availabilityDAO.getAvailability(availability)).thenReturn(invalidResponse);
    when(memberDAO.addAvailability(availability)).thenReturn(invalidResponse);
    assertThrows(BizException.class, () -> memberUCC.addAvailability(availability));
  }

  /**
   * Test of addAvailability, should return the availability that was insert into the database.
   */
  @Test
  @DisplayName("should return the availability that was insert into the database.")
  public void testAddAvailabilityOK() {
    when(memberDAO.addAvailability(availability)).thenReturn(2);
    assertEquals(availability, memberUCC.addAvailability(availability));
  }

  /**
   * Test of editMyProfile, should throw FatalException when problem occurs in DAO.
   */
  @Test
  @DisplayName("should throw FatalException when problem occurs in DAO to editMyProfile().")
  public void testEditMyProfileRollback() {
    when(memberDAO.getMemberByID(memberDTO.getId())).thenThrow(FatalException.class);
    assertThrows(FatalException.class, () -> memberUCC.editMyProfile(memberDTO));
  }

  /**
   * Test of editMyProfile, should throw DAOException when invalid id of member is given <=0.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw DAOException when invalid id of member is given <=0 to"
      + " editMyProfile().")
  public void testEditMyProfileInvalidID(int id) {
    memberDTO.setId(id);
    assertThrows(DAOException.class, () -> memberUCC.editMyProfile(memberDTO));
  }

  /**
   * Test of editMyProfile, when updating the profil and null is returned by DAO.
   */
  @Test
  @DisplayName("should throw DAOException when updating the profil and null is returned by DAO.")
  public void testEditMyProfileNullParameter() {
    when(memberDAO.getMemberByID(memberDTO.getId())).thenReturn(memberDTO);
    when(memberDAO.editMyProfile(memberDTO)).thenReturn(null);
    assertThrows(DAOException.class, () -> memberUCC.editMyProfile(memberDTO));
  }

  /**
   * Test of editMyProfile, should update the profil correctly.
   */
  @Test
  @DisplayName("should update the profil correctly.")
  public void testEditMyProfileOK() {
    when(memberDAO.getMemberByID(memberDTO.getId())).thenReturn(memberDTO);
    when(memberDAO.editMyProfile(memberDTO)).thenReturn(memberDTO);
    when(pictureDAO.updatePictureMember(memberDTO.getPicture())).thenReturn(memberDTO.getPicture());
    assertEquals(memberDTO, memberUCC.editMyProfile(memberDTO));
  }

  /**
   * Test of getOfferer, should throw FatalException when problem occurs in DAO.
   */
  @Test
  @DisplayName("should throw FatalException when problem occurs in DAO to getOfferer().")
  public void testGetOffererRollback() {
    when(memberDAO.getOfferer(memberDTO.getId())).thenThrow(FatalException.class);
    assertThrows(FatalException.class, () -> memberUCC.getOfferer(1));
  }

  /**
   * Test of getOfferer, should throw DAOException when invalid id of member is given <=0.
   */
  @ParameterizedTest
  @ValueSource(ints = {0, -1})
  @DisplayName("should throw DAOException when invalid id of member is given <=0 to"
      + " getOfferer().")
  public void testGetOffererInvalidID(int id) {
    memberDTO.setId(id);
    assertThrows(DAOException.class, () -> memberUCC.getOfferer(1));
  }

  /**
   * Test of getOfferer, when searching for the offerer of the object and null is returned by DAO.
   */
  @Test
  @DisplayName("should throw DAOException when searching for the offerer of the object"
      + " and null is returned by DAO.")
  public void testGetOffererNullParameter() {
    when(memberDAO.getOfferer(memberDTO.getId())).thenReturn(null);
    assertThrows(DAOException.class, () -> memberUCC.getOfferer(1));
  }

  /**
   * Test of getOfferer, should update the profil correctly.
   */
  @Test
  @DisplayName("should update the profil correctly.")
  public void testGetOffererOK() {
    when(memberDAO.getOfferer(memberDTO.getId())).thenReturn(memberDTO);
    assertEquals(memberDTO, memberUCC.getOfferer(1));
  }
}
