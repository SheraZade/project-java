package be.vinci.pae;


import be.vinci.pae.domain.availability.Availability;
import be.vinci.pae.domain.availability.AvailabilityImpl;
import be.vinci.pae.domain.factory.Factory;
import be.vinci.pae.domain.factory.FactoryImpl;
import be.vinci.pae.services.availability.AvailabilityDAO;
import be.vinci.pae.services.availability.AvailabilityDAOImpl;
import be.vinci.pae.services.availability.AvailabilityUCC;
import be.vinci.pae.services.availability.AvailabilityUCCImpl;
import be.vinci.pae.services.dal.DALServicesImpl;
import be.vinci.pae.services.dal.DalBackendServices;
import be.vinci.pae.services.dal.DalServices;
import be.vinci.pae.services.member.MemberDAO;
import be.vinci.pae.services.member.MemberDAOImpl;
import be.vinci.pae.services.member.MemberUCC;
import be.vinci.pae.services.member.MemberUCCImpl;
import be.vinci.pae.services.notification.NotificationDAO;
import be.vinci.pae.services.notification.NotificationDAOImpl;
import be.vinci.pae.services.notification.NotificationUCC;
import be.vinci.pae.services.notification.NotificationUCCImpl;
import be.vinci.pae.services.notificationmember.NotificationMembersDAO;
import be.vinci.pae.services.notificationmember.NotificationMembersDAOImpl;
import be.vinci.pae.services.notificationmember.NotificationMembersUCC;
import be.vinci.pae.services.notificationmember.NotificationMembersUCCImpl;
import be.vinci.pae.services.object.ObjectDAO;
import be.vinci.pae.services.object.ObjectDAOImpl;
import be.vinci.pae.services.object.ObjectUCC;
import be.vinci.pae.services.object.ObjectUCCImpl;
import be.vinci.pae.services.picture.PictureDAO;
import be.vinci.pae.services.picture.PictureDAOImpl;
import be.vinci.pae.utils.ApplicationBinder;
import jakarta.inject.Singleton;
import org.mockito.Mockito;

/**
 * This is a test class that extends the ApplicationBinder class.
 *
 * @author Loubna Eljattari
 */

public class ApplicationBinderTest extends ApplicationBinder {

  protected void configure() {
    bind(Mockito.mock(DALServicesImpl.class)).to(DalBackendServices.class).to(DalServices.class);
    bind(FactoryImpl.class).to(Factory.class).in(Singleton.class);
    bind(Mockito.mock(MemberDAOImpl.class)).to(MemberDAO.class);
    bind(MemberUCCImpl.class).to(MemberUCC.class).in(Singleton.class);
    bind(Mockito.mock(ObjectDAOImpl.class)).to(ObjectDAO.class);
    bind(ObjectUCCImpl.class).to(ObjectUCC.class).in(Singleton.class);
    bind(AvailabilityImpl.class).to(Availability.class).in(Singleton.class);
    bind(Mockito.mock(NotificationDAOImpl.class)).to(NotificationDAO.class);
    bind(NotificationUCCImpl.class).to(NotificationUCC.class).in(Singleton.class);
    bind(Mockito.mock(NotificationMembersDAOImpl.class)).to(NotificationMembersDAO.class);
    bind(NotificationMembersUCCImpl.class).to(NotificationMembersUCC.class).in(Singleton.class);
    bind(Mockito.mock(PictureDAOImpl.class)).to(PictureDAO.class);
    bind(Mockito.mock(AvailabilityDAOImpl.class)).to(AvailabilityDAO.class);
    bind(Mockito.mock(AvailabilityUCCImpl.class)).to(AvailabilityUCC.class);
  }
}
