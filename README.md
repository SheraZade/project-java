# Project Java

ResourceRie is an association that collects objects deposited at the waste disposal site, restores them, and sells them at highly affordable prices to families in need. They are seeking to outsource the development of a website with the goal of proposing objects and tracking their sales. The aim is to give these objects a second lease of life and to assist low-income individuals in acquiring them.

This project was developed by a team of 5 students, including Loubna, Dorcas Michelle, Gabriel, Sacha and myself, with the following objectives:

# Key Objectives

-  Develop a user-friendly website for proposing objects.
-  Implement a system to monitor the sales of these objects.
-  Provide an authentication system for registered users.

## Introduction

This project was developed in a school setting by a team of 5 students. It was designed to encompass several courses and technologies, including:

- Requirements analysis (Cahier des charges)
- Java
- JavaScript (Node.js technology)
- PostgreSQL
- MVC architecture
- Grizzly

## Getting started

To get started with our project on GitLab, follow these steps:

1. **Clone the Repository:**

   If you don't have the project's repository on your local machine, you can clone it using the following command:

    ``` git clone https://gitlab.com/Sheraaa1/project-java.git ```

## Special Thanks

We extend our heartfelt gratitude to three distinguished professors who played pivotal roles in the development of Project Java.

- **Professor Raphael Baroni**: we sincerely appreciate their dedicated support in frontend development. Their insights and mentorship were instrumental in enhancing the user interface and overall user experience.

- **Professor Brigitte Lehmann**: We owe a debt of gratitude to Professor Lehmann for their crucial role in facilitating effective communication with the client (M. Riez and Ms Riez). Their guidance in requirements analysis, DSD, and use case development was invaluable to our project's success.

- **Professor Laurent Leleux**: Our deepest thanks go to Professor Leleux for their invaluable assistance and feedback in the realm of Java backend development. Their guidance and expertise significantly contributed to the success of our project.

These professors' unwavering commitment to our academic journey and their expert guidance have been essential in shaping the triumph of Project Java.
